<?php

/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @copyright  Copyright (c) 2012 RocketWeb (http://rocketweb.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     RocketWeb
 */

require_once 'abstract.php';

/**
 * Google Shopping Feed Generator Shell Script
 *
 * @category    ShoppingFeeds
 * @package     ShoppingFeeds_Shell
 */
class ShoppingFeeds_Shell_Feed extends Mage_Shell_Abstract
{

    protected function _construct() {
        $this->addOption('schedule_id', uniqid(rand(), true), false);

        $this->addFlagOption('verbose')
            ->setHelp('Outputs skus and memory during processing');

        $this->addOption('feed_id', false)
            ->setHelp('Specify the feed ID if you need to run only one feed at a time. Missing feed_id will process the schedules.');

        $this->addOption('test_sku', false)
            ->setHelp('Generate the feed for one product SKU. To be used for tests and debugging.');

        $this->addOption('test_offset', false)
            ->setHelp('Record # to start testing from. Together with `test_limit`, sets a SQL-style LIMIT clause on the feed.');

        $this->addOption('test_limit', false)
            ->setHelp('Run a test for `test_limit` records, starting with `test_offset`');

        $this->addFlagOption('skip_ftp', false)
            ->setHelp('Do not upload the feeds to ftp accounts when done');

        $this->addFlagOption('skip_log', false)
            ->setHelp('Do not log anything in the feed\'s log');

        $this->getOptions();
    }
    /**
     * Fix for servers missing $_SERVER['argv']
     */
    protected function _parseArgs()
    {
        $current = null;
        $argv = !empty($_SERVER['argv']) ? $_SERVER['argv']: array_keys($_GET);

        foreach ($argv as $arg) {
            $match = array();
            if (preg_match('#^--([\w\d_-]{1,})$#', $arg, $match) || preg_match('#^-([\w\d_]{1,})$#', $arg, $match)) {
                $current = $match[1];
                $this->_args[$current] = true;
            } else {
                if ($current) {
                    $this->_args[$current] = $arg;
                } else if (preg_match('#^([\w\d_]{1,})$#', $arg, $match)) {
                    $this->_args[$match[1]] = true;
                }
            }
        }
        return $this;
    }

    public function addOption($key, $default = false, $user = true) {
        $optionObject = new Varien_Object(array(
            'key' => $key,
            'default' => $default,
            'is_user' => $user,
            'is_flag' => false
        ));

        if ($user && isset($this->_args[$key])) {
            $value = $this->getArg($key);
        }
        else {
            $value = $default;
        }
        $optionObject->setValue($value);

        $this->_options[$key] = $optionObject;

        return $optionObject;
    }

    public function addFlagOption($key, $default = false, $user = true) {
        $optionObject = new Varien_Object(array(
            'key' => $key,
            'default' => $default,
            'is_user' => $user,
            'is_flag' => true
        ));

        if ($user && isset($this->_args[$key])) {
            $value = $this->getArg($key);
        }
        else {
            $value = $default;
        }
        $optionObject->setValue($value);

        $this->_options[$key] = $optionObject;

        return $optionObject;
    }

    public function getOptions() {
        return $this->_options;
    }

    public function getOptionsAsArray() {
        $ret = array();

        foreach ($this->getOptions() as $key => $o) {
            $ret[$key] = $o->getValue();
        }

        return $ret;
    }

    public function run()
    {
        $data = $this->getOptionsAsArray();

        try {
            set_time_limit(0);
            /* Setting memory limit depends on the number of products exported.*/
            // ini_set('memory_limit','600M');
            error_reporting(E_ALL);

            @Mage::app('admin')->setUseSessionInUrl(false);

            // For when server rewrites are not available
            Mage::register('custom_entry_point', true);

            $data['test_mode'] = $data['test_sku'] || ($data['test_offset'] && $data['test_limit']);
            if ($data['test_sku'] && !$data['feed_id']) {
                Mage::throwException('In order to run a test for --test_sku, you\'ll have to provide the --feed_id as well');
            }

            if ($data['feed_id']) {
                $feed = Mage::getModel('rocketshoppingfeeds/feed')->load($data['feed_id']);
                if (!$feed->getId()) {
                    Mage::throwException(sprintf('Could not load feed configuration for ID \'%s\'.', $data['feed_id']));
                }

		        echo 'Running feed id '.$data['feed_id'] . PHP_EOL;
                $feed->saveStatus(RocketWeb_ShoppingFeeds_Model_Feed_Status::STATUS_PROCESSING);

                try {
                    /** @var RocketWeb_ShoppingFeeds_Model_Generator $Generator */
                    $Generator = Mage::helper('rocketshoppingfeeds')->getGenerator($feed)->addData($data);
                    $Generator->run();
                }
                catch (RocketWeb_ShoppingFeeds_Model_Exception $e) {
                    // Ending batch earlier due memory limit. Do not release the queue.
                    $Generator->log($e->getMessage());
                }
                catch (Exception $e) {
                    $feed->saveStatus(RocketWeb_ShoppingFeeds_Model_Feed_Status::STATUS_ERROR);
                    $log = Mage::getSingleton('rocketshoppingfeeds/log');
                    $log->write($e->getMessage(), Zend_Log::ERR, null, array('file' => $feed->getLogFile(), 'force' => true));
                    throw new Exception($e);
                }

                $batchMode = $Generator->getBatch();
                // Set the feed as completed
                if (!$batchMode || ($batchMode && $Generator->getBatch()->completedForToday())) {
                    $feed->saveStatus(RocketWeb_ShoppingFeeds_Model_Feed_Status::STATUS_COMPLETED);
                }

            } else {
                $schedule = Mage::getModel('Mage_Cron_Model_Schedule', $data);
                Mage::getModel('rocketshoppingfeeds/observer')->processSchedule($schedule);
                Mage::getModel('rocketshoppingfeeds/observer')->processQueue($schedule);
            }

        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        $__FILE__ = __FILE__;
        $message = array();

        $message[] = "Usage:  php ${__FILE__} --[options]";
        foreach ($this->getOptions() as $option) {
            if (!$option->getIsUser()) continue;

            $line = '  '.$option->getKey();
            if (!$option->getIsFlag()) $line .= ' <string>';
            else $line .= "\t";
            $line .= "\t\t" . $option->getHelp();

            $message[] = $line;
        }

        $message[] = "  help\t\t\t\tThis help";

        $message[] = "e.g. php ${__FILE__} --feed_id 3 --verbose";

        foreach ($message as $line) {
            echo $line.PHP_EOL;
        }
        echo PHP_EOL;
    }
}

$shell = new ShoppingFeeds_Shell_Feed();
$shell->run();
