<?php
require_once('abstract.php');
class SearchTap_Import extends Mage_Shell_Abstract
{
    public $collection_id;
    public function run()
    {
        Mage::getModel('gs_searchtap/observer')->cronReindex($this->collection_id);
    }
}
$script = new SearchTap_Import();
$script->collection_id = $argv[1];
$script->run();