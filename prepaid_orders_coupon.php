<?php

require_once "app/Mage.php";

Mage::app('1');
error_reporting(1);
$curr_date = date('Y-m-d',strtotime("-2 days"));
//$curr_date = date('Y-m-d');
$queryExpr = new \Zend_Db_Expr("DATE(updated_at)");
$orders = Mage::getModel('sales/order')->getCollection()
        ->join(
            array('payment' => 'sales/order_payment'),
            'main_table.entity_id=payment.parent_id'
        )
        ->addFieldToSelect(array('customer_email','customer_firstname','increment_id'))
        ->addFieldToFilter('status', array('eq' => 'complete'))
        ->addFieldToFilter($queryExpr,array('eq' => "$curr_date"))
        ->addFieldToFilter('customer_is_guest',array('eq' => '0'))
        ->addFieldToFilter('payment.method',array('neq' => 'cashondelivery'));
//echo $orders->getSelect();//exit;
$allCustomerArr = array();
foreach ($orders as $order) {
   $customerArr = array();
   $orderId = $order->getIncrementId();
   $customerName = $order->getCustomerName();
   $customerEmail = $order->getCustomerEmail();
   $customerArr = array('name'=>$customerName,'email'=>$customerEmail,'orderId'=>$orderId);
   array_push($allCustomerArr, $customerArr);
}

//echo '<pre>';print_r($allCustomerArr);//exit;
$customerGroupIds = Mage::getModel('customer/group')->getCollection()->getAllIds();

$DiscountType = Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION;
$discountval = 5;
$days = '30';
$ruleNamePrefix = '5% Discount for Prepaid Orders';

$emailArr = array();

if(sizeof($allCustomerArr)>0){
    foreach($allCustomerArr as $allCustomer){
    $customerEmail = $allCustomer['email'];   
    $customerName = $allCustomer['name'];
    
    
    if(!in_array($customerEmail,$emailArr)){
        array_push($emailArr,$customerEmail);
        // SalesRule Rule model
        $rule = Mage::getModel('salesrule/rule');
        // Rule data
        $generatorCode = Mage::getModel('salesrule/coupon_codegenerator')->setLength(8)->generateCode();
        $codeuppercase = strtoupper($generatorCode);

        $rule->setName($ruleNamePrefix)
                ->setDescription('*Only for non-discounted products')
                ->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
                ->setCouponCode(strtoupper($generatorCode))
                ->setUsesPerCustomer(1)
                ->setUsesPerCoupon(1)
                ->setIsActive(1)
                ->setCustomerGroupIds("0,1,2")
                ->setConditionsSerialized(serialize(array(
                    'type' => 'salesrule/rule_condition_address',
                    'attribute' => NULL,
                    'operator' => NULL,
                    'value' => '1',
                    'is_value_processed' => false,
                    'aggregator' => 'all',
                    'conditions' =>
                    array(
                        0 =>
                        array(
                            'type' => 'salesrule/rule_condition_address',
                            'attribute' => 'payment_method',
                            'operator' => '!=',
                            'value' => 'Cash on delivery',
                            'is_value_processed' => false,
                        ),
                        1 =>
                        array(
                            'type' => 'salerule/rule_condition_customer',
                            'attribute' => 'email',
                            'operator' => '==',
                            'value' => $customerEmail,
                            'is_value_processed' => false,
                        ),
                    ),
                )))
                ->setActionsSerialized(serialize(array(
                    "type" => "salesrule/rule_condition_product",
                    'attribute' => NULL,
                    'operator' => NULL,
                    'value' => '0',
                    'is_value_processed' => false,
                    'aggregator' => 'all',
                    "conditions" =>
                    array(
                        0 =>
                        array(
                            "type" => "salesrule/rule_condition_product",
                            "attribute" => 'special_price',
                            "operator" => ">=",
                            "value" => "1",
                            'is_value_processed' => false,
                            'aggregator' => 'all',
                        )
                    )
                )))
                ->setStopRulesProcessing(0)
                ->setIsAdvanced(1)
                ->setProductIds('')
                ->setSortOrder(0)
                ->setSimpleAction($DiscountType)
                ->setDiscountAmount($discountval)
                ->setDiscountStep(0)
                ->setSimpleFreeShipping('0')
                ->setApplyToShipping('0')
                ->setIsRss(0)
                ->setWebsiteIds(array(1))
                ->setFromDate(date('Y-m-d'))
                ->setToDate(date('Y-m-d', strtotime('+' . $days . ' days')))
                ->setStoreLabels(array('5%'));
    try{
            $rule->save();
        } catch(Exception $error) {
                Mage::log($error->getMessage());
                Mage::getSingleton('core/session')->addError($error->getMessage());
            }
            /*****************send email******************/
            try{
            $emailTemplate = Mage::getModel('core/email_template')->loadByCode('prepaidcoupon');

            //Getting the Store E-Mail Sender Name.
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');

            //Getting the Store General E-Mail.
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

            $subjectM = 'Paragon Footwear - Coupon on Prepaid orders';
            $emailTemplateVariables = array(
                'name' => $customerName,
                'couponcode' => $codeuppercase
            );
            $toAddress = array($customerEmail);

            $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);

            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);
            $emailTemplate->setTemplateSubject($subjectM);
            $emailTemplate->send($toAddress, $name, $emailTemplateVariables);
            } catch(Exception $error) {
                Mage::log($error->getMessage());
                Mage::getSingleton('core/session')->addError($error->getMessage());
            }
            /*****************send email******************/
            echo 'Coupon '.$codeuppercase.' generated successfully for '.$customerEmail.'<br>';
    Mage::log('Date: '.date("Y-m-d H:i:s").' Coupons generated successfully', null, 'prepaidorderscoupon.log', true);
    }
    }
}   else{
        echo 'No records found to generate coupons';
	Mage::log('Date: '.date("Y-m-d H:i:s").' No records found to generate coupons', null, 'prepaidorderscoupon.log',true);
}
?>
