<?php

$sku=$_GET['sku'];
$qty=$_GET['qty'];
$is_in_stock=$_GET['is_in_stock'];
/* Get Session Code */
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.paragonfootwear.com/index.php/api/soap/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns1=\"urn:Magento\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n  <SOAP-ENV:Body>\n     <ns1:login>\n        <username xsi:type=\"xsd:string\">VinculumAPI321</username>\n        <apiKey xsi:type=\"xsd:string\">adjksf324AEWRGGx</apiKey>\n     </ns1:login>\n  </SOAP-ENV:Body>\n</SOAP-ENV:Envelope>",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 696e342d-00bd-426f-b75a-49b06cf3aae1",
    "cache-control: no-cache"
  ),
));

$sessioncode = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  $sessioncode;
}
$xml1 = getXMLtoArray($sessioncode);
        $json = json_encode($xml1);
        $array = json_decode($json,TRUE);
/*Qty_update */
$a="<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:Magento' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
    <soapenv:Header />
    <soapenv:Body>
        <mag:call xmlns:mag='urn:Magento' xmlns:SOAP-ENC='http://schemas.xmlsoap.org/soap/encoding/' xmlns:SOAP-XML='http://xml.apache.org/xml-soap'>
            <sessionId>".$array['SOAP-ENV:ENVELOPE']['SOAP-ENV:BODY']['NS1:LOGINRESPONSE']['LOGINRETURN']['content']."</sessionId>
            <resourcePath>product_stock.update</resourcePath>
            <args SOAP-ENC:arrayType='xsd:ur-type[2]' xsi:type='SOAP-ENC:Array'>
                <item xsi:type='xsd:string'>".$sku."</item>
                <item xsi:type='SOAP-XML:Map'>
                    <item>
                        <key xsi:type='xsd:string'>qty</key>
                        <value xsi:type='xsd:float'>".$qty."</value>
                    </item>
                    <item>
                        <key xsi:type='xsd:string'>is_in_stock</key>
                        <value xsi:type='xsd:string'>".$is_in_stock."</value>
                    </item>
                </item>
            </args>
        </mag:call>
    </soapenv:Body>
</soapenv:Envelope>";
$curl1 = curl_init();
curl_setopt_array($curl1, array(
  CURLOPT_URL => "https://www.paragonfootwear.com/index.php/api/soap/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $a,
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/xml",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl1);
$err = curl_error($curl1);

curl_close($curl1);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
function getXMLtoArray($XML)
        {
        $XML = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $XML);
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $XML, $vals);
        xml_parser_free($xml_parser);
        // wyznaczamy tablice z powtarzajacymi sie tagami na tym samym poziomie
        $_tmp='';
        foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_level!=1 && $x_type == 'close') {
        if (isset($multi_key[$x_tag][$x_level]))
        $multi_key[$x_tag][$x_level]=1;
        else
        $multi_key[$x_tag][$x_level]=0;
        }
        if ($x_level!=1 && $x_type == 'complete') {
        if ($_tmp==$x_tag)
        $multi_key[$x_tag][$x_level]=1;
        $_tmp=$x_tag;
        }
        }
        // jedziemy po tablicy
        foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_type == 'open')
        $level[$x_level] = $x_tag;
        $start_level = 1;
        $php_stmt = '$xml_array';
        if ($x_type=='close' && $x_level!=1)
        $multi_key[$x_tag][$x_level]++;
        while ($start_level < $x_level) {
        $php_stmt .= '[$level['.$start_level.']]';
        if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
        $php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
        $start_level++;
        }
        $add='';
        if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
        if (!isset($multi_key2[$x_tag][$x_level]))
        $multi_key2[$x_tag][$x_level]=0;
        else
        $multi_key2[$x_tag][$x_level]++;
        $add='['.$multi_key2[$x_tag][$x_level].']';
        }
        if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
        if ($x_type == 'open')
        $php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
        else
        $php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
        eval($php_stmt_main);
        }
        if (array_key_exists('attributes', $xml_elem)) {
        if (isset($xml_elem['value'])) {
        $php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
        eval($php_stmt_main);
        }
        foreach ($xml_elem['attributes'] as $key=>$value) {
        $php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
        eval($php_stmt_att);
        }
        }
        }
        return $xml_array;
        }