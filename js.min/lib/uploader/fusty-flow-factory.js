/*! name 2020-02-07 */
!function(a,b,c){"use strict";var d=function(c){var d=new a(c);return d.support?d:new b(c)};c.fustyFlowFactory=d}(window.Flow,window.FustyFlow,window);