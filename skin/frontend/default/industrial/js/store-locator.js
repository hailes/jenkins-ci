
    /************
    Google Maps Code Initialiser
    ************/

    var icon = new google.maps.MarkerImage("http://images.saascart.com/sochimages/Map_Marker.png", 
               new google.maps.Size(50, 36), new google.maps.Point(0, 0), 
               new google.maps.Point(50, 36));
    function addMarker(lat, lng) {
        bounds = new google.maps.LatLngBounds();
        var pt = new google.maps.LatLng(lat, lng);
        bounds.extend(pt);
        var marker = new google.maps.Marker({
          position: pt,
          map: map,
          title: 'Hello World!'
      });

    }  


    function initMap(storeLat,storeLong,StoreID) {
        map = new google.maps.Map(document.getElementById(StoreID), {
            center: new google.maps.LatLng(0, 0),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
            },
            navigationControl: true,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.ZOOM_PAN
            }
        });
        //add marker on given coordinates
        addMarker(storeLat,storeLong);
        center = bounds.getCenter();
        map.fitBounds(bounds);

        // attach this event listener after map init
        google.maps.event.addListener(map, 'zoom_changed', function() {
            // set max zoom only when fitting bounds
            if (map.getZoom() > 20) {
                this.setZoom(15);
            }
        });
    }