/**************************** Google Shopping Feed GROUPED PRODUCT **************************/
GSF_Grouped = Class.create();

GSF_Grouped.prototype = {

    initialize: function(){

        this.selected = {};
        this.prices = {};

        var self = this;
        var n = 0;
        $$('.super-attribute-select').each(function (element) {
            n++;
            window.setTimeout(self.autoUpdate(element), 100 * n);
        });
    },

    autoUpdate: function(element) {
        var attributeId = element.id.replace(/[a-z]*/, '');
        this.selectOptions(attributeId);
    },

    selectOptions: function(attributeId) {
        var Params = document.URL.toQueryParams();
        var attr_name = 'super_attribute['+ Params.gid +']['+attributeId+']';
        var elements = $$('select').findAll(function (o) {return o.name == attr_name;});
        elements.each(function (select) {
            if (select !== null && typeof select != 'undefined') {
                for (var i = 0; i < select.options.length; i++) {
                    if (select.options[i].value == Params[attributeId]) {
                        select.selectedIndex = i;
                        if ("createEvent" in document) {
                            var evt = document.createEvent("HTMLEvents");
                            evt.initEvent("change", false, true);
                            select.dispatchEvent(evt);
                        }
                        else {
                            select.fireEvent("onchange");
                        }
                    }
                }
            }
        });
    }
};
// Use of document.ready() instead of Event.observe(window, 'load', function () {}); to awoid wait for image loads
// Timout is set here to allow events to register after document.ready() before firing them
document.observe("dom:loaded", function() {
    setTimeout(function() {
        var gsf_grouped = new GSF_Grouped();
    }, 10);
});
