<?php

/**
 * ShipSync
 *
 * @category   IllApps
 * @package    IllApps_Shipsync
 * @copyright  Copyright (c) 2014 EcoMATICS, Inc. DBA IllApps (http://www.illapps.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * IllApps_Shipsync_Model_Shipping_Carrier_Fedex_Ship
 */
class IllApps_Shipsync_Model_Shipping_Carrier_Fedex_Ship extends IllApps_Shipsync_Model_Shipping_Carrier_Fedex
{
    
    
    protected $_shipRequest;
    protected $_shipResult;
    protected $_shipResultError;
    protected $_shipServiceClient;
    protected $_shipServiceVersion = '13';
    protected $_shipServiceWsdlPath = 'ShipService_v13.wsdl';
    protected $_activeShipment;
    
    
    /**
     * Create shipment
     *
     *
     * @param object
     * @return object
     */
    public function createShipment($request)
    {		
        $this->_shipServiceClient = $this->_initWebServices($this->_shipServiceWsdlPath);

		/** Set ship request */
		$this->setShipRequest($request);		

		/** Set result */
		$this->_shipResult = $this->_createShipment();

		/** Return result */
		return $this->_shipResult;
    }
    
	
    /**
     * Set shipment request
     *
     * @param object
     * @return IllApps_Shipsync_Model_Shipping_Carrier_Fedex
     */
    public function setShipRequest($request)
    {				
        $shipRequest = Mage::getModel('shipsync/shipment_request');
        		
        $shipRequest->setShipmentObject($request->getShipmentObject());		
		$shipRequest->setOrderId($request->getOrderId());		
		$shipRequest->setOrder(Mage::getModel('sales/order')->loadByIncrementId($shipRequest->getOrderId()));
		$shipRequest->setStore($shipRequest->getOrder()->getStore());				
		$shipRequest->setPackages($request->getPackages());	
		$shipRequest->setMethodCode($request->getMethodCode());
		$shipRequest->setServiceType($this->getUnderscoreCodeFromCode($shipRequest->getMethodCode()));
		$shipRequest->setDropoffType($this->getUnderscoreCodeFromCode(Mage::getStoreConfig('carriers/fedex/dropoff')));
		$shipRequest->setCustomerReference($shipRequest->getOrderId() . '_pkg2' . $request->getPackageId());
		$shipRequest->setInvoiceNumber('INV' . $shipRequest->getOrderId());
					
		// Shipper region id
		$shipperRegionId = Mage::getStoreConfig('shipping/origin/region_id');				
        
		// Shipper region code
		if (is_numeric($shipperRegionId)) {
            $shipRequest->setShipperRegionCode(Mage::getModel('directory/region')->load($shipperRegionId)->getCode());
        }
		
		// Shipper streetlines
		$shipperStreetLines = array(Mage::getStoreConfig('shipping/origin/street_line1'));
		
        if (Mage::getStoreConfig('shipping/origin/street_line2') != '') {
			$shipperStreetLines[] = Mage::getStoreConfig('shipping/origin/street_line2');
        }
		if (Mage::getStoreConfig('shipping/origin/street_line3') != '') {
        	$shipperStreetLines[] = Mage::getStoreConfig('shipping/origin/street_line3');
        }
        
        $shipRequest->setShipperStreetLines($shipperStreetLines);
        $shipRequest->setShipperCity(Mage::getStoreConfig('shipping/origin/city'));
        $shipRequest->setShipperPostalCode(Mage::getStoreConfig('shipping/origin/postcode'));
        $shipRequest->setShipperCountryCode(Mage::getStoreConfig('shipping/origin/country_id', $this->getStore()));
        $shipRequest->setShipperPhone(Mage::getStoreConfig('shipping/origin/phone'));
        	if ($shipRequest->getShipperRegionCode()) {
			$shipRequest->setShipperStateOrProvinceCode($shipRequest->getShipperRegionCode());
		}else {
            $origRegionCode = Mage::getStoreConfig(
                Mage_Shipping_Model_Shipping::XML_PATH_STORE_REGION_ID,
                $shipRequest->getStoreId()
            );
			$shipRequest->setShipperStateOrProvinceCode($origRegionCode);
        }
        
		$shipRequest->setRecipientAddress($request->getRecipientAddress());        
		$shipRequest->setInsureShipment($request->getInsureShipment());

		// Set weight units
		$shipRequest->setWeightUnits(Mage::getModel('shipsync/shipping_carrier_fedex')->getWeightUnits());
		
		// Set weight coefficient
		$shipRequest->setWeightCoefficient(1.0);
		
		// Convert G to KG, update coefficient
		if ($shipRequest->getWeightUnits() == 'G') {
			$shipRequest->setWeightUnits('KG');
			$shipRequest->setWeightCoefficient(0.001);
		}			   

		// Enable/disable dimensions
		$shipRequest->setEnableDimensions(Mage::getStoreConfig('carriers/fedex/shipping_dimensions_disable'));
		
		// Dimension units
		$shipRequest->setDimensionUnits(Mage::getModel('shipsync/shipping_carrier_fedex')->getDimensionUnits());				
		
		// Customs value				
		//$shipRequest->setCustomsValue($shipRequest->getOrder()->getGrandTotal() - $shipRequest->getOrder()->getShippingAmount());
		$shipRequest->setCustomsValue($request->getCustomsValue());
		
		// Insurance amount
        if ($request->getInsureAmount() != '') {
            $shipRequest->setInsureAmount($request->getInsureAmount());
        } else {
            $shipRequest->setInsureAmount($shipRequest->getCustomsValue());
        }
				
        // Set delivery signature type
		$shipRequest->setSignature($request->getSignature());

		// Saturday delivery
		$shipRequest->setSaturdayDelivery($request->getSaturdayDelivery());
		
		// COD
        $shipRequest->setCod($request->getCod());       
        
		// Rate types
		$shipRequest->setRateType(Mage::getStoreConfig('carriers/fedex/rate_type'));
        
		// Timestamp
		$shipRequest->setShipTimestamp(date('c'));
		
        if (($request->getAddressValidation() == 'ENABLED') && ($request->getResidenceDelivery() == 'VALIDATE')) {
            $shipRequest->setResidential($this->getResidential($shipRequest->getRecipientAddress()->getStreet(), $shipRequest->getRecipientAddress()->getPostcode()));
        }
		else if ($request->getResidenceDelivery() == 'ENABLED') {
			$shipRequest->setResidential(true);
		}
		else if ($request->getResidenceDelivery() == 'DISABLED') {
			$shipRequest->setResidential(false);
		}

		$shipRequest->setLabelStockType($request->getLabelStockType());
		$shipRequest->setLabelImageType($request->getLabelImageType());
		$shipRequest->setLabelPrintingOrientation($request->getLabelPrintingOrientation());
		$shipRequest->setEnableJavaPrinting($request->getEnableJavaPrinting());
		$shipRequest->setPrinterName($request->getPrinterName());
		$shipRequest->setPackingList($request->getPackingList());
		$shipRequest->setShipperCompany($request->getShipperCompany());
		$shipRequest->setReturnLabel($request->getReturnLabel());
        $shipRequest->setB13AFilingOption(Mage::getStoreConfig('carriers/fedex/b13a_filing_option'));

        $this->_shipRequest = $shipRequest;
        
        return $this;
    }
    
    /**
     * Create shipment
     *
     * @return mixed
     */
	 
	 protected function addShipper(){
	$shipper = array(
		'Contact' => array(
			'PersonName' => 'Sasi',
			'CompanyName' => 'Tenovia',
			'PhoneNumber' => '9952086772'),
		'Address' => array(
			'StreetLines' => array('North USman Road,T.Nagar'),
			'City' => 'chennai',
			'StateOrProvinceCode' => 'TN',
			'PostalCode' => '600032',
			'CountryCode' => 'IN',
			'CountryName' => 'INDIA')
	);
	return $shipper;
}
	 
	 
	 protected  function addShippingChargesPayment(){
	$shippingChargesPayment = array(
		'PaymentType' => 'SENDER',
        'Payor' => array(
			'ResponsibleParty' => array(
				'AccountNumber' => $this->getFedexAccount(),
				'Contact' => null,
				'Address' => array('CountryCode' => 'IN'))));
	return $shippingChargesPayment;
}
	 
    protected function _createShipment()
    {
        $shipRequest = $this->_shipRequest;
		
		
        /** Iterate through each package to ship */
        foreach ($shipRequest->getPackages() as $packageToShip) {

			$request = $this->_prepareShipmentHeader();
			
	
			// Shipment request
			$request['RequestedShipment'] = array(
			
			'ShipTimestamp' => date('c'),
			'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
			'ServiceType' => $shipRequest->getServiceType(), // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_EXPRESS_SAVER
			'PackagingType' => $packageToShip->getContainerCode(), // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
			//'Shipper' => $shipRequest->getShipperDetails(),
			'Shipper' => $shipRequest->getShipperDetails(),
			'Recipient' => $shipRequest->getRecipientDetails(),	
				'TotalWeight' => array(
					'Value' => $packageToShip->getWeight(),
					'Units' => $shipRequest->getWeightUnits()
				),
				
			'ShippingChargesPayment' => $this->addShippingChargesPayment(),
				'LabelSpecification' => array(
					'LabelFormatType' => 'COMMON2D',
					'ImageType' => $shipRequest->getLabelImageType(),
					'LabelStockType' => $shipRequest->getLabelStockType(),
					'LabelPrintingOrientation' => $shipRequest->getLabelPrintingOrientation(),
					'CustomerSpecifiedDetail' => array(
						'DocTabContent' => array(
							'DocTabContentType' => 'STANDARD'
						)
					)
				),
				
				//'CustomerSpecifiedDetail' => array('MaskedData'=> 'SHIPPER_ACCOUNT_NUMBER'), 
			'RateRequestTypes' => array('ACCOUNT'), // valid values ACCOUNT and LIST
		'SpecialServicesRequested' => $specialServices,		
				
				
			//	'RateRequestTypes' => $shipRequest->getRateType(),
				'PreferredCurrency' => $this->getCurrencyCode(),
				'PackageDetail' => 'INDIVIDUAL_PACKAGES',
				'SignatureOptionDetail' => array(
					'OptionType' => $shipRequest->getSignature()
				),
				'ShippingChargesPayment' => array(
					'PaymentType' => $shipRequest->getPayorType(),
					'Payor' => array(
						'ResponsibleParty' => array(
							'AccountNumber' => $this->getFedexAccount(),
							'Contact' => null,
							'Address' => array(
								'CountryCode' => $shipRequest->getPayorAccountCountry()
							)
						)
					)
				),
				'RequestedPackageLineItems' => array(
					'SequenceNumber' => $packageToShip->getSequenceNumber(),
					'Weight' => array(
						'Value' => $packageToShip->getWeight(),
						'Units' => $shipRequest->getWeightUnits()
					),
					'CustomerReferences' => array(
						'0' => array(
							'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
							'Value' => $shipRequest->getCustomerReference()
						),
						'1' => array(
							'CustomerReferenceType' => 'INVOICE_NUMBER',
							'Value' => $shipRequest->getInvoiceNumber()
						)
					),
					'ContentRecords' => $packageToShip->getContents()
				)
			);

			$request['RequestedShipment'] = array_merge($request['RequestedShipment'], $shipRequest->getMPSData());

			// Saturday delivery
			if ($shipRequest->getSaturdayDelivery()) {
				$specialServiceTypes[] = 'SATURDAY_DELIVERY';
			}

			// Dangerous goods
			if ($packageToShip['dangerous']) {
				$request['RequestedShipment']['RequestedPackageLineItems']['SpecialServicesRequested'] = array(
					'DangerousGoodsDetail' => array(
						'Accessibility' => 'ACCESSIBLE',
						'Options' => 'ORM_D'
					)
				);
				$specialServiceTypes[] = 'DANGEROUS_GOODS';
			}

			// COD
			
			
			
			if ($shipRequest->getCod()) {
				$request['RequestedShipment']['SpecialServicesRequested'] = array(
					'SpecialServiceTypes' => array('COD'),
					'CodDetail' => array(
						'CodCollectionAmount' => array(
							'Amount' => $packageToShip['cod_amount'],
							'Currency' => $this->getCurrencyCode()
						),
						'CollectionType' => 'CASH',
						
		'RemitToName' => 'Remitter'
					)
				);
				$specialServiceTypes[] = 'COD';
			}

			
			
			
			if (isset($specialServiceTypes)) {
				//$request['RequestedShipment']['RequestedPackageLineItems']['SpecialServicesRequested']['SpecialServiceTypes'] = $specialServiceTypes;
				//$request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'] = $specialServiceTypes;
			}

			 // If Dimensions enabled for Shipment
			if (($packageToShip->getContainerCode() == "YOUR_PACKAGING") && (!$packageToShip->getEnableDimensions())
				&& ($packageToShip->getRoundedLength() && $packageToShip->getRoundedWidth() && $packageToShip->getRoundedHeight()))
			{
				 $request['RequestedShipment']['RequestedPackageLineItems']['Dimensions'] = array(
					'Length' => $packageToShip->getRoundedLength(),
					'Width' => $packageToShip->getRoundedWidth(),
					'Height' => $packageToShip->getRoundedHeight(),
					'Units' => $shipRequest->getDimensionUnits());
			}

			// Check if shipment needs to be insured and if insurance amount is available
			if ($shipRequest->getInsureShipment()) {
				$request['RequestedShipment']['RequestedPackageLineItems']['InsuredValue'] = array(
					'Currency' => $this->getCurrencyCode(),
					'Amount' => $shipRequest->getInsureAmount()
				);
			}

			// If SmartPost is enabled
			if (Mage::getStoreConfig('carriers/fedex/enable_smartpost')) {
				$request['RequestedShipment']['SmartPostDetail'] = $shipRequest->getSmartPostDetails();
			}

			// International shipments
			if ($shipRequest->getTransactionType() == 'International' || 1) {

				// If tax ID number is present
				if ($this->getConfig('tax_id_number') != '') {
					$request['TIN'] = $this->getConfig('tax_id_number');
				}

				$itemDetails = array();

				// Iterate through package items
				foreach ($packageToShip->getItems() as $_item) {
	
					/** Load item by order item id */
					$item = Mage::getModel('sales/order_item')->load($_item['id']);

					$itemDetails[] = array(
						'NumberOfPieces' => 1,
						'Description' => $item->getName(),
						'CountryOfManufacture' => $shipRequest->getShipperCountryCode(),
						'Weight' => array(
							'Value' => ($item->getWeight() * $shipRequest->getWeightCoefficient())*$_item['qty'],
							'Units' => $shipRequest->getWeightUnits()
						),
						'Quantity' => $_item['qty'],
						'QuantityUnits' => 'EA',
						'UnitPrice' => array(
							'Amount' => sprintf('%01.2f', $item->getPrice()),
							'Currency' => $this->getCurrencyCode()
						),
						'CustomsValue' => array(
							'Amount' => sprintf('%01.2f', ($item->getPrice()*$_item['qty'])),
							'Currency' => $this->getCurrencyCode()
						)
					);
				}
				
				$request['RequestedShipment']['CustomsClearanceDetail'] = array(
					'DutiesPayment' => array(
						'PaymentType' => 'SENDER',
						'Payor' => array(
							'ResponsibleParty' => array(
								'AccountNumber' => $this->getFedexAccount(),
								'Contact' => null,
								'Address' => array(
									'CountryCode' => $shipRequest->getShipperCountryCode()
								)
							)
						)
					),
					'DocumentContent' => 'NON_DOCUMENTS',
					'CustomsValue' => array(
						//'Amount' => sprintf('%01.2f', $shipRequest->getCustomsValue()),
						//'Amount' => '198',
						'Amount' => $shipRequest->getInsureAmount(),
						'Currency' => $this->getCurrencyCode()
					),
					'Commodities' => $itemDetails,
					'CommercialInvoice' => array(
						//'TermsOfSale' => 'FOB_OR_FCA' // 'CFR_OR_CPT', etc...
						'Purpose' => 'SOLD',
						'CustomerReferences' => array(
							'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
							'Value' => $shipRequest->getCustomerReference()
						),
					),
					'ExportDetail' => array(
						'B13AFilingOption' => $shipRequest->getB13AFilingOption()
					)
				);
			}

			$returnLabelImage = '';
			$package_id =Mage::getSingleton('core/session')->getMyValue();

			if ($shipRequest->getReturnLabel()) {

				$returnRequest = $request;

				$returnRequest['RequestedShipment']['Recipient'] = $shipRequest->getShipperDetails();
				$returnRequest['RequestedShipment']['Shipper']   = $shipRequest->getRecipientDetails();
				
				
				try {

					Mage::Helper('shipsync')->mageLog($returnRequest, 'ship');

					$returnResponse = $this->_shipServiceClient->processShipment($returnRequest);

					Mage::Helper('shipsync')->mageLog($returnResponse, 'ship');
				}
				catch (SoapFault $ex) {
					
					Mage::Helper('shipsync')->writeErrorLog($package_id,$ex);
					throw Mage::exception('Mage_Shipping', $ex->getMessage());
				}

				$returnLabelImage = base64_encode($returnResponse->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image);
			}
			
		//	$request['RequestedShipment']['SpecialServicesRequested']= array('CodDetail' => array('CodCollectionAmount'=>array('Amount' =>'258','Currency'=>'INR'),
		//	'CollectionType' => 'CASH'),'SpecialServiceTypes' =>array('0'=>'COD'));
			
			//$total_weight = $request['RequestedShipment']['TotalWeight']['Value'];
			
			// $qty_value = round($request['RequestedShipment']['CustomsClearanceDetail']['Commodities'][0]['Quantity']);
			
			
			
			//echo "<pre>";
			
			$arr = $request['RequestedShipment']['CustomsClearanceDetail']['Commodities'];
			
			if(is_array($arr))
			{
			foreach($arr as $result)
			{
			$qty[]  = round($result['Quantity']);
			$weight[] = $result['Weight']['Value'];
			}
			}
				 	$weight_value = array_sum($weight);
		 	$qty_value = array_sum($qty);
			  

			//print_r($request);exit;
			  
			try {

				Mage::Helper('shipsync')->mageLog($request, 'ship');

				$shipResponse = $this->_shipServiceClient->processShipment($request);

				Mage::Helper('shipsync')->mageLog($shipResponse, 'ship');
			}
			catch (SoapFault $ex) {
			
				Mage::Helper('shipsync')->writeErrorLog($package_id,$ex);
				throw Mage::exception('Mage_Shipping', $ex->getMessage());
			}
			
			/** destination code**/
			$trackfromid = $shipResponse->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->FormId;
			/* $destincode = $shipResponse->CompletedShipmentDetail->OperationalDetail->UrsaPrefixCode;
			$suffixcode = $shipResponse->CompletedShipmentDetail->OperationalDetail->UrsaSuffixCode; */
			$descode = $shipResponse->CompletedShipmentDetail->CompletedPackageDetails->OperationalDetail->OperationalInstructions[2]->Content;
			
			 $shipbarcode = $shipResponse->CompletedShipmentDetail->CompletedPackageDetails->OperationalDetail->OperationalInstructions[3]->Content;
			/** End the destination code ***/
			
			/**** return details ***/
			 $returnshipbarcode = $returnResponse->CompletedShipmentDetail->AssociatedShipments->PackageOperationalDetail->OperationalInstructions[2]->Content; 
			
			$returnawb = $returnResponse->CompletedShipmentDetail->AssociatedShipments->PackageOperationalDetail->OperationalInstructions[4]->Content;
			
			 $returnformid = $returnResponse->CompletedShipmentDetail->AssociatedShipments->PackageOperationalDetail->OperationalInstructions[1]->Content; 
			
			$returnmethod = $returnResponse->CompletedShipmentDetail->AssociatedShipments->PackageOperationalDetail->OperationalInstructions[5]->Content; 
			
			 $returnamount = $returnResponse->CompletedShipmentDetail->AssociatedShipments->PackageOperationalDetail->OperationalInstructions[9]->Content; 
			 
			 $descode1 = $returnResponse->CompletedShipmentDetail->CompletedPackageDetails->OperationalDetail->OperationalInstructions[2]->Content;
			 
			/*** End ***/
			
			/**** Origin/recipient  details**/
		     $originid = $shipResponse->CompletedShipmentDetail->OperationalDetail->OriginLocationId; 
			 $desid = $shipResponse->CompletedShipmentDetail->OperationalDetail->DestinationLocationId;
			//exit;
			/**** Origin end ***/


			$response = Mage::getModel('shipsync/shipment_response')->setResponse($shipResponse);

			$shipResult = Mage::getModel('shipsync/shipment_result');

			if ($response->setNotificationsErrors()) {
				throw Mage::exception('Mage_Shipping', $response->getErrors());
			} else {

				if (!Mage::getStoreConfig('carriers/fedex/third_party')) {
					$shipResult->setBillingWeightUnits($response->findStructure('Units'));
					$shipResult->setBillingWeight($response->findStructure('Value'));
				}

				$_packages = array();

				$_packages[] = array(
					'package_number' => $response->getSequenceNumber(),
					'tracking_number' => $response->getTrackingNumber(),
					'masterTrackingId' => $response->getMasterTrackingId(),
					'label_image_format' => $shipRequest->getLabelImageType(),
					'label_image' => base64_encode($shipResponse->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image),
					'return_label_image' => $returnLabelImage,
					'cod_label_image' => $response->getCodLabelImage()
				);

				$shipResult->setPackages($_packages);
			}


            /** Iterate through shipped packages */
            foreach ($shipResult->getPackages() as $packageShipped) {
                $convertor = Mage::getModel('sales/convert_order');
                
                if ($packageShipped['masterTrackingId'] != false) {
                    $shipRequest->setMasterTrackingId($packageShipped['masterTrackingId']);
                }
                
                if ($packageShipped['package_number'] == 1) {
                    $shipment = $convertor->toShipment($shipRequest->getOrder());
                }
                
                foreach ($this->getItemsById($packageToShip) as $itemToShip) {
					
                    $orderItem = $shipRequest->getOrder()->getItemById($itemToShip['id']);
                    $item      = $convertor->itemToShipmentItem($orderItem);
                    $item->setQty($itemToShip['qty']);
                    $shipment->addItem($item);
                }

                $track = Mage::getModel('sales/order_shipment_track')
					->setTitle($this->getCode('method', $shipRequest->getServiceType(), true))
					->setCarrierCode('fedex')
					->setNumber($packageShipped['tracking_number'])
					->setShipment($shipment);
                
                if ($packageShipped['package_number'] == $shipRequest->getPackageCount()) {
                    $shipment->addTrack($track);
                    $shipment->register();
                    $shipment->getOrder()->setIsInProcess(true);
                    
                    $transactionSave = Mage::getModel('core/resource_transaction')->addObject($shipment)->addObject($shipment->getOrder())->save();
                    
                    //$shipment->sendEmail();

                } else {
                    $shipment->addTrack($track);
                }
				
				
				 

				// Append packing list
				if ($shipRequest->getLabelImageType() == 'PDF' &&
					$shipRequest->getLabelStockType() == 'PAPER_7X4.75' &&
					$shipRequest->getLabelPrintingOrientation() == 'BOTTOM_EDGE_OF_TEXT_FIRST' &&
					$shipRequest->getPackingList()) {
                
					// Create Zend PDF object
					$pdf = Zend_Pdf::parse(base64_decode($packageShipped['label_image']));

					// Set font style
					$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);

					// Set font size and apply to current page
					$pdf->pages[0]->setFont($font, 8);

					// Set Y Position (## pixels from bottom of page)
					$yPosition = 360;

					// Draw header text (text, xPosition, yPosition)
					$pdf->pages[0]->drawText('ID', 30, $yPosition);
					$pdf->pages[0]->drawText('SKU', 60, $yPosition);
					$pdf->pages[0]->drawText('Name', 180, $yPosition);
					$pdf->pages[0]->drawText('Qty', 560, $yPosition);

					// Draw separator line (xPosition1, yPosition1, xPosition2, yPosition2)
					$pdf->pages[0]->drawLine(30, $yPosition-3, 575, $yPosition-3);

					// Set cursor (ie, line height)
					$cursor = 12;

					// Retrieve shipment items
					$items = $this->getItemsById($packageToShip);

					foreach ($items as $item) {

						// New line
						$yPosition -= $cursor;

						// Draw item text (text, xPosition, yPosition)
						$pdf->pages[0]->drawText($item['product_id'], 30, $yPosition);
						$pdf->pages[0]->drawText($item['sku'], 60, $yPosition);
						$pdf->pages[0]->drawText($item['name'], 180, $yPosition);
						$pdf->pages[0]->drawText($item['qty_to_ship'], 560, $yPosition);

						// Draw separator line (xPosition1, yPosition1, xPosition2, yPosition2)
						$pdf->pages[0]->drawLine(30, $yPosition-3, 575, $yPosition-3);
					}

					// Render & encode label image
					$labelImage = base64_encode($pdf->render());
				}
				else {
					$labelImage = $packageShipped['label_image'];
				}

				
				
                $pkg = Mage::getModel('shipping/shipment_package')
					->setOrderIncrementId($shipRequest->getOrder()->getIncrementId())
					->setOrderShipmentId($shipment->getEntityId())
					->setPackageItems($this->jsonPackageItems($packageToShip))
					->setCarrier('fedex')
					->setShippingMethod($track->getTitle())
					->setPackageType($this->getCode('packaging', $packageToShip->getContainerCode(), true))
					->setCarrierShipmentId($shipResult->getShipmentIdentificationNumber())
					->setWeightUnits($shipResult->getBillingWeightUnits())
					->setDimensionUnits($packageToShip->getDimensionUnitCode())
					->setWeight($shipResult->getBillingWeight())
					->setLength($packageToShip->getLength())
					->setWidth($packageToShip->getWidth())
					->setHeight($packageToShip->getHeight())
					->setTrackingNumber($packageShipped['tracking_number'])
					->setCurrencyUnits($shipResult->getCurrencyUnits())
					->setTransportationCharge($shipResult->getTransportationShippingCharges())
					->setServiceOptionCharge($shipResult->getServiceOptionsShippingCharges())
					->setShippingTotal($shipResult->getTotalShippingCharges())
					->setNegotiatedTotal($shipResult->getNegotiatedTotalShippingCharges())
					->setLabelFormat($packageShipped['label_image_format'])
					->setLabelImage($labelImage)
					->setReturnLabelImage($packageShipped['return_label_image'])
					->setCod($specialServiceTypes[0])
					->setOriginId($shipResponse->CompletedShipmentDetail->OperationalDetail->OriginLocationId)
					->setDestinationId($shipResponse->CompletedShipmentDetail->OperationalDetail->DestinationLocationId)
					->setFormid($shipResponse->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->FormId)
					->setReturnBarcode($returnshipbarcode)
					->setReturnAwb($returnawb)
					->setReturnFormid($returnformid)
					->setReturnMethod($returnmethod)
					->setReturnAmount($returnamount)					
					->setTrackBarcode($shipbarcode)
					->setDestinationCode($descode)
					->setReturnDestinationCode($descode1)
					->setCodLabelImage($packageShipped['cod_label_image'])
					->setDateShipped(date('Y-m-d H:i:s'))
					->save();
                
				
				
				
				$barcode_img = '/shipsync-shipment/'.$shipbarcode.'_barcode.jpg';
			
			 $write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$sql = "update sales_flat_shipment_track set airway_bill_barcode='".$shipbarcode."',image_path='".$barcode_img."',feetscience_package_id='".$package_id."',weight='".$weight_value."',qty='".$qty_value."' where track_number='".$packageShipped['tracking_number']."'";
			$resource = $write->query($sql);
                $retval[] = $pkg;
               
				
				
			
				
				/** Log files code -  Start */
				$tracknumber = $packageShipped['tracking_number'];
				$createdFolderPath  = $this->createLogFolder($tracknumber);
				
				//echo $createdFolderPath; exit;
				
				$requestLogFile = $createdFolderPath . DS .$tracknumber.'_request.log' ;	
	
				if (!file_exists($requestLogFile)) {				   
					$requestLogWrite =  fopen("$requestLogFile","w")  or die("Unable to open file!");
					fwrite($requestLogWrite, print_r($request,1));
					fclose($requestLogWrite);
				}
				
				/***** Return files***/
				if($pkg->getCod()=="COD"){
				$returnRequestLogFile = $createdFolderPath . DS .$tracknumber.'_return_request.log' ;	
	
				if (!file_exists($returnRequestLogFile)) {				   
					$returnRequestLogWrite =  fopen("$returnRequestLogFile","w")  or die("Unable to open file!");
					fwrite($returnRequestLogWrite, print_r($returnRequest,1));
					fclose($returnRequestLogWrite);
				}
				
				$returnResponseLogFile = $createdFolderPath . DS .$tracknumber.'_return_response.log' ;	
	
				if (!file_exists($returnResponseLogFile)) {				   
					$returnResponseLogWrite =  fopen("$returnResponseLogFile","w")  or die("Unable to open file!");
					fwrite($returnResponseLogWrite, print_r($returnResponse,1));
					fclose($returnResponseLogWrite);
				}
				}
				/*** End **/
				$responseLogFile = $createdFolderPath . DS .$tracknumber.'_response.log' ;	
	
				if (!file_exists($responseLogFile)) {				   
					$responseLogWrite =  fopen("$responseLogFile","w")  or die("Unable to open file!");
					fwrite($responseLogWrite, print_r($shipResponse,1));
					fclose($responseLogWrite);
				}
				
				$labelImage = $shipResponse->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image;
				
				$fedexLabelFile = $createdFolderPath . DS .$tracknumber.'_fedex_label.pdf' ;	
	
				if (!file_exists($fedexLabelFile)) {				   
					$fedexLabelWrite =  fopen("$fedexLabelFile","w")  or die("Unable to open file!");
					fwrite($fedexLabelWrite, print_r($labelImage,1));
					fclose($fedexLabelWrite);
				}
				
				$returnLabelImage = $returnResponse->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image;
				
				$fedexReturnLabelFile = $createdFolderPath . DS .$tracknumber.'_fedex_return_label.pdf' ;	
	         if($pkg->getCod()=="COD"){
				if (!file_exists($fedexReturnLabelFile)) {				   
					$fedexReturnLabelWrite =  fopen("$fedexReturnLabelFile","w")  or die("Unable to open file!");
					fwrite($fedexReturnLabelWrite, print_r($returnLabelImage,1));
					fclose($fedexReturnLabelWrite);
				}
				}
				/** Log files code -  End */
				
            }
            
            if (Mage::getStoreConfig('carriers/fedex/mps_shipments')) {
                foreach ($retval as $pkg) {
                    $pkg->setOrderShipmentId($shipment->getEntityId())->save();
                }
            }
        }
		
		
        return $retval;
		
		
    }

    
    public function jsonPackageItems($packageToShip)
    {
        $ret = array();
        
        foreach ($this->getItemsById($packageToShip) as $item) {
            $ret[] = array(
                'i' => $item['product_id'],
                'q' => $item['qty_to_ship']
            );
        }
        
        return json_encode($ret);
    }
    
    public function getItemsById($packageToShip)
    {
        $itemsById = array();

        foreach ($packageToShip->getItems() as $itemToShip) {
            $id                        = $itemToShip['id'];
            $count                     = isset($itemsById[$id]['qty_to_ship']) ? $itemsById[$id]['qty_to_ship'] : 0;
            $itemToShip['qty_to_ship'] = 1 + $count;
            $itemsById[$id]            = $itemToShip;        
        }
        
        return $itemsById;
    }

    /*
     * Prepare shipment header
     *
     * @return array
     */
    protected function _prepareShipmentHeader()
    {
        $shipRequest = $this->_shipRequest;
        
        $request['WebAuthenticationDetail'] = array(
            'UserCredential' => array(
                'Key' => $this->getFedexKey(),
                'Password' => $this->getFedexPassword()
            )
        );
        
        $request['ClientDetail'] = array(
            'AccountNumber' => $this->getFedexAccount(),
            'MeterNumber' => $this->getFedexMeter()
        );
        
        $transactionType = $shipRequest->getTransactionType();
        
        $transactionMethod = $shipRequest->getTransactionMethod();
        
        $request['TransactionDetail']['CustomerTransactionId'] = "*** Express Domestic Shipping Request v13 using PHP ***";
        $request['Version']                                    = array(
            'ServiceId' => 'ship',
            'Major' => '13',
            'Intermediate' => '0',
            'Minor' => '0'
        );
        
        return $request;
    }
    
    /*
     * Set special services
     * 
     * @param string
     * @param IllApps_Shipsync_Model_Shipment_Package
     * @return array
     */
    protected function _setSpecialServices($request, $package)
    {
    }
	
	/** Log files code -  Start */
	public function createLogFolder($tracknumber){
	
		$url1 = Mage::getBaseDir() . DS ."var" . DS ."shipsync";
		//exit;
		
		
		/**** folder creation ***/
		if (file_exists($url1)) {
			//echo "The file  exists";
		}
		else {
		   $mkdir =  mkdir($url1);
		 }
		
		$urlyear = $url1 . DS .date('Y');
			
		if (file_exists($urlyear)) {
			//echo "The file  exists";
		}
		else {
		   $mkdiryear =  mkdir($url1 . DS .date('Y'));
		}
		
		$urlmonth = $url1 . DS .date('Y') .DS . date('M');
			
		if (file_exists($urlmonth)) {
			//echo "The file  exists";
		}
		else {
		   $mkdirmonth =  mkdir($url1 . DS .date('Y').DS .date('M'));
		}
		
		$urldate = $url1 . DS .date('Y') .DS . date('M').DS. date('d');
			
		if (file_exists($urldate)) {
			//echo "The file  exists";
		}
		else {
		   $mkdirdate =  mkdir($url1 . DS .date('Y').DS .date('M').DS. date('d'));
		}
	  
		  
		$urltracknumber = $url1 . DS .date('Y') .DS . date('M').DS. date('d'). DS . $tracknumber;
			
		if (file_exists($urltracknumber)) {
			return $urltracknumber;
		}
		else {
		   $mkdirtracknumber =  mkdir($url1 . DS .date('Y').DS .date('M').DS. date('d'). DS . $tracknumber);
		   return $url1 . DS .date('Y').DS .date('M').DS. date('d'). DS . $tracknumber;
		}
	}
	/** Log files code -  End */

}
