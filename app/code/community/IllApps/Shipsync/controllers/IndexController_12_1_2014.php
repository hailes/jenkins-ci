<?php

/**
 * ShipSync
 *
 * @category   IllApps
 * @package    IllApps_Shipsync
 * @copyright  Copyright (c) 2014 EcoMATICS, Inc. DBA IllApps (http://www.illapps.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * IllApps_Shipsync_IndexController
 */
class IllApps_Shipsync_IndexController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->loadLayout();
        
        $this->_setActiveMenu('sales');
        
        $this->_addBreadcrumb($this->__('Sales'), $this->__('Sales'));
        $this->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));
        $this->_addBreadcrumb($this->__('ShipSync'), $this->__('ShipSync'));
		
		/** Retrieve post data */
        $post = $this->getRequest()->getPost();
		
		if(!empty($post)){
        
			// Get fedex model
			$fedex = Mage::getModel('usa/shipping_carrier_fedex');
			
			// Get rate request model
			$rateRequest = Mage::getModel('shipping/rate_request');
			
			// Get order model
			$order = Mage::getModel('sales/order')->loadByIncrementId($post['order_id']);
			
			// Set order model
			$rateRequest->setOrder($order);
			
			// Set request data
			$rateRequest->setAllItems($order->getAllItems());
			
			// Streets
			$streets[] = $post['recipient_street1'];
			
			if ($post['recipient_street2'] != '') {
				$streets[] = $post['recipient_street2'];
			}
			if ($post['recipient_street3'] != '') {
				$streets[] = $post['recipient_street3'];
			}
			
			// Set destination data
			$rateRequest->setDestStreet($streets);
			$rateRequest->setDestCity($post['recipient_city']);
			$rateRequest->setDestPostcode($post['recipient_postcode']);
			$rateRequest->setDestRegionCode($post['recipient_region']);
			$rateRequest->setDestCountryId($post['recipient_country']);
			
			// Set insure shipment
			if (isset($post['insure_shipment']) && ($post['insure_shipment'] == 'on')) {
				$rateRequest->setInsureShipment(true)->setInsureAmount($post['insure_amount']);
			} else {
				$rateRequest->setInsureShipment(false);
			}
			
			$rateRequest->setResidenceDelivery($post['residence_delivery']);
			$rateRequest->setAddressValidation($post['address_validation']);

			// Get all items to ship
			$items = Mage::getModel('shipsync/shipping_package')->getParsedItems($rateRequest->getAllItems(), true);
			
			// Sort items by sku, removing duplicates
			foreach ($items as $item) {
				$_itemsBySku[$item['sku']] = $item;
			}
			
			// Iterate packages for rating, and loads items details from getParsedItems based on sku
			foreach ($post['packages'] as $package) {
				$error = false;
				
				unset($_items);
				
				$itemsToPack = explode(',', $package['items']);
				
				foreach ($itemsToPack as $key => $itemToPack) {
					$_items[] = $_itemsBySku[$post['items'][$itemToPack - 1]['sku']];
				}
				
				$_packages[] = array(
					'volume' => $package['width'] * $package['height'] * $package['length'],
					'value' => $package['value'],
					'label' => $package['value'],
					'items' => $_items,
					'weight' => $package['weight'],
					'length' => $package['length'],
					'width' => $package['width'],
					'height' => $package['height']
				);
			}
			
			$rateRequest->setPackages($_packages);

			$rateResult = Mage::getModel('shipsync/shipping_carrier_fedex_rate')->collectRates($rateRequest);
			
			$possibleMethods = $rateResult->asArray();
			
			$methods = $possibleMethods['fedex']['methods'];
			
			if ($rateResult->getError()) {
				unset($post);
				
				$post['error'] = $rateResult->getRateById(0)->getErrorMessage();
			} else {
				if (isset($post['method'])) {
					$arrResult = $rateResult->asArray();
					
					if (isset($arrResult['fedex']['methods'][$post['method']]['price_formatted'])) {
						$post['shipping_amount'] = $arrResult['fedex']['methods'][$post['method']]['price_formatted'];
					} else {
						$post['shipping_amount']          = null;
						$post['shipping_allowed_methods'] = $arrResult;
					}
				} else {
					unset($post);
					$post['error'] = 'Invalid Request';
				}
			}
			$encoded = json_encode($post);
			
			$block = Mage::app()->getLayout()->getBlock('shipsync_index');
			if ($block){//check if block actually exists
				$block->setUpdateShipmentResponseBlock($encoded);
			}		
		}
        
        $this->renderLayout();
    }

    /**
     * optionAction
     */
    public function optionAction()
    {
        // Get post request
        $post = $this->getRequest()->getPost();
        
        // Load layout
        $this->loadLayout();
        
        $layout = $this->getLayout();
        
        // Get shipsync option block
        $block = $layout->getBlock('shipsync_option');
        
        $block->setData('i', $post['i']);
        $block->setData('z', $post['i']);
        
        echo $block->toHtml();
    }
    
    /**
     * packagesAction
     */
    public function packagesAction()
    {
        $id = $this->getRequest()->getParam('shipment_id');
        
        Mage::register('current_shipment', Mage::getModel('sales/order_shipment')->load($id));
        
        $this->loadLayout();
        
        Mage::getSingleton('shipsync/sales_order_shipment_package')->setShipmentId($id);
        
        $this->renderLayout();
    }

    /**
     * Attributes Action
     * 
     * Parses ajax call in the event that a package is changed in Actual Shipment Request.
     * Returns json object containing the relevant information.
     * 
     * @return json_object
     */
    public function attributesAction()
    {
        // Get post request
        $post = $this->getRequest()->getPost();
        
        // Get default packages
        $defaultPackages = Mage::getModel('shipsync/shipping_package')->getDefaultPackages(array(
            'fedex'
        ));
        
        foreach ($post['packages'] as $num => $package) {
            foreach ($defaultPackages as $defaultPackage) {
                if ($package['value'] == $defaultPackage['value']) {
                    foreach ($defaultPackage as $key => $element) {
                        $key = 'packages_' . $num . '_' . $key;
                        
                        $returnArray[$key] = $element;
                    }
                    
                    echo json_encode($returnArray);
                }
            }
        }
    }

    /**
     * Rate Action
     *
     * Ajax backend action, handling an ad hoc rate request in shipment creation.
     *
     * @return json_object
     */
    public function rateAction()
    {
        // Get post data
        $post = $this->getRequest()->getPost();
		

        
        // Get fedex model
        $fedex = Mage::getModel('usa/shipping_carrier_fedex');
        
        // Get rate request model
        $rateRequest = Mage::getModel('shipping/rate_request');
        
        // Get order model
        $order = Mage::getModel('sales/order')->loadByIncrementId($post['order_id']);
        
        // Set order model
        $rateRequest->setOrder($order);
        
        // Set request data
        $rateRequest->setAllItems($order->getAllItems());
        
        // Streets
        $streets[] = $post['recipient_street1'];
        
        if ($post['recipient_street2'] != '') {
            $streets[] = $post['recipient_street2'];
        }
        if ($post['recipient_street3'] != '') {
            $streets[] = $post['recipient_street3'];
        }
        
        // Set destination data
        $rateRequest->setDestStreet($streets);
        $rateRequest->setDestCity($post['recipient_city']);
        $rateRequest->setDestPostcode($post['recipient_postcode']);
        $rateRequest->setDestRegionCode($post['recipient_region']);
        $rateRequest->setDestCountryId($post['recipient_country']);
        
        // Set insure shipment
        if (isset($post['insure_shipment']) && ($post['insure_shipment'] == 'on')) {
            $rateRequest->setInsureShipment(true)->setInsureAmount($post['insure_amount']);
        } else {
            $rateRequest->setInsureShipment(false);
        }
        
        $rateRequest->setResidenceDelivery($post['residence_delivery']);
        $rateRequest->setAddressValidation($post['address_validation']);

        // Get all items to ship
        $items = Mage::getModel('shipsync/shipping_package')->getParsedItems($rateRequest->getAllItems(), true);
        
        // Sort items by sku, removing duplicates
        foreach ($items as $item) {
            $_itemsBySku[$item['sku']] = $item;
        }
        
        // Iterate packages for rating, and loads items details from getParsedItems based on sku
        foreach ($post['packages'] as $package) {
            $error = false;
            
            unset($_items);
            
            $itemsToPack = explode(',', $package['items']);
            
            foreach ($itemsToPack as $key => $itemToPack) {
                $_items[] = $_itemsBySku[$post['items'][$itemToPack - 1]['sku']];
            }
            
            $_packages[] = array(
                'volume' => $package['width'] * $package['height'] * $package['length'],
                'value' => $package['value'],
                'label' => $package['value'],
                'items' => $_items,
                'weight' => $package['weight'],
                'length' => $package['length'],
                'width' => $package['width'],
                'height' => $package['height']
            );
        }
        
        $rateRequest->setPackages($_packages);

        $rateResult = Mage::getModel('shipsync/shipping_carrier_fedex_rate')->collectRates($rateRequest);
        
        $possibleMethods = $rateResult->asArray();
        
        $methods = $possibleMethods['fedex']['methods'];
        
        if ($rateResult->getError()) {
            unset($post);
            
            $post['error'] = $rateResult->getRateById(0)->getErrorMessage();
        } else {
            if (isset($post['method'])) {
                $arrResult = $rateResult->asArray();
                
                if (isset($arrResult['fedex']['methods'][$post['method']]['price_formatted'])) {
                    $post['shipping_amount'] = $arrResult['fedex']['methods'][$post['method']]['price_formatted'];
                } else {
                    $post['shipping_amount']          = null;
                    $post['shipping_allowed_methods'] = $arrResult;
                }
            } else {
                unset($post);
                $post['error'] = 'Invalid Request';
            }
        }
        $encoded = json_encode($post);
        
        $this->getResponse()->setBody($encoded);
    }
    
    
    /**
     * Post action
     *
     * @return IllApps_Shipsync_IndexController
     */
    public function postAction()
    {
	
        $message = "";
        
        /** Retrieve post data */
        $post = $this->getRequest()->getPost();
		
		/******* Shipment status code ***/
		$totalproductcount = 0;
		if(is_array($post['items']))
		{
			foreach($post['items'] as $key=>$value)
			{  
			   $productqty =  $value['quantity'];
			   $totalproductcount += $productqty;	  
			} 
		}
		
		
		 $orderId = $this->getRequest()->getParam('order_id');
		
		
		//get ordered total
			$order = Mage::getModel("sales/order")->loadByIncrementId($orderId);
	 //load order by order id
	 $ordered_items = $order->getAllItems();
	 
	 
	 $qtotal = 0;
	
	
	 
	 
	 Foreach($ordered_items as $item){ 
	 if(!$item->getParentItemId())
	 {
		 $qty_array[$item->getItemId()] = round($item->getQtyOrdered());
	     $qtotal = $qtotal + round($item->getQtyOrdered()); // sum up the total number of ordered  items 
		 
	    
		}
	 }
	 //print_r($qty_array);
	 
	
	// get count the shipping items
		$orderId = $this->getRequest()->getParam('order_id');
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		if ($order->hasShipments()) { 
		$invIncrementIDs = array(); 
		$invItems = array(); 
		$total = 0; 
		foreach ($order->getShipmentsCollection() as $inv) { 
			$invIncrementIDs[] = $inv->getIncrementId(); 
			foreach ($inv->getAllItems() as $item) { 
				$invItems[] = $item->getQty(); 
				$total = $total + ($item->getQty()); 
			}
		}
		}
		
		
		
		$totalselectedqty = 0;
		if(is_array($post['product_sel_quantity']))
		{
	      foreach($post['product_sel_quantity'] as $k=>$v)
		  {
		     
		   $selectedqty = $v['selected'];
		   $totalselectedqty += $selectedqty;
		   
		  }
		}
		
		 $totalselectedqty = ($totalselectedqty + $total) ;
		
		if($qtotal==$totalselectedqty)
        {
			$status = "shipped";
        }		
		else
		{
			$status = "partially_shipped";
		} 
		
		
		/***** End the shipment status *****/
		
		/******* Shipment history ***/
		$UserInfo = Mage::getSingleton('admin/session')->getUser();
		$Username =' ';
		$UserName=$UserInfo->getUsername();
		
		
		/* echo '<pre>';
		print_r($UserName);
		print_r($history);
		
		exit; */
		/*** END HISTORY ***/
        
        /** Throw exception if post data is empty */
        if (empty($post)) {
            Mage::throwException($this->__('Invalid form data.'));
        }
        
        /** Load order model */
        $order = Mage::getModel('sales/order')->loadByIncrementId($post['order_id']);
        
        /** If orderEntityId is null, throw error */
        if ($order->getEntityId() == null) {
            /** Set error message */
            Mage::getSingleton('adminhtml/session')->addError("Error: Invalid Order ID");
            
            /** Redirect */
            $this->_redirectReferer();
            
            return $this;
        }
        
        /** If order is not shippable */
        if (!$order->canShip()) {
            /** Set error message */
            Mage::getSingleton('adminhtml/session')->addError("Error: Order unable to be shipped");
            
            /** Redirect */
            $this->_redirectReferer();
            
            return $this;
        }
        
        $i = 0;
        
        
        // Get all items to ship
        $items = Mage::getModel('shipsync/shipping_package')->getParsedItems($order->getAllItems(), true);
        
        // Sort items by sku, removing duplicates
        foreach ($items as $item) {
            $_itemsById[$item['id']] = $item;
        }
        
		/** QUANTITY MANIPULATION BEGINS */
		$selectString = ''; $finalSelectString = '';
		$initQtyStr = 1;
		foreach($post['product_sel_quantity'] as $proID => $proQtyArray){
			//echo $proQtyArray['selected']; echo "<br />";

			for($qtyStr = $initQtyStr, $limiter = 1; $qtyStr <= count($post['items']); $qtyStr++, $limiter++){
				//echo "qtyStr -- ".intval($qtyStr); echo "<br />";
				if(intval($limiter) <= intval($proQtyArray['selected'])){
					//echo "IN"; echo "<br />";
					$selectString .= $qtyStr . ',';
				}			
			}

			//echo "SELECT STRING MIDDLE === " . $selectString; echo "<br />";

			$initQtyStr	= $initQtyStr + $proQtyArray['org_qty'];	
		}
		
		//echo "SELECT STRING === " . $selectString; echo "<br />";
		
		$finalSelectString = trim($selectString, ",");
		$post['packages'][0]['items'] = $finalSelectString;
		//echo "SELECT STRING === " . $finalSelectString; echo "<br />";

		//echo "<pre>ITEMS"; print_r($_itemsById); echo "</pre>";
		//echo "<pre>POST"; print_r($post); echo "</pre>"; exit;
		//exit;
        /** QUANTITY MANIPULATION ENDS */
		
		
		
		
		
		
		
		
		
		
		
        /** Iterate through packages */
        foreach ($post['packages'] as $package) {
            unset($_items);
            
            $itemsToPack = explode(',', $package['items']);
            
            foreach ($itemsToPack as $key => $itemToPack) {
                $_items[] = $_itemsById[$post['items'][$itemToPack - 1]['item_id']];
            }
            
            $package['dangerous']    = false;
            $package['cod']          = false;
            $package['cod_amount']   = null;
            $package['confirmation'] = false;
            $package['saturday']     = false;
            
            
            if (isset($post['cod']) && ($post['cod'] == 'on')) {
                $package['cod']        = true;
                $package['cod_amount'] = $post['cod_amount'];
            }
            
            if (isset($post['confirmation']) && ($post['confirmation'] == 'on')) {
                $package['confirmation'] = true;
            }
            
            if (isset($post['saturday']) && $post['saturday'] == 'on') {
                $package['saturday'] = true;
            }
            
            $itemIds          = $package['items'];
            $package['items'] = $_items;
            
            foreach ($package['items'] as $items) {
                if (isset($item['dangerous']) && $item['dangerous']) {
                    $package['dangerous'] = true;
                }
            }
            
            /** If package items are not empty */
            if (isset($package['items'])) {
                /** Set package object */
                $_package = Mage::getModel('shipping/shipment_package')
					->setPackageNumber($i)
					->setItems($package['items'])
					->setCod($package['cod'])
					->setCodAmount($package['cod_amount'])
					->setConfirmation($package['confirmation'])
					->setSaturdayDelivery($package['saturday'])
					->setDangerous($package['dangerous'])
					->setWeight($package['weight'])
					->setDescription('Package ' . $i + 1 . ' for order id ' . $post['order_id'])
					->setContainerCode(Mage::getModel('usa/shipping_carrier_fedex_package')
									   ->isFedexPackage($package['value']) ? $package['value'] : 'YOUR_PACKAGING')
					->setContainerDescription('')
					->setWeightUnitCode($post['weight_units'])
					->setDimensionUnitCode($post['dimension_units'])
					->setHeight($package['height'])
					->setWidth($package['width'])
					->setLength($package['length'])
					->setOrigin($package['altOrigin'])
					->setIsChild($package['isChild'])
					->setPackageItemIds($itemIds);
                
                /** Add package object to packages array */
                $packages[] = $_package;
            }
            /** If package is empty, throw error */
            else {
                /** Set error message */
                Mage::getSingleton('adminhtml/session')->addError("Error: Please include all ordered items when creating shipments.");
                
                /** Redirect */
                $this->_redirectReferer();
                
                return $this;
            }
            
            $i++;
            /** Increment package counter */
        }
        
        /** Set carrier */
        $carrier = Mage::getModel('usa/shipping_carrier_fedex');
        
       // $order->setShippingDescription("Federal Express - " . $carrier->getCode('method', $post['method']));
	   $order->setShippingDescription("Free Shipping");
        $order->setShippingMethod("fedex_" . $post['method'])->save();
        
        $recipientAddress = new Varien_Object();
        
        $streets[] = $post['recipient_street1'];
        
        if ($post['recipient_street2'] != '') {
            $streets[] = $post['recipient_street2'];
        }
        if ($post['recipient_street3'] != '') {
            $streets[] = $post['recipient_street3'];
        }
        
        $recipientAddress->setName($post['recipient_name']);
        $recipientAddress->setCompany($post['recipient_company']);
        $recipientAddress->setStreet($streets);
        $recipientAddress->setCity($post['recipient_city']);
        $recipientAddress->setRegionCode($post['recipient_region']);
        $recipientAddress->setPostcode($post['recipient_postcode']);
        $recipientAddress->setCountryId($post['recipient_country']);
        $recipientAddress->setTelephone($post['recipient_telephone']);
        
        if (isset($post['insure_shipment']) && ($post['insure_shipment'] == 'on')) {
            $insureShipment = true;
        } else {
            $insureShipment = false;
        }
        
        if (isset($post['insure_amount']) && ($post['insure_amount'] != "")) {
            $insureAmount = $post['insure_amount'];
        } else {
            $insureAmount = 0.0;
        }

        $request = new Varien_Object();
        
        $request->setOrderId($post['order_id'])
			->setMethodCode($post['method'])
			->setRecipientAddress($recipientAddress)
			->setPackages($packages)
			->setInsureShipment($insureShipment)
			->setInsureAmount($insureAmount)
			->setSaturdayDelivery($package['saturday'])
			->setCod($package['cod'])
			->setOriginId($package['origin_id'])
			->setDestinationId($package['destination_id'])
			->setDestinationCode($package['destination_code'])
			->setReturnDestinationCode($package['return_destination_code'])
			->setTrackBarcode($package['track_barcode'])
			->setReturnBarcode($package['return_barcode'])
			->setReturnAwb($package['return_awb'])
			->setReturnFormid($package['return_formid'])
			->setReturnMethod($package['return_method'])
			->setReturnAmount($package['return_amount'])	
			->setFormid($package['ship_formid'])
			->setResidenceDelivery($post['residence_delivery'])
			->setAddressValidation($post['address_validation'])
			->setShipperCompany($post['shipper_company'])
			->setLabelStockType($post['label_stock_type'])
			->setLabelImageType($post['label_image_type'])
			->setLabelPrintingOrientation($post['label_printing_orientation'])
			->setEnableJavaPrinting($post['enable_java_printing'])
			->setPrinterName($post['printer_name'])
			->setPackingList($post['packing_list'])
			->setSignature($post['signature'])
			->setReturnLabel($post['return_label'])
			->setCustomsValue($post['customs_value_amount']);
        
		//$this->htmlGen($_package,$request,$post);
		//exit;
	   try {
            $results = $carrier->createShipment($request);
        }
        /** Catch exception */
        catch (Exception $e) {
            /** Set error message */
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            
            /** Redirect */
            $this->_redirectReferer();
            
            return $this;
        }
        
        /** If results are empty */
        if (empty($results)) {
            /** Set error message */
            Mage::getSingleton('adminhtml/session')->addError("Error: Empty API response");
            
            /** Redirect */
            $this->_redirectReferer();
            
            return $this;
        }
        
        $tracks     = array();
        $packageids = '';
        
        /** Iterate through results */
        $i = 0;
        foreach ($results as $res) {
            /** Set tracking number */
            $tracks[$i]['number'] = $res->getTrackingNumber();
            
            /** Set label URL */
            $tracks[$i]['url'] = '<a target="shipping_label" href="' . Mage::getSingleton('adminhtml/url')->getUrl('shipsync/index/label/', array(
                'id' => $res->getPackageId()
            )) . '">Print Shipping Label</a>';

            /** Set package id */
            $tracks[$i]['id'] = $res->getPackageId();
            
            $i++;
        }
        
        /** Set success message */
        $message = '<p>SUCCESS: ' . $i . ' shipment(s) created</p>';
		
		
		
        
        /** Iterate through tracking #s */
        foreach ($tracks as $track) {
            /** Set tracking message */
            $message .= "<p>Package ID: " . $track['id'] . "<br /> Tracking Number: " . $track['number'] . "<br />" . $track['url'] . "</p>";

			
			
			
				
				
				
			
        }
		$message .= '<p><a target="_blank" href="' . Mage::getSingleton('adminhtml/url')->getUrl('shipsync/index/customlabel/', array(
					'id' => $res->getPackageId()
				)) . '">Print Custom Label</a></p>';
		/* echo '<pre>';
		print_r($track);
		echo '</pre>'; */
		 
		
        $this->htmlGen($_package,$request,$post,$track,$results);
		//echo $track[number];
		
		
        /** Add success message */
        Mage::getSingleton('adminhtml/session')->addSuccess($message);
        
        /** Get order url */
        $url = $this->getUrl('adminhtml/sales_order/view', array(
            'order_id' => $order->getId()
        ));
		
		/**** partially shipped status set ***/
        
		$orderstatus = Mage::getModel('sales/order')->loadByIncrementId($orderId); 
		$orderstatus->setStatus($status);
		$comment = "The order moved to". ' ' . $status;
		$history = Mage::getModel('sales/order_status_history')->setStatus($status)->setComment($comment)->setTrackUser($UserName); 
		$orderstatus->addStatusHistory($history);
        $orderstatus->save();
		
		
		/* echo '<pre>';
		print_r($orderstatus);
		exit; */
		/*** End ***/
		
		
        
		/** Redirect */
        $this->_redirectUrl($url);
        
        return $this;

    }
    
    public function htmlGen($_package,$request,$post,$track = NULL,$results)
	{
	/** html and pdf creation **/
		$formid = $results[0]->getFormid();
		$destcode = $results[0]->getDestinationCode();
		$returnshiptrack = $results[0]->getReturnBarcode();
		$shiptrack = $results[0]->getTrackBarcode();	
		$returnawb = $results[0]->getReturnAwb();
		$returnformid = $results[0]->getReturnFormid();
		$returnmethod = $results[0]->getReturnMethod();
		$returnamount = $results[0]->getReturnAmount();
	    $shipdate = $results[0]->getDateShipped();
    
	     /*** product package details ***/
		$item = $results[0]->getPackageItems();
		$packageitem  = json_decode($item);
	
	    $urlShipmentFolder = Mage::getBaseDir(). DS ."media". DS ."shipsync-shipment";
			
		
		if (!file_exists($urlShipmentFolder)) {		
			mkdir($urlShipmentFolder);
		}
		
		/******* Start order Bar code ******/
			// Only the text to draw is required
			$barcodeOptions = array('text' => $shiptrack,'barThickWidth' => 25,'barHeight' => 30, 'factor'=>1.80);
			 
			// No required options
			$rendererOptions = array();
			 
			// Draw the barcode in a new image,
			// send the headers and the image
			$orderbarcode = Zend_Barcode::factory(
				'code128', 'image', $barcodeOptions, $rendererOptions
			);
			
			$shipmentorderImage = $urlShipmentFolder. DS . $shiptrack.'_barcode.jpg';
		    //imagejpeg($nimg,$shipmentImage,100);
			
			 //temporary save file into data folder
			imagejpeg($orderbarcode->draw(), $shipmentorderImage,100);
			
		    //imagejpeg($orderbarcode);
		/******* End Bar Code *********/
		
		
		/******* Start AWB Bar code ******/
			// Only the text to draw is required
			$barcodeOptions = array('text' => $returnshiptrack,'barThickWidth' => 25,'barHeight' => 30, 'factor'=>1.80);
			 
			// No required options
			$rendererOptions = array();
			 
			// Draw the barcode in a new image,
			// send the headers and the image
			$awbbarcode = Zend_Barcode::factory(
				'code128', 'image', $barcodeOptions, $rendererOptions
			);
			
			$shipmentawbImage = $urlShipmentFolder. DS . $returnshiptrack.'_barcode.jpg';
		    //imagejpeg($nimg,$shipmentImage,100);
			
			 //temporary save file into data folder
			imagejpeg($awbbarcode->draw(), $shipmentawbImage,100);
			
		    //imagejpeg($orderbarcode);
		/******* End  awb Bar Code *********/
	


	/* $packageId = $this->getRequest()->getParam('id');
	$package = Mage::getModel('shipping/shipment_package')->load($packageId);
	$shipdate = $package->getDateShipped(); */
	$tracknumber = $track['number'];
	
	 $url1 = Mage::getBaseDir() . DS ."var" . DS ."shipsync";
	//exit;
	
	
    /**** folder creation ***/
	if (file_exists($url1)) {
		//echo "The file  exists";
	}
	else {
	   $mkdir =  mkdir($url1);
	 }
	
	$urlyear = $url1 . DS .date('Y');
		
	if (file_exists($urlyear)) {
		//echo "The file  exists";
	}
	else {
	   $mkdiryear =  mkdir($url1 . DS .date('Y'));
	}
	
	$urlmonth = $url1 . DS .date('Y') .DS . date('M');
		
	if (file_exists($urlmonth)) {
		//echo "The file  exists";
	}
	else {
	   $mkdirmonth =  mkdir($url1 . DS .date('Y').DS .date('M'));
	}
	
	$urldate = $url1 . DS .date('Y') .DS . date('M').DS. date('d');
		
	if (file_exists($urldate)) {
		//echo "The file  exists";
	}
	else {
	   $mkdirdate =  mkdir($url1 . DS .date('Y').DS .date('M').DS. date('d'));
	}
  
      
	$urltracknumber = $url1 . DS .date('Y') .DS . date('M').DS. date('d'). DS . $tracknumber;
	//echo $urltracknumber; 
		
	if (file_exists($urltracknumber)) {
		//echo "The file  exists";
	}
	else {
	   $mkdirtracknumber =  mkdir($url1 . DS .date('Y').DS .date('M').DS. date('d'). DS . $tracknumber);
	}

	 $urltracknumberhtml = $url1 . DS .date('Y') .DS . date('M').DS. date('d'). DS . $tracknumber . DS .$tracknumber.'.html' ;
	
	
	if (file_exists($urltracknumberhtml)) {
		//echo "The file  exists";
	}
	else {
	   //$mkdirtracknumber =  fopen($url1 . DS .date('Y').DS .date('M').DS. date('d'). DS . $tracknumber . DS .$tracknumber.'.html',"w")  or die("Unable to open file!");
	    $mkdirtracknumber =  fopen("$urltracknumberhtml","w")  or die("Unable to open file!");
	} 
	 
	$test = 'Witco';
	/*** Recipient details and address ***/
	$request->getOrderId();
	$request->getMethodCode();
	$requestmethod = $request->getMethodCode();

	$recipient = $request->getRecipientAddress();
	$rece_name = $recipient->getName();
	$street = $recipient->getStreet();
	$streetadd =  $street[0];
	$city = $recipient->getCity();
	$region_code = $recipient->getRegionCode();
	$postcode = $recipient->getPostcode();
	$country_id = $recipient->getCountryId();
	$telephone = $recipient->getTelephone();
	
	/**** Sender address ***/
	
	$origin = Mage::getStoreConfig('shipping'); 
	
	 $sender_street1  = $origin['origin']['street_line1'];
	 $sender_street2  = $origin['origin']['street_line2'];
	 $sender_street3  = $origin['origin']['street_line3'];
	 $sender_city = $origin['origin']['city'];
	 $sender_region_code = $origin['origin']['region_id'];
	 $sender_postcode = $origin['origin']['postcode'];
	 $sender_country_id = $origin['origin']['country_id'];
	 $sender_telephone =$origin['origin']['phone'];
	
	
    /**** package item details ***/
	
	
	
	/**** post Order details ***/
	
	 $orderid  = $post['order_id'];
		
	 $orderweight_units  = $post['weight_units'];
	 $width  = $post['packages'][0]['width'];
	 $height  = $post['packages'][0]['height'];
	 $length  = $post['packages'][0]['length'];
	 
	 /******* Order details ***/
	 //echo $orderid;
	 
	$order = Mage::getModel('sales/order')->loadByIncrementId($orderid);
	$orderdate =  $order->getUpdatedAt();
	$orderItems = $order->getAllItems();
	
	
	
	/* echo '<pre>';
	print_r($request);
	echo '</pre>'; */
	
	/* $productinfo = $request->getpackages();
	$info = $productinfo['items'];
	
	echo '<pre>';
	print_r($info);
	echo '</pre>'; 

exit;	  	
	 */
	
    /*foreach($is as $i):
		echo "<pre>"; print_r($i->getData()); echo "</pre>";
        echo $i->getProductId();
		echo "<br />";
		echo $i->getProductName();
		echo "<br />";
    endforeach;*/
	
	$grandtotal = $order->getGrandTotal();
	$shippingamount = $order->getShippingAmount();
	$shippingmethod = $order->getShippingMethod();
	$orders = Mage::getModel('sales/order')->getCollection();
		
		
	  
	 
	 /*** product package details ***/
	 
		/* echo '<pre>';
		print_r($post);
		echo '</pre>'; 
		exit; */
	 $weight =  $_package->getWeight();
	 
	 $itemcode =  $_package->getItems();
	 
	 /*echo '<pre>';
		print_r($itemcode);
		echo '</pre>';*/
		$packageProductArr = array();
		foreach($itemcode as $arrkey => $arrValue){
			$proId = $arrValue['product_id'];
			$packageProductArr[$proId][] = 1;
		}
	 
	 $itemcount = count($itemcode);
	 $itemid  =  $itemcode[0]['product_id'];
	 $itemname = $itemcode[0]['name'];
	 $itemqty = $itemcode[0]['qty'];
	 $itemprice = $itemcode[0]['value'];
	 
	
	

//echo $html;
	$html1 ='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Shipping</title>
<style type="text/css">
body { background-color:#c0c0c0; }
#shippingprint{ background: none repeat scroll 0 0 white; overflow:hidden;width:460px; height:auto; margin:0 auto; font-family:Verdana, Arial, Helvetica, sans-serif; color:#000000;}
#header{margin:5px 5px;}
.shippingtxt h3{font-size:12px;}
.shippingtxt h2{font-size:14px; font-weight:bolder;}
.shippingtxt{font-weight:bold; font-size:14px;}
.shippingtxt1{font-weight:bold; font-size:14px;}
.shippingtxt4{font-weight:bold; font-size:15px;}
.shippingtxt2{font-weight:bold; font-size:11px;}
.shippingtxt3{font-weight:bold; font-size:13px;}
.shippingborder td{ border-top:#000 1px solid;border-bottom:#000 1px solid;}
.shippingborder1 td{border-bottom:#000 1px solid;}
.shippingborderleft{border-left:#000 1px solid;}
.shiptable td{}
p{display:inline;}
.shippingtxt_awn{font-weight:bold; font-size:18px;}
.border td{ border-bottom:#000 1px solid !important; padding-bottom:5px; }
.desc .desc_txt {  font-size: 12px;
    font-weight: bold; }
.shipping_condtions { font-size:10px;text-align:justify; }
.shippingtxt10{font-weight:normal; font-size:15px;}
.shippingtxten{font-weight:normal; font-size:12px;}
.destnation {
	font-size: 24px;
    font-weight: bold;}
	
.order { padding-right:26px !important; }
.total { font-weight:normal !important;font-size: 12px !important;padding-left: 33px !important; }
.Service_Type_Desc { padding-left:33px;}
.Bill_sender { padding-left:33px;}
.return_barcode{border: 1px solid #000 !important; width: 408px;border-bottom:none !important;}
.return_barcode_img{border: 1px solid #000 !important;width:408px;border-top:none !important;padding-top:5px;}
.formidborder{border: 1px solid #000 !important;padding-top:5px;}
</style>
</head>

<body>
<div id="shippingprint" align="center"><div id="header">
<table  width="408" border="0" cellpadding="0" cellspacing="0" class="shiptable">
  <tr class="border">
    <td colspan="2" align="left"><img src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'logo/logo.gif" border="0"></td>
    <td align="center" colspan="4">&nbsp;</td>
  </tr>
 <!-- <tr class="border"> <td align="center" colspan="4">&nbsp;</td></tr>-->
  <tr>
    <td  align="left" class="AwbNumber">AWB:<span class="shippingtxt" >'.$tracknumber.'</span></td>
    <td colspan="2" align="left" class="Service_Type_Desc" valign="middle"><span class="shippingtxt" >'.$shipmethod.'</span></td>
   </tr>
   
   <tr> <td align="center" colspan="4">&nbsp;</td></tr>
   
    <tr>
    <td align="left" class="destnation"><span>'.$destcode.'</span><br><span class="shippingtxt10" >FORM ID : <b>'.$formid.'</b></span><br>
    <span class="shippingtxten" >Date Shipped: '.$shipdate.'</span></td>
   <td colspan="2" align="left" class="Bill_sender" valign="middle"><span class="shippingtxt10" >Bill T/C : Sender</span><br><span class="shippingtxt10" >Bill D/T : Sender</span><br><span class="shippingtxt10" >Weight :  '.$weight.$orderweight_units.'</span><br><span class="shippingtxt10" >Ship Through : '.$requestmethod.'</span></td>
   </tr>
   
  
   
   <tr> <td align="center" colspan="4">&nbsp;</td></tr>   
    <tr>
   <td colspan="4" align="center" valign="middle">
   <span class="shippingtxt_awn" >
   <img style="height: 62px;width: 400px; font-weight:bold;" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/shipsync-shipment/'.$shiptrack.'_barcode.jpg" >
   </span></td>
   </tr>
  <tr> <td align="center" colspan="4">&nbsp;</td></tr>
   <tr>
   <td colspan="4">
   <span class="shipping_condtions" >Subject to the Conditions of Carriage, which limits the liability of FedEx for any loss, delay or damage to the consignment. Visit <a href="http://www.fedex.com/in/">www.fedex.com/in</a> to view the conditions of carriage.</span></td>
   </tr>
   <tr class="border"> <td align="center" colspan="4">&nbsp;</td></tr>
   <tr align="left" valign="middle" >
    <td class="order"><span class="shippingtxt1" >Order#'. $orderid.'</span> </td>';
	$numpiece=0;
		foreach ($packageitem as $key=>$value)
		{
		$itemcode =  $value->i; 
		$itemqty =   $value->q;
		$orderitems = Mage::getModel('catalog/product')->load($itemcode); 
        if($orderitems->getSpecialPrice())
		{
		$specialprice = round($orderitems->getSpecialPrice());
		}
		else
		{
		$specialprice = round($orderitems->getPrice());
		}
		
		$itemtotal = $itemqty * $specialprice;
		
		$numpiece +=$itemqty;
		}
		 $itemshiptotal =  $itemshiptotal + $itemtotal;	
	$html1 .='<td class="total"><span>Total : Rs.'.$itemshiptotal.'</span></td>
	</tr>
   
</table>
<table  width="408" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="left" valign="top" class="shippingtxt1">To: </td>
  </tr>
  <tr class="shippingtxt4">
    <td width="197" align="left"><p>
	<span style="display:none;"> ,<br></span>
	<span >'.$rece_name.',</span><br>
	<span style="display:">'.$streetadd.',</span><br>
	<span style="display:">'.$city.',<br></span>
	<span style="display:">'.$region_code.' - '.$postcode.',</span><br>
	<span style="display:">'.$country_id .',</span><br>
	<span style="display:">Ph:'.$telephone .'.</span><br>	
</p>
</td>     
     <td width="211" colspan="2" align="left" style="vertical-align:top;"><p style="font-size:12px;">
          <span style="display:">DESCRIPTION :Bags <br></span>
          </p><br>
          <h3 style="font-size:15px;">';
		 
		 
		 
		if($_package->getCod()==1){
		$html1 .= '<h4>Cash to collect Rs.'.$_package->getCodAmount().'</h4>';
		}
		else{ 
		$html1 .= '<h4>Do Not Collect Amount</h4>';
		} 
		  
		  
		  
		 
		  
		  
		  $html1 .='</h3></td>
    
  </tr>
  <tr> <td colspan="3" align="center">&nbsp;</td></tr>
  
</table>

<table  width="408" border="0" cellpadding="0" cellspacing="0">
  <tr class="shippingborder shippingtxt3 " >
    <td > From: </td>
	 <td  align="center" valign="middle"> <h3>Witco</h3>
	'.$sender_street1.', 
	'.$sender_street2.', 
	'.$sender_street3.', 
	'.$sender_city.',
	'.$sender_region_code.',
	'.$sender_postcode.',
	'.$sender_country_id.',
	'.$sender_telephone .'</td>
</tr>
</table>

<table  width="408" border="0" cellpadding="0" cellspacing="0" class="desc">
  <tr class="desc_txt">
  		<td >(If not delivered please send it back to the above address)</td>
  </tr>
  <tr> <td colspan="3" align="center">&nbsp;</td></tr>';
  if($_package->getCod()==1){
  $html1 .='<tr > <td colspan="3" align=""><b>Return Track :</b></td></tr>
  <tr >
  	<td>
    	<table class="return_barcode">
          <tr>
            <td  align="left" class="AwbNumber">AWB: '.$returnawb.'<span class="shippingtxt" ></span></td>
            <td colspan="2" align="left" class="Service_Type_Desc" valign="middle"><span class="shippingtxt" >'.$returnmethod.'</span></td>
          </tr>
          <tr class="formidborder">
            <td colspan="2" align="left">FORM ID <span class="shippingtxt" >'.$returnformid.'</span></td>
            <td colspan="" align="left" class="Service_Type_Desc" valign="middle"><span class="shippingtxt" >'.$returnamount.'</span></td>
          </tr>
         </table>
        <table class="return_barcode_img">
          <tr>
           	<td  align="left" class="AwbNumber"><img style="height: 62px;width: 400px; font-weight:bold;" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/shipsync-shipment/'.$returnshiptrack.'_barcode.jpg" ></td>
           </tr>
           <tr><td>&nbsp;</td></tr>
         </table>
     </td>
   </tr>';
   }
   
  $html1 .= '<tr> <td colspan="3" align="center">&nbsp;</td></tr>
</table>
</div>
</div>
<script type="text/javascript">window.setTimeout("window.print();", 1000);</script>
</body>
</html>';	
	
	
	
	
	
	
	
	
   	
	//$txt = "DEEPA\n";
	fwrite($mkdirtracknumber, $html1);
	fclose($mkdirtracknumber);
	//exit;
	}
    
    /**
     * Show label
     */
    public function labelAction()
    {
	    
		 
        if ($packageId = $this->getRequest()->getParam('id')) {
            $package = Mage::getModel('shipping/shipment_package')->load($packageId);
            
            $carrier = strtoupper($package->getCarrier());
            
            $labelFormat = strtolower($package->getLabelFormat());
            $labelImage  = $package->getLabelImage();
            
            $this->labelPrint($labelImage, $labelFormat, $carrier, $package);
        }
		
		
    }
    
    
	    /**
     * Show customize label
     */
    public function customlabelAction($_package,$request,$post,$track = NULL)
    {
	    
	$this->customHtml();
		
    }
	
	
	public function customHtml()
	{
		$packageId = $this->getRequest()->getParam('id');
		$package = Mage::getModel('shipping/shipment_package')->load($packageId);
		
		/*** config ***/
		$configValue = Mage::getStoreConfig('carriers/fedex');
		$testmode =  $configValue['test_mode'];
		$testnumber = $configValue['test_meter'];
		$productionnumber = $configValue['meter_number'];
				
		/*** store name**/
		$storename = Mage::getStoreConfig('general/store_information/name');
				
		$cod = $package->getCod();
		$shipcarrier = $package->getCarrier();
		$shipmethod = $package->getShippingMethod();
		$formid = $package->getFormid();
		$destcode = $package->getDestinationCode();
		$returnshiptrack = $package->getReturnBarcode();
		$shiptrack = $package->getTrackBarcode();	
		$returnawb = $package->getReturnAwb();
		$returnformid = $package->getReturnFormid();
		$returnmethod = $package->getReturnMethod();
		$returnamount = $package->getReturnAmount();
		$retdes = $package->getReturnDestinationCode();
		
		
		
		
		/**** Barcode Creation Code - Start ***/
		 $url1 = Mage::getBaseDir() . DS ."var" . DS ."shipsync";
		if (!file_exists($url1)) {		
			mkdir($url1);
		}

		$urlyear = $url1 . DS .date('Y');
			
		if (!file_exists($urlyear)) {
			mkdir($url1 . DS .date('Y'));
		}

		$urlmonth = $url1 . DS .date('Y') .DS . date('M');
			
		if (!file_exists($urlmonth)) {
			mkdir($url1 . DS .date('Y').DS .date('M'));
		}

		$urldate = $url1 . DS .date('Y') .DS . date('M').DS. date('d');
			
		if (!file_exists($urldate)) {
			mkdir($url1 . DS .date('Y').DS .date('M').DS. date('d'));
		}
		
		$tracknumber = $package->getTrackingNumber();
		$urltracknumber = $url1 . DS .date('Y') .DS . date('M').DS. date('d'). DS . $tracknumber;
			
		if (!file_exists($urltracknumber)) {
			mkdir($url1 . DS .date('Y').DS .date('M').DS. date('d'). DS . $tracknumber);
		}		
		
		$image = $package->getLabelImage();
		$labelImage = mb_convert_encoding($image, 'UTF-8', 'BASE64');
		
		$urltracknumberpng = $url1 . DS .date('Y') .DS . date('M').DS. date('d'). DS . $tracknumber . DS .$tracknumber.'.png' ;
		$urltracknumberpngTHUMB = $url1 . DS .date('Y') .DS . date('M').DS. date('d'). DS . $tracknumber . DS .$tracknumber.'_thumb.png' ;
		if (!file_exists($urltracknumberpng)) {
			$mkdirtracknumberPNG = fopen($url1 . DS .date('Y').DS .date('M').DS. date('d'). DS . $tracknumber . DS .$tracknumber.'.png',"w")  or die("Unable to open file!");
		}else{
			$mkdirtracknumberPNG = fopen($urltracknumberpng,"w")  or die("Unable to open file!");
		}
		
		fwrite($mkdirtracknumberPNG, $labelImage);
		fclose($mkdirtracknumberPNG);	
		
		$ini_filename = $urltracknumberpng;
		$im = imagecreatefrompng($ini_filename );
		
		$imageSize = getimagesize($ini_filename);
 		$ini_x_size = $imageSize[0];
		$ini_y_size = $imageSize[1];

		$nw = 720;
		$nh = 260;
		$nimg = imagecreatetruecolor($nw,$nh);
		imagecopyresampled($nimg,$im,0,0,660,690,1400,950,$ini_x_size,$ini_y_size);
		imagejpeg($nimg,$urltracknumberpngTHUMB,100);
		
		
		
		$urlShipmentFolder = Mage::getBaseDir(). DS ."media". DS ."shipsync-shipment";
			
		
		if (!file_exists($urlShipmentFolder)) {		
			mkdir($urlShipmentFolder);
		}
		
		/* $shipmentImage = $urlShipmentFolder. DS . $tracknumber.'_barcode.png';
		imagejpeg($nimg,$shipmentImage,100); */
		
		/**** Barcode Creation Code - End ***/
		
		
		/***** Package Details ***/
		$custorderid = $package->getOrderIncrementId();
		$shipdate = $package->getDateShipped();
		$custtracknumber = $package->getTrackingNumber();
		$custweightunit = $package->getWeightUnits();
		$custweight = $package->getWeight();
		$custwidth = $package->getWidth();
		$custheight = $package->getHeight();
		$custlength = $package->getLength();
		$package->getDimensionUnits(); 
		
		
		 /******* Order details ***/
	
		$order = Mage::getModel('sales/order')->loadByIncrementId($custorderid);
		$orderdate = $order->getUpdatedAt();
		 
		
		/*** Recipient details and address ***/
	
		$customer = Mage::getModel('customer/customer')->load($order->getCustomerId()); 
		$recipient = $customer->getDefaultBillingAddress();
		$rece_name = $recipient->getFirstname() . ' '  .$recipient->getLastname() ;
		$street = $recipient->getStreet();
		$streetadd =  $street[0];
		$city = $recipient->getCity();
		$region_code = $recipient->getRegionCode();
		$postcode = $recipient->getPostcode();
		$country_id = $recipient->getCountryId();
		$telephone = $recipient->getTelephone(); 
	
		/**** Sender address ***/
		$origin = Mage::getStoreConfig('shipping'); 
		$sender_street1  = $origin['origin']['street_line1'];
		$sender_street2  = $origin['origin']['street_line2'];
		$sender_street3  = $origin['origin']['street_line3'];
		$sender_city = $origin['origin']['city'];
		$sender_region_code = $origin['origin']['region_id'];
		$sender_postcode = $origin['origin']['postcode'];
		$sender_country_id = $origin['origin']['country_id'];
		$sender_telephone =$origin['origin']['phone'];
		
	
    /**** package item details ***/
	
		$grandtotal = $order->getGrandTotal();
		$shippingamount = $order->getShippingAmount();
		$shippingmethod = $order->getShippingMethod();
		$orders = Mage::getModel('sales/order')->getCollection();
		
	 
		
		$shipdate = $package->getDateShipped(); 	  
	  
	 
		 /*** product package details ***/
		$item = $package->getPackageItems();
		$packageitem  = json_decode($item);
		
		
		
		
		
		
		
		
		
		/******* Start order Bar code ******/
			// Only the text to draw is required
			$barcodeOptions = array('text' => $shiptrack,'barThickWidth' => 25,'barHeight' => 30, 'factor'=>1.80, 'drawText'=>0);
			
						 
			// No required options
			$rendererOptions = array();
			 
			// Draw the barcode in a new image,
			// send the headers and the image
			$orderbarcode = Zend_Barcode::factory(
				'code128', 'image', $barcodeOptions, $rendererOptions
			);
			/* echo '<pre>';
			print_r($orderbarcode);
			exit; */
			
			$shipmentorderImage = $urlShipmentFolder. DS . $shiptrack.'_barcode.jpg';
		    //imagejpeg($nimg,$shipmentImage,100);
			
			 //temporary save file into data folder
			imagejpeg($orderbarcode->draw(), $shipmentorderImage,100);
			
		    //imagejpeg($orderbarcode);
		/******* End Bar Code *********/
		
		
		/******* Start AWB Bar code ******/
			// Only the text to draw is required
			$barcodeOptions = array('text' => $returnshiptrack,'barThickWidth' => 25,'barHeight' => 30, 'factor'=>1.80, 'drawText'=>0);
			 
			// No required options
			$rendererOptions = array();
			 
			// Draw the barcode in a new image,
			// send the headers and the image
			$awbbarcode = Zend_Barcode::factory(
				'code128', 'image', $barcodeOptions, $rendererOptions
			);
			
			$shipmentawbImage = $urlShipmentFolder. DS . $returnshiptrack.'_barcode.jpg';
		    //imagejpeg($nimg,$shipmentImage,100);
			
			 //temporary save file into data folder
			imagejpeg($awbbarcode->draw(), $shipmentawbImage,100);
			
		    //imagejpeg($orderbarcode);
		/******* End  awb Bar Code *********/
		
		
		
		
	

$html1 ='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Shipping</title>
<style type="text/css">
body { background-color:#c0c0c0; }
#shippingprint{ background: none repeat scroll 0 0 white; overflow:hidden; margin:0 auto; font-family:Verdana, Arial, Helvetica, sans-serif; color:#000000;}
#header{margin-left: 5px;
    margin-top: 5px;
    }
.shippingtxt h3{font-size:12px;  font-family: arial;}
.shippingtxt h2{font-size:14px; font-weight:bolder;  font-family: arial;}
.shippingtxt1{ font-size:12px;  font-family: arial;}
.shippingtxt4{ text-transform: capitalize;  font-family: arial; font-size: 12px;}
.shippingtxt2{font-weight:bold;  font-family: arial; font-size: 12px;}
.shippingtxt3{font-weight:bold;  font-family: arial;}
.shippingborder td{ border-top:#000 1px solid;border-bottom:#000 1px solid; font-size: 14px;}
.shippingborder1 td{border-bottom:#000 1px solid;}
.shippingborderleft{border-left:#000 1px solid;}
.shiptable{}
#shippingprint .shiptable .AwbNumber, #shippingprint .shiptable .shippingtxt{font-size:14px; font-weight:bold;  font-family: arial;}

p{display:inline;}
.shippingtxt_awn{}
.border td{ border-bottom:#000 1px solid !important; padding-bottom:5px; }
.desc .desc_txt {  font-size: 10px;  font-family: arial;
    font-weight: bold; }
.shipping_condtions { display: block;
    font-size: 12px;  font-family: arial;
    text-align: center;
    width: 350px; margin-top:3px;}

.destnation {
	font-size: 16px;  font-family: arial; 
    font-weight: bold; width:225px;}
	
.order { padding-right:26px !important; }
.total { font-weight:normal !important;padding-left: 20px !important; }
.Service_Type_Desc { padding-left:0px;}
.Bill_sender { padding-left:0px; padding-top:5px; padding-bottom:5px;}
.return_barcode{border: 1px solid #000 !important; width: 350px;border-bottom:none !important;}
.return_barcode_img{border: 1px solid #000 !important;width:350px;border-top:none !important;padding-top:5px;}
.formidborder{border: 1px solid #000 !important;padding-top:5px; font-size:11px; font-family: arial;}
.return_barcode .AwbNumber,.return_barcode.Service_Type_Desc{font-size:11px;  font-family: arial;}
.return_barcode .Service_Type_Desc{ text-align: right;  padding-right: 5px;}
.return_barcode .formidborder .Service_Type_Desc .shippingtxt{text-align: right; }
.meter{font-size:10px; width:122px;  font-family: arial;} 
.metershippingtxt{font-size:10px;  font-family: arial;}
.descript{font-weight:normal !important;}

.Service_Type_Desc,.AwbNumber{font-size:14px;  font-family: arial; font-weight:bold; text-align: center; text-transform:uppercase;}
.tg-031e{font-size:12px;  font-family: arial;} 
.tg-031a{ float: right;
    font-family: arial;
    font-size: 12px;
    padding-right: 4px;}
.receivename{font-weight: bold;  font-family: arial;}
</style>
</head>

<body>';
if($cod == 'COD'){
		$html1 .= '<div id="shippingprint" align="center" style="width:360px; height:690px;">';
		}
		else{ 
		$html1 .= '<div id="shippingprint" align="center" style="width:360px; height:520px;">';
		} 

$html1 .='
<div id="header">
<table border="0" cellpadding="0" cellspacing="0" class="shiptable">
  <tr class="border">
    <td colspan="1" align="left" width="233"><img src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'logo/logo.gif" border="0"></td>
	<td align="left" class="meter"><p style="float: left; margin: 0px; padding-top: 57px;">Fedex Meter :</p> <span class="metershippingtxt" >';
	if($testmode ==1){ 	$html1 .= '<h4 style="margin-top: 57px; margin-bottom: 0px;">'.$testnumber.'</h4>';}
	else{ $html1 .= '<h4 style="margin-top: 57px; margin-bottom: 0px;">$productionnumber</h4>'; } 
		  
   $html1 .='</span>  </td>
   
  </tr>
  
  <tr>
    <td class="tg-031e">
	    <span class="" >Order#'. $custorderid.'</span><br>
		<span class="shippingtxten" >Date Shipped: '.$shipdate.'</span><br>
		<span class="shippingtxt10" >Ship Through : <span style="text-transform: capitalize;"> FedEx '.$shipmethod.'<br></span></span></td>
    <td class="tg-031a"><span class="shippingtxt10" >Bill T/C : Sender</span><br><span class="shippingtxt10" >Bill D/T : Sender</span><br><span class="shippingtxt10" >Weight : '.number_format($custweight,2). $custweightunit.'</span></td>
  </tr>
 


 <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid;   padding: 2px 0;">
  <tr>
    <td  align="left" class="AwbNumber">TRK:<span class="shippingtxt" >'.$custtracknumber.'</span>
	<span class="shippingtxt10" >FORM ID : <b>'.$formid.'</b></span> / 
	<span>'.$destcode.'</span>
	</td>
	
   </tr>
   
    <tr>
   <td colspan="4" align="center" valign="middle">
   <span class="shippingtxt_awn" >
   <img style="font-weight: bold;
    height: 96px;
    width: 336px;" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/shipsync-shipment/'.$shiptrack.'_barcode.jpg" >
   </span></td>
   </tr>
   <tr> <td colspan="2" align="left" class="Service_Type_Desc" valign="middle"><span class="shippingtxt" >'.$shipmethod.'</span></td></tr>
   </table>
    <table  width="360" border="0" cellpadding="0" cellspacing="0">
  
   <tr>
   <td colspan="4">
   <span class="shipping_condtions" >Subject to the Conditions of Carriage, which limits the liability of FedEx for any loss, delay or damage to the consignment.<br> Visit <a href="http://www.fedex.com/in/">www.fedex.com/in</a> to view the conditions of carriage.</span></td>
   </tr>
   
   <tr align="left" valign="middle" >';
	$numpiece=0;
		foreach ($packageitem as $key=>$value)
		{
		$itemcode =  $value->i; 
		$itemqty =   $value->q;
		$orderitems = Mage::getModel('catalog/product')->load($itemcode); 
        if($orderitems->getSpecialPrice())
		{
		$specialprice = round($orderitems->getSpecialPrice());
		}
		else
		{
		$specialprice = round($orderitems->getPrice());
		}
		
		$itemtotal = $itemqty * $specialprice;
		
		$numpiece +=$itemqty;
		 $itemshiptotal =  $itemshiptotal + $itemtotal;	
		}
		
	$html1 .='<!--<td class="total"><span>Total : Rs.'.$itemshiptotal.'</span></td>-->
	</tr>
   
</table>
<table  style="border-top: 1px solid #000 !important;
    margin-top: 3px;" width="360" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="2" align="left" valign="top" class="shippingtxt1">To: </td>
  </tr>
  <tr class="shippingtxt4">
    <td width="190" align="left">
	
	<span class="receivename">'.$rece_name.',</span><br>
	<span>'.$streetadd.',</span><br>
	<span>'.$city.',<br></span>
	<span>'.$region_code.' - '.$postcode.',</span><br>
	<span>'.$country_id .',</span><br>
	<span>Ph:'.$telephone .'.</span><br>	

</td>     
     <td width="211" colspan="2" align="left" style="vertical-align:top;">
     <h4 style="font-weight:normal; text-align: center; padding: 0px;   font-size: 12px; margin: 0px;">Description</h4>
	 <h5 style="text-align: center; margin: 5px;  font-size: 12px; padding: 0px;">Cloths</h5>';
       
		if($cod == 'COD'){
		$invoicevalue = number_format($itemshiptotal,2);
		$html1 .= '<h4 style="font-weight:normal; text-align: center; padding: 0px;   font-size: 12px; margin: 0px;">Invoice Amount: Rs. '.str_replace("," ,"", $invoicevalue).'</h4>';
		$html1 .= '<h4 style="font-weight:bold; font-size: 16px; text-align: center; margin-bottom: 0px; margin-top:10px;">COD CASH <br> PLEASE COLLECT <br></h4>';
		$html1 .= '<h2 style="font-weight:bold; font-size: 16px;text-align: center; margin: 0px; padding: 0px;">INR : Rs.'.$itemshiptotal.'</h2>';
		}
		else{ 
		$invoicevalue = number_format($itemshiptotal,2);
		$html1 .= '<h4 style="font-weight:normal; text-align: center; padding: 0px;   font-size: 12px; margin: 0px; margin-top:10px;">Invoice Amount: Rs. '.str_replace("," ,"", $invoicevalue).'</h4>';
		$html1 .= '<h4 style="font-size: 16px; margin-top:2px;font-weight:bold;text-transform: uppercase; text-align: center;">PRE PAID</h4>';
		}
		
		
		 $html1 .='</td>
    
  </tr>
  
  
</table>

<table  style="margin-top:0px;" width="360" border="0" cellpadding="0" cellspacing="0">
  <tr class="shippingborder shippingtxt3 " >
   
	 <td  align="center" valign="middle"> <div style="margin: 0px;">'.$storename.'</div>
	'.$sender_street1.', 
	'.$sender_street2.', 
	'.$sender_street3.', 
	'.$sender_city.',
	'.$sender_region_code.',
	'.$sender_postcode.',
	'.$sender_country_id.',
	'.$sender_telephone .'</td>
</tr>
</table>

<table  width="360" border="0" cellpadding="0" cellspacing="0" class="desc">
  <tr class="desc_txt">
  		<td style="text-align:center" >(If not delivered, please return to the above address)</td>
  </tr>';
  if($cod == 'COD'){
  $html1 .='<tr > <td colspan="3" align=""><b style="font-size: 14px; font-weight: normal;">RETURN TRACK :</b></td></tr>
  </table>
  
   <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid;   padding: 2px 0;">
  <tr>
    <td  align="left" class="AwbNumber">TRK:<span class="shippingtxt" >'.$returnawb.'</span>
	<span class="shippingtxt10" >FORM ID : <b>'.$returnformid.'</b></span> / 
	<span>'.$retdes.'</span>
	</td>
	
   </tr>
   
    <tr>
   <td colspan="4" align="center" valign="middle">
   <span class="shippingtxt_awn" >
   <img style="font-weight: bold;
    height: 96px;
    width: 336px;" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/shipsync-shipment/'.$returnshiptrack.'_barcode.jpg" >
   </span></td>
   </tr>
   <tr> <td colspan="2" align="left" class="Service_Type_Desc" valign="middle"><span class="shippingtxt" >'.$returnmethod.'</span></td></tr>
   </table>';
   }
   
  $html1 .= '<tr> <td colspan="3" align="center">&nbsp;</td></tr>
</table>
</div>
</div>
<!--<script type="text/javascript">window.setTimeout("window.print();", 1000);</script>-->
</body>
</html>';	


echo $html1;
exit;
	
	}
    
	
    /**
     * Show COD Label if present
     */
    public function codlabelAction()
    {
        if ($packageId = $this->getRequest()->getParam('id')) {
            $package = Mage::getModel('shipping/shipment_package')->load($packageId);
            $carrier = strtoupper($package->getCarrier());
            
            $labelFormat = strtolower($package->getLabelFormat());
            $labelImage  = $package->getCodLabelImage();
            
            $this->labelPrint($labelImage, $labelFormat, $carrier . '_COD_', $package);
        }
    }
    
    
    /**
     * Show Return Label if present
     */
    public function returnLabelAction()
    {
        if ($packageId = $this->getRequest()->getParam('id')) {
            $package = Mage::getModel('shipping/shipment_package')->load($packageId);
            $carrier = strtoupper($package->getCarrier());

            $labelFormat = strtolower($package->getLabelFormat());
            $labelImage  = $package->getReturnLabelImage();

            $this->labelPrint($labelImage, $labelFormat, $carrier . '_RETURN_', $package);
        }
    }

    
    public function labelPrint($labelImage, $labelFormat, $carrier, $package)
    {
        switch ($labelFormat) {
            case 'pdf':
                
               $labelImage = mb_convert_encoding($labelImage, 'UTF-8', 'BASE64');
               $labelPath  = $carrier . $package->getTrackingNumber() . '.' . substr($labelFormat, 0, 3);
                //echo $labelPath; exit;
               
			   $print = $this->getResponse()->setHeader('Content-type', 'application/pdf', true)->setHeader('Content-Disposition', 'inline' . '; filename="' . $labelPath . '"')->setBody($labelImage);
			   
			   /* echo '<pre>';
			   print_r($this->getResponse());
			   echo '</pre>';
			   exit; */
                //echo 'test';
                break;
            
            case 'png':
                
                $labelImage = mb_convert_encoding($labelImage, 'UTF-8', 'BASE64');
                $labelPath  = $carrier . '_COD_' . $package->getTrackingNumber() . '.' . substr($labelFormat, 0, 3);
                $this->getResponse()->setHeader('Content-type', 'application/octet-stream', true)->setHeader('Content-Disposition', 'inline' . '; filename="' . $labelPath . '"')->setBody($labelImage);
                
                break;
            
            
            default:
                $this->thermalPrint($labelImage, $labelFormat);
                break;
        }
    }
    
    
    public function thermalPrint($labelImage, $labelFormat)
    {
        $jsUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'js/illapps/shipsync/jzebra.js';

        $javaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'java/jZebra/';
        
        $printerName = Mage::getStoreConfig('carriers/fedex/printer_name');
        
        $htmlBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <head>
				<script type="text/javascript">
					var label = "' . $labelImage . '";
				</script>
			 	<script type="text/javascript" src="' . $jsUrl . '"></script>
            </head>
            <body style="background-color: #ccc; padding: 20px;">
                <p><strong>ShipSync Thermal Printing</strong></p>
                    <div style="border: 1px #000 solid; padding: 10px; background-color: #fff; margin: 20px; ">
                        <applet name="jZebra" code="jzebra.PrintApplet.class" archive="' . $javaUrl . 'jzebra.jar" width="0" height="0">
                            <param name="sleep" value="200">
                            <param name="printer" value="' . $printerName . '">
                        </applet><br />
						<input type=button onClick="findPrinter()" value="Detect Printer"><br />
   						<input type=button onClick="useDefaultPrinter()" value="Use Default Printer"><br />
                        <input type=button onClick="print()" value="Print">
                    </div>
				</body>
        </html>';
        
        $this->getResponse()->setBody($htmlBody);
        
    }
    
    
    
    public function deleteAction()
    {
	    
        $console = Mage::helper('shipsync');

        $orderId = $this->getRequest()->getParam('order_id');

        $order = Mage::getModel('sales/order');

        $order->loadByIncrementId($orderId);

        $items = Mage::getResourceModel('sales/order_item_collection')->setOrderFilter($orderId)->load();

        foreach ($items as $item) {
            if ($item->getQtyShipped() > 0) {
                $item->setQtyShipped(0)->save();
            }
        }

        $shipments = Mage::getResourceModel('sales/order_shipment_collection')->setOrderFilter($orderId)->load();

        foreach ($shipments as $shipment) {

            $shipment->delete();
        }

        $shipments->save();

        $this->_getSession()->addSuccess($this->__('Shipment successfully deleted.'));

        $this->_redirect('adminhtml/sales_order/view', array(
            'order_id' => $orderId
			
        ));
    }
}
