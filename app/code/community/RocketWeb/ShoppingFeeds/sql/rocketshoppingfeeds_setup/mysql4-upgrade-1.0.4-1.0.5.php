<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @copyright  Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     RocketWeb
 */

/**
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @author     RocketWeb
 */

/**
 * @var $installer RocketWeb_ShoppingFeeds_Model_Resource_Eav_Mysql4_Setup
 */
$installer = $this;
$installer->startSetup();

// Fix rw_gfeed_feed constraint
$fkName = $installer->getConnection()->getForeignKeyName(
    $installer->getTable('rocketshoppingfeeds/feed'), 'store_id',
    $installer->getTable('core/store'), 'store_id'
);

$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed'), 'FK_FEED_STORE');
$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed'), $fkName);
//$installer->getConnection()->addForeignKey($fkName,
//    $installer->getTable('rocketshoppingfeeds/feed'), 'store_id',
//    $installer->getTable('core/store'), 'store_id',
//    Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE, Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE
//);


// Fix rw_gfeed_feed_config constraint
$fkName = $installer->getConnection()->getForeignKeyName(
    $installer->getTable('rocketshoppingfeeds/feed_config'), 'feed_id',
    $installer->getTable('rocketshoppingfeeds/feed'), 'id'
);

$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed_config'), 'FK_FEED_CONFIG_FEED');
$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed_config'), $fkName);
//$installer->getConnection()->addForeignKey($fkName,
//    $installer->getTable('rocketshoppingfeeds/feed_config'), 'feed_id',
//    $installer->getTable('rocketshoppingfeeds/feed'), 'id',
//    Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE, Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE
//);


// Fix rw_gfeed_feed_ftp constraint
$fkName = $installer->getConnection()->getForeignKeyName(
    $installer->getTable('rocketshoppingfeeds/feed_ftp'), 'feed_id',
    $installer->getTable('rocketshoppingfeeds/feed'), 'id'
);

$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed_ftp'), 'FK_RSF_FEED_FTP_FEED_ID_RSF_FEED_ID');
$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed_ftp'), $fkName);
//$installer->getConnection()->addForeignKey($fkName,
//    $installer->getTable('rocketshoppingfeeds/feed_ftp'), 'feed_id',
//    $installer->getTable('rocketshoppingfeeds/feed'), 'id',
//    Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE, Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE
//);


// Fix rw_gfeed_feed_schedule constraint
$fkName = $installer->getConnection()->getForeignKeyName(
    $installer->getTable('rocketshoppingfeeds/feed_schedule'), 'feed_id',
    $installer->getTable('rocketshoppingfeeds/feed'), 'id'
);

$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed_schedule'), 'FK_RW_GFEED_FEED_SCHEDULE_FEED_ID_RW_GFEED_FEED_ID');
$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/feed_schedule'), $fkName);
//$installer->getConnection()->addForeignKey($fkName,
//    $installer->getTable('rocketshoppingfeeds/feed_schedule'), 'feed_id',
//    $installer->getTable('rocketshoppingfeeds/feed'), 'id',
//    Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE, Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE
//);


// Fix rw_gfeed_feed_schedule constraint
$fkName = $installer->getConnection()->getForeignKeyName(
    $installer->getTable('rocketshoppingfeeds/queue'), 'feed_id',
    $installer->getTable('rocketshoppingfeeds/feed'), 'id'
);

$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/queue'), 'FK_FEED_QUEUE_FEED');
$installer->getConnection()->dropForeignKey($installer->getTable('rocketshoppingfeeds/queue'), $fkName);
//$installer->getConnection()->addForeignKey($fkName,
//    $installer->getTable('rocketshoppingfeeds/queue'), 'feed_id',
//    $installer->getTable('rocketshoppingfeeds/feed'), 'id',
//    Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE, Varien_Db_Adapter_Pdo_Mysql::FK_ACTION_CASCADE
//);

$installer->endSetup();