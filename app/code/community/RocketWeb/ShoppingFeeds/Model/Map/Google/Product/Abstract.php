<?php

/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  RocketWeb
 * @package   RocketWeb_ShoppingFeeds
 * @copyright Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author    RocketWeb
 */

/**
 * @method Mage_Catalog_Model_Product getProduct() Current product or null
 */
class RocketWeb_ShoppingFeeds_Model_Map_Google_Product_Abstract
    extends RocketWeb_ShoppingFeeds_Model_Map_Abstract
{
    /**
     * @param array $params
     * @return string
     */
    public function mapDirectivePriceBuckets($params = array())
    {
        $values = array();
        $buckets = $this->getAdapter()->getFeed()->getConfig('filters_adwords_price_buckets');

        if ($buckets) {
            $prices = $this->getAdapter()->getPrices();
            $price = $this->getAdapter()->hasSpecialPrice() ? $prices['sp_excl_tax'] : $prices['p_excl_tax'];
            foreach ($buckets as $bucket) {
                if (floatval($bucket['price_from']) <= floatval($price) && floatval($price) < floatval($bucket['price_to'])) {
                    array_push($values, $bucket['bucket_name']);
                }
            }
        }

        $cell = implode(',', $values);
        $this->getAdapter()->findAndReplace($cell, $params['map']['column']);
        return $cell;
    }

    /**
     * @param array $params
     * @return string
     */
    public function mapDirectiveIdentifier($params = array())
    {
        $val = '';
        $product = $this->getAdapter()->getProduct();
        $attr_code = $params['map']['param'];

        if (!empty($attr_code) && $product->hasData($attr_code)) {
            $attribute = $this->getAdapter()->getGenerator()->getAttribute($attr_code);
            $val = $this->getAdapter()->cleanField($this->getAdapter()->getAttributeValue($product, $attribute), $params);
        }

        return $val;
    }

    /**
     * If $params['map']['param'] is not specified, or is default ('brand', 'gtin', 'mpn')
     * gtin and mpn would exclude each other, so only brand and one of gtin or mpn are required
     *
     * @param array $params
     * @return string
     */
    public function mapDirectiveIdentifierExists($params = array())
    {
        $identifiers = array_key_exists('param', $params['map']) ? array_filter(explode(',', $params['map']['param'])) : array();
        $identifiersToLoad = !empty($identifiers) ? $identifiers : array('brand', 'gtin', 'mpn');

        $cacheMapValues = $this->getAdapter()->getCacheMapValues();
        foreach ($identifiersToLoad as $column) {
            if (!array_key_exists($column, $cacheMapValues) && array_key_exists($column, $this->getAdapter()->getColumnsMap())) {
                $this->getAdapter()->mapColumn($column);
            }
        }
        $cacheMapValues = $this->getAdapter()->getCacheMapValues();

        // Default params, or empty: special case for Google spec - gtin and mpn exclude each other
        if (empty($identifiers) || $identifiers == array('brand', 'gtin', 'mpn')) {
            $identifiers = array('brand');
            // if gtin is missing, we'll check require mpn instead
            if (!array_key_exists('gtin', $cacheMapValues)
                || (array_key_exists('gtin', $cacheMapValues) && empty($cacheMapValues['gtin'])))
            {
                array_push($identifiers, 'mpn');
            }
        }

        $score = 0;
        foreach ($identifiers as $column) {
            if (array_key_exists($column, $cacheMapValues) && !empty($cacheMapValues[$column])) {
                $score++;
            }
        }

        return ($score == count($identifiers)) ? "" : "FALSE";
    }

    public function mapDirectivePromotionIds($params = array())
    {
        $promotionModel = Mage::getSingleton('rocketshoppingfeeds/provider_google_promotions');
        $promotionModel->setFeed($this->getAdapter()->getFeed());
        $promotionIds = $promotionModel->getPromotionIds($this->getAdapter()->getProduct());

        return implode(',', $promotionIds);
    }
}