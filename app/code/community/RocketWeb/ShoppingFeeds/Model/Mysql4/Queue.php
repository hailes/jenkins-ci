<?php

/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @copyright  Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     RocketWeb
 */

class RocketWeb_ShoppingFeeds_Model_Mysql4_Queue extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct() {
        $this->_init('rocketshoppingfeeds/queue', 'id');
    }

    /**
     * Clean leftover records what used to be key constraints
     * @return Zend_Db_Statement_Interface
     */
    public function removeDeadRows()
    {
        return $this->_getWriteAdapter()->query('DELETE q FROM '. $this->getMainTable(). ' q
            LEFT JOIN '. $this->getTable('rocketshoppingfeeds/feed'). ' f ON f.id = q.feed_id
            WHERE f.id IS NULL');
    }
}