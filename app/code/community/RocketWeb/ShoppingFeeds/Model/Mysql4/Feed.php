<?php

/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @copyright  Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     RocketWeb
 */

class RocketWeb_ShoppingFeeds_Model_Mysql4_Feed extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct() {
        $this->_init('rocketshoppingfeeds/feed', 'id');
    }

    /**
     * Operate what used to be foreign key constraint.
     * Constraints have been removed because they where failing on some mysql servers.
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Core_Model_Resource_Db_Abstract
     */
    protected function _afterDelete(Mage_Core_Model_Abstract $object)
    {
        $cond = $this->_getWriteAdapter()->quoteInto('feed_id=?', $object->getId());

        $this->_getWriteAdapter()->delete($this->getTable('rocketshoppingfeeds/queue'), $cond);
        $this->_getWriteAdapter()->delete($this->getTable('rocketshoppingfeeds/feed_schedule'), $cond);
        $this->_getWriteAdapter()->delete($this->getTable('rocketshoppingfeeds/feed_config'), $cond);
        $this->_getWriteAdapter()->delete($this->getTable('rocketshoppingfeeds/process'), $cond);
        $this->_getWriteAdapter()->delete($this->getTable('rocketshoppingfeeds/feed_ftp'), $cond);
        $this->_getWriteAdapter()->delete($this->getTable('rocketshoppingfeeds/shipping'), $cond);
        return parent::_afterDelete($object);
    }
}