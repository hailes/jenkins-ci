<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @copyright  Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     RocketWeb
 */

/**
 * Extended class of salesrule/rule_collection to implement multiple
 * customer groups filter
 *
 * Class RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions_Collection
 */
class RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions_Collection
    extends Mage_SalesRule_Model_Resource_Rule_Collection
    // extends Mage_SalesRule_Model_Mysql4_Rule_Collection // earlier magento versions
{
    /**
     * Filter collection by customer group(s) and date.
     * Filter collection to only active rules.
     * Sorting is not involved
     *
     * @param array $customerGroupIds
     * @param string|null $now
     * @use $this->addWebsiteFilter()
     *
     * @return Mage_SalesRule_Model_Mysql4_Rule_Collection
     */
    public function addGroupDateFilter($customerGroupIds, $now = null)
    {
        if (!$this->getFlag('website_group_date_filter')) {
            if (is_null($now)) {
                $now = Mage::getModel('core/date')->date('Y-m-d');
            }

            $entityInfo = $this->_getAssociatedEntityInfo('customer_group');
            $connection = $this->getConnection();
            $customerGroupList = array();
            foreach ($customerGroupIds as $groupId) {
                $customerGroupList[] = $connection->quoteInto(
                    'customer_group_ids.' . $entityInfo['entity_id_field'] . ' = ?',
                    $groupId
                );
            }
            $this->getSelect()
                ->joinInner(
                    array('customer_group_ids' => $this->getTable($entityInfo['associations_table'])),
                        'main_table.' . $entityInfo['rule_id_field']
                        . ' = customer_group_ids.' . $entityInfo['rule_id_field']
                        . ' AND ' . implode(' OR ', $customerGroupList)
                    ,
                    array()
                )
                ->group('rule_id')
                ->where('from_date is null or from_date <= ?', $now)
                ->where('to_date is null or to_date >= ?', $now);

            $this->addIsActiveFilter();

            $this->setFlag('website_group_date_filter', true);
        }

        return $this;
    }

    public function addSpecificProductsFilter()
    {
        if (!$this->getFlag('specific_products_filter')) {
            $searchString = '%s:32:"salesrule/rule_condition_product";s:9:"attribute";s:%';
            $this->addFieldToFilter(
                array('conditions_serialized', 'actions_serialized'),
                array('like' => $searchString)
            );
            $this->setFlag('specific_products_filter', true);
        }
        return $this;
    }
}