<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @copyright  Copyright (c) 2016 RocketWeb (http://rocketweb.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     RocketWeb
 */

/**
 * Mapping class for Google Promotions feed
 *
 * Class RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions_Map
 */
class RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions_Map
{
    const APPLICABILITY_ALL         = 'ALL_PRODUCTS';
    const APPLICABILITY_SPECIFIC    = 'SPECIFIC_PRODUCTS';
    const GENERIC_CODE              = 'GENERIC_CODE';
    const NO_CODE                   = 'NO_CODE';

    /** @var RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions */
    protected $_promotionModel = null;

    /**
     * @param $promotionModel
     * @return $this
     */
    public function setPromotionModel(RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions  $promotionModel)
    {
        $this->_promotionModel = $promotionModel;
        return $this;
    }

    /**
     * Creates promotion_id
     *
     * @param $counter
     * @param $rule
     * @return string
     */
    public function mapPromotionId($counter, $rule)
    {
        return sprintf('PROMO_%s_%s', $counter, $rule->getId());
    }

    /**
     * Set promotion applicability (all products / specific products)
     *
     * @param Mage_SalesRule_Model_Rule $rule
     * @return string
     */
    public function mapProductApplicability(Mage_SalesRule_Model_Rule $rule)
    {
        $resource = $rule->getResource();
        // TODO: we also need condition here?
        $actionsAttributes = $resource->getProductAttributes($rule->getActionsSerialized());
        $conditionsAttributes = $resource->getProductAttributes($rule->getConditionsSerialized());
        if (count($actionsAttributes) > 0 || count($conditionsAttributes) > 0) {
            return self::APPLICABILITY_SPECIFIC;
        }
        return self::APPLICABILITY_ALL;
    }

    /**
     * Prepare effective dates
     *
     * @param array $row
     * @return string
     */
    public function mapEffectiveDates($row = array())
    {
        return $this->_mapDates($row['date']);
    }

    /**
     * Prepare display dates
     *
     * @param array $row
     * @return string
     */
    public function mapDisplayDates($row = array())
    {
        return $this->_mapDates($row['display']);
    }

    /**
     * Helper method for dates
     *
     * @param array $row
     * @return string
     */
    public function _mapDates($row = array())
    {
        $fromDate = $this->_promotionModel->prepareDate('', $row['from'], false, Zend_Date::ISO_8601);
        $toDate = $this->_promotionModel->prepareDate('', $row['to'], false, Zend_Date::ISO_8601);
        return $fromDate . "/" . $toDate;
    }

    /**
     * Set promotion type (coupon code / no code)
     *
     * @param Mage_SalesRule_Model_Rule $rule
     * @return string
     */
    public function mapOfferType(Mage_SalesRule_Model_Rule $rule)
    {
        return $rule->getCouponType() == 1 ? self::NO_CODE : self::GENERIC_CODE;
    }

    /**
     * Prepare if minimum purchase amount exists
     *
     * @param Mage_SalesRule_Model_Rule $rule
     * @return string
     */
    public function mapMinimumPurchaseAmount(Mage_SalesRule_Model_Rule $rule)
    {
        $conditions = unserialize($rule->getConditionsSerialized());
        $minimumPurchaseAmount = 0;
        if (isset($conditions['conditions']) && is_array($conditions['conditions']) && count($conditions['conditions']) > 0) {
            foreach ($conditions['conditions'] as $condition) {
                if (
                    in_array($condition['attribute'], array('base_subtotal'))
                    && in_array($condition['operator'], array('>', '>=', '=='))
                    && is_numeric($condition['value'])
                    && $condition['value'] > 0
                    && $condition['value'] > $minimumPurchaseAmount
                ) {
                    $minimumPurchaseAmount = $condition['value'];
                }
            }
        }

        $currency = $this->_promotionModel->getFeed()->getStore()->getData('current_currency')->getCode();
        return $minimumPurchaseAmount > 0 ? sprintf("%.2F", $minimumPurchaseAmount) . ' ' . $currency : '';
    }

    /**
     * Returns coupon code if exists
     *
     * @param Mage_SalesRule_Model_Rule $rule
     * @return string
     */
    public function mapGenericRedemptionCode(Mage_SalesRule_Model_Rule $rule)
    {
        if ($rule->getCouponType() == 2) {
            return $rule->getCouponCode();
        }
        return '';
    }
}