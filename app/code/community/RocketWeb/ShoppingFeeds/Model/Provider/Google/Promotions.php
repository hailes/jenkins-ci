<?php
/**
 * RocketWeb
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   RocketWeb
 * @package    RocketWeb_ShoppingFeeds
 * @copyright  Copyright (c) 2015 RocketWeb (http://rocketweb.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     RocketWeb
 */

/**
 * Main model class regarding Google Promotions
 *
 * Class RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions
 */
class RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions extends Varien_Object
{
    const MAX_PROMOTION_DAYS = 183;

    /**
     * Generates all promotion ids for given product
     * from all cart rules (enabled in feed configuration)
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getPromotionIds(Mage_Catalog_Model_Product $product)
    {
        $productId = $product->getId();
        $promotions = $this->getFeed()->getConfig('google_promotions');
        $promotionIds = array();

        /**
         * If promotion is not correctly structured, we don't go any further
         * If hash is not set, the promotions weren't generated
         * If mode is not set & true, promotions are disabled
         */
        if (
            is_array($promotions)
            && isset($promotions['mode'])
            && $promotions['mode']
            && isset($promotions['hash'])
            && isset($promotions['promotion'])
            && count($promotions['promotion']) > 0
        ) {
            $promotionIds = $this->_getPromotionCache($productId, $promotions['hash']);
            if ($promotionIds === false) {
                $promotionIds = array();
                $mapModel = Mage::getSingleton('rocketshoppingfeeds/provider_google_promotions_map');
                $mapModel->setPromotionModel($this);

                $cartRulesCollection = $this->getMagentoCartRules();
                //$cartRulesCollection->addSpecificProductsFilter();

                /** @var Mage_SalesRule_Model_Rule $rule */
                foreach ($cartRulesCollection as $rule) {

                    // Only include enabled promotions in the feed
                    if (!in_array($rule->getData('rule_id'), array_keys($promotions['promotion']))) {
                        continue;
                    }

                    $ruleActions = $rule->getActions();

                    /**
                     * Because the rule->validate works on different types (quote/quote_item/...)
                     * this little manipulation is needed
                     */
                    $quoteItem = Mage::getModel('sales/quote_item')->setProduct($product);
                    $object = Mage::getModel('sales/quote')->addItem($quoteItem)->setProduct($product);
                    $object->setQuote($object); // Ugly manipulation so it work on address & product/found

                    // Conditions may or may not validate depending on what user adds to the cart, 
                    // so google should only care about where the product will receive the discount, meaning actions validate
                    if (is_null($ruleActions) || (!is_null($ruleActions) && $ruleActions->validate($object))) {
                        $promotionIds[] = $mapModel->mapPromotionId($promotions['counter'], $rule);
                    }
                }

                // Save to cache
                $this->_setPromotionCache($productId, $promotions['hash'], $promotionIds);
            }
        }

        return $promotionIds;
    }

    /**
     * Returns magento cart rules collection with filters:
     * - in feed website
     * - given customer groups
     * - without coupon generator
     * - is active
     *
     * @return RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions_Collection
     */
    public function getMagentoCartRules()
    {
        if (!$this->hasData('magento_collection')) {
            $store = $this->getFeed()->getStore();
            $websiteId = $store->getWebsiteId();
            $promotions = $this->getFeed()->getConfig('google_promotions');
            $groupIds = isset($promotions['groups']) ? $promotions['groups'] : Mage_Customer_Model_Group::NOT_LOGGED_IN_ID;

            if (!is_array($groupIds)) {
                $groupIds = array($groupIds);
            }
            /** @var Mage_SalesRule_Model_Resource_Rule_Collection $rulesCollection */
            $rulesCollection = Mage::getModel('rocketshoppingfeeds/provider_google_promotions_collection');
            if (method_exists($rulesCollection, 'addAllowedSalesRulesFilter')) {
                $rulesCollection->addAllowedSalesRulesFilter();
            }
            $conn = $rulesCollection->getConnection();
            if ($conn->tableColumnExists($rulesCollection->getResource()->getTable('salesrule/rule'), 'website_ids')) {
                $rulesCollection->addWebsiteFilter($websiteId);
            }

            if (method_exists($rulesCollection, 'addIsActiveFilter')) {
                $rulesCollection->addIsActiveFilter();
            }
            //->addGroupDateFilter($groupIds)

            $this->setData('magento_collection', $rulesCollection);
        }
        return $this->getData('magento_collection');
    }

    /**
     * Compare magento collection & saved data collection - if saved row doesn't exists
     * in magento collection, it should be removed!
     *
     * @return $this
     * @throws Exception
     */
    public function validateGooglePromotions()
    {
        $promotions = $this->getFeed()->getConfig('google_promotions');
        $cartRulesIds = $this->getMagentoCartRules()->getAllIds();

        if (isset($promotions['promotion'])) {
            $promotionIds = array_keys($promotions['promotion']);
            $removeIds = array_diff($promotionIds, $cartRulesIds);
            if (count($removeIds) > 0) {
                foreach ($removeIds as $removeId) {
                    unset($promotions['promotion'][$removeId]);
                }
                $this->updateGooglePromotions($promotions);
            }
        }
        return $this;
    }

    /**
     * Updates google promotions configuration
     *
     * @param array $promotions
     * @return $this
     * @throws Exception
     */
    public function updateGooglePromotions($promotions = array())
    {
        $lookup = Mage::getModel('rocketshoppingfeeds/config')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('feed_id', $this->getFeed()->getId())
            ->addFieldToFilter('path', 'google_promotions')
            ->load();
        $model = count($lookup) == 0 ? Mage::getModel('rocketshoppingfeeds/config') : $lookup->getFirstItem();

        $model->addData(array(
            'feed_id' => $this->getFeed()->getId(),
            'path' => 'google_promotions',
            'value' => $promotions
        ));
        $model->save();
        $this->getFeed()->unsetData('config'); // Force a reload

        return $this;
    }

    /**
     * Before configuration saves, this transforms given date into timestamp
     *
     * @param array $rows
     * @return array
     */
    public function beforeSave($rows = array())
    {
        if (isset($rows['promotion'])) {
            $promotion = $rows['promotion'];

            foreach ($promotion as $key => $row) {
                if (is_array($row)) {
                    if (!isset($row['include']) || !$row['include']) {
                        unset($promotion[$key]);
                        continue;
                    }

                    if (isset($row['date'])) {
                        $promotion[$key]['date']['from'] = $this->prepareTimestamp($promotion[$key]['date']['from']);
                        $promotion[$key]['date']['to'] = $this->prepareTimestamp($promotion[$key]['date']['to']);
                    }
                    if (isset($row['display'])) {
                        $promotion[$key]['display']['from'] = $this->prepareTimestamp($promotion[$key]['display']['from']);
                        $promotion[$key]['display']['to'] = $this->prepareTimestamp($promotion[$key]['display']['to']);
                    }
                }
            }
            $rows['promotion'] = $promotion;
        }
        return $rows;
    }

    /**
     * Prepare date for output
     *
     * @param $promotionDate
     * @param $rowDate
     * @param bool $addDays
     * @return string
     * @throws Zend_Date_Exception
     */
    public function prepareDate($promotionDate, $rowDate, $addDaysFrom = false, $format = false)
    {
        $timezone = $this->getFeed()->getStore()->getConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE);

        $add = true;
        $date = new Zend_Date(null, null, $this->getStoreLocale());

        if (!empty($rowDate)) {
            $date = new Zend_Date($rowDate, null, $this->getStoreLocale());
            $add = false;
        } else if (!empty($promotionDate)) {
            $date = new Zend_Date($promotionDate, $this->getDateFormat(), $this->getStoreLocale());
            $add = false;
        }
        if (!$date) {
            return '';
        }
        $date->setTime('12:00:00');

        if ($addDaysFrom !== false && $add === true) {
            $date = new Zend_Date($addDaysFrom, $this->getDateFormat(), $this->getStoreLocale());
            $date->add(self::MAX_PROMOTION_DAYS, Zend_Date::DAY);
        }

        if ($timezone) {
            $date->setTimezone($timezone);
        }

        if ($format === false) {
            $format = $this->getDateFormat();
        }

        return $date->toString($format);
    }

    /**
     * Preapre timestamp for saving from date string
     *
     * @param $dateString
     * @return int|string
     */
    public function prepareTimestamp($dateString)
    {
        if (is_numeric($dateString)) {
            // We already have a timestamp
            return $dateString;
        }
        $format = $this->getDateFormat();
        $date = new Zend_Date(null, null, $this->getStoreLocale());
        $date->setDate($dateString, $format);
        return $date->getTimestamp();
    }

    /**
     * Returns locale
     *
     * @return string
     */
    public function getStoreLocale()
    {
        //Mage::getStoreConfig('general/locale/code', $this->getFeed()->getStoreId()); // Not working on save
        return Mage::app()->getLocale()->getDefaultLocale();
    }

    /**
     * Returns date format for consistency over the code
     *
     * @return string
     */
    public function getDateFormat()
    {
        return Zend_Date::DAY. '/'. Zend_Date::MONTH. '/'. Zend_Date::YEAR;
    }

    //***************************************
    // CACHING FOR mapDirectivePromotionIds()
    //***************************************

    /**
     * Removes cache file
     * Not used currently (it should be in the observer)
     *
     * @return $this
     */
    public function clearPromotionCache()
    {
        $cacheFile = $this->_getHashFile();
        @unlink($cacheFile);
        return $this;
    }

    /**
     * Getter method to handle cache
     *
     * @param int $productId
     * @param string $hash
     * @return bool
     */
    protected function _getPromotionCache($productId = 0, $hash = '')
    {
        $cache = $this->_getHashCache($hash);
        if (isset($cache[$productId]) && is_array($cache[$productId])) {
            return $cache[$productId];
        }
        return false;
    }

    /**
     * Setter method which also writes to file cache
     * This is used so we have a constant cache over all the tests & shopping feed generations
     *
     * @param int $productId
     * @param string $hash
     * @param array $promotionIds
     * @return $this
     */
    protected function _setPromotionCache($productId = 0, $hash = '', $promotionIds = array())
    {
        $cache = $this->_getHashCache($hash);
        $cache[$productId] = $promotionIds;

        // Update current cache
        $this->setData($hash, $cache);

        //Update file cache
        $cacheFile = $this->_getHashFile();
        $fileContent = array(
            'cache' => $cache,
            'hash' => $hash
        );
        @file_put_contents($cacheFile, Mage::helper('core')->jsonEncode($fileContent));

        return $this;
    }

    /**
     * Gets cache from file if its not available inside the Varien_Object
     *
     * @param string $hash
     * @return array
     */
    protected function _getHashCache($hash = '')
    {
        if (!$this->hasData($hash)) {
            $cacheFile = $this->_getHashFile();
            $cache = array();
            if (file_exists($cacheFile) && is_readable($cacheFile)) {
                $fileContent = file_get_contents($cacheFile);
                $tmpCache = Mage::helper('core')->jsonDecode($fileContent);
                if (is_array($tmpCache) && isset($tmpCache['hash']) && $tmpCache['hash'] == $hash && isset($tmpCache['cache'])) {
                    $cache = $tmpCache['cache'];
                }
            }
            $this->setData($hash, $cache);
        }
        return $this->getData($hash);
    }

    /**
     * Returns promotion cache file location
     *
     * @return string
     */
    protected function _getHashFile()
    {
        return rtrim(Mage::getBaseDir('var'), DS) . DS . 'cache' . DS . 'promotions.cache';
    }


    // Other protected methods
    /**
     * Returns registry feed if no feed is set
     * This allows to set the feed object in other
     * case then just adminhtml edit page
     *
     * @return RocketWeb_ShoppingFeeds_Model_Feed|null
     */
    protected function getFeed()
    {
        if (!$this->hasData('feed')) {
            $this->setData('feed', Mage::registry('rocketshoppingfeeds_feed'));
        }
        return $this->getData('feed');
    }
}