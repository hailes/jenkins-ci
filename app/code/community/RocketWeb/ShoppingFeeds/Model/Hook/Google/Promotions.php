<?php


class RocketWeb_ShoppingFeeds_Model_Hook_Google_Promotions
{
    /** @var RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions */
    protected $_promotionModel = null;

    protected $_changed = false;

    /**
     * Creates a promotion feed if needed
     *
     * @param RocketWeb_ShoppingFeeds_Model_Feed $feed
     */
    public function preHook($args)
    {
        if (!array_key_exists('feed', $args) || !is_a($args['feed'], 'RocketWeb_ShoppingFeeds_Model_Feed')) {
            throw new Exception('preHook feed argument missing or wrong type');
        }
        $feed = $args['feed'];

        $promotions = $feed->getConfig('google_promotions');
        if (!isset($promotions['mode']) || !$promotions['mode']) {
            return 'Promotions disabled!';
        }

        $promotionModel = Mage::getModel('rocketshoppingfeeds/provider_google_promotions');
        $promotionModel->setFeed($feed);
        $this->_promotionModel = $promotionModel;

        // Check if we need to regenerate the promotions feed
        $this->_promotionModel->validateGooglePromotions();


        $promotion = $promotions['promotion'];
        $hashString = $promotions['counter'] . Mage::helper('core')->jsonEncode($promotion);
        $hash = md5($hashString);

        $promotionFile = $this->_getPromotionFile($feed);

        if (!isset($promotions['hash']) || $promotions['hash'] != $hash || !file_exists($promotionFile)) {
            $promotionModel->clearPromotionCache();
            /** @var RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions_Map $map */
            $map = Mage::getSingleton('rocketshoppingfeeds/provider_google_promotions_map');
            $map->setPromotionModel($this->_promotionModel);
            // We need to regenerate the promotions feed
            $fileLines = array();
            $fileLines[] = $this->createFeedHeader();
            foreach ($promotion as $key => $row) {
                /** @var Mage_SalesRule_Model_Rule $rule */
                $rule = Mage::getModel('salesrule/rule')->load($key);
                $fileLines[] = $this->createFeedLine($promotions['counter'], $rule, $row, $map);
            }

            $fileOutput = '';
            foreach ($fileLines as $line) {
                $fileOutput .= implode("\t", $line) . "\n";
            }

            if (file_exists($promotionFile) && !is_writable($promotionFile)) {
                Mage::throwException(sprintf('Not enough permissions to write to file %s.', $promotionFile));
            }
            file_put_contents($promotionFile, $fileOutput);
            if (!file_exists($promotionFile)) {
                Mage::throwException(sprintf('Not enough permissions to write to file %s.', $promotionFile));
            }

            // Save the new hash so we don't regenerate the feed if no changes were made
            $promotions['hash'] = $hash;
            $this->_promotionModel->updateGooglePromotions($promotions);

            $this->_changed = true;
            return sprintf('Recreated Promotions feed - added %s lines', count($fileLines));
        }

        return 'No changes in Promotions feed found';
    }

    /**
     * Runs after SFTP upload completes for the main feed (if FTP is active)
     *
     * @param array $args
     * @return array|bool true or array of FTP errors
     */
    public function ftpHook($args) {
        $feed = $args['feed'];
        $ftpAccounts = $args['accounts'];

        $errors = array();

        foreach ($ftpAccounts as $account) {
            $result = Mage::helper('rocketshoppingfeeds')->ftpUpload($account, true, $this->_getPromotionFile($feed));

            if ($result !== true) {
                $errors[] = $result;
            }
        }
        return $errors;
    }

    protected function _getPromotionFile($feed) {
        return rtrim(Mage::getBaseDir(), DS) . DS
        . rtrim($feed->getConfig('general_feed_dir'), DS) . DS
        . sprintf($feed->getData('promotion_filename'), $feed->getId());
    }

    /**
     * Create feed line
     *
     * @param $counter
     * @param Mage_SalesRule_Model_Rule $rule
     * @param array $row
     * @param RocketWeb_ShoppingFeeds_Model_Provider_Google_Promotions_Map $map
     * @return array
     */
    protected function createFeedLine($counter, Mage_SalesRule_Model_Rule $rule, $row = array(), $map)
    {
        return array(
            $map->mapPromotionId($counter, $rule),
            $map->mapProductApplicability($rule),
            $row['title'],
            $map->mapEffectiveDates($row),
            $map->mapDisplayDates($row),
            'ONLINE',
            $map->mapOfferType($rule),
            $map->mapGenericRedemptionCode($rule),
            $map->mapMinimumPurchaseAmount($rule)
        );
    }

    /**
     * Creates feed header
     *
     * @return array
     */
    protected function createFeedHeader()
    {
        return array(
            'promotion_id',
            'product_applicability',
            'long_title',
            'promotion_effective_dates',
            'promotion_display_dates',
            'redemption_channel',
            'offer_type',
            'generic_redemption_code',
            'minimum_purchase_amount'
        );
    }
}