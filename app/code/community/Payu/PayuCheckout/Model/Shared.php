<?php   

class Payu_PayuCheckout_Model_Shared extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'payucheckout_shared';

    protected $_isGateway               = false;
    protected $_canAuthorize            = false;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;

    protected $_formBlockType = 'payucheckout/shared_form';
    protected $_paymentMethod = 'shared';
     
    
    protected $_order;


    public function cleanString($string) {
        
        $string_step1 = strip_tags($string);
        $string_step2 = nl2br($string_step1);
        $string_step3 = str_replace("<br />","<br>",$string_step2);
        $cleaned_string = str_replace("\""," inch",$string_step3);        
        return $cleaned_string;
    }


    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }
    
    
    /**
     * Get order model
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->_order) {
            $paymentInfo = $this->getInfoInstance();
            $this->_order = Mage::getModel('sales/order')
                            ->loadByIncrementId($paymentInfo->getOrder()->getRealOrderId());
        }
        return $this->_order;
    }
	

    public function getCustomerId()
    {
        return Mage::getStoreConfig('payment/' . $this->getCode() . '/customer_id');
    }
	
    public function getAccepteCurrency()
    {
        return Mage::getStoreConfig('payment/' . $this->getCode() . '/currency');
    }
	
	
	
	
    public function getOrderPlaceRedirectUrl()
    {
          return Mage::getUrl('payucheckout/shared/redirect');
    }

    /**
     * prepare params array to send it to gateway page via POST
     *
     * @return array
     */
    public function getFormFields()
    {
	
	    $billing = $this->getOrder()->getBillingAddress();
        $coFields = array();
        $items = $this->getQuote()->getAllItems();
		
		if ($items) {
            $i = 1;
            foreach($items as $item){
                if ($item->getParentItem()) {
                   continue;
                }        
                $coFields['c_prod_'.$i]            = $this->cleanString($item->getSku());
                $coFields['c_name_'.$i]            = $this->cleanString($item->getName());
                $coFields['c_description_'.$i]     = $this->cleanString($item->getDescription());
                $coFields['c_price_'.$i]           = number_format($item->getPrice(), 2, '.', '');
            $i++;
            }
        }
        
        $request = '';
        foreach ($coFields as $k=>$v) {
            $request .= '<' . $k . '>' . $v . '</' . $k . '>';
        }
		
		
		$key=Mage::getStoreConfig('payment/payucheckout_shared/key');
		$salt=Mage::getStoreConfig('payment/payucheckout_shared/salt');
		$debug_mode=Mage::getStoreConfig('payment/payucheckout_shared/debug_mode');
	
	    $orderId = $this->getOrder()->getRealOrderId(); 
	   
	   	$txnid = $this->generateTransactionId($orderId); 
		
		$coFields['key']          = $key;
		$coFields['txnid']        =  $txnid;
		
		$coFields['amount']       =  number_format($this->getOrder()->getBaseGrandTotal(),0,'','');  
		$coFields['productinfo']  = 'Prpduct Information';  
		$coFields['firstname']    = $billing->getFirstname();
		$coFields['Lastname']     = $billing->getLastname();
		$coFields['City']         = $billing->getCity();
        $coFields['State']        = $billing->getRegion();
		$coFields['Country']      = $billing->getCountry();
        $coFields['Zipcode']      = $billing->getPostcode();
		$coFields['email']        = $this->getOrder()->getCustomerEmail();
        $coFields['phone']        = $billing->getTelephone();
		 
		$coFields['surl']         =  Mage::getBaseUrl().'payucheckout/shared/success/';  
		$coFields['furl']         =  Mage::getBaseUrl().'payucheckout/shared/failure/';
		//$coFields['curl']         =  Mage::getBaseUrl().'payucheckout/shared/canceled/id/'.$this->getOrder()->getRealOrderId();
		
		

		
		//$coFields['Pg']           =  'CC';
		$debugId='';
		
        if ($debug_mode==1) {
		
		$requestInfo= $key.'|'.$coFields['txnid'].'|'.$coFields['amount'].'|'.
		$coFields['productinfo'].'|'.$coFields['firstname'].'|'.$coFields['email'].'|'.$debugId.'||||||||||'.$salt;
		            $debug = Mage::getModel('payucheckout/api_debug')
		                ->setRequestBody($requestInfo)
		                ->save();
							
					$debugId = $debug->getId();	
					
					$coFields['udf1']=$debugId;
					$coFields['Hash']    =   hash('sha512', $key.'|'.$coFields['txnid'].'|'.$coFields['amount'].'|'.
		$coFields['productinfo'].'|'.$coFields['firstname'].'|'.$coFields['email'].'|'.$debugId.'||||||||||'.$salt);
		        }
		else
		{
		 $coFields['Hash']         =   strtolower(hash('sha512', $key.'|'.$coFields['txnid'].'|'.$coFields['amount'].'|'.
		 $coFields['productinfo'].'|'.$coFields['firstname'].'|'.$coFields['email'].'|||||||||||'.$salt));
		}

		 /** Resposne log **/
        $logData = [];
        $logData['order_id'] =  $orderId;
        $logData['request'] = json_encode($coFields);
        $logData['external_id'] = $debugId;
        $logData['updated_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $logData['created_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $logData['status'] = 'pending';
        $logData['payment_type'] = 'Payu';

        Mage::getModel('paymentlog/paymentlog')
            ->setData($logData)
            ->save();

        return $coFields;
    }

    /**
     * Get url of Payu payment
     *
     * @return string
     */
    public function getPayuCheckoutSharedUrl()
    {
        $mode=Mage::getStoreConfig('payment/payucheckout_shared/demo_mode');
		
		$url='https://test.payu.in/_payment.php';
		
		if($mode=='')
		{
		  $url='https://secure.payu.in/_payment.php';
		}
		 
         return $url;
    }
       

    /**
     * Get debug flag
     *
     * @return string
     */
    public function getDebug()
    {
        return Mage::getStoreConfig('payment/' . $this->getCode() . '/debug_flag');
    }

    public function capture(Varien_Object $payment, $amount)
    {
        $payment->setStatus(self::STATUS_APPROVED)
                ->setLastTransId($this->getTransactionId());

        return $this;
    }

    public function cancel(Varien_Object $payment)
    {
        $payment->setStatus(self::STATUS_DECLINED)
                ->setLastTransId($this->getTransactionId());

        return $this;
    }

    /**
     * parse response POST array from gateway page and return payment status
     *
     * @return bool
     */
    public function parseResponse()
    {       

            return true;
    
    }

    /**
     * Return redirect block type
     *
     * @return string
     */
    public function getRedirectBlockType()
    {
        return $this->_redirectBlockType;
    }

    /**
     * Return payment method type string
     *
     * @return string
     */
    public function getPaymentMethodType()
    {
        return $this->_paymentMethod;
    }
	
	
	public function getResponseOperation($response)
	{
	   $order = Mage::getModel('sales/order');	
	   $debug_mode=Mage::getStoreConfig('payment/payucheckout_shared/debug_mode');
	   $key=Mage::getStoreConfig('payment/payucheckout_shared/key');
	   $salt=Mage::getStoreConfig('payment/payucheckout_shared/salt');

	    /** Resposne log **/
        $_OrderId =  empty($response['udf2']) === false ? $response['udf2'] : $response['id'];
        $transctionId = empty($response['txnid']) === false ? $response['txnid'] : null;
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $errorMsg = empty($response['error_Message']) === false ? $response['error_Message'] : '';
        $errorCode = empty($response['error']) === false ? $response['error'] : '';
        $additionalError = empty($response['field9']) === false ? $response['field9'] : '';
        $error = sprintf('%s,%s,%s', $errorCode, $errorMsg, $additionalError);
    
        $model = Mage::getModel('paymentlog/paymentlog');
        if (empty($response['udf1']) === false) {
            $model->load($response['udf1'], 'external_id');
        } else {
		   	$orderid = $this->generateTransactionId($transctionId, false);
            $model->load($orderid, 'order_id');
        }

        $model->setStatus('success')
            ->setTransactionId($transctionId)
            ->setUpdatedAt($date)
            ->setResponse(json_encode($response))
            ->setError($error)
            ->save();

	    if(isset($response['status']))
		{
		   $txnid=$response['txnid'];
		   Mage::log("in first if when status is set", null, $file, true);
		   $orderid = $this->generateTransactionId($txnid, false);
		   if($response['status']=='success')
			{
                Mage::log("in if when mode is COD or status is success", null, $file, true);
				$status=$response['status'];
				$order->loadByIncrementId($orderid);
				$billing = $order->getBillingAddress();
				$amount      = $response['amount'];
				$productinfo = $response['productinfo'];  
				$firstname   = $response['firstname'];
				$email       = $response['email'];
				$keyString='';
				$Udf1 = $response['udf1'];
		 		$Udf2 = $response['udf2'];
		 		$Udf3 = $response['udf3'];
		 		$Udf4 = $response['udf4'];
		 		$Udf5 = $response['udf5'];
		 		$Udf6 = $response['udf6'];
		 		$Udf7 = $response['udf7'];
		 		$Udf8 = $response['udf8'];
		 		$Udf9 = $response['udf9'];
		 		$Udf10 = $response['udf10'];
				if($debug_mode==1)
				{
				 $keyString =  $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|'.$Udf1.'|'.$Udf2.'|'.$Udf3.'|'.$Udf4.'|'.$Udf5.'|'.$Udf6.'|'.$Udf7.'|'.$Udf8.'|'.$Udf9.'|'.$Udf10;
				}
				else
				{
			      $keyString =  $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|'.$Udf1.'|'.$Udf2.'|'.$Udf3.'|'.$Udf4.'|'.$Udf5.'|'.$Udf6.'|'.$Udf7.'|'.$Udf8.'|'.$Udf9.'|'.$Udf10;	
				}
				
				$keyArray = explode("|",$keyString);
				$reverseKeyArray = array_reverse($keyArray);
				$reverseKeyString=implode("|",$reverseKeyArray);
				$saltString     = $salt.'|'.$status.'|'.$reverseKeyString;
				$sentHashString = strtolower(hash('sha512', $saltString));
				 $responseHashString=$_REQUEST['hash'];
				if($sentHashString==$responseHashString)
				{
                        Mage::log("in if when hashstring matches", null, $file, true);
						$order->addStatusToHistory('prepaid_authorized' ,'Payment successful through Payu PG');
						$order->save();
						$order->sendNewOrderEmail();
				
				}
				else
				{
                                        Mage::log("in when hashstring not matche", null, $file, true);
					$order->addStatusToHistory('pending_payment' ,'Payment failure through Payu PG : '.$error);
					$order->cancel()->save();
					}
				
				if ($debug_mode==1) {
			    	$debugId=$response['udf1'];  
					$data = array('response_body'=>implode(",",$response));
					$model = Mage::getModel('payucheckout/api_debug')->load($debugId)->addData($data);
					$model->setId($id)->save();
				  }
			   }
		   
		   if($response['status']=='failure')
		   {
                Mage::log("in if when status is failure", null, $file, true);
		       $order->loadByIncrementId($orderid);
		       $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
		       $order->addStatusToHistory('transaction_declined' ,'Payment declined through Payu PG : '.$error);
		       // Inventory updated 
			   $this->updateInventory($orderid);
			   
			   $order->cancel()->save();
			   
			   if ($debug_mode==1) {
				$debugId=$response['udf1'];
						$data = array('response_body'=>implode(",",$response));
					$model = Mage::getModel('payucheckout/api_debug')->load($debugId)->addData($data);
					$model->setId($id)->save();
				  }
		   
		   }
		   else  if($response['status']=='pending')
		   {
                Mage::log("in if when status is pending", null, $file, true);
		       $order->loadByIncrementId($orderid);
		       $order->addStatusToHistory('pending_payment' ,'Payment failure through Payu PG : '.$error);
		       // Inventory updated  
		       $this->updateInventory($orderid);
			   $order->cancel()->save();
			 		   
			   if ($debug_mode==1) {
				$debugId=$response['udf1'];
						$data = array('response_body'=>implode(",",$response));
					$model = Mage::getModel('payucheckout/api_debug')->load($debugId)->addData($data);
					$model->setId($id)->save();
				  }
		   
		   }
		   
		}
        else
		{
		   Mage::log("In last else ", null, $file, true);		   
		   $order->loadByIncrementId($response['id']);
		   $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true);
		  // Inventory updated 
		   $order_id=$response['id'];
		   $this->updateInventory($order_id);
		   
		   $order->cancel()->save();
		   
		 
		}
	}
	
    public function updateInventory($order_id)
    {
  
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
        $items = $order->getAllItems();
		foreach ($items as $itemId => $item)
		{
		   $ordered_quantity = $item->getQtyToInvoice();
		   $sku=$item->getSku();
		   $product = Mage::getModel('catalog/product')->load($item->getProductId());
		   $qtyStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId())->getQty();
		  
		   $updated_inventory=$qtyStock + $ordered_quantity;
					
		   $stockData = $product->getStockItem();
		   $stockData->setData('qty',$updated_inventory);
		   $stockData->save(); 
			
	   } 
    }

    public function generateTransactionId($txnid, $generate = true)
    {
    	$mode = Mage::getStoreConfig('payment/payucheckout_shared/demo_mode');
    	$randString = "fs_";
    	
    	if ($mode === "Y") {
    		if ($generate) {
    			$txnid = $randString . $txnid;
    		} else {
    			$txnid = substr($txnid, 3);
    		}
    	}

    	return $txnid;
    }
	
}