<?php
class One97_paytm_PaytmstatusController extends Mage_Core_Controller_Front_Action{
    private $jsonResponse;
    private $order_id;
    private $order_success;
    private $log_file;
    private $response_code;
    private $dbConnection;
    private $orderStatusInComment;

	public function paytmOrderAction()
	{
        $request = $_POST;
        if(isset($request['ORDERID'])){
            $this->order_id = $request['ORDERID'];
            $this->order_success = $request['STATUS'];
            $this->response_code = $request['RESPCODE'];
            $this->log_file = date('Y-m-d').'_'.$this->order_id.'.log';
            $this->jsonResponse = json_encode($request);
            $this->createLogFile($this->jsonResponse);
            $this->dbConnection = $this->getDbConnection();
            $this->handleOrderData();
        } else{
            $response = array("status"=> false, "message"=> "Order data not getting");
            echo json_encode($response);
        }
	}

	private function handleOrderData(){
        $order_data = $this->getOrderDetails();
        if(isset($order_data) && !empty($order_data)){
            if($this->salesOrderUpdateQuery($order_data)){
                $response = array("status"=> true, "message"=> "Order detail updated");
                echo json_encode($response);
            }
        } else{
            $response = array("status"=> false, "message"=> "Order not available");
            echo json_encode($response);
        }
    }

    private function getOrderDetails(){
        $orderDetailGetsql = "select entity_id,increment_id,state,status from sales_flat_order where increment_id='%s'";
        $orderDetailGetsqlQuery = sprintf($orderDetailGetsql, $this->order_id);
        $this->createLogFile($orderDetailGetsqlQuery);
        $result = $this->dbConnection->query($orderDetailGetsqlQuery);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                return $row;
            }
        }
    }

    private function orderSalesStatusInsert($parent_id){
        
        if ($this->dbConnection->query($this->orderHistoryInsertQuery($parent_id)) === TRUE) {
            return true;
        } else {
            echo "Error: " . $salesOrderStatusInsertSqlQuery . "<br>" . $this->dbConnection->error;
        }
    }

    private function orderHistoryInsertQuery($parent_id){
        $salesOrderStatusInsertSql = "INSERT INTO sales_flat_order_status_history (parent_id,is_customer_notified,is_visible_on_front,comment,status, created_at, entity_name, is_sms_sent) 
        VALUES ('%s', 0, 0, '%s','%s', '%s', 'order', 0)";
        $salesOrderStatusInsertSqlQuery = sprintf($salesOrderStatusInsertSql, $parent_id, $this->jsonResponse, $this->orderStatusInComment, date('Y/m/d h:i:s a', time()));
        $this->createLogFile($salesOrderStatusInsertSqlQuery);
        return $salesOrderStatusInsertSqlQuery;
    }

    private function salesOrderUpdateQuery($order_data){
        $orderStatusUpdateSQL = '';
        $orderStatusGridUpdateSQL = '';
        if($this->order_success == 'TXN_SUCCESS' && $this->response_code == '01'){
            if( $order_data['state'] != 'new' && $order_data['status'] != 'prepaid_authorized'){
                $orderStatusUpdateSQL = "update sales_flat_order set state='new',status='prepaid_authorized' where increment_id='%s'";
                $orderStatusGridUpdateSQL = "update sales_flat_order_grid set status='prepaid_authorized' where increment_id='%s'";
            } else{
                if($this->orderSalesStatusInsert($order_data['entity_id'])){
                    return TRUE;
                }
            }
            $this->orderStatusInComment = 'prepaid_authorized';
        } else{
            $orderStatusUpdateSQL = "update sales_flat_order set state='canceled',status='transaction_declined' where increment_id='%s'";$orderStatusGridUpdateSQL = "update sales_flat_order_grid set status='transaction_declined' where increment_id='%s'";
            $this->orderStatusInComment = 'transaction_declined';
        }
        $orderStatusUpdateSQLQuery = sprintf($orderStatusUpdateSQL, $this->order_id);
        $orderStatusGridUpdateSQLQuery = sprintf($orderStatusGridUpdateSQL, $this->order_id);
        $orderStatusResult = $this->dbConnection->query($orderStatusUpdateSQLQuery);
        $orderStatusGridResult = $this->dbConnection->query($orderStatusGridUpdateSQLQuery);

        //Write log into file
        $this->createLogFile($orderStatusUpdateSQLQuery);
        $this->createLogFile($orderStatusGridUpdateSQLQuery);
        if($orderStatusResult === TRUE && $orderStatusGridResult === TRUE){
            if($this->orderSalesStatusInsert($order_data['entity_id'])){
                return TRUE;
            }
        }
    }

    private function getDbConnection(){
        $config  = Mage::getConfig()->getResourceConnectionConfig('default_setup');

        $dbname = $config->dbname;
        $username = $config->username;
        $password = $config->password;
        $servername = $config->host;

        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        return $conn;
    }

    private function createLogFile($log_data){
        //Check current order logfile exit or not
        Mage::log($log_data, null, 'payTMlog/'.$this->log_file, true);
    }
	
}