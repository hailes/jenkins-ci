<?php
class Vsourz_Catproduct_Block_Productslider extends Mage_Catalog_Block_Product_Abstract{
	public function getProducts(){
		$catId = $this->getCategoryId();
		// $prodCnt = $this->getProductCount();
		// $_productCollection = Mage::getModel("vsourz_catproduct/productslider")->getItemCollection($catId,$prodCnt);
		// return $_productCollection;
		$productCollection = Mage::getModel('catalog/product');
		$collection = $productCollection->getCollection()->addAttributeToSelect('*')->setStoreId(Mage::app()->getStore()->getId())->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
		$collection->addAttributeToSort('entity_id', 'DESC')->setPageSize(15);
		return $collection;
	}
}
