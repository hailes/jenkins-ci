<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class VS_Ajax_IndexController extends Mage_Checkout_CartController
{
	public function addAction()
	{
		
		$cart   = $this->_getCart();
		$params = $this->getRequest()->getParams();

		$parentProduct = Mage::getModel('catalog/product')->load($params['product']);
		$product = Mage::getModel('catalog/product')->load($params['product']);
		
		$childProduct = Mage::getModel('catalog/product_type_configurable')
		->getProductByAttributes($params['super_attribute'], $product);
		

		if($params['isAjax'] == 1){
			$response = array();
			try {
                            
                                if (Mage::getSingleton('core/session')->getIsBulkOrder() == 1 && Mage::helper('checkout/cart')->getSummaryCount() >=1) {		
                                        $response['status'] = 'ERROR';		
                                        $response['message'] = 'Industrial products already in cart. Regular products and industrial products can not be bought together';		
                                }
				if (isset($params['qty'])) {
					$filter = new Zend_Filter_LocalizedToNormalized(
					array('locale' => Mage::app()->getLocale()->getLocaleCode())
					);
					$params['qty'] = $filter->filter($params['qty']);
				}

				$product = Mage::getModel('catalog/product')->load($childProduct->getId());
		

				//$product = $this->_initProduct();
				$related = $this->getRequest()->getParam('related_product');

				/**
				 * Check product availability
				 */
				if (!$product) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Unable to find Product ID');
				}

				if($response['status'] != 'ERROR'){			
                                    $cart->addProduct($product, $params);							
                                }
                                
				if (!empty($related)) {
					$cart->addProductsByIds(explode(',', $related));
				}

				$cart->save();
                                			
                                $quote = $cart->getQuote();			
                                $quoteId = $quote->getId();			
                                $write = Mage::getSingleton('core/resource')->getConnection('core_write');			
                                $write->query("UPDATE `sales_flat_quote` 			
                                SET `is_bulk_order`=0 WHERE entity_id='$quoteId'");

				$this->_getSession()->setCartWasUpdated(true);

				/**
				 * @todo remove wishlist observer processAddToCart
				 */
				Mage::dispatchEvent('checkout_cart_add_product_complete',
				array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
				);

				if (!$cart->getQuote()->getHasError() && $response['status'] != 'ERROR'){
                                    
                                    Mage::getSingleton('core/session')->setIsBulkOrder('2');
					
					 //$product_image_src=Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100);
                    $product_image_src=Mage::helper('catalog/image')->init($parentProduct, 'small_image')->resize(100,100);
                    $product_image = '<img src="'.$product_image_src.'" class="product-image" alt=""/>';
					
					$proSku = $childProduct->getSku();
					//$message = '<div class="msg">'.$this->__("You've just added this product to the cart:").'<p class="product-name theme-color">'.Mage::helper('core')->htmlEscape($product->getName()).'</p></div>'.$product_image;
					$message = '<div class="msg product-details">'.$product_image.'<p class="product-name">'.Mage::helper('core')->htmlEscape($product->getName()).'</p><p>'.$proSku.'</p></div>';
					$response['status'] = 'SUCCESS';
					$response['message'] = $message;
					$response['price'] = Mage::helper("core")->currency($product->getFinalPrice(),true,false);
					$response['proId'] = $product->getId();
					$response['sku'] = $product->getSku();
					// $response['proColor'] = $childProduct->getAttributeText("color");
					$response['proColor'] = Mage::getModel('catalog/product')->load($childProduct->getId())->getAttributeText('color');
					$response['proSize'] = $childProduct->getAttributeText("size");
					$response['subtotal'] = Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal();
					$productCollection = Mage::getModel('catalog/product')->loadByAttribute('sku',$product->getSku());
					//$thumbnail = $productCollection->getImageUrl();
                                        $media = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
                                        $thumbnail = $media.'catalog/product'. $parentProduct['izooto_desktop'];
                                        
					$response['new_image']=$thumbnail;
					//$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
					//$response['status'] = 'SUCCESS';
					//$response['message'] = $message;
					//New Code Here
					$this->loadLayout();
					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
					$sidebar_block = $this->getLayout()->getBlock('cart_sidebar');
					Mage::register('referrer_url', $this->_getRefererUrl());
					$sidebar = $sidebar_block->toHtml();
					$response['toplink'] = $toplink;
					$response['sidebar'] = $sidebar;
					$response['cartQty']  = Mage::helper('checkout/cart')->getSummaryCount();
                    $response['cartQty2']  = Mage::helper('checkout/cart')->getItemsCount();
				}
			} catch (Mage_Core_Exception $e) {
				$msg = "";
				if ($this->_getSession()->getUseNotice(true)) {
					$msg = $e->getMessage();
				} else {
					$messages = array_unique(explode("\n", $e->getMessage()));
					foreach ($messages as $message) {
						$msg .= $message.'<br/>';
					}
				}

				$response['status'] = 'ERROR';
				$response['message'] = $msg;
			} catch (Exception $e) {
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Cannot add the item to shopping cart.');
				Mage::logException($e);
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
		}else{
			return parent::addAction();
		}
	}
	public function optionsAction(){
		$productId = $this->getRequest()->getParam('product_id');
		// Prepare helper and params
		$viewHelper = Mage::helper('catalog/product_view');

		$params = new Varien_Object();
		$params->setCategoryId(false);
		$params->setSpecifyOptions(false);

		// Render page
		try {
			$viewHelper->prepareAndRender($productId, $this, $params);
		} catch (Exception $e) {
			if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
				if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
					$this->_redirect('');
				} elseif (!$this->getResponse()->isRedirect()) {
					$this->_forward('noRoute');
				}
			} else {
				Mage::logException($e);
				$this->_forward('noRoute');
			}
		}
	}
	protected function _getWishlist()
	{
		$wishlist = Mage::registry('wishlist');
		if ($wishlist) {
			return $wishlist;
		}

		try {
			$wishlist = Mage::getModel('wishlist/wishlist')
			->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer(), true);
			Mage::register('wishlist', $wishlist);
		} catch (Mage_Core_Exception $e) {
			Mage::getSingleton('wishlist/session')->addError($e->getMessage());
		} catch (Exception $e) {
			Mage::getSingleton('wishlist/session')->addException($e,
			Mage::helper('wishlist')->__('Cannot create wishlist.')
			);
			return false;
		}

		return $wishlist;
	}
	public function newsletterAction()
    {
     
        if ($_REQUEST['email']) {
            $session            = Mage::getSingleton('core/session');
            $customerSession    = Mage::getSingleton('customer/session');
            $email              = $_REQUEST['email']; 
          try {
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    $result=0;
                    $message= "Please enter a valid email address.";
                     echo json_encode(array("message"=>$message,"status"=>$result));
                    return;
                }

                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 && 
                    !$customerSession->isLoggedIn()) {
                    echo 'Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl();
                    return;
                    }

                $ownerId = Mage::getModel('newsletter/subscriber')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->loadByEmail($email);
                        $registeremail = $ownerId->getId();
                        $customeraccountid = $ownerId->getCustomerId();
                if ($registeremail !== null) {
                    $result=0;
                    $message= 'This email address is already assigned to another user.';
                    echo json_encode(array("message"=>$message,"status"=>$result));
                    return;
                }

                $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                    $result= 1;
                  $message= "Confirmation request has been sent.";
                    echo json_encode(array("message"=>$message,"status"=>$result));
                    return;
                }
                else {
                    $result=1;
                   $message= "Thank you for your subscription.";
                   echo json_encode(array("message"=>$message,"status"=>$result));
                   
                   return;
                }
            }
            catch (Mage_Core_Exception $e) {
                echo "There was a problem with the subscription:".$e;
                return;
            }
            catch (Exception $e) {
                 echo "There was a problem with the subscription".$e;
                 return;
            }
        }
        $this->_redirectReferer();
    }
	public function WishlistAction()
	{
		$params = $this->getRequest()->getParams();
		
		$response = array();
		if (!Mage::getStoreConfigFlag('wishlist/general/active')) {
			$response['status'] = 'ERROR';
			$response['message'] = $this->__('Wishlist Has Been Disabled By Admin');
		}
		if(!Mage::getSingleton('customer/session')->isLoggedIn()){
			$response['status'] = 'ERROR';
			$response['message'] = $this->__('Please Login First');
		}

		if(empty($response)){
			$session = Mage::getSingleton('customer/session');
			$wishlist = $this->_getWishlist();
			if (!$wishlist) {
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Unable to Create Wishlist');
			}else{

				$productId = (int) $this->getRequest()->getParam('product');
				if (!$productId) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Product Not Found');
				}else{

					$product = Mage::getModel('catalog/product')->load($productId);
					if (!$product->getId() || !$product->isVisibleInCatalog()) {
						$response['status'] = 'ERROR';
						$response['message'] = $this->__('Cannot specify product.');
					}else{

						try {
							$requestParams = $this->getRequest()->getParams();
							$buyRequest = new Varien_Object($requestParams);

							$result = $wishlist->addNewItem($product, $buyRequest);
							if (is_string($result)) {
								Mage::throwException($result);
							}
							$wishlist->save();

							Mage::dispatchEvent(
                				'wishlist_add_product',
							array(
			                    'wishlist'  => $wishlist,
			                    'product'   => $product,
			                    'item'      => $result
							)
							);

							Mage::helper('wishlist')->calculate();

							$message = $this->__('%1$s has been added to your wishlist.', $product->getName(), $referer);
							$response['status'] = 'SUCCESS';
							$response['message'] = $message;

							Mage::unregister('wishlist');

							$this->loadLayout();
							//$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
							//$sidebar_block = $this->getLayout()->getBlock('wishlist_sidebar');
							//$sidebar = $sidebar_block->toHtml();
							//$response['toplink'] = $toplink;
							//$response['sidebar'] = $sidebar;
						}
						catch (Mage_Core_Exception $e) {
							$response['status'] = 'ERROR';
							$response['message'] = $this->__('An error occurred while adding item to wishlist: %s', $e->getMessage());
						}
						catch (Exception $e) {
							mage::log($e->getMessage());
							$response['status'] = 'ERROR';
							$response['message'] = $this->__('An error occurred while adding item to wishlist.');
						}
					}
				}
			}

		}
		
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		return;
	}
	public function updateshoppingcartAction()
	{
	$params = $this->getRequest()->getParams();
	
	    if($params['productid']){
            $response = array();
            try {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }
				$cartHelper = Mage::helper('checkout/cart');
				$items = $cartHelper->getCart()->getItems();
				 $cart = $this->_getCart();
				if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }
				
				foreach ($items as $item) {
					
					if ($item->getItemId() ==$params['productid']) {
						if( $params['qty']  == 0 ){
							$cartHelper->getCart()->removeItem($item->getItemId())->save();
						}
						else if($params['qty'] >= 1){
							
							$model = Mage::getModel('catalog/product'); 
							
							$_product = $model->load($params['pid']); 
							// $item->getProduct()->getId();
							 $avaliablequantity =  Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty() ;
							
												
						/*	$model = Mage::getModel('catalog/product'); 
							$_product = $model->load($params['productid']); 
							$avaliablequantity = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();*/
							
							if($params['qty'] <= $avaliablequantity  ){
							$item->setQty($params['qty']);
							$cartHelper->getCart()->save();
							$message = $this->__('%s was updated to your shopping cart.', Mage::helper('core')->htmlEscape($item->getProduct()->getName()));
							$response['status'] = 'SUCCESS';
							$response['message'] = $message;
							 
							/*$response['subtotal'] = Mage::helper('core')->currency(Mage::getSingleton('checkout/cart')->getQuote()->getSubtotal());
							$response['grandtotal'] = Mage::helper('core')->currency(Mage::getSingleton('checkout/cart')->getQuote()->getGrandTotal());*/
							
							$tax_amount = 0;
							$quote = Mage::getModel('checkout/session')->getQuote();
						$address = $quote->getShippingAddress();
					if ($address) {
						$tax_amount = $address->getTaxAmount();
							}
 
							$response['tax_amount'] = number_format($tax_amount,2);
													
							 $totals = Mage::getSingleton('checkout/session')->getQuote()->getTotals(); //Total object
							$subtotal = round($totals["subtotal"]->getValue()); //Subtotal value
							$grandtotal = round($totals["grand_total"]->getValue()); //Grandtotal value
												
							//$response['itemtotal'] =Mage::helper('checkout')->formatPrice( $item->getRowTotalInclTax());*/
							$response['subtotal'] = Mage::helper('core')->currency($subtotal);
							$response['grandtotal'] = Mage::helper('core')->currency($grandtotal);
							
							$response['itemtotal'] =Mage::helper('core')->currency( $item->getRowTotalInclTax()).'.00';
							$toplink  =  $this->getLayout()->createBlock('core/template')->setTemplate('page/topcheckout.phtml')->toHtml();
							$response['toplink'] = $toplink;
							}else
							{
							$response['status'] = 'FAILURE';
							$response['message'] = "Requested Quantity not avaliable "; 
							}
							
						}
						break;
					}
				}
        }  catch (Mage_Core_Exception $e) {
                $msg = "";
                if ($this->_getSession()->getUseNotice(true)) {
                    $msg = $e->getMessage();
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $msg .= $message.'<br/>';
                    }
                }
 
                $response['status'] = 'ERROR';
                $response['message'] = $msg;
            } catch (Exception $e) {
                $response['status'] = 'ERROR';
                $response['message'] = $this->__('Cannot update the item to shopping cart.');
                Mage::logException($e);
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		}	
	}
}