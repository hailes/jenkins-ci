<?php
class Raveinfosys_Deleteorder_Model_Deleteorder extends Mage_Core_Model_Abstract
{
    public function _construct() 
    {
        parent::_construct();
        $this->_init('deleteorder/deleteorder');
    }

    public function _initOrder($id) 
    {
        $order = $this->getOrder($id);
        if (!$order->getId()) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('This order no longer exists.'));
            return false;
        }

        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }

    public function getOrder($id)
    {
        return Mage::getModel('sales/order')->load($id);
    }
    
    public function _remove($orderId) 
    {
        $order = $this->getOrder($orderId);
        if ($order->hasInvoices()) {
            $order->getInvoiceCollection()->walk('delete');
        }

        if ($order->hasShipments()) {
            $order->getShipmentsCollection()->walk('delete');
        }

        if ($order->hasCreditmemos()) {
            $order->getCreditmemosCollection()->walk('delete');
        }

        $order->delete();
        return true;
    }

    


}