<?php

class N98_ApiLogger_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function controllerActionPredispatchApiSoapIndex(Varien_Event_Observer $observer)
    {
        if (!Mage::getSingleton('n98_apilogger/config')->isSoapLogActive()) {
            return;
        }

        if (!$this->_shouldLogWsdlRequest($observer)) {
            return;
        }

        $request = $observer->getControllerAction()->getRequest();
        $xml1 = $this->getXMLtoArray($request->getRawBody());
        $json = json_encode($xml1);
        $array = json_decode($json,TRUE);
        if($array['SOAPENV:ENVELOPE']['SOAPENV:BODY']['NS1:CALL']['RESOURCEPATH']['content']=='product_stock.update'){
        /* @var $request Mage_Core_Controller_Request_Http */
        Mage::helper('n98_apilogger')->log(
            array(
                'body'             => $request->getRawBody(),
                'direction'        => N98_ApiLogger_Model_Log::DIRECTION_IN,
                'api_adapter'      => 'soap',
                'http_status_code' => null,
                'http_method'      => $request->getMethod(),
            )
        );
    }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function controllerActionPredispatchApiV2SoapIndex(Varien_Event_Observer $observer)
    {
        if (!Mage::getSingleton('n98_apilogger/config')->isSoapV2LogActive()) {
            return;
        }

        if (!$this->_shouldLogWsdlRequest($observer)) {
            return;
        }

        $request = $observer->getControllerAction()->getRequest();
        /* @var $request Mage_Core_Controller_Request_Http */
        Mage::helper('n98_apilogger')->log(
            array(
                'body'             => $request->getRawBody(),
                'direction'        => N98_ApiLogger_Model_Log::DIRECTION_IN,
                'api_adapter'      => 'soap_v2',
                'http_status_code' => null,
                'http_method'      => $request->getMethod(),
            )
        );
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function controllerActionPostdispatchApiSoapIndex(Varien_Event_Observer $observer)
    {
        if (!Mage::getSingleton('n98_apilogger/config')->isSoapLogActive()) {
            return;
        }

        if (!$this->_shouldLogWsdlRequest($observer)) {
            return;
        }

        $response  = $observer->getControllerAction()->getResponse();
        $xml1 = $this->getXMLtoArray($response->getBody());
        $json = json_encode($xml1);
        $array = json_decode($json,TRUE);
        if($array['SOAP-ENV:ENVELOPE']['SOAP-ENV:BODY']['NS1:CALLRESPONSE']['CALLRETURN']['content']=='true'){
        /* @var $response Mage_Core_Controller_Response_Http */
        Mage::helper('n98_apilogger')->log(
            array(
                'body'             => $response->getBody(),
                'direction'        => N98_ApiLogger_Model_Log::DIRECTION_OUT,
                'api_adapter'      => 'soap',
                'http_status_code' => $response->getHttpResponseCode(),
                'http_method'      => '',
            )
        );
    }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function controllerActionPostdispatchApiV2SoapIndex(Varien_Event_Observer $observer)
    {
        if (!Mage::getSingleton('n98_apilogger/config')->isSoapV2LogActive()) {
            return;
        }

        if (!$this->_shouldLogWsdlRequest($observer)) {
            return;
        }

        $response  = $observer->getControllerAction()->getResponse();
        /* @var $response Mage_Core_Controller_Response_Http */
        Mage::helper('n98_apilogger')->log(
            array(
                'body'             => $response->getBody(),
                'direction'        => N98_ApiLogger_Model_Log::DIRECTION_OUT,
                'api_adapter'      => 'soap_v2',
                'http_status_code' => $response->getHttpResponseCode(),
                'http_method'      => '',
            )
        );
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return bool
     */
    protected function _shouldLogWsdlRequest($observer)
    {
        if ($observer->getControllerAction()->getRequest()->getParam('wsdl') === null) {
            return true;
        }

        if (!Mage::getSingleton('n98_apilogger/config')->isLogWsdlRequestActive()) {
            return false;
        }

        return true;
    }
    public function getXMLtoArray($XML)
        {
        $XML = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $XML);
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $XML, $vals);
        xml_parser_free($xml_parser);
        // wyznaczamy tablice z powtarzajacymi sie tagami na tym samym poziomie
        $_tmp='';
        foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_level!=1 && $x_type == 'close') {
        if (isset($multi_key[$x_tag][$x_level]))
        $multi_key[$x_tag][$x_level]=1;
        else
        $multi_key[$x_tag][$x_level]=0;
        }
        if ($x_level!=1 && $x_type == 'complete') {
        if ($_tmp==$x_tag)
        $multi_key[$x_tag][$x_level]=1;
        $_tmp=$x_tag;
        }
        }
        // jedziemy po tablicy
        foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_type == 'open')
        $level[$x_level] = $x_tag;
        $start_level = 1;
        $php_stmt = '$xml_array';
        if ($x_type=='close' && $x_level!=1)
        $multi_key[$x_tag][$x_level]++;
        while ($start_level < $x_level) {
        $php_stmt .= '[$level['.$start_level.']]';
        if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
        $php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
        $start_level++;
        }
        $add='';
        if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
        if (!isset($multi_key2[$x_tag][$x_level]))
        $multi_key2[$x_tag][$x_level]=0;
        else
        $multi_key2[$x_tag][$x_level]++;
        $add='['.$multi_key2[$x_tag][$x_level].']';
        }
        if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
        if ($x_type == 'open')
        $php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
        else
        $php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
        eval($php_stmt_main);
        }
        if (array_key_exists('attributes', $xml_elem)) {
        if (isset($xml_elem['value'])) {
        $php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
        eval($php_stmt_main);
        }
        foreach ($xml_elem['attributes'] as $key=>$value) {
        $php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
        eval($php_stmt_att);
        }
        }
        }
        return $xml_array;
        }
}