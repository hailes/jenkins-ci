<?php
class OrganicInternet_SimpleConfigurableProducts_Catalog_Block_Product_Price
    extends Mage_Catalog_Block_Product_Price
{
    #This is overridden as an admittedly nasty hack to not have to change the contents of catalog/product/price.phtml
    #This is because there's no nice way to keep price.phtml in sync between this extension and the magento core version
    #Yes, it's dependent on the value of $htmlToInsertAfter; I'm not aware of a better alternative.
    public function _toHtml() {

        $htmlToInsertAfter = '<div class="price-box">';
        if ($this->getTemplate() == 'catalog/product/price.phtml') {
            $product = $this->getProduct();
            $extraHtml = "";
            if (is_object($product) && $product->isConfigurable()) {
               /* $extraHtml = '<span class="label" id="configurable-price-from-'
                . $product->getId()
                . $this->getIdSuffix()
                . '"><span class="configurable-price-from-label">';*/

                if ($product->getMaxPossibleFinalPrice() != $product->getFinalPrice()) {
                    //$extraHtml .= $this->__('Price From:');
                }
               // $extraHtml .= '</span></span>';

                //code for display price of selected attribute
                $_product = $this->getProduct();
                if($_product->getTypeId() == "configurable")
                {
                    $_coreHelper = $this->helper('core');
                    $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($_product); 
                    $simple_collection = $conf->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
                    $simple_collection->setOrder('weight', 'ASC');
                    foreach($simple_collection as $simple_product)
                    {
                        $price = $_coreHelper->formatPrice($simple_product->getPrice(), false);
                        $qtyStock =  Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple_product)->getQty();       
                        $stock =  Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple_product)->getQty();
                        $stock = round($stock,2);
                        if ( $stock > 0 )
                        {
                            $inv[$_product->getId()][$simple_product->getId()] = $stock;
                            $main_id[$_product->getId()][$simple_product->getId()] = $simple_product->getId(); 
                        }
                        $itemsinstock+= $stock;
                        if((isset($main_id))&&($itemsinstock >0))
                        {
                            $pid = current($main_id[$_product->getId()]); // $mode = 'foot';
                        }
                        if($simple_product->getId()==$pid)
                        {

                            $priceHtml = "<div class='price-box'><span class='regular-price'</span><span class='price'>".$price."</span></div>";
                        }
                    }
                }
                //$priceHtml = parent::_toHtml();
                #manually insert extra html needed by the extension into the normal price html
                return substr_replace($priceHtml, $extraHtml, strpos($priceHtml, $htmlToInsertAfter)+strlen($htmlToInsertAfter),0);
            }
	    }
        return parent::_toHtml();
    }
}
