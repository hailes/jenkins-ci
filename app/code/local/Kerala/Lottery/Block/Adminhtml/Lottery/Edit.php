<?php
	
class Kerala_Lottery_Block_Adminhtml_Lottery_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "lottery";
				$this->_controller = "adminhtml_lottery";
				$this->_updateButton("save", "label", Mage::helper("lottery")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("lottery")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("lottery")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("lottery_data") && Mage::registry("lottery_data")->getId() ){

				    return Mage::helper("lottery")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("lottery_data")->getId()));

				} 
				else{

				     return Mage::helper("lottery")->__("Add Item");

				}
		}
}