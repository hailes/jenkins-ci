<?php
class Kerala_Lottery_Block_Adminhtml_Lottery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("lottery_form", array("legend"=>Mage::helper("lottery")->__("Item information")));

				
						$fieldset->addField("id", "hidden", array(
						"label" => Mage::helper("lottery")->__("ID"),
						"name" => "id",
						));
					
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("lottery")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("lottery")->__("Email"),
						"name" => "email",
						));
					
						$fieldset->addField("mobile", "text", array(
						"label" => Mage::helper("lottery")->__("Mobile Number"),
						"name" => "mobile",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getLotteryData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getLotteryData());
					Mage::getSingleton("adminhtml/session")->setLotteryData(null);
				} 
				elseif(Mage::registry("lottery_data")) {
				    $form->setValues(Mage::registry("lottery_data")->getData());
				}
				return parent::_prepareForm();
		}
}
