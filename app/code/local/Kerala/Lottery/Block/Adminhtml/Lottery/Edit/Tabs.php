<?php
class Kerala_Lottery_Block_Adminhtml_Lottery_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("lottery_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("lottery")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("lottery")->__("Item Information"),
				"title" => Mage::helper("lottery")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("lottery/adminhtml_lottery_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
