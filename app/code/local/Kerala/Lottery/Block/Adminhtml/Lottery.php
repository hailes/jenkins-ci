<?php


class Kerala_Lottery_Block_Adminhtml_Lottery extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_lottery";
	$this->_blockGroup = "lottery";
	$this->_headerText = Mage::helper("lottery")->__("Lottery Manager");
	$this->_addButtonLabel = Mage::helper("lottery")->__("Add New Item");
	parent::__construct();
	
	}

}