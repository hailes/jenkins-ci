<?php
/**
*	Author		: 	Cybernetikz
*	Author Email:   info@cybernetikz.com
*	Blog		: 	http://blog.cybernetikz.com
*	Website		: 	http://www.cybernetikz.com
*/

class Cybernetikz_Salesreport_Helper_Data extends Mage_Core_Helper_Abstract {

	const REPORT_NAME = "salesreports/reportsetting/report_name";
	const REPORT_ADDRESS = "salesreports/reportsetting/report_address";
	
	public function getReportName($store = null){
		return Mage::getStoreConfig(self::REPORT_NAME, $store);		
	}
	
	public function getReportAddress($store = null){
		return Mage::getStoreConfig(self::REPORT_ADDRESS, $store);		
	}

	public function getSalesReport($from_date, $to_date)
	{

		$_manifest = Mage::getModel('manifest/manifest')->getCollection()
								->addFieldToFilter('main_table.created_at', array('from'=>$from_date, 'to'=>$to_date));
			$_manifest->getSelect()
					->join( array('sales_package'=> 'sales_flat_package_item_details'), 'main_table.id = sales_package.manifest_id', array('package_id','shipment_id','manifest_id','item_name','order_id','sku','sum(sales_package.qty) as package_qty', 'original_price', 'discount_amount_item','(item_special_discount + discount_amount_item + item_store_credit_discount) as discount', 'shipping_charge','tax_amount_item', 'unit_price', 'tax_class_id'))
					->join( array('shipment'=> 'sales_flat_shipment_track'), 'sales_package.package_id = shipment.feetscience_package_id', array('track_number','carrier_code','title','created_at as shipment_created_date','weight'))
					->join( array('invoice'=> 'sales_flat_invoice'), 'sales_package.invoice_id = invoice.entity_id', array('increment_id as invoice_increment_id', 'created_at as invoice_created_date', 'grand_total'))
					->group('sales_package.order_id')
					->group('sales_package.package_id')
					->group('sales_package.sku');

			$orders_csv_row = array();

			$manifest = $_manifest->getData();
			if (count($manifest) > 0) {
				foreach ($manifest as $key=>$report) {
					$_Order = Mage::getModel('sales/order')->load($report['order_id']);
					$orders_csv_row[$key]['Date'] = $_Order->getCreatedAt();
					$orders_csv_row[$key]['Order Id'] = $_Order->getIncrementId();
					$orders_csv_row[$key]['Package ID'] = $report['package_id'];
					$orders_csv_row[$key]['Payment Method'] = $_Order->getPayment()->getMethodInstance()->getTitle();
					$orders_csv_row[$key]['Invoice Number'] = $report['invoice_increment_id'];
					$orders_csv_row[$key]['Invoice Date'] = $report['invoice_created_date'];
					$orders_csv_row[$key]['Courier Code'] = $report['title'];
					$orders_csv_row[$key]['AWB No.'] = $report['track_number'];

					$rma = Mage::getModel('awrma/entity')->load($_Order->getIncrementId(), 'order_id');

					$orders_csv_row[$key]['RMA Code'] = $rma->getRmaId();
					$orders_csv_row[$key]['RMA-Created-Date'] = $rma->getCreatedAt();
					$orders_csv_row[$key]['Product Name'] = $report['item_name'];
					$orders_csv_row[$key]['Sku'] = $report['sku'];
					$orders_csv_row[$key]['Qty'] = $report['package_qty'];
					$orders_csv_row[$key]['MRP'] = $report['unit_price'];
					$orders_csv_row[$key]['Discount'] = $report['discount'];
					$orders_csv_row[$key]['Tax amount'] = $report['tax_amount_item'];
					$orders_csv_row[$key]['Tax %'] = $this->getTaxPercentage($report['tax_class_id']);
					/*$shippingAddress = Mage::getModel('sales/order_address')->load($report['shipping_address_id']);
					$orders_csv_row[$key]['State'] = $shippingAddress->getRegion();
					$orders_csv_row[$key]['City'] = $shippingAddress->getCity();
					$orders_csv_row[$key]['Pincode'] = $shippingAddress->getPostcode();*/
					$orders_csv_row[$key]['Total Amount'] = ($report['unit_price'] * $report['package_qty'] ) - $orders_csv_row[$key]['Discount'];
					$orders_csv_row[$key]['Shipping Charge'] = $report['shipping_charge'];
					$orders_csv_row[$key]['COD Charge'] = $_Order->getPaymentCharge();
					$orders_csv_row[$key]['Net Total Value'] = $orders_csv_row[$key]['Total Amount'];
					$orders_csv_row[$key]['Refund Amount'] = $_Order->getTotalRefunded();
					$orders_csv_row[$key]['Customer Name'] = $_Order->getCustomerName();
					$orders_csv_row[$key]['Customer Email'] = $_Order->getCustomerEmail();				}
			}

		return $orders_csv_row;
	}

	public function getSalesPackageReport($from_date, $to_date)
	{

		$_manifest = Mage::getModel('manifest/manifest')->getCollection()
								->addFieldToFilter('main_table.created_at', array('from'=>$from_date, 'to'=>$to_date));
			$_manifest->getSelect()
					->join( array('sales_package'=> 'sales_flat_package_item_details'), 'main_table.id = sales_package.manifest_id', array('package_id','shipment_id','manifest_id', 'order_id'))
					->join( array('shipment'=> 'sales_flat_shipment_track'), 'sales_package.package_id = shipment.feetscience_package_id', array('track_number','carrier_code','title','created_at as shipment_created_date','weight'))
					->joinleft( array('reconciliation'=> 'sales_flat_shipment_reconciliation'), 'reconciliation.awb_no = shipment.track_number', array('awb_no', 'payment_received_on', 'delivered_date', 'amount'))
					->join( array('invoice'=> 'sales_flat_invoice'), 'sales_package.invoice_id = invoice.entity_id', array('increment_id', 'created_at as invoice_created_date', 'grand_total'))
					->group('sales_package.package_id');

			$orders_csv_row = array();

			$manifest = $_manifest->getData();
			if (count($manifest) > 0) {
				foreach ($manifest as $key=>$report) {
					$_Order = Mage::getModel('sales/order')->load($report['order_id']);
					$orders_csv_row[$key]['Date'] = $_Order->getCreatedAt();
					$orders_csv_row[$key]['Order Id'] = $_Order->getIncrementId();
					$shippingAddress = Mage::getModel('sales/order_address')->load($report['shipping_address_id']);
					$orders_csv_row[$key]['Package ID'] = $report['package_id'];
					$orders_csv_row[$key]['Payment Method'] = $_Order->getPayment()->getMethodInstance()->getTitle();
					$orders_csv_row[$key]['Invoice Number'] = $report['increment_id'];
					$orders_csv_row[$key]['Invoice Date'] = $report['invoice_created_date'];
					$orders_csv_row[$key]['Courier Code'] = $report['title'];
					$orders_csv_row[$key]['AWB No.'] = $report['track_number'];
					$rma = Mage::getModel('awrma/entity')->load($_Order->getIncrementId(), 'order_id');
					$orders_csv_row[$key]['RMA Code'] = $rma->getRmaId();
					$orders_csv_row[$key]['RMA-Created-Date'] = $rma->getCreatedAt();
					$orders_csv_row[$key]['Net Total Value'] = $report['grand_total'];
					$orders_csv_row[$key]['Refund Amount'] = $_Order->getTotalRefunded();
					$orders_csv_row[$key]['Customer Name'] = $_Order->getCustomerName();
					$orders_csv_row[$key]['Customer Email Id'] = $_Order->getCustomerEmail();
					$orders_csv_row[$key]['Payment Received on'] = $report['payment_received_on'];
					$orders_csv_row[$key]['Amount Received'] = $report['amount'];
					$orders_csv_row[$key]['Delivered on'] = $report['delivered_date'];
				}
			}

		return $orders_csv_row;
	}

	public function getDailySalesReport($from_date, $to_date)
	{

		$orderCollection = Mage::getResourceModel('sales/order_collection');
 
    	$orderCollection = $orderCollection->addFieldToFilter('main_table.created_at', array('from'=>$from_date, 'to'=>$to_date))
    		->addFieldToSelect(['entity_id', 'created_at', 'increment_id', 'customer_email', 'status', 'shipping_amount', 'coupon_code', 'discount_description', 'grand_total']);

		$orderCollection->getSelect()
			->join( array('order_item'=> 'sales_flat_order_item'), 'main_table.entity_id = order_item.order_id', array('qty_ordered', 'qty_invoiced', 'qty_canceled', 'qty_shipped', 'original_price', 'tax_percent', 'tax_amount', 'discount_amount', 'discount_percent', 'row_total', 'row_total_incl_tax', 'item_id', 'order_id', 'sku', 'name'))
			->join( array('address'=> 'sales_flat_order_address'), 'main_table.entity_id = address.parent_id and address_type="shipping"', array('telephone', 'postcode', 'country_id', 'region', 'city', 'firstname', 'lastname', 'address_type'))

			// ->joinLeft( array('sales_package'=> 'sales_flat_package_item_details'), 'main_table.entity_id = sales_package.entity_id', array('package_id','shipment_id','manifest_id','item_name'))
		->joinLeft( array('shipment'=> 'sales_flat_shipment_track'), 'main_table.entity_id = shipment.order_id', array('track_number','carrier_code','title','created_at as shipment_created_date','weight'))

			->joinLeft( array('payment'=> 'sales_flat_order_payment'), 'main_table.entity_id = payment.parent_id', array('method'))
			->group('order_item.item_id');


		$orderStatus = Mage::getModel('sales/order_status')->getResourceCollection()->getData();

		$statusList = array_column($orderStatus, 'status', 'label');

		$orders_csv_row = array();

		$orderCollection = $orderCollection->getData();
		if (count($orderCollection) > 0) {
			foreach ($orderCollection as $key => $report) {
				$order = new Mage_Sales_Model_Order();
				$order->loadByIncrementId($report['increment_id']);
				$payment = $order->getPayment()->getMethodInstance()->getTitle();
				$status = $order->getStatusLabel();

				$orders_csv_row[$key]['Id'] = $report['entity_id'];
				$orders_csv_row[$key]['Order Id'] = $report['increment_id'];
				$orders_csv_row[$key]['Order Date'] = $report['created_at'];
				$orders_csv_row[$key]['Sku'] = $report['sku'];
				$orders_csv_row[$key]['Name'] = $report['name'];
				$orders_csv_row[$key]['Qty Ordered'] = $report['qty_ordered'];
				$orders_csv_row[$key]['Qty Invoiced'] = $report['qty_invoiced'];
				$orders_csv_row[$key]['Qty Canceled'] = $report['qty_canceled'];
				$orders_csv_row[$key]['Qty Shipped'] = $report['qty_shipped'];
				$orders_csv_row[$key]['Original Price'] = $report['original_price'];
				$orders_csv_row[$key]['Tax %'] = $report['tax_percent'];
				$orders_csv_row[$key]['Tax Amount'] = $report['tax_amount'];
				$orders_csv_row[$key]['Discount %'] = $report['discount_percent'];
				$orders_csv_row[$key]['Discount Amount'] = $report['discount_amount'];
				$orders_csv_row[$key]['Discount Description'] = $report['discount_description'];
				$orders_csv_row[$key]['Row Total'] = $report['row_total_incl_tax'] - $report['discount_amount'];
				$orders_csv_row[$key]['Status'] = array_search($report['status'], $statusList);
				$orders_csv_row[$key]['Coupon Code'] = $report['coupon_code'];
				$orders_csv_row[$key]['Shipping Amount'] = $report['shipping_amount'];
				$orders_csv_row[$key]['Total Order Value'] = $report['grand_total'];
				$orders_csv_row[$key]['Customer Email'] = $report['customer_email'];
				$orders_csv_row[$key]['Customer Name'] = $report['firstname'] . $report['lastname'];
				$orders_csv_row[$key]['City'] = $report['city'];
				$orders_csv_row[$key]['Region'] = $report['region'];
				$orders_csv_row[$key]['Country'] = $report['country_id'];
				$orders_csv_row[$key]['Postcode'] = $report['postcode'];
				$orders_csv_row[$key]['Telephone'] = $report['telephone'];
				$orders_csv_row[$key]['Payment Type'] = $payment;
				$orders_csv_row[$key]['Courier Code'] = $report['title'];
				$orders_csv_row[$key]['AWB No.'] = $report['track_number'];
			}
		}

		return $orders_csv_row;
	}

	private function getTotalDayOfShip($from,$to)
	{
		$fromDate = date_create($from);
		$toDate = date_create($to);

		$diff = date_diff($fromDate,$toDate);

		return $diff->format("%a days");
	}

	public function getTaxPercentage($taxClassId)
    {
        $store = Mage::app()->getStore();
        $taxCalculation = Mage::getModel('tax/calculation');
        $request = $taxCalculation->getRateRequest(null, null, null, $store);
        $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));

        return $percent;
    }
}