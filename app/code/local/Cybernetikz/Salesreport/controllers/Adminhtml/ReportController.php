<?php
class Cybernetikz_Salesreport_Adminhtml_ReportController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Returns generated CSV file
     */
    public function indexAction()
    { 
		$this->loadLayout();
	   	$this->_title($this->__("Report"));
	   	$this->renderLayout();
		
    }
	
	public function newAction() {
		$from = Mage::app()->getRequest()->getPost('from-date');
		$to = Mage::app()->getRequest()->getPost('to-date');

		$from_date = date('Y-m-d' . ' 00:00:00', strtotime($from));
		$to_date = date('Y-m-d' . ' 23:59:59', strtotime($to));

		
		
		$current_date = Date('d-m-Y H:m:s');
        $filename = $current_date.'.csv';
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		 $select = "SELECT ordertable.increment_id,
		 ordertable.customer_email as email,ordertable.total_refunded , track_order.carrier_code as code,track_order.track_number as track_number,
		 manifest.id,rma.rma_id as rma_code,manifestdetails.shipping_charge as shipping_charge,invoice_table.increment_id as invoice_number,
		 DATE_FORMAT(rma.created_at,'%d-%m-%Y') AS rma_created ,
		 DATE_FORMAT(invoice_table.created_at,'%d-%m-%Y') AS invoice_created ,
		 DATE_FORMAT(manifest.created_at,'%d-%m-%Y') AS created_at,address.firstname AS firstname,
			address.lastname AS lastname,
		 manifestdetails.item_name,manifestdetails.order_id,(SELECT unit FROM `stockupdate` WHERE material=manifestdetails.sku GROUP BY material) AS UOM,
		 manifestdetails.sku,manifestdetails.qty,manifestdetails.unit_price AS MRP,
		 manifestdetails.discount_amount_item+manifestdetails.item_special_discount+manifestdetails.item_store_credit_discount AS dicount,
		 manifestdetails.unit_price-(manifestdetails.discount_amount_item+manifestdetails.item_special_discount+manifestdetails.item_store_credit_discount) AS Total,
		 manifestdetails.tax_amount_item,manifestdetails.tax_class_id FROM `feetscience_manifest` AS manifest INNER JOIN `sales_flat_package_item_details` AS manifestdetails ON manifest.id=manifestdetails.manifest_id
		LEFT JOIN `sales_flat_invoice` as invoice_table on invoice_table.entity_id=manifestdetails.invoice_id
		
		 LEFT JOIN  `sales_flat_order` AS ordertable ON  ordertable.entity_id=manifestdetails.order_id
		 LEFT JOIN aw_rma_entity as rma on rma.order_id=ordertable.increment_id
		 LEFT JOIN sales_flat_shipment_track as track_order on track_order.order_id=ordertable.entity_id
		LEFT JOIN sales_flat_order_address AS address ON ordertable.entity_id = address.parent_id 

		 WHERE manifest.created_at >= '$from_date' AND manifest.created_at <= '$to_date'";	
		$read->query($select);
		$rows = $read->fetchAll($select);
		
		
		/*
		Date

		Order Id
		Payment
		Invoice-Number	
		
		Invoice Dt	
		RMA Code	
		RMA Created Date	
		Product name
		sku code
		qty
		UOM
		MRP
		discount
		Tax amount
		Tax %
		Total amount
		Shipping Charge	
		COD Charge
		Net Total Value
		Refund Amount
		Customer Name
		Customer Email Id
		*/

		
		
		
		$csv = new Varien_File_Csv();

		
		
		
		
		$_columns = array(
		
'Date',
'Order-Id',
'Payment',
'Invoice-Number',	

'Invoice Dt',
'Carrier Code',
'Track Number',
	
'RMA Code',	
'RMA-Created-Date',	
'Product name',
'sku code',
'qty',

'MRP',
'discount',
'Tax amount',
'Tax %',
'Total amount',
'Shipping Charge',	
'COD Charge',
'Net Total Value',
'Refund Amount',
'Customer Name',
'Customer Email Id'   
);

$data = array();
$data1 = array();
$csvData = array();
// prepare CSV header...
foreach ($_columns as $column) {
       $data1[] = $column;
}
//$csv .= implode(',', $data)."\n";
foreach( $rows as $values )
	{
		$order = Mage::getModel('sales/order')->load($values['order_id']);
		$data['Date'] = $values['created_at'];
		$data['Order-Id'] = $values['increment_id'];
		$data['payment'] = $order->getPayment()->getMethodInstance()->getTitle();
		
		$data['invoice_number'] = $values['invoice_number'];		
		$data['invoice_date'] = $values['invoice_created'];	
		$data['carrier_code'] = $values['code'];	
		$data['track_number'] = $values['track_number'];			
		$data['rma_code'] = $values['rma_code'];		
		$data['rma_created'] = $values['rma_created'];
		$data['item_name'] = $values['item_name'];
		
		$data['sku'] = $values['sku'];
		$data['qty'] = $values['qty'];
		
		$data['MRP'] = $values['MRP'];
		$data['discount'] = $values['dicount'];
		$data['tax_amount_item'] = $values['tax_amount_item'];
		
		$percent=0;
		if( $values['tax_class_id'] != 0 && $values['tax_class_id'] != "" )
		{
			$store = Mage::app()->getStore();
			$taxCalculation = Mage::getModel('tax/calculation');
			$request = $taxCalculation->getRateRequest(null, null, null, $store);
			$percent = $taxCalculation->getRate($request->setProductClassId($values['tax_class_id']));
		}
		$data['tax_per'] = $percent;
		$dism = abs($values['dicount']);
		$data['Total'] = ($values['MRP'] * $values['qty'] - $dism);
		$data['shipping_charge'] = $values['shipping_charge'];
		$data['cod_charge'] = 0;
		$data['net_total'] = $data['Total'];
		$data['refund_amount'] = $values['total_refunded'];
		$data['customer_name'] = $values['firstname'].$values['lastname'];
		$data['customer_email'] = $values['email'];
		//$csv .= implode(',', $data)."\n";
		$csvData[] = $data;
	}
	
		//array_unshift($csvData, $data1);$csv->saveData($filename, $csvData);
		
        	$io = new Varien_Io_File();
			$path = Mage::getBaseDir().'/feetsciencereports' . DS;
			$name = "Order-Reconcilation-Report".md5(microtime());
			
			$csvfile = $path . DS . $name . '.csv';
			$io->setAllowCreateFolders(true);
			$io->open(array('path' => $path));
			$io->streamOpen($csvfile, 'w+');
			$io->streamLock(true);
			$io->streamWriteCsv($data1);
		//end
		
	$csvData = $csvData;
	foreach ($csvData as $product) {
    $io->streamWriteCsv($product);
     }
		$url  = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'feetsciencereports/' . $name . '.csv';
		$link = '<a href="'.$url.'">Order-Reconcilation-Report.csv</a>';
		//$filename = 'Order-Reconcilation-Report.csv';
		//$this->_prepareDownloadResponse($filename, $csvData, 'text/csv');
		Mage::getSingleton('core/session')->addSuccess('Download your CSV File ' . $link);
		$this->_redirect('*/*/');	
	//
		
			
	}

}

?>
