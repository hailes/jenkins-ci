<?php
class Cybernetikz_Salesreport_Adminhtml_ReportsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Returns generated CSV file
     */
    public function indexAction()
    {
		$this->loadLayout();
	   $this->_title($this->__("New Stock Report"));
	   $this->renderLayout();
		
    }
	
	public function newAction() {
		
		$current_date = Date('d-m-Y H:m:s');
        $filename = $current_date.'.csv';
		
		
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql ="SELECT 
 items.order_id AS order_id,
			orders.increment_id AS order_increment_id,
			items.sku AS SKU,
			items.name AS ProductName,
			items.price_incl_tax AS price,
			items.qty_ordered AS qty_ordered,
			items.qty_shipped AS qty_shipped,
			items.qty_invoiced AS qty_invoiced,
			items.qty_canceled AS qty_canceled,
			items.tax_percent AS tax_percent,
			items.tax_amount AS tax_amount,
			items.discount_percent,
			items.discount_amount,
			items.row_total_incl_tax,
			orders.coupon_code,
			orders.shipping_amount as shipping_amount,
			orders.discount_description,
			items.created_at AS orderdate,
			address.email AS email,
			address.firstname AS firstname,
			address.lastname AS lastname,
			address.street AS address,
			address.city AS city,
			address.region AS region,
			address.country_id AS country,
			address.postcode AS postcode,
			address.telephone AS telephone,
			orders.status AS statusvalues
       FROM sales_flat_order AS orders 
        JOIN sales_flat_order_item AS items 
          ON items.order_id = orders.entity_id 
        LEFT JOIN sales_flat_order_address AS address
          ON orders.entity_id = address.parent_id 
      WHERE       
    items.product_type='simple'
        AND orders.status != ''
		 AND  orders.created_at BETWEEN '$from' AND '$to'	
        GROUP BY items.item_id";
		$read->query($sql);
	$rows = $read->fetchAll($sql);
	$csvData = array();		
	if (is_array($rows))
{
	foreach( $rows as $values )
	{ 
	$data['Order Date']	=	$values['orderdate'];
	$data['order_id'] 	=	$values['order_increment_id'];
	
	$data['sku']	=	$values['SKU'];
	$data['name']	=	$values['ProductName'];
		
	$data['qty_ordered']	=	$values['qty_ordered'];
	$data['qty_invoiced']	=	$values['qty_invoiced'];
	$data['qty_canceled']	=	$values['qty_canceled'];
	$data['qty_shipp]ed']	=	$values['qty_shipp]ed'];
	$data['original_price']	=	$values['price'];
	$data['tax_percent']	=	$values['tax_percent'];
	$data['tax_amount']	=	$values['tax_amount'];
	$data['discount_percent']	=	$values['discount_percent'];
	$data['discount_amount']	=	$values['discount_amount'];
	$data['row_total']	=	($values['row_total_incl_tax'] - $values['discount_amount']) ;
	$data['status']	=	$values['statusvalues'];
	$data['coupon_code']	=	$values['coupon_code'];
	$data['shipping_amount']	=	$values['shipping_amount'];
	$data['customer_email']	=	$values['email'];
	$data['customer_firstname']	=	$values['firstname'];
	$data['customer_lastname']	=	$values['lastname'];
	$data['discount_description']	=	$values['discount_description'];
	
	//$data['shipping_incl_tax']	=	$values['shipping_incl_tax'];
	 $csvData[] = $data;
	}
	}
	
	 $_columns = array (
		'Order Date',
		'order_id',
		'sku',
		'name',
		'qty_ordered',
		'qty_invoiced',
		'qty_canceled',
		'qty_shipped',
		'original_price',
		'tax_percent',
		'tax_amount',
		'discount_percent',
		'discount_amount',
		'row_total',
		'status',
		'coupon_code',
		'shipping_amount',
		'customer_email',
		'customer_firstname',
		'customer_lastname',
		'discount_description',

);
$data = array();
// prepare CSV header...
foreach ($_columns as $column) {
       $data[] = $column;
}

	

	$io = new Varien_Io_File();
			$path = Mage::getBaseDir().'/feetsciencereports' . DS;
			$name = "Order-Sales-Report".md5(microtime());
			$csvfile = $path . DS . $name . '.csv';
			$io->setAllowCreateFolders(true);
			$io->open(array('path' => $path));
			$io->streamOpen($csvfile, 'w+');
			$io->streamLock(true);
			$io->streamWriteCsv($data);
		//end
		
	$csvData = $csvData;
	foreach ($csvData as $product) {
    $io->streamWriteCsv($product);
     }
	 	$url  = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'feetsciencereports/' . $name . '.csv';
		$link = '<a href="'.$url.'">Order-Sales-Report.csv</a>';
		Mage::getSingleton('core/session')->addSuccess('Download your CSV File ' . $link);
		$this->_redirect('*/*/');
		
	}

}

?>
