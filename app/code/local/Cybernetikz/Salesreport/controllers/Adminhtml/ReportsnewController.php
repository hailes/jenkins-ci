<?php
class Cybernetikz_Salesreport_Adminhtml_ReportsnewController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Returns generated CSV file
     */
    public function indexAction()
    {
        
        $this->loadLayout();
        $this->_title($this->__("New Stock Report"));
        $this->renderLayout();
        
    }
    
    public function newAction()
    {
        
        $current_date = Date('d-m-Y H:m:s');
        $filename     = $current_date . '.csv';
        $read         = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select       = "SELECT t2.qty as websiteStock,fi.sku as SKU,t3.value as websitePrice,t4.value AS productName, fi.qty as tallyStock, fi.price as tallyPrice,fi.created_at as lastUpdated FROM feetscience_tally_catalog AS fi LEFT JOIN catalog_product_entity as t1 on fi.sku=t1.sku  and t1.type_id='simple' LEFT JOIN cataloginventory_stock_item AS t2 ON t2.product_id = t1.entity_id LEFT JOIN catalog_product_entity_decimal AS t3 ON t3.entity_id = t1.entity_id and t3.attribute_id=75 LEFT JOIN catalog_product_entity_varchar AS t4 ON t4.entity_id = t1.entity_id AND t4.entity_type_id = 4 AND t4.attribute_id = 71";
        
        $read->query($select);
        
        $rows = $read->fetchAll($select);
        
        $csv = new Varien_File_Csv();
        
        $_columns = array(
            'SKU',
            'Product Name',
            'Tally Stock',
            'Tally Price',
            'Website Stock',
            'Website Price',
            'Last Updated'
        );
        
        $data    = array();
        $data1   = array();
        $csvData = array();
        // prepare CSV header...
        /*foreach ($_columns as $column) {
            $data1[] = $column;
        }
        */
        foreach ($rows as $values) {
            $data['SKU']           = $values['SKU'];
            $data['productName']  = $values['productName'];
            $data['tallyStock'] = $values['tallyStock'];
            $data['tallyPrice'] = $values['tallyPrice'];
            $data['websiteStock'] = $values['websiteStock'];
            $data['websitePrice'] = $values['websitePrice'];
            $data['lastUpdated'] = $values['lastUpdated'];
            $csvData[]             = $data;
        }
        array_unshift($csvData, $_columns);
        $csv->saveData($filename, $csvData);
        
        $io      = new Varien_Io_File();
        $path    = Mage::getBaseDir() . '/feetsciencereports' . DS;
        $name    = "Order-Stock-Report" . md5(microtime());
        $csvfile = $path . DS . $name . '.csv';
        $io->setAllowCreateFolders(true);
        $io->open(array(
            'path' => $path
        ));
        $io->streamOpen($csvfile, 'w+');
        $io->streamLock(true);
        $io->streamWriteCsv($data1);
        //end
        
        $csvData = $csvData;
        foreach ($csvData as $product) {
            $io->streamWriteCsv($product);
        }
        $url  = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'feetsciencereports/' . $name . '.csv';
        $link = '<a href="' . $url . '">Order-Stock-Report.csv</a>';
        //$filename = 'Order-Reconcilation-Report.csv';
        //$this->_prepareDownloadResponse($filename, $csvData, 'text/csv');
        Mage::getSingleton('core/session')->addSuccess('Download your CSV File ' . $link);
        $this->_redirect('*/*/');
        
        $this->_prepareDownloadResponse($filename, $content, 'text/csv');
        
    }
    
}

?>
