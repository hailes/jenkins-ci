<?php
/**
*	Author		: 	Cybernetikz
*	Author Email:   info@cybernetikz.com
*	Blog		: 	http://blog.cybernetikz.com
*	Website		: 	http://www.cybernetikz.com
*/

class Cybernetikz_Salesreport_Adminhtml_PackageController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
        $this->loadLayout()->renderLayout();
    }

    protected function _isAllowed()
    {
        if (Mage::getSingleton('admin/session')->isAllowed('admin/report/salesreport/order_reconcilation')) {
            return true;
        }

        return false;
    }
    
	public function packagemanageAction()
    {
        $this->loadLayout()->renderLayout();
    }
	
	public function exportCsvAction()
    {
		if ($data = $this->getRequest()->getPost()) {

			$salesReport = Mage::helper('salesreport');
			$trackHelper = Mage::helper('trackorder');

			$from = $_REQUEST['from'];
			$to = $_REQUEST['to'];
			
			$from_date = date('Y-m-d' . ' 00:00:00', strtotime($from));
			$to_date = date('Y-m-d' . ' 23:59:59', strtotime($to));

			$salesDate = $salesReport->getSalesPackageReport($from_date,$to_date);
	
			$fileName   = time().'.csv';

			 $io = new Varien_Io_File();
                $path = Mage::getBaseDir('var') . DS . 'export' . DS;
                $name = md5(microtime());
                $file = $path . DS . $name . '.csv';
                $io->setAllowCreateFolders(true);
                $io->open(array('path' => $path));
                $io->streamOpen($file, 'w+');
                $io->streamLock(true);
 
                $io->streamWriteCsv($this->_getCsvHeaders($salesDate));
                foreach ($salesDate as $product) {
					$io->streamWriteCsv($product);
                }
               
             $content =  array(
                    'type'  => 'filename',
                    'value' => $file,
                    'rm'    => true 
                );
             
            $this->_prepareDownloadResponse($fileName, $content, 'text/csv');
			
		}
    }

    protected function _getCsvHeaders($products)
    {
        $product = current($products);
		
        $headers = array_keys($product);
		
        return $headers;
    }
	
	
}