<?php

class Cybernetikz_Salesreport_Block_Adminhtml_Reportsnew extends Mage_Adminhtml_Block_Template {
	
	public function __construct() {
    parent::__construct();
    $this->setTemplate('salesreport/reportsnew.phtml');
    $this->setFormAction(Mage::getUrl('*/*/new'));
  }

}
?>
