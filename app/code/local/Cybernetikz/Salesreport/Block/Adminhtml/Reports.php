<?php

class Cybernetikz_Salesreport_Block_Adminhtml_Reports extends Mage_Adminhtml_Block_Template {
	
	public function __construct() 
	{
	    parent::__construct();
	    $this->setTemplate('salesreport/reports.phtml');
	    $this->setFormAction(Mage::getUrl('*/*/new'));
  	}
}
?>
