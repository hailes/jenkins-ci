<?php  

class Cybernetikz_Salesreport_Block_Adminhtml_Report extends Mage_Adminhtml_Block_Template {
	
	public function __construct() {
    parent::__construct();
    $this->setTemplate('salesreport/report.phtml');
    $this->setFormAction(Mage::getUrl('*/*/new'));
  }

}