<?php
// app/code/local/Envato/Customshippingmethod/Model
class Envato_Customshippingmethod_Model_Demo
extends Mage_Shipping_Model_Carrier_Abstract
implements Mage_Shipping_Model_Carrier_Interface
{
  public $_code = 'envato_customshippingmethod';
 
  public function collectRates(Mage_Shipping_Model_Rate_Request $request)
  {
    $result = Mage::getModel('shipping/rate_result');
    $result->append($this->_getDefaultRate());
 
    return $result;
  }
 
  public function getAllowedMethods()
  {
    return array(
      'envato_customshippingmethod' => $this->getConfigData('name'),
    );
  }
 
  protected function _getDefaultRate()
  {
            $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
            $weight = 0;
            foreach($items as $item) {
            $weight += ($item->getWeight() * $item->getQty()) ;
            }
            Mage::getSingleton('core/session')->setTotalWeight($weight);
            $pincode=Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getPostcode(); 
            $quote = Mage::getSingleton('checkout/session')->getQuote();
                $subtotal = 0;
                $items = $quote->getAllItems();
                foreach ($items as $item) {
                    $subtotal += $item->getRowTotalInclTax();
                }
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $table = $resource->getTableName('shipping_tablerate');
            $query = 'SELECT price FROM ' . $table . ' WHERE dest_zip = '. $pincode .' AND condition_value= '. $weight. ' LIMIT 1';
            $price = $readConnection->fetchOne($query);   
            Mage::getSingleton('core/session')->setActualShipping($price);       
            if($subtotal<229)
            {
            $percent=0.30;
            $discountcharge= round($percent*$subtotal);
            Mage::getSingleton('core/session')->setDiscount($discountcharge); 
            $shippingcharge=$price - $discountcharge;
            Mage::getSingleton('core/session')->setShippingch($shippingcharge); 
            }
            elseif ($subtotal>230 && $subtotal<499) {
            $percent=0.25;
            $discountcharge= round($percent*$subtotal);
            Mage::getSingleton('core/session')->setDiscount($discountcharge); 
            $shippingcharge=$price - $discountcharge;
             Mage::getSingleton('core/session')->setShippingch($shippingcharge);
            }
            elseif ($subtotal>500 && $subtotal<599) {
            $percent=0.20;
            $discountcharge= round($percent*$subtotal);
            Mage::getSingleton('core/session')->setDiscount($discountcharge); 
            $shippingcharge=$price - $discountcharge;
            Mage::getSingleton('core/session')->setShippingch($shippingcharge);
            }
            else{
            $percent=0.15;
            $discountcharge= round($percent*$subtotal);
            Mage::getSingleton('core/session')->setDiscount($discountcharge); 
            $shippingcharge=$price - $discountcharge;
            Mage::getSingleton('core/session')->setShippingch($shippingcharge);
            } 
            if($shippingcharge<0)
            {
            $finalshippingcharge=0.00;
            }
            else
            {
            $finalshippingcharge=$shippingcharge;
            }
            $rate = Mage::getModel('shipping/rate_result_method');     
            $rate->setCarrier($this->_code);
            $rate->setCarrierTitle($this->getConfigData('title'));
            $rate->setMethod($this->_code);
            $rate->setMethodTitle($this->getConfigData('name'));
            $rate->setPrice(round($finalshippingcharge));
            $rate->setCost(0);

            return $rate;
  }
  
}