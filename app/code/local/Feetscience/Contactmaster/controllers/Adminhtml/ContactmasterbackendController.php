<?php
class Feetscience_Contactmaster_Adminhtml_ContactmasterbackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('contactmaster/contactmasterbackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Contact"));
	   $this->renderLayout();
    }
}