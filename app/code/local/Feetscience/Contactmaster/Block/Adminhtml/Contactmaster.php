<?php


class Feetscience_Contactmaster_Block_Adminhtml_Contactmaster extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_contactmaster";
	$this->_blockGroup = "contactmaster";
	$this->_headerText = Mage::helper("contactmaster")->__("Contact Manager");
	$this->_addButtonLabel = Mage::helper("contactmaster")->__("Add New Item");
	parent::__construct();
	
	}

}