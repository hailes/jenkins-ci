<?php

class Feetscience_Contactmaster_Block_Adminhtml_Contactmaster_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("contactmasterGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("contactmaster/contactmaster")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("contactmaster")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("name", array(
				"header" => Mage::helper("contactmaster")->__("name"),
				"index" => "name",
				));
				$this->addColumn("subject", array(
				"header" => Mage::helper("contactmaster")->__("subject"),
				"index" => "subject",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("contactmaster")->__("email"),
				"index" => "email",
				));
				
				$this->addColumn("contact_number", array(
				"header" => Mage::helper("contactmaster")->__("mobile"),
				"index" => "contact_number",
				));
				$this->addColumn("website", array(
				"header" => Mage::helper("contactmaster")->__("website"),
				"index" => "website",
				));
				$this->addColumn("city", array(
				"header" => Mage::helper("contactmaster")->__("city"),
				"index" => "city",
				));
				$this->addColumn("state", array(
				"header" => Mage::helper("contactmaster")->__("state"),
				"index" => "state",
				));
				$this->addColumn("country", array(
				"header" => Mage::helper("contactmaster")->__("country"),
				"index" => "country",
				));
				
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_contactmaster', array(
					 'label'=> Mage::helper('contactmaster')->__('Remove Contactmaster'),
					 'url'  => $this->getUrl('*/adminhtml_contactmaster/massRemove'),
					 'confirm' => Mage::helper('contactmaster')->__('Are you sure?')
				));
			return $this;
		}
			

}