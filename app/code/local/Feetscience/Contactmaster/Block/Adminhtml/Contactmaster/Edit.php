<?php
	
class Feetscience_Contactmaster_Block_Adminhtml_Contactmaster_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "contactmaster";
				$this->_controller = "adminhtml_contactmaster";
				$this->_updateButton("save", "label", Mage::helper("contactmaster")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("contactmaster")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("contactmaster")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("contactmaster_data") && Mage::registry("contactmaster_data")->getId() ){

				    return Mage::helper("contactmaster")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("contactmaster_data")->getId()));

				} 
				else{

				     return Mage::helper("contactmaster")->__("Add Item");

				}
		}
}