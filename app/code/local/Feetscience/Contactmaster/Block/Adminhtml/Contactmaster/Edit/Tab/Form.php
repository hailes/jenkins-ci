<?php
class Feetscience_Contactmaster_Block_Adminhtml_Contactmaster_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("contactmaster_form", array("legend"=>Mage::helper("contactmaster")->__("Item information")));

				
						/*$fieldset->addField("id", "text", array(
						"label" => Mage::helper("contactmaster")->__("id"),
						"name" => "id",
						));*/
						$fieldset->addField("subject", "select", array(
						"label" => Mage::helper("contactmaster")->__("Subject"),
						"name" => "subject",
						 'options' => $this->getSubjectoptions(),
						));
						$fieldset->addField("caption", "select", array(
						"label" => Mage::helper("contactmaster")->__("I am a "),
						"name" => "caption",
						 'options' => $this->getCaptionoptions(),
						));
						
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("contactmaster")->__("Name"),
						"name" => "name",
						 'required' => true,
						));
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("contactmaster")->__("Email"),
						"name" => "email",
						 'required' => true,
						));
						
						
						$fieldset->addField("image", "file", array(
						"label" => Mage::helper("contactmaster")->__("image"),
						"name" => "image",
						));
					
						$fieldset->addField("contact_number", "text", array(
						"label" => Mage::helper("contactmaster")->__("Contact Number"),
						"name" => "contact_number",
						 'required' => true,
						));
					
						$fieldset->addField("website", "text", array(
						"label" => Mage::helper("contactmaster")->__("website"),
						"name" => "website",
						));
					
						$fieldset->addField("city", "text", array(
						"label" => Mage::helper("contactmaster")->__("city"),
						"name" => "city",
						 'required' => true,
						));
					
						$fieldset->addField("state", "text", array(
						"label" => Mage::helper("contactmaster")->__("state"),
						"name" => "state",
						 'required' => true,
						));

						$fieldset->addField("country", "text", array(
						"label" => Mage::helper("contactmaster")->__("country"),
						"name" => "country",
						 'required' => true,
						));
					
						$fieldset->addField("message", "textarea", array(
						"label" => Mage::helper("contactmaster")->__("Message"),
						"name" => "message",
						 'required' => true,
						));
					

				if (Mage::getSingleton("adminhtml/session")->getContactmasterData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getContactmasterData());
					Mage::getSingleton("adminhtml/session")->setContactmasterData(null);
				} 
				elseif(Mage::registry("contactmaster_data")) {
				    $form->setValues(Mage::registry("contactmaster_data")->getData());
				}
				return parent::_prepareForm();
		}
		public function getSubjectoptions()
    {
        return array('complaints'=>'complaints','suggestions'=>'suggestions','business_enquiry'=>'Business Enquiry','Export Enquiries'=>'Export Enquiries','Dealership Enquiries'=>'Dealership Enquiries','Order Related Queries'=>'Order Related Queries');
           
    }
	public function getCaptionoptions()
    {
        return array('retailer'=>'Retailer','loyal_customer'=>'Loyal Customer');
           
    }
}
