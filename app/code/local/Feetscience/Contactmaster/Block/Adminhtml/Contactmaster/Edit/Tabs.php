<?php
class Feetscience_Contactmaster_Block_Adminhtml_Contactmaster_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("contactmaster_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("contactmaster")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("contactmaster")->__("Item Information"),
				"title" => Mage::helper("contactmaster")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("contactmaster/adminhtml_contactmaster_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
