<?php 

/*
 * Core cart controller extend for special coupon code
 *
 * @method couponPostAction()
 * @category    Feetscience
 * @package     Feetscience_Couponmapping
 * @author      Amar Dhuri
 */

require_once 'Mage/Checkout/controllers/CartController.php';
class Feetscience_Couponmapping_CartController extends Mage_Checkout_CartController
{
	## Apply coupon code and set the flag for special coupon code discount.
    public function couponPostAction() {

    	/**
         * No reason continue with empty shopping cart
         */
        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $this->_getQuote()->getCouponCode()) {

                	/// get special discout category ids
					$specialDiscountCategoryIds = explode(",", Mage::getStoreConfig('coupon_mapping/general/category_ids')) ;
					
					// check coupon code category mapping
					$category = Mage::getModel('feetscience_couponmapping/couponmapping')->getCollection()
										->addFieldToSelect('category_id')
										->addFieldToFilter('coupon_code',$couponCode)
										->getData();

					if( isset($category[0]['category_id']) && in_array($category[0]['category_id'], $specialDiscountCategoryIds) )
						Mage::getSingleton('core/session')->setSpecialCategoryDiscount(true) ;
					else
						Mage::getSingleton('core/session')->setSpecialCategoryDiscount(false) ;

                    $this->_getSession()->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                } else {
                    $this->_getSession()->addError(
                        $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                }
            } else {
            	// unset special discount category flag
				Mage::getSingleton('core/session')->setSpecialCategoryDiscount(false) ;

                $this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }

        $this->_goBack();

    }
}