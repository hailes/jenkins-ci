<?php

require_once 'Mage/Customer/controllers/AccountController.php';
class Feetscience_Couponmapping_AccountController extends Mage_Customer_AccountController
{
	public function logoutAction()
    {
    	Mage::getSingleton('core/session')->unsCouponCode();
    	Mage::getSingleton('core/session')->unsCategoryId();
    	Mage::getSingleton('core/session')->unsParentCategoryId();
    	Mage::getSingleton('core/session')->unsCategoryUrl();
        $this->_getSession()->logout()
            ->renewSession();
            
        $this->_redirect('*/*/logoutSuccess');
    } 
}