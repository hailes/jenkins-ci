<?php

class Feetscience_Couponmapping_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');

		/// get special discout category ids
		$specialDiscountCategoryIds = explode(",", Mage::getStoreConfig('coupon_mapping/general/category_ids')) ;
		
		$code = trim($this->getRequest()->getPost('code'));
		$customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
		$category = Mage::getModel('feetscience_couponmapping/couponmapping')->getCollection()
						->addFieldToSelect('category_id')
						->addFieldToFilter('coupon_code',$code)
						->getData();
		if(!empty($category))
		{
			//check coupon code validaty
			$couponData = Mage::getModel('salesrule/coupon')->getCollection()
							->addFieldToSelect(array('expiration_date','usage_limit','times_used','usage_per_customer','coupon_id'))
							->addFieldToFilter('code',$code)
							->getData();
			if(!empty($couponData))
			{
				if($couponData[0]['usage_limit'] != 0 && $couponData[0]['times_used'] >= $couponData[0]['usage_limit'])
				{
					echo '3';
				}else{
					if($customerId)
					{
						$query = "select * from ".$resource->getTableName('salesrule/coupon_usage')." where customer_id=".$customerId." and coupon_id=".$couponData[0]['coupon_id'];
						$result = $readConnection->fetchAll($query);
						$resultCnt = count($result);
						if($resultCnt >= $couponData[0]['usage_per_customer'])
						{
							echo '3';
						}else{
							$nowDate = date("Y-m-d h:i:s");
							$expireDate = $couponData[0]['expiration_date'];
							if($nowDate > $expireDate)
							{	
								echo '2';
							}else{
								$categoryId = $category[0]['category_id'];
								$categoryData = Mage::getModel('catalog/category')->load($categoryId);
								$categoryUrl = $categoryData->getUrl();
								$parentId = $categoryData->getParentId();

								Mage::getSingleton('core/session')->setCouponCode($code);
								Mage::getSingleton('core/session')->setCategoryId($categoryId);
								Mage::getSingleton('core/session')->setParentCategoryId($parentId);
								Mage::getSingleton('core/session')->setCategoryUrl($categoryUrl);

								if( in_array($categoryId, $specialDiscountCategoryIds) )
									Mage::getSingleton('core/session')->setSpecialCategoryDiscount(true) ;
								else
									Mage::getSingleton('core/session')->setSpecialCategoryDiscount(false) ;

								echo $categoryUrl;
							}
						}
					}else{
						$nowDate = date("Y-m-d h:i:s");
						$expireDate = $couponData[0]['expiration_date'];
						if($nowDate > $expireDate)
						{
							echo '2';
						}else{
							$categoryId = $category[0]['category_id'];
							$categoryData = Mage::getModel('catalog/category')->load($categoryId);
							$categoryUrl = $categoryData->getUrl();
							$parentId = $categoryData->getParentId();

							Mage::getSingleton('core/session')->setCouponCode($code);
							Mage::getSingleton('core/session')->setCategoryId($categoryId);
							Mage::getSingleton('core/session')->setParentCategoryId($parentId);
							Mage::getSingleton('core/session')->setCategoryUrl($categoryUrl);

							if( in_array($categoryId, $specialDiscountCategoryIds) )
								Mage::getSingleton('core/session')->setSpecialCategoryDiscount(true) ;
							else
								Mage::getSingleton('core/session')->setSpecialCategoryDiscount(false) ;

							echo $categoryUrl;
						}
					}
				}
			}else{
				echo '1';
			}
		}else{
			echo '1';
		}
	}
}