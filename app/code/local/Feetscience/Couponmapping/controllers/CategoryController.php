<?php

require_once 'Mage/Catalog/controllers/CategoryController.php';

class Feetscience_Couponmapping_CategoryController extends Mage_Catalog_CategoryController
{
	protected function _initCatagory()
    {
    	$couponCategoryId = Mage::getSingleton('core/session')->getCategoryId();
    	
        $categoryId = (int) $this->getRequest()->getParam('id', false);	
    	$categoryData = Mage::getModel('catalog/category')->load($categoryId)->getParentId();
    	
    	Mage::getSingleton('core/session')->setParentCategoryId($categoryData);
    	$parentId = Mage::getSingleton('core/session')->getParentCategoryId();

        Mage::dispatchEvent('catalog_controller_category_init_before', array('controller_action' => $this));


        if($couponCategoryId && $parentId==3)
        {
        	$categoryId = $couponCategoryId;
        }else{
        	$categoryId = (int) $this->getRequest()->getParam('id', false);	
        }

        if (!$categoryId) {
            return false;
        }

        $category = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($categoryId);

        if (!Mage::helper('catalog/category')->canShow($category)) {
            return false;
        }
        Mage::getSingleton('catalog/session')->setLastVisitedCategoryId($category->getId());
        Mage::register('current_category', $category);
        Mage::register('current_entity_key', $category->getPath());

        try {
            Mage::dispatchEvent(
                'catalog_controller_category_init_after',
                array(
                    'category' => $category,
                    'controller_action' => $this
                )
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return false;
        }

        return $category;
    }
}