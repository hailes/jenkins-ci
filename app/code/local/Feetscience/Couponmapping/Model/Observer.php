<?php

class Feetscience_Couponmapping_Model_Observer extends Varien_Object
{
	public function applyCoupon(Varien_Event_Observer $observer)
	{
		$response = $observer->getResponse();
		$request = $observer->getRequest();
		$coupon = Mage::getSingleton('core/session')->getCouponCode();
		if(!$coupon || !strlen($coupon))
		{
			return;
		}
		
		$cart = Mage::getSingleton('checkout/cart')->getQuote();
		$cart->getShippingAddress()->setCollectShippingRates(true);
		$cart->setCouponCode(strlen($coupon) ? $coupon : '')
			->collectTotals()
			->save();
		
	}
}