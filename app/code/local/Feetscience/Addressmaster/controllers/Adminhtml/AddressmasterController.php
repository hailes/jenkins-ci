<?php

class Feetscience_Addressmaster_Adminhtml_AddressmasterController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
		//return Mage::getSingleton('admin/session')->isAllowed('addressmaster/addressmaster');
			return true;
		}

		protected function _initAction()
		{
			$this->loadLayout()->_setActiveMenu("contactmaster/contactmaster")->_addBreadcrumb(Mage::helper("adminhtml")->__("Contactmaster  Manager"),Mage::helper("adminhtml")->__("Contactmaster Manager"));	
				//$this->_setActiveMenu("contactmaster/contactmaster");
				//$this->loadLayout()->_setActiveMenu("addressmaster/addressmaster")->_addBreadcrumb(Mage::helper("adminhtml")->__("Addressmaster  Manager"),Mage::helper("adminhtml")->__("Addressmaster Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Email Address"));
			    $this->_title($this->__("Manager Email Address"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Addressmaster"));
				$this->_title($this->__("Addressmaster"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("addressmaster/addressmaster")->load($id);
				if ($model->getId()) {
					Mage::register("addressmaster_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("addressmaster/addressmaster");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addressmaster Manager"), Mage::helper("adminhtml")->__("Addressmaster Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addressmaster Description"), Mage::helper("adminhtml")->__("Addressmaster Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("addressmaster/adminhtml_addressmaster_edit"))->_addLeft($this->getLayout()->createBlock("addressmaster/adminhtml_addressmaster_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("addressmaster")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Addressmaster"));
		$this->_title($this->__("Addressmaster"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("addressmaster/addressmaster")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("addressmaster_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("addressmaster/addressmaster");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addressmaster Manager"), Mage::helper("adminhtml")->__("Addressmaster Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Addressmaster Description"), Mage::helper("adminhtml")->__("Addressmaster Description"));


		$this->_addContent($this->getLayout()->createBlock("addressmaster/adminhtml_addressmaster_edit"))->_addLeft($this->getLayout()->createBlock("addressmaster/adminhtml_addressmaster_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("addressmaster/addressmaster")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Addressmaster was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setAddressmasterData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setAddressmasterData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("addressmaster/addressmaster");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("addressmaster/addressmaster");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'addressmaster.csv';
			$grid       = $this->getLayout()->createBlock('addressmaster/adminhtml_addressmaster_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'addressmaster.xml';
			$grid       = $this->getLayout()->createBlock('addressmaster/adminhtml_addressmaster_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
