<?php
class Feetscience_Addressmaster_Block_Adminhtml_Addressmaster_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("addressmaster_form", array("legend"=>Mage::helper("addressmaster")->__("Item information")));
				
				 $regionsCollection = Mage::getResourceModel('directory/region_collection')->addCountryFilter('IN')->load();
				 
		$state = array();
        foreach($regionsCollection as $region) {
			$state[$region['default_name']]=$region['default_name'];
        }
		
						
					
						$fieldset->addField("subject", "select", array(
						"label" => Mage::helper("addressmaster")->__("Subject"),
						"name" => "subject",
						 'options' => $this->getSubjectoptions(),
						));
						$fieldset->addField("state", "select", array(
						"label" => Mage::helper("addressmaster")->__("State"),
						"name" => "state",
						 'options' => $this->getStateoptions(),
						));
						
					
						$fieldset->addField("email_to", "textarea", array(
						"label" => Mage::helper("addressmaster")->__("Email To"),
						"name" => "email_to",
						));
					
						
					

				if (Mage::getSingleton("adminhtml/session")->getAddressmasterData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getAddressmasterData());
					Mage::getSingleton("adminhtml/session")->setAddressmasterData(null);
				} 
				elseif(Mage::registry("addressmaster_data")) {
				    $form->setValues(Mage::registry("addressmaster_data")->getData());
				}
				return parent::_prepareForm();
		}
		public function getSubjectoptions()
    {
        return array('complaints'=>'complaints','suggestions'=>'suggestions','business_enquiry'=>'Business Enquiry','Export Enquiries'=>'Export Enquiries','Dealership Enquiries'=>'Dealership Enquiries','Order Related Queries'=>'Order Related Queries');
           
    }
	public function getStateoptions()
    {
		$regionsCollection = Mage::getResourceModel('directory/region_collection')->addCountryFilter('IN')->load();
				 
		$state = array();
		$other = array();
        foreach($regionsCollection as $region) {
			$state[$region['default_name']]=$region['default_name'];
        }
		$state['other'] .= 'other';
		//array_push($state, $other);
        return $state;
           
    }
		

}
