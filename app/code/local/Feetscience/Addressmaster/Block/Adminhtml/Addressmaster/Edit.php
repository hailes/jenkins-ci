<?php
	
class Feetscience_Addressmaster_Block_Adminhtml_Addressmaster_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "addressmaster";
				$this->_controller = "adminhtml_addressmaster";
				$this->_updateButton("save", "label", Mage::helper("addressmaster")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("addressmaster")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("addressmaster")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("addressmaster_data") && Mage::registry("addressmaster_data")->getId() ){

				    return Mage::helper("addressmaster")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("addressmaster_data")->getId()));

				} 
				else{

				     return Mage::helper("addressmaster")->__("Add Item");

				}
		}
}