<?php
class Feetscience_Addressmaster_Block_Adminhtml_Addressmaster_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("addressmaster_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("addressmaster")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("addressmaster")->__("Item Information"),
				"title" => Mage::helper("addressmaster")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("addressmaster/adminhtml_addressmaster_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
