<?php

class Feetscience_Addressmaster_Block_Adminhtml_Addressmaster_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("addressmasterGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("addressmaster/addressmaster")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("addressmaster")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("subject", array(
				"header" => Mage::helper("addressmaster")->__("Subject"),
				"index" => "subject",
				));
				$this->addColumn("state", array(
				"header" => Mage::helper("addressmaster")->__("State"),
				"index" => "state",
				));
				$this->addColumn("created_time", array(
				"header" => Mage::helper("addressmaster")->__("created_time"),
				"index" => "created_time",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_addressmaster', array(
					 'label'=> Mage::helper('addressmaster')->__('Remove Addressmaster'),
					 'url'  => $this->getUrl('*/adminhtml_addressmaster/massRemove'),
					 'confirm' => Mage::helper('addressmaster')->__('Are you sure?')
				));
			return $this;
		}
			

}