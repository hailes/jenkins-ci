<?php


class Feetscience_Addressmaster_Block_Adminhtml_Addressmaster extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_addressmaster";
	$this->_blockGroup = "addressmaster";
	$this->_headerText = Mage::helper("addressmaster")->__("Addressmaster Manager");
	$this->_addButtonLabel = Mage::helper("addressmaster")->__("Add New Item");
	parent::__construct();
	
	}

}