<?php 

class Feetscience_Productmaster_Adminhtml_ProductmasterController extends Mage_Adminhtml_Controller_action
{
	public function indexAction()
	{
		$this->loadLayout()->renderLayout();

	}

	public function addAction()
	{
		$this->loadLayout()->renderLayout();

		//create directory for storing xml file
		$storeXmlPath = $this->createDirectory();

		//generate xml file and store to the directory
		$this->generateXml($storeXmlPath);
		
		//chnage the permission of the xml file
		chmod($storeXmlPath."/ProductMaster.xml",0777);

	
		
	}

	public function createDirectory()
	{
		//get path of the TallyData directory and create sub directory
		$tallyDataPath = Mage::getBaseDir().'/TallyData';
		if(!file_exists($tallyDataPath))
		{
			mkdir($tallyDataPath,0777,true);
		}

		$productPath = $tallyDataPath."/Product";
		if(!file_exists($productPath))
		{
			mkdir($productPath,0777,true);
		}

		//get current year
		$year = date('Y');
		$yearPath = $productPath."/".$year;
		if(!file_exists($yearPath))
		{
			mkdir($yearPath,0777,true);
		}

		//get current month
		$month = date('M');
		$monthPath = $yearPath."/".$month;
		if(!file_exists($monthPath))
		{
			mkdir($monthPath,0777,true);
		}

		$day = date('d');
		$dayPath = $monthPath."/".$day;
		if(!file_exists($dayPath))
		{
			mkdir($dayPath,0777,true);
		}
		return $dayPath;
	}

	public function generateXml($storeXmlPath)
	{
		//read xml data from the Tally API and create xml file
		Header('Content-type: text/xml');
		$myXMLData = "http://115.249.0.195:8009/TallyAPI/Master/stockItem";
		$xml=simplexml_load_file($myXMLData) or die("Error: Cannot create object");

		$newxml = new SimpleXMLElement("<STOCKTABLE></STOCKTABLE>");
		foreach($xml as $key)
		{
			$stockitem = $newxml->addChild('STOCKITEM');
			$sku = $stockitem->addChild('STOCKSKU',$key->STOCKSKU);
			$name = $stockitem->addChild('PROUDR',$key->PROUDR);
			$brand = $stockitem->addChild('BRAND',$key->BRAND);
			$articleno = $stockitem->addChild('ARTICALNO',$key->ARTICALNO);
			$short_description = $stockitem->addChild('ATTDESCT',$key->ATTDESCT);
			$color = $stockitem->addChild('COLOR',$key->COLOR);
			$colorcode = $stockitem->addChild('COLORCODE',$key->COLORCODE);
			$size = $stockitem->addChild('SIZE',$key->SIZE);
			$bchcode = $stockitem->addChild('BCHCODE',$key->BCHCODE);
			$variant = $stockitem->addChild('VARIANT',$key->VARIANT);
			$dept = $stockitem->addChild('DEPARTMENT',$key->DEPARTMENT);
			$description = $stockitem->addChild('PRODUCTDESCRIP',$key->PRODUCTDESCRIP);
			$type = $stockitem->addChild('TYPE',$key->TYPE);
			$modbisness = $stockitem->addChild('MODEBUSSIN',$key->MODEBUSSIN);
			$vendor = $stockitem->addChild('VENDOR',$key->VENDOR);
			$lethertype = $stockitem->addChild('LETHERTYPE',$key->LETHERTYPE);
			$group = $stockitem->addChild('GROUP',$key->GROUP);
			$articalname = $stockitem->addChild('ARTICALNAME',$key->ARTICALNAME);
			$minstkli = $stockitem->addChild('MINSTKLI',$key->MINSTKLI);
			$artcode = $stockitem->addChild('ARTCODE',$key->ARTCODE);
			$vrtcode = $stockitem->addChild('VRTCODE',$key->VRTCODE);
			$qty = $stockitem->addChild('QTY',$key->QTY);
			$mrp = $stockitem->addChild('MRP',$key->MRP);
			$category = $stockitem->addChild('CATOGRY',$key->CATOGRY);
			$weight = $stockitem->addChild('WEIGHT',$key->WEIGHT);
			$categoryid = $stockitem->addChild('CATEGORYID',$key->CATEGORYID);
			$excisetarcode = $stockitem->addChild('EXCISETARCODE',$key->EXCISETARCODE);
			$img1 = $stockitem->addChild('IMGLINK1',$key->IMGLINK1);
			$img2 = $stockitem->addChild('IMGLINK2',$key->IMGLINK2);
			$img3 = $stockitem->addChild('IMGLINK3',$key->IMGLINK3);
			$img4 = $stockitem->addChild('IMGLINK4',$key->IMGLINK4);
			$img5 = $stockitem->addChild('IMGLINK5',$key->IMGLINK5);
			$img6 = $stockitem->addChild('IMGLINK6',$key->IMGLINK6);
		}
		$newxml->asXML($storeXmlPath."/ProductMaster.xml");
		echo $sku;
		// $sendProductMasterPath =Mage::helper("adminhtml")->getUrl('productmaster/adminhtml_productmaster/index');
	 // 	Mage::app()->getResponse()->setRedirect($sendProductMasterPath);
	}

	


}