<?php

class Feetscience_Productmaster_Block_Adminhtml_Productmaster extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_productmaster';
		$this->_blockGroup = 'productmaster';
		$this->_headerText = Mage::helper('productmaster')->__('Manual Product Management');

		$data = array(
				'label' => 'Product Master',
				'class' => 'some-class',
				'onclick' => 'setLocation (\' ' .Mage::helper('adminhtml')->geturl('productmaster/adminhtml_productmaster/add'). '\')',
			);
			Mage_Adminhtml_Block_Widget_Container::addButton('my_button_identifier',$data);
			parent::__construct();
			$this->_removeButton('add');
	}
}