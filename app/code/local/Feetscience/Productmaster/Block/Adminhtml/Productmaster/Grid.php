<?php

class Feetscience_Productmaster_Block_Adminhtml_Productmaster_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();

		$this->setDefaultSort('id');
		$this->setId('productmasterGrid');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}
}