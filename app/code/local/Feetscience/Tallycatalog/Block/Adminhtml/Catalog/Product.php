<?php
class Feetscience_Tallycatalog_Block_Adminhtml_Catalog_Product extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'feetscience_tallycatalog';
        $this->_controller = 'adminhtml_catalog_product';
        $this->_headerText = Mage::helper('catalog')->__('Tally Catalog Sync');
        
        $data = array(
               'label' 		=>  'Sync Catalog',
               'onclick'   	=> "setLocation('". $this->getUrl('*/*/catalogsync') ."')"
               );
       	Mage_Adminhtml_Block_Widget_Container::addButton('sync_catalog', $data, 0, 100,  'header', 'header');

        parent::__construct();

        $this->_removeButton('add');
    }
}