<?php
class Feetscience_Tallycatalog_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('feetscience_tallycatalog_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        // $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('tallycatalog/tallycatalog')->getCollection() ;
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('catalog');
        $currency = (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);

        $this->addColumn('id', array(
            'header' => $helper->__('#'),
            'index'  => 'id',
            'filter' => false,
            'sortable'   => false
        ));
        $this->addColumn('sku', array(
            'header' => $helper->__('Sku'),
            'index'  => 'sku',
            'filter' => false,
            'sortable'   => false
        ));
        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index'  => 'name',
            'filter' => false,
            'sortable'   => false
        ));
        $this->addColumn('qty', array(
            'header' => $helper->__('Qty'),
            'index'  => 'qty',
            'filter' => false,
            'sortable'   => false
        ));
        $this->addColumn('price', array(
            'header' => $helper->__('Price'),
            'index'  => 'price',
            'filter' => false,
            'sortable'   => false
        ));
        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index'  => 'status',
            'filter' => false,
            'sortable'   => false
        ));
        $this->addColumn('error_message', array(
            'header' => $helper->__('Message'),
            'index'  => 'error_message',
            'filter' => false,
            'sortable'   => false
        ));
        $this->addColumn('created_at', array(
            'header' => $helper->__('Created On'),
            'type'   => 'datetime',
            'index'  => 'created_at',
            'filter' => false,
            'sortable'   => false
        ));
        
        $this->addExportType('*/*/exportCsv', $helper->__('CSV'));
        $this->addExportType('*/*/exportExcel', $helper->__('Excel XML'));
        return parent::_prepareColumns();
    }

    public function getMainButtonsHtml() {
        return '';
    }

    public function getGridUrl()
    {
        return '';
    }
}