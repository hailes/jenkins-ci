<?php
class Feetscience_Tallycatalog_Adminhtml_CatalogController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Catalog'))->_title($this->__('Tally Catalog Sync'));
        $this->loadLayout();
        $this->_setActiveMenu('catalog/catalog');
        $this->_addContent($this->getLayout()->createBlock('feetscience_tallycatalog/adminhtml_catalog_product'));
        $this->renderLayout();
    }

    public function catalogsyncAction() {
        Mage::helper('feetscience_tallycatalog')->syncCatalog() ;

        Mage::getSingleton('adminhtml/session')->addSuccess("Tally catalog sync is done. Please review report for more details.");
        $this->_redirect('*/*/index');
    }
    

    public function exportCsvAction()
    {
        $fileName = 'tally_catalog_sync.csv';
        $grid = $this->getLayout()->createBlock('feetscience_tallycatalog/adminhtml_catalog_product_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
    public function exportExcelAction()
    {
        $fileName = 'tally_catalog_sync.xml';
        $grid = $this->getLayout()->createBlock('feetscience_tallycatalog/adminhtml_catalog_product_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}