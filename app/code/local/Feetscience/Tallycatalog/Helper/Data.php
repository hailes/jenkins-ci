<?php
 
class Feetscience_Tallycatalog_Helper_Data extends Mage_Core_Helper_Abstract {

	public function syncCatalog() {

        try {
				

                Mage::log('Tally catalog sync start.', null, 'tally_catalog.log');
                $skip_indexing = array('catalog_url','tag_summary') ;

                // pull catalog from tally
               $filename = $this->pulltallycatalog() ;
                Mage::log('API call successful..', null, 'tally_catalog.log');
								
                // db connection string for read and write
                $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                $write      = Mage::getSingleton('core/resource')->getConnection('core_write');

                // Pull data from temp table
                $select             = $connection->select()->from('feetscience_tally_catalog', array('*'));                     
                $tally_products     = $connection->fetchAll($select);
                Mage::log('Pull data from temp table.', null, 'tally_catalog.log');

                // check any products returned from API..
                if( count($tally_products) ) {

                    // set re-indexing to manual mode
                    $indexCollection = Mage::getModel('index/process')->getCollection();
                    /*foreach ($indexCollection as $index) {
                    	if( !in_array($index->getData('indexer_code'), $skip_indexing) ) :
                    		$index->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
                    	endif;
                    }*/
                    Mage::log('Update indexing to manual mode.', null, 'tally_catalog.log');
                    
                    // make products out of stock..
                   // $products = Mage::getModel('catalog/product')->getCollection();

                    // disable foraign key check..
                    /*$write->query("SET FOREIGN_KEY_CHECKS = 0;") ;

                    foreach($products as $product) :
                        if( $product->getTypeId() == "simple" ) :
                            $product_id = $product->getId();

                            /*$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_id);
                            if ($stockItem->getId() > 0 and $stockItem->getManageStock()) :
                                $qty = 0;
                                $stockItem->setQty($qty);
                                $stockItem->setIsInStock('0');
                                $stockItem->save();
                                unset($stockItem);
        						//unset($product);
                            endif;*/

                            //$qty_sql = "UPDATE cataloginventory_stock_item SET qty = '". $qty ."', is_in_stock = '0' WHERE product_id = '" . $product_id . "'" ;

                           // $write->query($qty_sql) ;
                       // endif;
                   // endforeach;
						// empty the all products

						$stock_empty_sql = "UPDATE cataloginventory_stock_item item_stock JOIN cataloginventory_stock_status status_stock
						JOIN catalog_product_entity c1 SET item_stock.qty = '0', item_stock.is_in_stock = 0,
						status_stock.qty = '0', status_stock.stock_status = 0 WHERE 
						item_stock.product_id = c1.`entity_id` AND  status_stock.product_id = c1.`entity_id` 
						AND item_stock.product_id = status_stock.product_id AND c1.`type_id`!='configurable'";

						$connection->query($stock_empty_sql);
                    Mage::log('All products out of stock.', null, 'tally_catalog.log');
                   
				    foreach ($tally_products as $tally_product)
					{
						$arr[] = $tally_product['sku'];
					}
				   $list =array();
                   $nolist = [];
				   foreach($arr as $sku_value)
				   {
					$sku = $sku_value;
					$id = Mage::getModel('catalog/product')->getIdBySku($sku);
					if ($id) {
					$list[] .= $sku;
					}
					else {
					//sku does not exist
					$nolist[] .= $sku;
					}
				   }
				   				   
                    // update catalog information received from tally API
					
					
					$media = Mage::getBaseDir('media');
		$websiteId = Mage::app()->getStore(true)->getWebsiteId();
		$storeId = Mage::app()->getStore()->getStoreId();
		$visibility = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
		$date = Mage::getModel('core/date')->date('Y-m-d');
$sDefaultAttributeSetId = Mage::getSingleton('eav/config')->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getDefaultAttributeSetId();
		// magmi script to import products
		require_once ( Mage::getBaseDir() . "/magmi/inc/magmi_defs.php");
		require_once ( Mage::getBaseDir() . "/magmi/integration/inc/magmi_datapump.php");
		require_once ( Mage::getBaseDir() . "/magmi/integration/samples/logger.php");

		// magmi import object.
		$dp = Magmi_DataPumpFactory::getDataPumpInstance("productimport");
		$dp->beginImportSession("import_products", "create", new TestLogger());
		
		
					
                    foreach ($tally_products as $tally_product) :
					
					if (in_array($tally_product['sku'], $list))
					{

											
					
					
						//$_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$tally_product['sku']);
//$_product = Mage::getModel('catalog/product')->loadByAttribute( $tally_product['sku'],'sku');
						
                       //$pid = Mage::getModel('catalog/product')->getIdBySku($tally_product['sku']);
							//if(isset($_product)):
                            //if( $_product->getTypeId() == "simple" ) :

                                try {
									
                                    // $product_id = $_product->getId();
									
									
                                     $qty = $tally_product['qty'];
									
                                    $price = $tally_product['price'] ;
									$sku_value = $tally_product['sku'];
									 $isInStock      = $qty > 0 ? 1 : 0;
										 $stockStatus    = $qty > 0 ? 1 : 0;
										 $data['qty'] = $qty;	
										 $data['website_ids'] = array($websiteId);
					$data['store_id'] = $storeId;
					$data['attribute_set_id'] = $sDefaultAttributeSetId;
					$data['type_id'] = 'simple';
					$data['created_at'] = strtotime('now');
					$data['updated_at'] = strtotime('now');
					$data['sku'] = $tally_product['sku'];
					$data['price'] = $tally_product['price'];
					$data['is_in_stock'] = $isInStock;
						$_product_data = $data;				
										$dp->ingest($_product_data);
										$status=1;
				 Mage::log('Imported - ' . $_product_data['sku']. ' Status: '. $status,NULL,"import.log", true);
				  $write->query("UPDATE feetscience_tally_catalog SET status = '1', error_message = 'Updated successfully' WHERE id = " . $tally_product['id']);
				
			} catch(Exception $e) {
				Mage::log('Error: ' . $e->getMessage(), null, "import.log");
			 $write->query("UPDATE feetscience_tally_catalog SET status = '0', error_message = 'SKU does not exists.' WHERE id = " . $tally_product['id']);
			}
										
				
                           // endif;

                       // else:
                           
                       // endif;
					}
                    endforeach;
				$dp->endImportSession();
				
				
                    // disable foraign key check..
                    $write->query("SET FOREIGN_KEY_CHECKS = 1;") ;

                    Mage::log('Updated products based on API response.', null, 'tally_catalog.log');

                    // run re-indexing
                   /* foreach ($indexCollection as $index) {
                    	if( !in_array($index->getData('indexer_code'), $skip_indexing) ) :
                        	$index->reindexAll();
                        endif;
                    }*/
                    Mage::log('Re-index data after product update.', null, 'tally_catalog.log');

                    // set indexing to automatic mode..
                    /*foreach ($indexCollection as $index) {
                    	if( !in_array($index->getData('indexer_code'), $skip_indexing) ) :
                        	$index->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)->save();
                        endif;
                    }*/
                    Mage::log('revert back to auto mode.', null, 'tally_catalog.log');

                    // trigger email with report..
                    $select                 = $connection->select()->from('feetscience_tally_catalog', array('*'));                     
                    $tally_product_report   = $connection->fetchAll($select);

                    $report_filename = "report_". date("Y-m-d-H-i").".csv" ;
                    $fp = fopen( Mage::getBaseDir() . "media/Tallydata/Response/" . $report_filename, "w") ;
                    foreach ($tally_product_report as $report_item) {
                        fwrite($fp, implode(",", $report_item)."\n");
                    }
                    fclose($fp) ;
                    Mage::log('created report file file', null, 'tally_catalog.log');

                    $this->_sendreportemail($report_filename) ;
                    Mage::log('Sent report as email attachment.', null, 'tally_catalog.log');

                } else {
                    // log error if there are no products in API response.
                    Mage::log('Error: No products in API response.', null, 'tally_catalog.log'); 
                }

        } catch (Exception $e) {
            // log error if there is any exception..
            Mage::log('Error: '. $e->getMessage(), null, 'tally_catalog.log');  
        }
        
    }

    public function _sendreportemail($report_filename) {
        try {
            
            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('tally_catalog_report');

            // email varib;es
            $emailTemplateVariables = array();
            $emailTemplateVariables['execution_time'] = date("Y-m-d H:i:s") ;

            $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
            $email_ids         = Mage::getStoreConfig('tally_orderpush/general/report_notification') ;
            $email_array       = explode(",", $email_ids);

            //attached order xml file to current email template
            $fileContents         = file_get_contents( Mage::getBaseDir() . "media/Tallydata/Response/" . $report_filename );
            $attachment           = $emailTemplate->getMail()->createAttachment($fileContents);
            $attachment->filename = $report_filename;

            // from email address
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

            // email configs
            foreach ($email_array as $notify_email) :
                $emailTemplate->setSenderName('Tally catalog sync report');
                $emailTemplate->setSenderEmail($senderEmail);
                $emailTemplate->send($notify_email, 'Admin', $emailTemplateVariables);
            endforeach;

        } catch (Exception $e) {
            Mage::log('Email trigger:' . $e->getMessage(), null, 'tally_catalog.log');
        }
    }
    
    public function pulltallycatalog() {

        // API url for catalog tally
        $api_url = Mage::getStoreConfig('tally_orderpush/general/catalog_apiurl') ;

        // xml schema
        $headers = array("Content-type: application/xml","Accept: application/xml");

        // curl post
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $api_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 100);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($curl);

        curl_close($curl);

        // parse response xml to array
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $responseArray = json_decode($json,true);

        $filename = "tally_catalog_". date("Y-m-d-H-i").".xml" ;
        file_put_contents(Mage::getBaseDir() . "/media/Tallydata/Response/" . $filename, $response);

        if( count($responseArray['STOCKITEM']) ) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            // empty temp table
            $write->query("TRUNCATE feetscience_tally_catalog;");

            foreach($responseArray['STOCKITEM'] as $item) :
                // insert records in temp record..
                $insertstatus = $write->query("insert into feetscience_tally_catalog(sku,name,qty,price,status,error_message,created_at) values('". $item['STOCKSKU']."','". $item['ARTICALNAME']."','". $item['QTY']."','". $item['MRP'] ."','0','',NOW())");

            endforeach;

        }
        return $filename;
    }
 
}