<?php
class Feetscience_Tallycatalog_Model_Observer
{
	public function tallySync()
	{
		Mage::log('Strted for tallySync');
		Mage::helper('feetscience_tallycatalog')->syncCatalog();
		Mage::log('Completed for tallySync');
	}
}