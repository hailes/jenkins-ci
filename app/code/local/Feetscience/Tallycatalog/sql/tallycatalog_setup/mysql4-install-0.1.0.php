<?php

$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS `feetscience_tally_catalog` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qty` int(4) NOT NULL,
  `price` float(10,2) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=Failed, 1=success',
  `error_message` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;");
$installer->endSetup();