<?php

class Feetscience_Couponsales_Block_Adminhtml_Sales_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();

		//Mage::getSingleton("adminhtml/session")->setCustomValue(0); 
		$this->setDefaultSort('id');
		$this->setId('salesGrid');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$userId = Mage::getSingleton('admin/session')->getUser()->getUserId();
		$userData = Mage::getModel('feetscience_couponsales/couponsales')->getCollection()
							->addFieldToSelect('sub_category')
							->addFieldToFilter('admin_user_id',$userId)
							->getData();
		if(!empty($userData))
		{
			$categoryIds = array();
			foreach($userData as $categoryId)
			{
				$categoryIds[] = $categoryId['sub_category'];
			}
			
		
			$orderId = Mage::getModel('feetscience_couponsales/ordercategorymapping')
						->getCollection()
						->addFieldToSelect('order_id')
						->addFieldToFilter('category_id',array('in'=>$categoryIds))
						->getData();

			$orderIds = array();
			foreach ($orderId as $id) 
			{
				$orderIds[]=$id['order_id'];
			}

			$orderData = Mage::getModel('sales/order')->getCollection()
						->addAttributeToFilter('entity_id',array('in'=>$orderIds));
		}else{
			$orderData = Mage::getModel('sales/order')->getCollection();
			$orderData->getSelect()->join(Mage::getConfig()->getTablePrefix().'feetscience_coupon_mapping','main_table.coupon_code='.Mage::getConfig()->getTablePrefix().'feetscience_coupon_mapping.coupon_code',array('*'));
		}
		

		$this->setCollection($orderData);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() 
	{
		$this->addColumn('school',
			array(
				'header'=>$this->__('School Name'),
				'index'=>'coupon_code',
				'frame_callback' => array($this, 'schoolName')
			)
		);

		$this->addColumn('couponcode',
			array(
				'header'=>$this->__('Coupon Code'),
				'index'=>'coupon_code'
			)
		);

		$this->addColumn('used',
			array(
				'header'=>$this->__('Coupon Used'),
				'index'=>'coupon_code',
				'frame_callback' => array($this, 'couponUsed')
			)
		);

		$this->addColumn('orderno',
			array(
				'header'=>$this->__('Order Number'),
				'index'=>'increment_id'
			)
		);

		$this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            'filter_index' => 'main_table.status',
        ));

		$this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'filter_index' => 'main_table.created_at',
        ));

		return parent::_prepareColumns();
	}

	public function couponUsed($value)
	{
		$coupon = Mage::getModel('salesrule/coupon')->load($value,'code');
		return $coupon->getTimesUsed();
	}

	public function schoolName($value)
	{
		$categoryIds = Mage::getModel('feetscience_couponmapping/couponmapping')
							->getCollection()
							->addFieldToSelect('category_id')
							->addFieldToFilter('coupon_code',$value)
							->getData();

		return Mage::getModel('catalog/category')->load($categoryIds[0]['category_id'])->getName();
	}
}