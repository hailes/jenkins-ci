<?php 

class Feetscience_Couponsales_Block_Adminhtml_Sales extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_sales';
		$this->_blockGroup = "couponsales";
		$this->_headerText = $this->__('View Sales Order');

		parent::__construct();
		$this->_removeButton('add');
	}
} 