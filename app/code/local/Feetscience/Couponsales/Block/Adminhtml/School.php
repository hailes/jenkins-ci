<?php

class Feetscience_Couponsales_Block_Adminhtml_School extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_school';
		$this->_blockGroup = "couponsales";
		$this->_headerText = $this->__('Configure School');
		$this->_addButtonLabel = 'Add New School';
		parent::__construct();
	}
}