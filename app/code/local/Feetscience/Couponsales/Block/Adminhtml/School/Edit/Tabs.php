<?php

class Feetscience_Couponsales_Block_Adminhtml_School_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('couponsales_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle('School Information');
	}

	protected function _beforeToHtml()
	{
		$this->addTab('form_section',array(
				'label'=>'School Information',
				'title'=>'School Information',
				'content'=>$this->getLayout()->createBlock('couponsales/adminhtml_school_edit_tab_form')->toHtml()
			));
		return parent::_beforeToHtml();
	}
}