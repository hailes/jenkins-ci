<?php

class Feetscience_Couponsales_Block_Adminhtml_School_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();
		$this->objectId = 'id';
		$this->_blockGroup = "couponsales";
		$this->_controller = 'adminhtml_school';
		$this->_updateButton('save','label','Save');
		$this->_updateButton('delete','label','Delete');
	}

	public function getHeaderText()
	{
		if(Mage::registry('couponsales') && Mage::registry('couponsales')->getId())
		{	
			return "Edit ".$this->htmlEscape(Mage::registry('couponsales')->getUser()).'<br />';
		}else{
			return "Add School";	
		}
		
	}
}