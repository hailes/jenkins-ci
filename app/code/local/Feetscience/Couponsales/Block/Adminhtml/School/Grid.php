<?php

class Feetscience_Couponsales_Block_Adminhtml_School_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();

		$this->setDefaultSort('id');
		$this->setId('schoolGrid');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('feetscience_couponsales/couponsales')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id',
				array(
					'header'=>$this->__('ID'),
					'align'=>'right',
					'width'=>'50px',
					'index'=>'id'	
					)
			);

		$this->addColumn('user',
				array(
					'header'=>$this->__('User'),
					'index'=>'admin_user_id',
					'frame_callback' => array($this, 'userName')
					)
			);

		$this->addColumn('rootcategory',
				array(
					'header'=>$this->__('Root Category'),
					'index'=>'root_category',
					'frame_callback' => array($this, 'renderRootCategory')
					)
			);

		$this->addColumn('subcategory',
				array(
					'header'=>$this->__('Child Category'),
					'index'=>'sub_category',
					'frame_callback' => array($this, 'renderSubCategory')
					)
			);

		return parent::_prepareColumns(); 
	}

	public function getRowUrl($row)
	{
		return $this->geturl('*/*/edit',array('id'=>$row->getId()));
	}

	public function userName($value) 
	{
		$adminUserModel = Mage::getModel('admin/user')->load($value)->getData();
		return $adminUserModel['username'];
	}

	public function renderRootCategory($value)
	{
		return Mage::getModel('catalog/category')->load($value)->getName();
	}

	public function renderSubCategory($value)
	{
		return Mage::getModel('catalog/category')->load($value)->getName();
	}
}