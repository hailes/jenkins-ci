<?php

class Feetscience_Couponsales_Block_Adminhtml_School_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$this->setTemplate('couponsales/categories.phtml');
		// $data = Mage::registry('couponsales')->getData();
		// $fieldset = $form->addFieldset('couponsales_form',
		// 					array('legend'=>'School Information'));
		// $fieldset->addField('user','select',
		// 		array(
		// 			'label'=>'Manage User',
		// 			'class'=>'required-entry',
		// 			'required'=>true,
		// 			'name'=>'user',
		// 			'values'=>$this->getRoleUser(),
		// 		)
		// 	);
		// $fieldset->addField('category','select',
		// 		array(
		// 			'label'=>'Category',
		// 			'class'=>'required-entry ajax',
		// 			'required'=>true,
		// 			'name'=>'rootcategory',
		// 			'onchange' => "displaySubCategory(this.value)",
		// 			'values' => $this->getRootCategories(),

		// 		)
		// 	);
		
		// if(Mage::registry('couponsales'))
		// {
		// 	$form->setValue(Mage::registry('couponsales'));
		// }
		// $this->_formScripts[] = "

		// function displaySubCategory(value){
  //               alert('working');
  //           }
  //       ";
		return parent::_prepareForm();
	}

	public function getRootCategories() 
	{
    	$categoriesArray = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            ->addFieldToFilter('level',2)
            ->load()
            ->toArray();

    	$categories = array();
    	foreach ($categoriesArray as $categoryId => $category) 
    	{
        	if (isset($category['name']) && isset($category['level'])) 
        	{
            	$categories[] = array(
                	'label' => $category['name'],
                	'value' => $categoryId
            	);
        	}
    	}
    	return $categories;
	}

	public function getRoleUser()
	{
		$roles = Mage::getModel('admin/roles')->getCollection()->addFieldToFilter('role_name','couponsales');
		$data = array();
		$userRole = array();
		foreach($roles as $key)
 		{
 			$data = $roles = Mage::getModel('admin/roles')->load($key->getRoleId())->getRoleUsers();
 			foreach($data as $user)
 			{
 				$adminUserModel = Mage::getModel('admin/user')->load($user)->getData();
 				
 				$userRole[] = array(
 					'label' => $adminUserModel['username'],
 					'value' => $adminUserModel['user_id']
 				);
 				
 			}
 		}
 		return $userRole;
	}

	public function getSchoolInformation()
	{
		$id = $this->getRequest()->getParam('id');
		$schoolData = Mage::getModel('feetscience_couponsales/couponsales')->load($id);
		return $schoolData;
	}
}