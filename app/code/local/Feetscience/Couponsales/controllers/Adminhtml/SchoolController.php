<?php

class Feetscience_Couponsales_Adminhtml_SchoolController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction()
	{
		return $this;
	}

	public function indexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		$collection = Mage::getModel('feetscience_couponsales/couponsales')->load($id);
		if($collection->getId() || $id==0)
		{
			Mage::register('couponsales',$collection);
			$this->loadLayout();
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('couponsales/adminhtml_school_edit'))
				 ->_addLeft($this->getLayout()->createBlock('couponsales/adminhtml_school_edit_tabs')
			);
			$this->renderLayout();
		}else{
			Mage::getSingleton('adminhtml/session')->addError('School does not exist');
			$this->_redirect('*/*/');
		}
	}

	public function newAction()
	{
		$this->_forward('edit');
	}

	public function saveAction()
	{
		if($this->getRequest()->getPost())
		{
			try{
				$postData = $this->getRequest()->getPost();
				$data = array('admin_user_id'=>$postData['user'],'root_category'=>$postData['rootcategory'],'sub_category'=>$postData['subcategory']);
				
				$collection = Mage::getModel('feetscience_couponsales/couponsales');
				if($this->getRequest()->getParam('id')<=0)
				{
					$collection->addData($data)->save();
					Mage::getSingleton('adminhtml/session')->addSuccess('Successfully Saved');
					$this->_redirect('*/*/');
					return;
				}else{
					$collection->load($this->getRequest()->getParam('id'))
								->setAdminUserId($this->getRequest()->getPost('user'))
								->setRootCategory($this->getRequest()->getPost('rootcategory'))
								->setSubCategory($this->getRequest()->getPost('subcategory'))
								->save();
					Mage::getSingleton('adminhtml/session')->addSuccess('Successfully Saved');
					$this->_redirect('*/*/');
					return;
				}
			}catch(Exception $e)
			{
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit',array('id'=>$this->getRequest()->getParam('id')));
				return;
			}
		}
		$this->_redirect('*/*/');
	}

	public function deleteAction()
	{
		if($this->getRequest()->getParam('id')>0)
		{
			try{
				$collection = Mage::getModel('feetscience_couponsales/couponsales');
				$collection->setId($this->getRequest()->getParam('id'))->delete();
				Mage::getSingleton('adminhtml/session')
                               ->addSuccess('successfully deleted');
                $this->_redirect('*/*/');
			}catch (Exception $e){
                Mage::getSingleton('adminhtml/session')
                      ->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
            $this->_redirect('*/*/');
		}
	}

    public function renderSubCategoryAction()
    {
    	$categoryId = $_GET['categoryId'];
    	$category = Mage::getModel('catalog/category')->load($categoryId);
    	$subCategoryIds = $category->getChildren();
    	$html = null;
    	foreach (explode(',', $subCategoryIds) as $subCatId) 
    	{
    		$subCategory = Mage::getModel('catalog/category')->load($subCatId);
    		if($subCategory->getIsActive())
    		{
    			$catId = $subCategory->getId();
    			$catName = $subCategory->getName();
    			$html.='<option value='.$catId.'>'.$catName.'</option>';
    		}
    	}
    	echo $html;
    }
}