<?php

class Feetscience_Couponsales_Model_Mysql4_Couponsales_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	protected function _construct()
	{
		$this->_init('feetscience_couponsales/couponsales');
	}
}