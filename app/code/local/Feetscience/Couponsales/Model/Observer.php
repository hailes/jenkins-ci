<?php

class Feetscience_Couponsales_Model_Observer extends Varien_Object
{
	// cronjob runs every 5 minutes to check orders mapping
	public function schoolOrderCategoryMapping() {

		// current time
		$time = time();
        $to = date('Y-m-d H:i:s', $time);
        
        // 5 minute old timestamp
        $lastTime = $time - (5 * 60) ;
        $from = date('Y-m-d H:i:s', $lastTime);

        try {
	        // select orders placed in last 5 minutes.
	        $order_collection = Mage::getModel('sales/order')->getCollection()
	                                                    ->addFieldToFilter('created_at', array('from' => $from, 'to' => $to))
	                                                    ->addFieldToFilter('coupon_code', array('notnull' => true))
	                                                    ->load();

	        // read through orders.
	        foreach ($order_collection as $order) :

	        	// applied coupon code to order
	        	$couponCode = $order->getCouponCode() ;

	        	if( $couponCode ) :
				    // get category id from coupon code
				    $couponmapping_collection = Mage::getModel('feetscience_couponmapping/couponmapping')
							                        ->getCollection()
							                        ->addFieldToFilter('coupon_code',$couponCode)
							                        ->getData() ;

					// category id from collection
					$category_id = $couponmapping_collection[0]['category_id'];
					
					if( $category_id ) :
						// order items loop
						foreach($order->getAllItems() as $item) :
							// get category assigned to product
							$product = Mage::getModel('catalog/product')->load($item->getProductId());
							$categoryIds = $product->getCategoryIds();

							// category loop
							foreach($categoryIds as $categoryId) :
								// check copoun category
								if( $category_id == $categoryId ) :
									
									    // save order/category mapping
										Mage::getModel('feetscience_couponsales/ordercategorymapping')
												->setOrderId($item->getOrderId())
												->setProductId($item->getProductId())
												->setCategoryId($categoryId)
												->save();								
								endif;
							endforeach;
						endforeach;
					endif;
				endif;
	        endforeach;

	    } catch (Exception $e) {
		    Mage::log('Order: ' . $order->getIncrementId() . ' - Exception - ' . $e->getMessage(), null, 'order_category_mapping.log');
		}

	}

	/*public function orderCategoryMapping($event)
	{
		// get coupon code from session
		$couponCode = Mage::getSingleton('core/session')->getCouponCode();
		// current order object
		$order = $event->getOrder();

		if( $couponCode ) :
		    // get category id from coupon code
		    $couponmapping_collection = Mage::getModel('feetscience_couponmapping/couponmapping')
					                        ->getCollection()
					                        ->addFieldToFilter('coupon_code',$couponCode)
					                        ->getData() ;

			// category id from collection
			$category_id = $couponmapping_collection[0]['category_id'];
			
			if( $category_id ) :
				// order items loop
				foreach($order->getAllVisibleItems() as $item) :
					// get category assigned to product
					$product = Mage::getModel('catalog/product')->load($item->getProductId());
					$categoryIds = $product->getCategoryIds();

					// category loop
					foreach($categoryIds as $categoryId) :
						// check copoun category
						if( $category_id == $categoryId ) :
							// save order/category mapping
							Mage::getModel('feetscience_couponsales/ordercategorymapping')
								->setOrderId($item->getOrderId())
								->setProductId($item->getProductId())
								->setCategoryId($categoryId)
								->save();
						endif;
					endforeach;
				endforeach;
			endif;
		endif;
	}*/
}