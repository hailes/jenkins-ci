<?php

class Feetscience_Stockupdate_Adminhtml_StockupdateController extends Mage_Adminhtml_Controller_action
{
    
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('sapsync/items')->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        
        return $this;
    }
    
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }
    
    public function stockAction()
    {
		
        $this->sapupdate();
    }
    
    
    public function sapupdate()
    {
        set_time_limit(0);
        ini_set('display_errors', 1);
        ini_set('memory_limit', '1024M');
		
		
		//Get the file from ftp location
		$ftp_helper = Mage::helper("sapprocess/ftp");
		$filter_key_word = 'ecom_stock_';
		$ftp_files_list  = $ftp_helper->getOrderResponceList($filter_key_word);
	//	print_r($ftp_files_list);exit;
		foreach($ftp_files_list as $file_name)
		{
			$local_file = "SAP/Data/stock/".$file_name;
			$server_file = "IN/".$file_name;
			$result = $ftp_helper->downloadFile($local_file,$server_file);
			
			/*  $myfile = fopen($local_file, "wr") or die("Unable to open file!");
			$result = fread($myfile,filesize($local_file)); */
			/* $local_file = "SAP/Data/stock/ecka_stock.xml";
			$parseObj = file_get_contents(Mage::getBaseDir() . '/SAP/Data/stock/ecka_stock.xml');
			$myfile = fopen($local_file, "r") or die("Unable to open file!");
			$result = fread($myfile,filesize($local_file)); */
			$result = file_get_contents($local_file);
		   
			if ($result) {
           
            $result = str_replace('<?xml version="1.0" encoding="utf-16"?>','<?xml version="1.0" encoding="utf-8"?>',$result);
			$result = str_replace('<asx:abap xmlns:asx="http://www.sap.com/abapxml" version="1.0"><asx:values>', '', $result);
            $result = str_replace('</asx:values></asx:abap>', '', $result); 
			
            //$str = $str1; 
			//$local_file = Mage::getBaseDir() .'/SAP/Data/stock/ecka_stock.xml';	 							/* $fp = fopen(Mage::getBaseDir() .'/SAP/new.xml', 'wb');			if(is_writable(Mage::getBaseDir() .'/SAP/new.xml')){				echo "file is writable<br>";			}			if(fwrite($fp, $str) == FALSE){				echo "failed to write data<br>";			}			fclose($fp); */			$string = file_get_contents($local_file);
					/* $data = new SimpleXMLElement($result);			if (!$data) {				echo "Failed loading XML\n";				foreach(libxml_get_errors() as $error) {					echo "\t", $error->message;				}			} */			
					$data = simplexml_load_string($result); 
					$data = (array)$data;
					
					//print_r($data);
				//	exit;
           
            if (is_array($data)) {
                $list_values = $data['ZTSTOCK'];
                $storeId = Mage::app()->getStore()->getStoreId();
                //create the code to write the CSV file
                $myfile = fopen(Mage::getBaseDir() . "/stockreport/" . date('d-m-Y') . "-data.csv", "w") or die("Unable to open file!");
                $file_path = Mage::getBaseDir() . "/stockreport/" . date('d-m-Y') . "-data.csv"; //file path of the CSV file in which the data to be saved
                
                $mage_csv     = new Varien_File_Csv(); //mage CSV   
                $products_row = array();
                $_columns     = array(
                    "Plant",
                    "Material",
                    "BatchNo",
                    "Unit",
                    "QTY",
                    "Mrp",
                    "status",
                    "DateTime"
                );
                $data         = array();
                // prepare CSV header...
                foreach ($_columns as $column) {
                    $data[] = $column;
                }
                for ($i = 0; $i < count((array)$list_values); $i++) {
                    if ($list_values[$i]['Quantity'] > 0) {
                        $stat = 1;
                    } else {
                        $stat = 0;
                    }	
					$list_values[$i]=(array)$list_values[$i];
					
					$qty = $list_values[$i]['Quantity'];
					 if ($qty > 0) {
                        $stat = 1;
                    } else {
                        $stat = 0;
                    }	
					
                    $result                                        = array();
                    $result['plant']                               = $list_values[$i]['PLANT'];
                    $result['material']                            = $list_values[$i]['MATERIAL'];
                    $result['batchno']                             = $list_values[$i]['BATCH_NO'];
                    $result['unit']                                = $list_values[$i]['UNIT'];
                    $result['qty']                                 = $list_values[$i]['QUANTITY'];
                    $result['mrp']                                 = $list_values[$i]['MRP'];
                    $result['status']                              = $stat;
                    $result['datetime']                            = now();
                    $products_row[]                                = $result;
                    $sku[$list_values[$i]['MATERIAL']]['PLANT']    = $list_values[$i]['PLANT'];
                    $sku[$list_values[$i]['MATERIAL']]['MATERIAL'] = $list_values[$i]['MATERIAL'];
                    $sku[$list_values[$i]['MATERIAL']]['BATCH_NO']  = $list_values[$i]['BATCH_NO'];
                    $sku[$list_values[$i]['MATERIAL']]['UNIT']     = $list_values[$i]['UNIT'];
                    $sku[$list_values[$i]['MATERIAL']]['QUANTITY'] = $list_values[$i]['QUANTITY'];
                    $sku[$list_values[$i]['MATERIAL']]['MRP']      = $list_values[$i]['MRP'];
                    $sku[$list_values[$i]['MATERIAL']]['status']   = $stat;
                    $sku[$list_values[$i]['MATERIAL']]['datetime'] = now();
                    $sku[$list_values[$i]['MATERIAL']]['storeid']  = $storeId;
                    
                    if ($list_values[$i]['PLANT'] != '' && $list_values[$i]['MATERIAL']) {
                        $insert_array[] = array(
                            'plant' => $list_values[$i]['PLANT'],
                            'material' => $list_values[$i]['MATERIAL'],
                            'batchno' => $list_values[$i]['BATCH_NO'],
                            'unit' => $list_values[$i]['UNIT'],
                            'quantity' => $list_values[$i]['QUANTITY'],
                            'mrp' => $list_values[$i]['MRP'],
                            'status' => $stat,
                            'storeid' => $storeId
                        );
                    }
                    $duplicate_set[] = $list_values[$i]['PLANT'] . '--' . $list_values[$i]['MATERIAL'];
                    
                    
                }
				
                array_unshift($products_row, $data);
				
				
                $mage_csv->saveData($file_path, $products_row);
				
				
                
              //  die;
                //inser the values in DB
                
                $resource       = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $write          = Mage::getSingleton("core/resource")->getConnection("core_write");
                
                //$query = "TRUNCATE stockupdate";
                //$write->query($query);
                // Concatenated with . for readability
                $table = Mage::getSingleton('core/resource')->getTableName('stockupdate');
                if (is_array($insert_array)) {
                    $write->insertMultiple($table, $insert_array); // Insert stock update
                }
				
				
				
                //read connection
                //email to stock
                $resource       = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                
                $sql    = "SELECT t1.*,COUNT(*) AS c FROM stockupdate AS t1
					  WHERE t1.created_time = (SELECT MAX(t2.created_time) FROM stockupdate AS t2)
					  GROUP BY t1.material,t1.plant HAVING c > 1";
                $allsku = $readConnection->fetchAll($sql);
                //print_r($allsku);
                
                
                
                $to      = "sasiananth1984@gmail.com,jayachandran@tenovia.com";
                $subject = "Stock Report Email";
                
                $message = "
							<html>
							<head>
							<title>Stock Report</title>
							</head>
							<body>
							<p>This email contains Stock Offline records</p>
							<table border='1' cellspacing='0' cellspadding='0'>
							<tr>
							<th> SL# </th>
							<th> Plant</th>
							<th> Material</th>
							<th> BatchNo</th>
							<th> Unit</th>
							<th> Quantity</th>
							<th> Mrp</th>
							<th> Status (0= stock not updated and offlined, 1= stock updated)</th>
							<th> DateTime </th></tr>";
                $i       = 0;
                $msg     = '';
                foreach ($allsku as $values) {
                    if ($values['status'] = '0') {
                        $status = 'stock not updated and offlined';
                    } else {
                        $status = 'stock updated';
                    }
                    $msg .= "<tr>
				<td>" . $i . "</td>
				<td>" . $values['plant'] . "</td>
				<td>" . $values['material'] . "</td>
				<td>" . $values['batchno'] . "</td>
				<td>" . $values['unit'] . "</td>
				<td>" . $values['quantity'] . "</td>
				<td>" . $values['mrp'] . "</td>
				<td>" . $status . "</td>
				<td>" . $values['datetime'] . "</td>
				</tr>";
                    $i              = $i + 1;
                    $duplicateSku[] = $values['material'];
                }
                $eod     = "</table></bodY></html>";
                $message = $message . $msg . $eod;
                
                //email
                $mail = new Zend_Mail('utf-8');
                
                $recipients = array(
                    'jayachandran@tenovia.com'
                );
                $mailBody   = $message;
                
                $mail->setBodyHtml($mailBody)->setSubject('Sap response')->addTo($recipients)->setFrom('admin@feetscience.com', "Admin");
                
                //file content is attached
                $file       = $file_path;
                $attachment = file_get_contents($file);
                $mail->createAttachment($attachment, Zend_Mime::TYPE_OCTETSTREAM, Zend_Mime::DISPOSITION_ATTACHMENT, Zend_Mime::ENCODING_BASE64, 'Report.csv');
                try {
                    $mail->send();
                }
                catch (Exception $e) {
                    Mage::logException($e);
                }
                //email End								
                //end the mail process
                
                //disbale the all products
		$oCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('type_id', array(
                    'eq' => 'simple'
                ))
				->addAttributeToFilter('attribute_set_id', array(
                    'eq' => '4'
                ));
                $aSku        = array();
                foreach ($oCollection as $oProduct) {
                    $aSku[] = "'" . $oProduct->getSku() . "'";
					 $sql    = "UPDATE cataloginventory_stock_item as csi,cataloginventory_stock_status as css
                       SET
                       csi.qty = ?,
                       csi.is_in_stock = ?,
                       css.qty = ?,
                       css.stock_status = ?
                       WHERE
                       csi.product_id = ?
                       AND csi.product_id = css.product_id";
                    
                    $isInStock   = 1;
                    $stockStatus =  1;
		 $readConnection->query($sql, array($oProduct->getSku(), $isInStock,'0', $stockStatus, $oProduct->getId()));
                    
					
					
                }
		
                
                /* $list_simple    = implode(',', $aSku);
                $sql_offline    = "UPDATE catalog_product_entity_int cpei, catalog_product_entity cpe SET cpei.value = '2'
						WHERE cpe.entity_id = cpei.entity_id AND cpe.sku IN (" . $list_simple . ") AND cpei.attribute_id = '96'";
                //die;
                $offline_result = $write->query($sql_offline); */
                
                
                //stock update
                $resource       = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                
                $allsku            = $readConnection->fetchAll('SELECT * FROM stockupdate WHERE created_time = (SELECT MAX(created_time) FROM stockupdate ORDER BY created_time DESC)');
                $list_enable_array = array();
                foreach ($allsku as $values) {
                    $sku_list[]                    = $values['material'];
                    $qty_list[$values['material']] = $values['quantity'];
                    $mrp_list[$values['material']] = $values['mrp'];
                    if (!in_array($values['material'], $duplicateSku)) {
                        $list_enable_array[] = "'" . $values['material'] . "'";
                    }
                }
                $list_enable         = implode(',', $list_enable_array);
                $productsCollection2 = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('sku', array(
                    'in' => $sku_list
                ));
                
                foreach ($productsCollection2 as $product) {
                    $sku_value          = $product->getSku();/*
                    $productId          = $product->getId();
                    $arr[]              = $product->getSku();*/
                    $newQty[$sku_value] = round($qty_list[$sku_value]);
					$isInStock   = $newQty[$sku_value] > 0 ? 1 : 0;
                    $stockStatus = $newQty[$sku_value] > 0 ? 1 : 0;					
										
					$product->setPrice((real)$mrp_list[$sku_value]);
					
					$product->save();

					$productId = $product->getId();
					/* $stockItem =Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
					$stockItemId = $stockItem->getId();

					$stockItem->setData('manage_stock', 1);
					$stockItem->setData('is_in_stock', $isInStock);
					$stockItem->setData('qty', (integer)$qty_list[$sku_value]);

					$stockItem->save(); */
					
                     $sql    = "UPDATE cataloginventory_stock_item as csi,cataloginventory_stock_status as css
                       SET
                       csi.qty = ?,
                       csi.is_in_stock = ?,
                       css.qty = ?,
                       css.stock_status = ?
                       WHERE
                       csi.product_id = ?
                       AND csi.product_id = css.product_id";
                    
                   /*  $isInStock   = $newQty[$sku_value] > 0 ? 1 : 0;
                    $stockStatus = $newQty[$sku_value] > 0 ? 1 : 0; */
                    $readConnection->query($sql, array(
                        $newQty[$sku_value],
                        $isInStock,
                        $newQty[$sku_value],
                        $stockStatus,
                        $productId
                    )); 
	 $sql_enable = "UPDATE catalog_product_entity_int cpei, catalog_product_entity cpe SET cpei.value = '1'
			WHERE cpe.entity_id = cpei.entity_id AND cpe.attribute_set_id = '4' AND  cpe.sku IN ('" . $sku_value . "') AND  cpe.type_id='simple' AND cpei.attribute_id = '102'";
					$result     = $write->query($sql_enable);
                }
                
		 $process = Mage::getModel('index/process')->load(2);
                
                $process->reindexAll();
                $process1 = Mage::getModel('index/process')->load(8);
                $process1->reindexAll();                
                
                //end for stock update
                //enable the products
             //   $sql_enable = "UPDATE catalog_product_entity_int cpei, catalog_product_entity cpe SET cpei.value = '1'
	//					WHERE cpe.entity_id = cpei.entity_id AND cpe.sku IN (" . $list_enable . ") AND cpei.attribute_id = '96'";
          //      $result     = $write->query($sql_enable);
                
                //end
                
                /* $process = Mage::getModel('index/process')->load(2);
                
                $process->reindexAll();
                $process1 = Mage::getModel('index/process')->load(8);
                $process1->reindexAll(); */
                
            }
			
			$ftp_helper->renameTheFile($file_name);
            
        }
			
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('stockupdate')->__('Stock Update successfully'));
        $this->_redirect('*/*/');
        //die;
    }
    
    
    
    public function editAction()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = Mage::getModel('stockupdate/stockupdate')->load($id);
        
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            
            Mage::register('stockupdate_data', $model);
            
            $this->loadLayout();
            $this->_setActiveMenu('stockupdate/items');
            
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
            
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            
            $this->_addContent($this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_edit'))->_addLeft($this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_edit_tabs'));
            
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('stockupdate')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            
            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                try {
                    /* Starting upload */
                    $uploader = new Varien_File_Uploader('filename');
                    
                    // Any extention would work
                    $uploader->setAllowedExtensions(array(
                        'jpg',
                        'jpeg',
                        'gif',
                        'png'
                    ));
                    $uploader->setAllowRenameFiles(false);
                    
                    // Set the file upload mode 
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders 
                    //	(file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader->setFilesDispersion(false);
                    
                    // We set media as the upload dir
                    $path = Mage::getBaseDir('media') . DS;
                    $uploader->save($path, $_FILES['filename']['name']);
                    
                }
                catch (Exception $e) {
                    
                }
                
                //this way the name is saved in DB
                $data['filename'] = $_FILES['filename']['name'];
            }
            
            
            $model = Mage::getModel('stockupdate/stockupdate');
            $model->setData($data)->setId($this->getRequest()->getParam('id'));
            
            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())->setUpdateTime(now());
                } else {
                    $model->setUpdateTime(now());
                }
                
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('stockupdate')->__('Item was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array(
                        'id' => $model->getId()
                    ));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('stockupdate')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }
    
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('stockupdate/stockupdate');
                
                $model->setId($this->getRequest()->getParam('id'))->delete();
                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function massDeleteAction()
    {
        $stockupdateIds = $this->getRequest()->getParam('stockupdate');
        if (!is_array($stockupdateIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($stockupdateIds as $stockupdateId) {
                    $stockupdate = Mage::getModel('stockupdate/stockupdate')->load($stockupdateId);
                    $stockupdate->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($stockupdateIds)));
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function massStatusAction()
    {
        $stockupdateIds = $this->getRequest()->getParam('stockupdate');
        if (!is_array($stockupdateIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($stockupdateIds as $stockupdateId) {
                    $stockupdate = Mage::getSingleton('stockupdate/stockupdate')->load($stockupdateId)->setStatus($this->getRequest()->getParam('status'))->setIsMassupdate(true)->save();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were successfully updated', count($stockupdateIds)));
            }
            catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function exportCsvAction()
    {
        $fileName = 'stockupdate.csv';
        $content  = $this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_grid')->getCsv();
        
        $this->_sendUploadResponse($fileName, $content);
    }
    
    public function exportXmlAction()
    {
        $fileName = 'stockupdate.xml';
        $content  = $this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_grid')->getXml();
        
        $this->_sendUploadResponse($fileName, $content);
    }
    
    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
