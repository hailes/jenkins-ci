<?php

class Feetscience_Stockupdate_Adminhtml_StockinvoiceupdateController extends Mage_Adminhtml_Controller_action
{
    
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('sapsync/items')->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'))->_addContent($this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_stockinvoiceupdate_grid'));
        
        return $this;
    }
    
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }
    
    public function stockAction()
    {
        
        //Get the file from ftp location
        $ftp_helper      = Mage::helper("sapprocess/ftp");
        $filter_key_word = 'ecom_invoice_';
        $ftp_files_list  = $ftp_helper->getOrderResponceList($filter_key_word);
        foreach ($ftp_files_list as $file_name) {
            $local_file  = "SAP/Data/invoice/" . $file_name;
            $server_file = "IN/" . $file_name;
            $result      = $ftp_helper->downloadFile($local_file, $server_file);
            
            $result = file_get_contents($local_file);
            
            if ($result) {
                
                $result = str_replace('<?xml version="1.0" encoding="utf-16"?>', '<?xml version="1.0" encoding="utf-8"?>', $result);
                $result = str_replace('<asx:abap xmlns:asx="http://www.sap.com/abapxml" version="1.0"><asx:values>', '', $result);
                $result = str_replace('</asx:values></asx:abap>', '', $result);
                
                $data = simplexml_load_string($result);
                $data = (array) $data;
                if (is_array($data)) {
                    $data = $data['ZST_ORDER'];
                    
                    /// $status      = $data['response']['Status'];
                    //$Return      = $data['response']['Return'];
                    //$list_values = $data['response']['ItOrder'];
                    $status = "Success";
                    $Return = "";
                    $stockm = Mage::getModel('stockupdate/stockinvoiceupdate');
                    
                    //unset($list_values[])
                    //if ($status == 'Success') {
                    foreach ($data as $list_values) {
                        $list_values              = (array) $list_values;
                        $storeId                  = Mage::app()->getStore()->getStoreId();
                        $result                   = array();
                        $result['portal_id']      = $list_values['PORTAL_ID'];
                        $result['material']       = $list_values['MATNR'];
                        $result['invoiceno']      = $list_values['INVOICE_NO'];
                        $result['status']         = $status;
                        $result['created_time']   = now();
                        $result['update_time']    = now();
                        $result['return_message'] = $Return;
                        $collection               = $stockm->getCollection();
                        $collection->addFieldToFilter('invoiceno', $list_values['INVOICE_NO']);
                        $count = $collection->count();
                        if ($count == 0) {
                            $products_row[] = $result;
                        }
                    }
                    // }
                    
                    
                    if (isset($products_row)) {
                        $resource       = Mage::getSingleton('core/resource');
                        $readConnection = $resource->getConnection('core_read');
                        $write          = Mage::getSingleton("core/resource")->getConnection("core_write");
                        
                        
                        // Concatenated with . for readability
                        $table = Mage::getSingleton('core/resource')->getTableName('stockinvoiceupdate');
                        if (is_array($products_row)) {
                            $write->insertMultiple($table, $products_row);
                            
                        }
                    }
                }
            }
            $ftp_helper->renameTheFile($file_name);
            
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('stockupdate')->__('Invoice Update successfully'));
        $this->_redirect('*/*/');
    }
    
    
    public function massDeleteAction()
    {
        $stockupdateIds = $this->getRequest()->getParam('stockupdate');
        if (!is_array($stockupdateIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($stockupdateIds as $stockupdateId) {
                    $stockupdate = Mage::getModel('stockupdate/stockinvoiceupdate')->load($stockupdateId);
                    $stockupdate->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($stockupdateIds)));
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    
    public function exportCsvAction()
    {
        $fileName = 'stockinvoiceupdate.csv';
        $content  = $this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_gridinvoice')->getCsv();
        
        $this->_sendUploadResponse($fileName, $content);
    }
    
    public function exportXmlAction()
    {
        $fileName = 'stockinvoiceupdate.xml';
        $content  = $this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_gridinvoice')->getXml();
        
        $this->_sendUploadResponse($fileName, $content);
    }
    
    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
    
}