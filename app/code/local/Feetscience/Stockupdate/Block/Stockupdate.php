<?php
class Feetscience_Stockupdate_Block_Stockupdate extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getStockupdate()     
     { 
        if (!$this->hasData('stockupdate')) {
            $this->setData('stockupdate', Mage::registry('stockupdate'));
        }
        return $this->getData('stockupdate');
        
    }
}