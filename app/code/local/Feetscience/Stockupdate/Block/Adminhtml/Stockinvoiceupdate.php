<?php
class Feetscience_Stockupdate_Block_Adminhtml_Stockinvoiceupdate extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_stockinvoiceupdate';
    $this->_blockGroup = 'stockupdate';
    $this->_headerText = Mage::helper('stockupdate')->__('Tally Invoice Response');
    //$this->_addButtonLabel = Mage::helper('stockupdate')->__('Add Item');
	
	
	$data = array(
            'label'     => 'Stock Invoice Update',
            'class'     => 'some-class',
            'onclick'   => 'setLocation(\' '  .  Mage::helper('adminhtml')->getUrl('stockupdate/adminhtml_stockinvoiceupdate/stock') . '\')',
        );
        Mage_Adminhtml_Block_Widget_Container::addButton('my_button_identifier', $data);
	
    parent::__construct();
	 $this->_removeButton('add');
  }
  
  
  protected function _prepareLayout()
  {
	  //$this->getLayout()->createBlock('stockupdate/adminhtml_stockupdate_gridinvoice')->toHtml();
    //return parent::_prepareLayout();
  }
  
}