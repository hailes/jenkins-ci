<?php

class Feetscience_Stockupdate_Block_Adminhtml_Stockupdate_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('stockupdate_form', array('legend'=>Mage::helper('stockupdate')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('stockupdate')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('stockupdate')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('stockupdate')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('stockupdate')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('stockupdate')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('stockupdate')->__('Content'),
          'title'     => Mage::helper('stockupdate')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getStockupdateData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getStockupdateData());
          Mage::getSingleton('adminhtml/session')->setStockupdateData(null);
      } elseif ( Mage::registry('stockupdate_data') ) {
          $form->setValues(Mage::registry('stockupdate_data')->getData());
      }
      return parent::_prepareForm();
  }
}