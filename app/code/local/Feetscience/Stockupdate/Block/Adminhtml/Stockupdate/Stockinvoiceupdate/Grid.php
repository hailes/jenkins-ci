<?php

class Feetscience_Stockupdate_Block_Adminhtml_Stockupdate_Stockinvoiceupdate_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('stockinvoiceupdateGrid');
      $this->setDefaultSort('stockinvoiceupdate_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('stockupdate/stockinvoiceupdate')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('stockinvoiceupdate_id', array(
          'header'    => Mage::helper('stockupdate')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'stockinvoiceupdate_id',
      ));
	  
	  $this->addColumn('portal_id', array(
          'header'    => Mage::helper('stockupdate')->__('Portal Id'),
          'align'     =>'left',
          'index'     => 'portal_id',
      ));
	  
	  $this->addColumn('material', array(
          'header'    => Mage::helper('stockupdate')->__('material'),
          'align'     =>'left',
          'index'     => 'material',
      ));
	  
	   $this->addColumn('invoiceno', array(
          'header'    => Mage::helper('stockupdate')->__('invoiceno'),
          'align'     =>'left',
          'index'     => 'invoiceno',
      ));	  	  
	  

	  
	   $this->addColumn('status', array(
          'header'    => Mage::helper('stockupdate')->__('status'),
          'align'     =>'left',
          'index'     => 'status',
      ));
	  
	  $this->addColumn('return_message', array(
          'header'    => Mage::helper('stockupdate')->__('Return message'),
          'align'     =>'left',
          'index'     => 'return_message',
      ));
	  
	   $this->addColumn('created_time', array(
          'header'    => Mage::helper('stockupdate')->__('created time'),
          'align'     =>'left',
          'index'     => 'created_time',
      ));
	  
	  

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('stockupdate')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */
     
	  
      return parent::_prepareColumns();
  }

   

  public function getRowUrl($row)
  {
      //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}