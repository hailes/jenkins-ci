<?php

class Feetscience_Stockupdate_Block_Adminhtml_Stockupdate_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('stockupdateGrid');
      $this->setDefaultSort('stockupdate_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('stockupdate/stockupdate')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('stockupdate_id', array(
          'header'    => Mage::helper('stockupdate')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'stockupdate_id',
      ));      
	  
	  $this->addColumn('plant', array(
          'header'    => Mage::helper('stockupdate')->__('plant'),
          'align'     =>'left',
          'index'     => 'plant',
      ));
	  
	  $this->addColumn('material', array(
          'header'    => Mage::helper('stockupdate')->__('material'),
          'align'     =>'left',
          'index'     => 'material',
      ));
	  
	   $this->addColumn('batchno', array(
          'header'    => Mage::helper('stockupdate')->__('batchno'),
          'align'     =>'left',
          'index'     => 'batchno',
      ));
	  
	    $this->addColumn('unit', array(
          'header'    => Mage::helper('stockupdate')->__('unit'),
          'align'     =>'left',
          'index'     => 'unit',
      ));
	  
	   $this->addColumn('quantity', array(
          'header'    => Mage::helper('stockupdate')->__('quantity'),
          'align'     =>'left',
          'index'     => 'quantity',
      ));
	  
	   $this->addColumn('mrp', array(
          'header'    => Mage::helper('stockupdate')->__('mrp'),
          'align'     =>'left',
          'index'     => 'mrp',
      ));
	  
	   $this->addColumn('status', array(
          'header'    => Mage::helper('stockupdate')->__('status'),
          'align'     =>'left',
          'index'     => 'status',
      ));
	  
	   $this->addColumn('created_time', array(
          'header'    => Mage::helper('stockupdate')->__('created time'),
          'align'     =>'left',
          'index'     => 'created_time',
      ));
	  
	   $this->addColumn('storeid', array(
          'header'    => Mage::helper('stockupdate')->__('store id'),
          'align'     =>'left',
          'index'     => 'storeid',
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('stockupdate')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

     
		
		
	  
      return parent::_prepareColumns();
  }

   

  public function getRowUrl($row)
  {
      //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}