<?php

class Feetscience_Stockupdate_Block_Adminhtml_Stockupdate_Stockitemmanager_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('stockitemmanagerGrid');
      $this->setDefaultSort('stockitemmanager_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('stockupdate/stockitemmanager')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('stockitemmanager_id', array(
          'header'    => Mage::helper('stockupdate')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'stockitemmanager_id',
      ));     
	  
	  
	   $this->addColumn('invoiceno', array(
          'header'    => Mage::helper('stockupdate')->__('S.No'),
          'align'     =>'left',
          'index'     => 'invoiceno',
      ));	  
	  
	     $this->addColumn('materialno', array(
          'header'    => Mage::helper('stockupdate')->__('Material No'),
          'align'     =>'left',
          'index'     => 'materialno',
      ));

	   $this->addColumn('created_time', array(
            'header' => Mage::helper('stockupdate')->__('Date'),
            'index' => 'created_time',
            'type' => 'datetime',
            'width' => '100px',
        ));

	      $this->addColumn('productname', array(
          'header'    => Mage::helper('stockupdate')->__('Product Description'),
          'align'     =>'left',
          'index'     => 'productname',
      ));	  
	  
	      $this->addColumn('weight', array(
          'header'    => Mage::helper('stockupdate')->__('Quantity'),
          'align'     =>'left',
          'index'     => 'weight',
      ));	  
	  
	      $this->addColumn('price', array(
          'header'    => Mage::helper('stockupdate')->__('MRP'),
          'align'     =>'left',
          'index'     => 'price',
      ));	  
	  
	  
	  
	  
	   $this->addColumn('totprice', array(
          'header'    => Mage::helper('stockupdate')->__('Total'),
          'align'     =>'left',
          'index'     => 'totprice',
      ));
	  
	  
	  

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('stockupdate')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */
	  
	$this->addExportType('*/*/exportCsv', Mage::helper('recipe')->__('CSV'));
	$this->addExportType('*/*/exportXml', Mage::helper('recipe')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    

  public function getRowUrl($row)
  {
      //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}