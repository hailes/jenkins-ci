<?php

class Feetscience_Stockupdate_Block_Adminhtml_Stockupdate_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'stockupdate';
        $this->_controller = 'adminhtml_stockupdate';
        
        $this->_updateButton('save', 'label', Mage::helper('stockupdate')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('stockupdate')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('stockupdate_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'stockupdate_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'stockupdate_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('stockupdate_data') && Mage::registry('stockupdate_data')->getId() ) {
            return Mage::helper('stockupdate')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('stockupdate_data')->getTitle()));
        } else {
            return Mage::helper('stockupdate')->__('Add Item');
        }
    }
}