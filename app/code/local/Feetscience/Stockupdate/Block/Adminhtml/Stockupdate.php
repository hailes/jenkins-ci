<?php
class Feetscience_Stockupdate_Block_Adminhtml_Stockupdate extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    
    $this->_controller = 'adminhtml_stockupdate';
    $this->_blockGroup = 'stockupdate';
    $this->_headerText = Mage::helper('stockupdate')->__('Manual Stock Management');
    //$this->_addButtonLabel = Mage::helper('stockupdate')->__('Add Item');
	
	$data = array(
            'label'     => 'Stock Update',
            'class'     => 'some-class',
            'onclick'   => 'setLocation(\' '  .  Mage::helper('adminhtml')->getUrl('stockupdate/adminhtml_stockupdate/stock') . '\')',
        );
        Mage_Adminhtml_Block_Widget_Container::addButton('my_button_identifier', $data);
	
	
    parent::__construct();
	 $this->_removeButton('add');
  }
}