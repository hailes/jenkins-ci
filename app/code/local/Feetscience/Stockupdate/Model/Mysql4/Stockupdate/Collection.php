<?php

class Feetscience_Stockupdate_Model_Mysql4_Stockupdate_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('stockupdate/stockupdate');
    }
}