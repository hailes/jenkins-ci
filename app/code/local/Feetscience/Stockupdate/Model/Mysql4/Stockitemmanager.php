<?php

class Feetscience_Stockupdate_Model_Mysql4_Stockitemmanager extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the stockupdate_id refers to the key field in your database table.
        $this->_init('stockupdate/stockitemmanager', 'stockitemmanager_id');
    }
}