<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('stockupdate')};
CREATE TABLE IF NOT EXISTS {$this->getTable('stockupdate')} (
  `stockupdate_id` int(11) unsigned NOT NULL auto_increment,
  `plant` varchar(50) NOT NULL default '',
  `material` varchar(50) NOT NULL default '',
  `batchno` varchar(50) NOT NULL default '',
  `unit` varchar(50) NOT NULL default '',
  `quantity` decimal(10,2) DEFAULT NULL,
   `mrp` decimal(10,2) DEFAULT NULL,
   `status` int(10) DEFAULT NULL,
 `datetime` datetime DEFAULT NULL,
 `storeid` int(10) DEFAULT NULL, 
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`stockupdate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");
	

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('stockinvoiceupdate')};
CREATE TABLE IF NOT EXISTS {$this->getTable('stockinvoiceupdate')} (
  `stockinvoiceupdate_id` int(11) unsigned NOT NULL auto_increment,  
  `portal_id` varchar(50) NOT NULL default '',
  `material` varchar(50) NOT NULL default '',
  `invoiceno` varchar(50) NOT NULL default '',     
   `status` varchar(255) DEFAULT NULL,  
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  `return_message` text NULL,
  PRIMARY KEY (`stockinvoiceupdate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('stockitemmanager')};
CREATE TABLE IF NOT EXISTS {$this->getTable('stockitemmanager')} (
  `stockitemmanager_id` int(11) unsigned NOT NULL auto_increment,
  `invoiceno` varchar(50) NOT NULL default '',
  `materialno` varchar(50) NOT NULL default '',
  `productname` text NULL default '',
  `weight` varchar(50) NOT NULL default '',
  `price` varchar(50) NOT NULL default '',     
  `totprice` varchar(50) NOT NULL default '',        
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`stockitemmanager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");
	
	
	
	

$installer->endSetup(); 