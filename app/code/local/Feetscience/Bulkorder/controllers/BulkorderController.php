<?php 

class Feetscience_Bulkorder_BulkorderController extends Mage_Core_Controller_Front_Action{

	public function indexAction()
	{

		$this->loadLayout()->renderLayout();
	}

	public function saveAction()
	{
		//getting infromation from the form
		$interest = $_POST['interest'];
		$qty = $_POST['qty'];
		$email = $_POST['email'];
		$telephone = $_POST['telephone'];
		$comment = $_POST['comment'];
		$date = Mage::getModel('core/date')->date('Y-m-d');
		
		if(!$email)
		{
			Mage::getSingleton('core/session')->addSuccess('Error for Submit Bulk order data');
			$this->_redirect('bulkorder/bulkorder/index');
		}else{
			//save data to the database
			$data = array('interest'=>$interest,'qty'=>$qty,'email'=>$email,'phone'=>$telephone,'comment'=>$comment,'receiveddate'=>$date);
			Mage::getModel('feetscience_bulkorder/bulkorder')->setData($data)->save();

		
			$to = 'rajram@feetscience.in';
			$subject = 'Bulk order';
			$from = 'info@feetscience.in';
			$cc = "sonu@tenovia.com,jayachandran@tenovia.com";
			$bcc = 'gautam.kakadiya37@gmail.com';

			$header = 'MIME-Version: 1.0'."\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";

			$header .='From: '.$from."\r\n".
				  'cc: '.$cc."\r\n".
		   		  'Bcc: '.$bcc."\r\n".
		   		  'X-Mailer: PHP/'.phpversion();

			$message = '<html><body>';
			$message .= '<h3>Bulk order information</h3>';
			$message .= '<table><tr><td>I am interested in : </td><td>'.$interest.'</td></tr>';
			$message .= '<tr><td>Number of pairs of shoes : </td><td>'.$qty.'</td></tr>';
			$message .= '<tr><td>E-mail address : </td><td>'.$email.'</td></tr>';
			$message .= '<tr><td>Phone No. : </td><td>'.$telephone.'</td></tr>';
			$message .= '<tr><td>Additional information : </td><td>'.$comment.'</td></tr></table>';
			$message .= '</body></html>';
		try{
			mail($to,$subject,$message,$header);
		} catch(Exception $error) {
			Mage::log($error->getMessage());
	        Mage::getSingleton('core/session')->addError($error->getMessage());
		}

		$data = "success";

		return $data;
		
		}
	}
}
