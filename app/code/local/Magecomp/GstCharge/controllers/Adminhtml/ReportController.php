<?php
class Magecomp_Gstcharge_Adminhtml_ReportController extends Mage_Adminhtml_Controller_action
{
    protected function _initAction()
    {
        $this->_title($this->__('gstcharge'))->_title($this->__('gstcharge'))->_title($this->__('Gst Reports'));
        $this->loadLayout()
        	->_setActiveMenu('gstcharge');
        return $this;
    }
	protected function _initReportAction($blocks)
    {
        if (!is_array($blocks)) 
		{
            $blocks = array($blocks);
        }
		
        $requestData    = Mage::helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'));
        $requestData    = $this->_filterDates($requestData, array('from', 'to'));
        $params         = $this->_getDefaultFilterData();

        foreach ($requestData as $key => $value) 
		{
            if (!empty($value)) 
			{
                $params->setData($key, $value);
            }
        }
        foreach ($blocks as $block) 
		{
            if ($block) 
			{
	            $block->setFilterData($params);
            }
        }
        return $this;
    }
	public function indexAction()
    {	
        $this->_initAction();
		$gridBlock = $this->getLayout()->getBlock('adminhtml_report.grid');
        $this->_initReportAction(array(
            $gridBlock,
        ));
        $this->renderLayout();
    }
	public function exportTotalsCsvAction()
    {
        $fileName   = 'Gst_report.csv';
        $content    = $this->getLayout()->createBlock('gstcharge/adminhtml_report_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    public function exportTotalsExcelAction()
    {
        $fileName   = 'Gst_report.xml';
        $content    = $this->getLayout()->createBlock('gstcharge/adminhtml_report_grid')->getExcel($fileName);
        $this->_prepareDownloadResponse($fileName, $content);
    }
	protected function _getDefaultFilterData()
    {
        return new Varien_Object(array(
            'from'      => date('Y-m-d G:i:s', strtotime('-1 month -1 day')),
            'to'        => date('Y-m-d G:i:s', strtotime('-1 day'))
        ));
    }
}