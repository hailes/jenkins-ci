<?php
/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('quote_item', 'excl_price', array('type' => 'int'));
$installer->addAttribute('order_item', 'excl_price', array('type' => 'int'));
$installer->addAttribute('invoice_item', 'excl_price', array('type' => 'int'));
$installer->addAttribute('creditmemo_item', 'excl_price', array('type' => 'int'));

$installer->run("ALTER TABLE `{$installer->getTable('sales/quote')}` ADD `excl_price` int(1) NOT NULL DEFAULT 1 ;");


$installer->endSetup();