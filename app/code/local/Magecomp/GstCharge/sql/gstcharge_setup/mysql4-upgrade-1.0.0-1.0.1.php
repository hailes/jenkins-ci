<?php
/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'gst_source', array(
    'type'                       => 'varchar',
    'label'                      => 'GST Percent',
    'input'                      => 'select',
    'required'                   => false,
    'sort_order'                 => 6,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'searchable'        => false,
	'filterable'        => false,
	'source' => 'gstcharge/source_gstoption',
));


$installer->addAttribute('catalog_category', 'gst_cat_source', array(
    'group'         => 'General Information',
    'type'          => 'varchar',
    'label'         => 'GST Percent',
    'input'         => 'select',
    'required'      => false,
    'sort_order'    => 90,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'source'        => 'gstcharge/source_gstoption'
));

//Delete old state of India
$installer->run("DELETE FROM `{$installer->getTable('directory_country_region')}` WHERE `country_id`='IN'");

$installer->run("

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'AP', 'Andhra Pradesh');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Andhra Pradesh');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'AR', 'Arunachal Pradesh');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Arunachal Pradesh');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'AS', 'Assam');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Assam');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'BR', 'Bihar');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Bihar');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'CG', 'Chhattisgarh');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Chhattisgarh');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'GA', 'Goa');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Goa');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'GJ', 'Gujarat');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Gujarat');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'HR', 'Haryana');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Haryana');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'HP', 'Himachal Pradesh');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Himachal Pradesh');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'JK', 'Jammu and Kashmir');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Jammu and Kashmir');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'JH', 'Jharkhand');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Jharkhand');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'KA', 'Karnataka');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Karnataka');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'KL', 'Kerala');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Kerala');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'MP', 'Madhya Pradesh');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Madhya Pradesh');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'MH', 'Maharashtra');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Maharashtra');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'MN', 'Manipur');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Manipur');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'ML', 'Meghalaya');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Meghalaya');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'MZ', 'Mizoram');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Mizoram');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'NL', 'Nagaland');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Nagaland');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'OR', 'Orissa');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Orissa');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'PB', 'Punjab');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Punjab');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'RJ', 'Rajasthan');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Rajasthan');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'SK', 'Sikkim');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Sikkim');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'TN', 'Tamil Nadu');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Tamil Nadu');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'TR', 'Tripura');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Tripura');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'UK', 'Uttarakhand');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Uttarakhand');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'UP', 'Uttar Pradesh');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Uttar Pradesh');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'WB', 'West Bengal');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'West Bengal');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'AN', 'Andaman and Nicobar Islands');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Andaman and Nicobar Islands');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'CH', 'Chandigarh');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Chandigarh');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'DH', 'Dadra and Nagar Haveli');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Dadra and Nagar Haveli');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'DD', 'Daman and Diu');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Daman and Diu');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'DL', 'Delhi');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Delhi');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'LD', 'Lakshadweep');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Lakshadweep');

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'PY', 'Pondicherry');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Pondicherry');
");



$installer->addAttribute('quote_item', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_item', 'cgst_percent', array('type' => 'decimal'));

$installer->addAttribute('quote_item', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_item', 'sgst_percent', array('type' => 'decimal'));

$installer->addAttribute('quote_item', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_item', 'igst_percent', array('type' => 'decimal'));


$installer->addAttribute('catalog_product', 'hsncode', array(
    'type'                       => 'varchar',
    'label'                      => 'HSN Code',
    'input'                      => 'text',
    'required'                   => false,
    'sort_order'                 => 6,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'searchable'        => false,
	'filterable'        => false,
));


$installer->addAttribute('order_item', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('order_item', 'cgst_percent', array('type' => 'decimal'));

$installer->addAttribute('order_item', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('order_item', 'sgst_percent', array('type' => 'decimal'));

$installer->addAttribute('order_item', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('order_item', 'igst_percent', array('type' => 'decimal'));


$installer->addAttribute('creditmemo_item', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo_item', 'cgst_percent', array('type' => 'decimal'));

$installer->addAttribute('creditmemo_item', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo_item', 'sgst_percent', array('type' => 'decimal'));

$installer->addAttribute('creditmemo_item', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo_item', 'igst_percent', array('type' => 'decimal'));


$installer->addAttribute('invoice_item', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice_item', 'cgst_percent', array('type' => 'decimal'));

$installer->addAttribute('invoice_item', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice_item', 'sgst_percent', array('type' => 'decimal'));

$installer->addAttribute('invoice_item', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice_item', 'igst_percent', array('type' => 'decimal'));


$installer->addAttribute('shipment_item', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('shipment_item', 'cgst_percent', array('type' => 'decimal'));

$installer->addAttribute('shipment_item', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('shipment_item', 'sgst_percent', array('type' => 'decimal'));

$installer->addAttribute('shipment_item', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('shipment_item', 'igst_percent', array('type' => 'decimal'));

 
$this->addAttribute('customer_address', 'buyer_gst_number', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'Buyer GST No#',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'visible_on_front' => 1
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'buyer_gst_number')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address'))
    ->save();
	

$tablequote = $this->getTable('sales/quote_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `buyer_gst_number` varchar(255) NOT NULL
");
 
$tablequote = $this->getTable('sales/order_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `buyer_gst_number` varchar(255) NOT NULL
");


$installer->endSetup();