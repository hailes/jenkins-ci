<?php
$installer = $this;
$installer->startSetup();


$installer->run("ALTER TABLE `{$installer->getTable('directory_country_region')}` ADD `state_code` varchar(3) COMMENT 'State Code';");

$installer->run("ALTER TABLE `{$installer->getTable('directory_country_region_name')}` ADD `state_code` varchar(3) COMMENT 'State Code';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '35' WHERE `default_name` = 'Andaman and Nicobar Islands';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '35' WHERE `name` = 'Andaman and Nicobar Islands';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '37' WHERE `default_name` = 'Andhra Pradesh';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '37' WHERE `name` = 'Andhra Pradesh';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '12' WHERE `default_name` = 
'Arunachal Pradesh';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '12' WHERE `name` = 'Arunachal Pradesh';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '18' WHERE `default_name` = 'Assam';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '18' WHERE `name` = 'Assam';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '10' WHERE `default_name` = 'Bihar';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '10' WHERE `name` = 'Bihar';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '04' WHERE `default_name` = 'Chandigarh';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '04' WHERE `name` = 'Chandigarh';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '22' WHERE `default_name` = 'Chhattisgarh';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '22' WHERE `name` = 'Chhattisgarh';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '26' WHERE `default_name` = 'Dadra and Nagar Haveli';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '26' WHERE `name` = 'Dadra and Nagar Haveli';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '07' WHERE `default_name` = 'Delhi';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '07' WHERE `name` = 'Delhi';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '30' WHERE `default_name` = 'Goa';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '30' WHERE `name` = 'Goa';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '24' WHERE `default_name` = 'Gujarat';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '24' WHERE `name` = 'Gujarat';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '06' WHERE `default_name` = 'Haryana';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '06' WHERE `name` = 'Haryana';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '02' WHERE `default_name` = 'Himachal Pradesh';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '02' WHERE `name` = 'Himachal Pradesh';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '01' WHERE `default_name` = 'Jammu and Kashmir';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '01' WHERE `name` = 'Jammu and Kashmir';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '20' WHERE `default_name` = 'Jharkhand';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '20' WHERE `name` = 'Jharkhand';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '29' WHERE `default_name` = 'Karnataka';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '29' WHERE `name` = 'Karnataka';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '32' WHERE `default_name` = 'Kerala';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '32' WHERE `name` = 'Kerala';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '31' WHERE `default_name` = 'Lakshadweep';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '31' WHERE `name` = 'Lakshadweep';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '23' WHERE `default_name` = 'Madhya Pradesh';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '23' WHERE `name` = 'Madhya Pradesh';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '27' WHERE `default_name` = 'Maharashtra';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '27' WHERE `name` = 'Maharashtra';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '14' WHERE `default_name` = 'Manipur';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '14' WHERE `name` = 'Manipur';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '15' WHERE `default_name` = 'Meghalaya';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '15' WHERE `name` = 'Meghalaya';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '13' WHERE `default_name` = 'Nagaland';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '13' WHERE `name` = 'Nagaland';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '21' WHERE `default_name` = 'Orissa';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '21' WHERE `name` = 'Orissa';"); 

$installer->run("

INSERT INTO `{$installer->getTable('directory_country_region')}` (`country_id`, `code`, `default_name`) VALUES
    ('IN', 'PY', 'Pondichery');
INSERT INTO `{$installer->getTable('directory_country_region_name')}` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Pondichery');
");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '34' WHERE `default_name` = 'Pondichery';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '34' WHERE `name` = 'Pondichery';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '03' WHERE `default_name` = 'Punjab';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '03' WHERE `name` = 'Punjab';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '08' WHERE `default_name` = 'Rajasthan';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '08' WHERE `name` = 'Rajasthan';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '11' WHERE `default_name` = 'Sikkim';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '11' WHERE `name` = 'Sikkim';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '33' WHERE `default_name` = 'Tamilnadu';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '33' WHERE `name` = 'Tamilnadu';");

$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '36' WHERE `default_name` = 'Telangana';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '36' WHERE `name` = 'Telangana';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '16' WHERE `default_name` = 'Tripura';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '16' WHERE `name` = 'Tripura';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '09' WHERE `default_name` = 'Uttar Pradesh';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '09' WHERE `name` = 'Uttar Pradesh';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '05' WHERE `default_name` = 'Uttarakhand';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '05' WHERE `name` = 'Uttarakhand';");


$installer->run("UPDATE `{$installer->getTable('directory_country_region')}` SET `state_code` = '19' WHERE `default_name` = 'West Bengal';");
$installer->run("UPDATE `{$installer->getTable('directory_country_region_name')}` SET `state_code` = '19' WHERE `name` = 'West Bengal';");

$this->addAttribute('customer_address', 'state_code', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'State Code',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'visible_on_front' => 0
));

$tablequote = $this->getTable('sales/quote_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `state_code` varchar(3) NOT NULL
");

$tablequote = $this->getTable('sales/order_address');
$installer->run("
ALTER TABLE  $tablequote ADD  `state_code` varchar(3) NOT NULL
");

$installer->addAttribute('quote_address', 'shipping_sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_address', 'percent_shipping_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('order', 'shipping_sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('order', 'percent_shipping_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('invoice', 'shipping_sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'percent_shipping_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'shipping_sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'percent_shipping_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('quote_address', 'shipping_cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_address', 'percent_shipping_cgst_charge', array('type' => 'decimal'));

$installer->addAttribute('order', 'shipping_cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('order', 'percent_shipping_cgst_charge', array('type' => 'decimal'));

$installer->addAttribute('invoice', 'shipping_cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'percent_shipping_cgst_charge', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'shipping_cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'percent_shipping_cgst_charge', array('type' => 'decimal'));


$installer->addAttribute('quote_address', 'shipping_igst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_address', 'percent_shipping_igst_charge', array('type' => 'decimal'));

$installer->addAttribute('order', 'shipping_igst_charge', array('type' => 'decimal'));
$installer->addAttribute('order', 'percent_shipping_igst_charge', array('type' => 'decimal'));

$installer->addAttribute('invoice', 'shipping_igst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'percent_shipping_igst_charge', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'shipping_igst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'percent_shipping_igst_charge', array('type' => 'decimal'));

$installer->endSetup();
