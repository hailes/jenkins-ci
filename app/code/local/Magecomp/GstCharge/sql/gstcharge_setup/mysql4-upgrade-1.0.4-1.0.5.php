<?php
/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttributeToGroup(
    Mage_Catalog_Model_Product::ENTITY, 
    $installer->getDefaultAttributeSetId(Mage_Catalog_Model_Product::ENTITY), 
    'Prices',
    'hsncode',
	960
);

$installer->addAttributeToGroup(
    Mage_Catalog_Model_Product::ENTITY, 
    $installer->getDefaultAttributeSetId(Mage_Catalog_Model_Product::ENTITY), 
    'Prices',
    'gst_source',
	970
);

$installer->addAttributeToGroup(
    Mage_Catalog_Model_Product::ENTITY, 
    $installer->getDefaultAttributeSetId(Mage_Catalog_Model_Product::ENTITY), 
    'Prices',
    'gst_source_after_minprice',
	980
);

$installer->addAttributeToGroup(
    Mage_Catalog_Model_Product::ENTITY, 
    $installer->getDefaultAttributeSetId(Mage_Catalog_Model_Product::ENTITY), 
    'Prices',
    'gst_source_minprice',
	990
);


$installer->updateAttribute(
	'catalog_product', 
	'gst_source', 
	array(
    	'frontend_label'  => 'GST Rate(in Percentage)',
	)
);

$installer->updateAttribute(
	'catalog_product', 
	'gst_source_minprice', 
	array(
    	'frontend_label'  => 'Minimum Product Price to Apply GST Rate',
	)
);

$installer->updateAttribute(
	'catalog_product', 
	'gst_source_after_minprice', 
	array(
    	'frontend_label'  => 'GST Rate if Product Price Below Minimum Set Price',
	)
);


$installer->updateAttribute(
	'catalog_category', 
	'gst_cat_source', 
	array(
    	'frontend_label'  => 'GST Rate(in Percentage)',
	)
);

$installer->updateAttribute(
	'catalog_category', 
	'gst_cat_source_minprice', 
	array(
    	'frontend_label'  => 'Minimum Product Price to Apply GST Rate',
	)
);

$installer->updateAttribute(
	'catalog_category', 
	'gst_cat_source_after_minprice', 
	array(
    	'frontend_label'  => 'GST Rate if Product Price Below Minimum Set Price',
	)
);

$setId = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getDefaultAttributeSetId();
$installer->addAttributeGroup('catalog_category',$setId, 'Indian GST');

$entityTypeId = $installer->getEntityTypeId('catalog_category');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);

$installer->addAttributeToGroup($entityTypeId, $attributeSetId, 'Indian GST', 'gst_cat_source_minprice');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, 'Indian GST', 'gst_cat_source_after_minprice');
$installer->addAttributeToGroup($entityTypeId, $attributeSetId, 'Indian GST', 'gst_cat_source');


$installer->endSetup();