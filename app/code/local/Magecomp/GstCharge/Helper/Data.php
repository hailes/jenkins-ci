<?php
class Magecomp_GstCharge_Helper_Data extends Mage_Core_Helper_Abstract
{
	const GSTCHARGE_OPTIONS_STATE                  =  'gstcharge/options/state';
	const GSTCHARGE_OPTIONS_TAXTYPE                =  'gstcharge/options/taxtype';
	const GSTCHARGE_OPTIONS_ENABLE                 =  'gstcharge/options/enabled';
	const GSTCHARGE_OPTIONS_TAX_MINPRICE           =  'gstcharge/options/tax_minprice';
	const GSTCHARGE_OPTIONS_TAX_PERCENT            =  'gstcharge/options/tax_percent';
	const GSTCHARGE_OPTIONS_TAX_PERCENT_MINPRICE   =  'gstcharge/options/tax_percent_minprice';
	
	public function getCgstCharge($quote=null)
	{
		$taxPrice=0;
		try
		{
			$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
			$quote = Mage::getModel('sales/quote')->load($quoteId);
			
			if($quote->getBaseGrandTotal() > 0)
			{
				$shippingAddress = $quote->getShippingAddress();	
				if($shippingAddress)
				{
					$taxPrice=Mage::helper('gstcharge')->calculateGst();			
					$CountryId=$shippingAddress->getCountryId();
					$CustomerRegionId=$shippingAddress->getRegionId();
					$SystemRegionId=Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);			
					
					if(!($CountryId=='IN' && $CustomerRegionId==$SystemRegionId))
					{
						$taxPrice=0;	
					}
				}
			}
		}
		catch(Exception $e)
		{
			$e->getMessage();	
		}
		return $taxPrice/2;
	}
	public function getSgstCharge($quote=null)
	{
		$taxPrice=0;
		try
		{
			$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
			$quote = Mage::getModel('sales/quote')->load($quoteId);
			
			if($quote->getBaseGrandTotal() > 0)
			{
				$shippingAddress = $quote->getShippingAddress();
				if($shippingAddress)
				{
					$taxPrice=Mage::helper('gstcharge')->calculateGst();
					$CountryId=$shippingAddress->getCountryId();
					$CustomerRegionId=$shippingAddress->getRegionId();
					$SystemRegionId=Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);			
					
					if(!($CountryId=='IN' && $CustomerRegionId==$SystemRegionId))
					{
						$taxPrice=0;	
					}
				}
			}
		}
		catch(Exception $e)
		{
			$e->getMessage();
		}
		return $taxPrice/2;
	}
	public function getIgstCharge($quote=null)
	{
		$taxPrice=0;
		try
		{
			$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
			$quote = Mage::getModel('sales/quote')->load($quoteId);
			
			if($quote->getBaseGrandTotal() > 0)
			{
				$shippingAddress = $quote->getShippingAddress();
				if($shippingAddress)
				{
					$taxPrice=Mage::helper('gstcharge')->calculateGst();
					$CountryId=$shippingAddress->getCountryId();
					$CustomerRegionId=$shippingAddress->getRegionId();
					$SystemRegionId=Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);			
					
					if($CountryId!='IN' || $CustomerRegionId==$SystemRegionId)
					{
						$taxPrice=0;	
					}
				}
			}
		}
		catch(Exception $e)
		{
			$e->getMessage();	
		}
		return $taxPrice;
	}
	public function calculateGst()
	{
		try
		{
			if(!(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_ENABLE)))
			{
				return 0;
			}
			
			$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
			$quote = Mage::getModel('sales/quote')->load($quoteId);
			
			if($quote->getBaseGrandTotal() > 0)
			{
			
				$shippingAddress = $quote->getShippingAddress();
				if($shippingAddress)
				{
					$CountryId=$shippingAddress->getCountryId();
					$CustomerRegionId=$shippingAddress->getRegionId();
					$SystemRegionId=Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);
					
					if($CountryId!='IN')
					{
						  return 0;
					}
					
					$TotalGstPrice=0;
					
					$cart = Mage::getModel('sales/quote')->load($quoteId);
					if(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAXTYPE))
					{
						$quote = Mage::getSingleton('checkout/session')->getQuote();
						$quote->setExclPrice(1)->save();
						$cart->setExclPrice(1);
					}
					else
					{
					 	 $quote = Mage::getSingleton('checkout/session')->getQuote();
						 $quote->setExclPrice(0)->save();
					 	 $cart->setExclPrice(0);
					}
					$cart->save();
					
					$maxGstPercent = 0;
		
	//				foreach ($cart->getAllVisibleItems() as $item) 
					foreach ($cart->getAllItems() as $item) 
					{
						$gstPercent=0;
					   if ($item->getData('product_type') == "bundle")
						   	continue;
	   
						$product=Mage::getModel('catalog/product')->load($item->getProductId());
						   
						$itemPriceAfterDiscount= ($item->getPrice() * $item->getDiscountPercent())/100 ;
						$prdPrice=$item->getPrice()-$itemPriceAfterDiscount;
						   
						$gstPercent=$product->getGstSource();
						$gstPercentMinPrice=$product->getGstSourceMinprice();
						$gstPercentAfterMinprice=$product->getGstSourceAfterMinprice();
						   
						if($gstPercent<=0)
						{
							$cats = $product->getCategoryIds();
							foreach ($cats as $category_id) 
							{
								$_cat = Mage::getModel('catalog/category')->load($category_id) ;
									
								$gstPercent=$_cat->getGstCatSource();
								$gstPercentMinPrice=$_cat->getGstCatSourceMinprice();
								$gstPercentAfterMinprice=$_cat->getGstCatSourceAfterMinprice();
									
								if($gstPercent!='')
								{
									if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
									{
										$gstPercent=$gstPercentAfterMinprice;
									}
									break;
								}
						   }   
						}
						else
						{
							if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
							{
								$gstPercent=$gstPercentAfterMinprice;
							}
						}
						      
						if($gstPercent<=0)
						{
							$gstPercent				=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT);
							$gstPercentMinPrice		=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_MINPRICE);
							$gstPercentAfterMinprice=   Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT_MINPRICE);
							  
							if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
							{
								$gstPercent=$gstPercentAfterMinprice;
							}
						}
						
						$qty          = $item->getQty();
						$rowTotal     = $item->getRowTotal();
						$DiscountAmount=$item->getDiscountAmount();
						   
						if(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAXTYPE))
						{
							$GstPrice= ((($rowTotal-$DiscountAmount)*$gstPercent)/100);   
						}
						else
						{
							$totalPercent = 100 + $gstPercent;
							$perPrice     = ($rowTotal-$DiscountAmount) / $totalPercent;
							$GstPrice     = $perPrice * $gstPercent;
						}
						    
						$TotalGstPrice+=$GstPrice;
						if($CountryId=='IN' && $CustomerRegionId==$SystemRegionId)
						{
							$item->setCgstCharge($GstPrice/2);
							$item->setCgstPercent($gstPercent/2);
							$item->setSgstCharge($GstPrice/2);
							$item->setSgstPercent($gstPercent/2);
							$item->setIgstCharge(0);
							$item->setIgstPercent(0);
						}
						else if ($CountryId=='IN' && $CustomerRegionId!=$SystemRegionId)
						{
							$item->setIgstCharge($GstPrice);
							$item->setIgstPercent($gstPercent);
						}
						   
						if(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAXTYPE))
						{
							$item->setExclPrice(1);
						}
						else
						{
							$item->setExclPrice(0);
						}
						$item->save();
					}
				}
			}
		}
		catch(Exception $e)
		{
			$e->getMessage();	
		}
		return  $TotalGstPrice;
	}
	
	public function isUniTerrState($state)
    {
        $uniTerrState = array('Chandigarh',
            'Dadra and Nagar Haveli',
            'Daman and Diu',
            'Lakshadweep',
            'Andaman and Nicobar Islands');

        if (in_array(trim($state), $uniTerrState)) {
            return true;
        }
        return false;
    }
}
