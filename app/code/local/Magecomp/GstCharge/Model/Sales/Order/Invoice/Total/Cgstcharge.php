<?php
class Magecomp_GstCharge_Model_Sales_Order_Invoice_Total_Cgstcharge extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
		$invoice->setCgstCharge(0);
        $invoice->setBaseCgstCharge(0);

    	$quoteId=$invoice->getOrder()->getQuoteId();
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
					 
		$exckudingTax=$quote->getExclPrice();
		
    	$amount = $invoice->getOrder()->getCgstCharge();
        $invoice->setCgstCharge($amount);
        
        $amount = $invoice->getOrder()->getBaseCgstCharge();
        $invoice->setBaseCgstCharge($amount);
       
	    if($exckudingTax)
		{
        	$invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getCgstCharge());
        	$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getBaseCgstCharge());
		}

        return $this;
    }
}