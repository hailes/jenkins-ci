<?php
class Magecomp_GstCharge_Model_Sales_Order_Invoice_Total_Igstcharge extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
    	$invoice->setIgstCharge(0);
        $invoice->setBaseIgstCharge(0);
		
		$quoteId=$invoice->getOrder()->getQuoteId();
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
					 
		$exckudingTax=$quote->getExclPrice();
    	$amount = $invoice->getOrder()->getIgstCharge();        
        $invoice->setIgstCharge($amount);
        
        $amount = $invoice->getOrder()->getBaseIgstCharge();
        $invoice->setBaseIgstCharge($amount);
        
		if($exckudingTax)
		{
        	$invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getIgstCharge());
        	$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getBaseIgstCharge());
		}

        return $this;
    }
}