<?php
class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_Igstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$creditmemo->setIgstCharge(0);
        $creditmemo->setBaseIgstCharge(0);

        $amount = $creditmemo->getOrder()->getIgstCharge();        
        $creditmemo->setIgstCharge($amount);
        
		$quoteId=$creditmemo->getOrder()->getQuoteId();
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
		$exckudingTax=$quote->getExclPrice();
		
        $amount = $creditmemo->getOrder()->getBaseIgstCharge();
        $creditmemo->setBaseIgstCharge($amount);
		
		if($exckudingTax)
		{
        	$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getIgstCharge());
        	$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseIgstCharge());
		}
        return $this;
    }
}