<?php
class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_Shippingcgstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) 
	{
		$creditmemo->setPercentShippingCgstCharge(0);
		$creditmemo->setShippingCgstCharge(0);
		
    	$amount = $creditmemo->getOrder()->getShippingCgstCharge();
		$percent = $creditmemo->getOrder()->getPercentShippingCgstCharge();
		
		$creditmemo->setPercentShippingCgstCharge($percent);
		$creditmemo->setShippingCgstCharge($amount);
		
		return $this;
	}
}
