<?php
class Magecomp_GstCharge_Model_Sales_Order_Invoice_Total_Shippingcgstcharge extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Invoice $invoice) 
	{
		$invoice->setPercentShippingCgstCharge(0);
		$invoice->setShippingCgstCharge(0);
		
    	$amount = $invoice->getOrder()->getShippingCgstCharge();
		$percent = $invoice->getOrder()->getPercentShippingCgstCharge();
		
		$invoice->setPercentShippingCgstCharge($percent);
		$invoice->setShippingCgstCharge($amount);
		
		$invoice->setGrandTotal($invoice->getOrder()->getGrandTotal());
       	$invoice->setBaseGrandTotal($invoice->getOrder()->getBaseGrandTotal());
		
        return $this;
	}
	
}
