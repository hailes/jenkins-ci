<?php
class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_Sgstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$creditmemo->setSgstCharge(0);
        $creditmemo->setBaseSgstCharge(0);

        $amount = $creditmemo->getOrder()->getSgstCharge();        
        $creditmemo->setSgstCharge($amount);
        
		$quoteId=$creditmemo->getOrder()->getQuoteId();
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
		$exckudingTax=$quote->getExclPrice();
		
        $amount = $creditmemo->getOrder()->getBaseSgstCharge();
        $creditmemo->setBaseSgstCharge($amount);
		
		if($exckudingTax)
		{
        	$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getSgstCharge());
        	$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseSgstCharge());
		}
		
        return $this;
    }
}