<?php
class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_ShippingIgstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) 
	{
		$creditmemo->setPercentShippingIgstCharge(0);
		$creditmemo->setShippingIgstCharge(0);
		
    	$amount = $creditmemo->getOrder()->getShippingIgstCharge();
		$percent = $creditmemo->getOrder()->getPercentShippingIgstCharge();
		
		$creditmemo->setPercentShippingIgstCharge($percent);
		$creditmemo->setShippingIgstCharge($amount);

        return $this;
	}
}
