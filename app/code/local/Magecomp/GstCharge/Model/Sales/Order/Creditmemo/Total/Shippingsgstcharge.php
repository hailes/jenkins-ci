<?php
class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_ShippingSgstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) 
	{
		$creditmemo->setPercentShippingSgstCharge(0);
		$creditmemo->setShippingSgstCharge(0);
		
		$amount = $creditmemo->getOrder()->getShippingSgstCharge();
		$percent = $creditmemo->getOrder()->getPercentShippingSgstCharge();
		
		$creditmemo->setPercentShippingSgstCharge($percent);
		$creditmemo->setShippingSgstCharge($amount);
		
		return $this;
	}
}
