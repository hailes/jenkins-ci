<?php
class Magecomp_GstCharge_Model_Sales_Order_Invoice_Total_Shippingigstcharge extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Invoice $invoice) 
	{
		$invoice->setPercentShippingIgstCharge(0);
		$invoice->setShippingIgstCharge(0);
		
    	$amount = $invoice->getOrder()->getShippingIgstCharge();
		$percent = $invoice->getOrder()->getPercentShippingIgstCharge();
		
		$invoice->setPercentShippingIgstCharge($percent);
		$invoice->setShippingIgstCharge($amount);
		
		$invoice->setGrandTotal($invoice->getOrder()->getGrandTotal());
       	$invoice->setBaseGrandTotal($invoice->getOrder()->getBaseGrandTotal());
		
        return $this;
	}
	
}
