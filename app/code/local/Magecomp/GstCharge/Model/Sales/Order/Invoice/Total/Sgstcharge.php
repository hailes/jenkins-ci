<?php
class Magecomp_GstCharge_Model_Sales_Order_Invoice_Total_Sgstcharge extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
    	$invoice->setSgstCharge(0);
        $invoice->setBaseSgstCharge(0);
		
		$quoteId=$invoice->getOrder()->getQuoteId();

		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
					 
		$exckudingTax=$quote->getExclPrice();
    	$amount = $invoice->getOrder()->getSgstCharge();        
        $invoice->setSgstCharge($amount);
        
        $amount = $invoice->getOrder()->getBaseSgstCharge();
        $invoice->setBaseSgstCharge($amount);
        
		if($exckudingTax)
		{
		    $invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getSgstCharge());
        	$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getBaseSgstCharge());
		}

        return $this;
    }
}