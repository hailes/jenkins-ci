<?php
class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_Cgstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$creditmemo->setCgstCharge(0);
        $creditmemo->setBaseCgstCharge(0);

        $amount = $creditmemo->getOrder()->getCgstCharge();
        $creditmemo->setCgstCharge($amount);
        
		$quoteId=$creditmemo->getOrder()->getQuoteId();
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
					 
		$exckudingTax=$quote->getExclPrice();
		
		$amount = $creditmemo->getOrder()->getBaseCgstCharge();
        $creditmemo->setBaseCgstCharge($amount);
		
		if($exckudingTax)
		{
        	$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getCgstCharge());
        	$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseCgstCharge());
		}
		
        return $this;
    }
}