<?php
class Magecomp_GstCharge_Model_Sales_Order_Invoice_Total_Shippingsgstcharge extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Invoice $invoice) 
	{
		$invoice->setPercentShippingSgstCharge(0);
		$invoice->setShippingSgstCharge(0);
		
    	$amount = $invoice->getOrder()->getShippingSgstCharge();
		$percent = $invoice->getOrder()->getPercentShippingSgstCharge();
		
		$invoice->setPercentShippingSgstCharge($percent);
		$invoice->setShippingSgstCharge($amount);
		
		$invoice->setGrandTotal($invoice->getOrder()->getGrandTotal());
       	$invoice->setBaseGrandTotal($invoice->getOrder()->getBaseGrandTotal());
        return $this;
	}
	
}
