<?php
/**
 * @category   Magecomp
 * @package    Magecomp_GstCharge
 */
class Magecomp_GstCharge_Model_Sales_Quote_Address_Total_Igstcharge extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	const GSTCHARGE_OPTIONS_TAXTYPE   =  'gstcharge/options/taxtype'; 
	 
    public function __construct()
    {
        $this->setCode('cgst_charge');
    }
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $address->setIgstCharge(0);
        $address->setBaseIgstCharge(0);
        
    	$items = $address->getAllItems();
        if (!count($items)) 
		{
            return $this;
        }
        
		$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
		
		if($quote->getBaseGrandTotal() > 0)
		{
			$shippngAddress=$quote->getShippingAddress();
			if ($shippngAddress) 
			{ 
				$amount = Mage::helper('gstcharge')->getIgstCharge($address->getQuote());
				$address->setIgstCharge($amount);
				$address->setBaseIgstCharge($amount);
			}
			
			if(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAXTYPE))
			{
				$address->setGrandTotal($address->getGrandTotal() + $address->getIgstCharge());
				$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseIgstCharge());
			}
			else
			{
				$address->setGrandTotal($address->getGrandTotal());
				$address->setBaseGrandTotal($address->getBaseGrandTotal());
			}
		}
		
        return $this;
    }
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $amount = $address->getIgstCharge();
        if (($amount!=0)) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('sales')->__('IGST Charge'),
                'full_info' => array(),
                'value' => $amount
            ));
        }
        return $this;
    }
}