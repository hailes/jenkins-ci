<?php
/**
 * @category   Magecomp
 * @package    Magecomp_GstCharge
 */
class Magecomp_GstCharge_Model_Sales_Quote_Address_Total_Sgstcharge extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	const GSTCHARGE_OPTIONS_TAXTYPE   =  'gstcharge/options/taxtype'; 
	 
    public function __construct()
    {
        $this->setCode('cgst_charge');
    }
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $address->setSgstCharge(0);
        $address->setBaseSgstCharge(0);
        
    	$items = $address->getAllItems();
        if (!count($items)) 
		{
            return $this;
        }
        
       	$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
		
		if($quote->getBaseGrandTotal() > 0)
		{
			$shippngAddress=$quote->getShippingAddress();
			if ($shippngAddress) 
			{ 
				$amount = Mage::helper('gstcharge')->getSgstCharge($address->getQuote());
				$address->setSgstCharge($amount);
				$address->setBaseSgstCharge($amount);
			}
			
			if(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAXTYPE))
			{
				$address->setGrandTotal($address->getGrandTotal() + $address->getSgstCharge());
				$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseSgstCharge());
			}
			else
			{
				$address->setGrandTotal($address->getGrandTotal());
				$address->setBaseGrandTotal($address->getBaseGrandTotal());
			}
		}
        return $this;
    }
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
		$region = $address->getRegion();
		$Urstate = Mage::helper('gstcharge')->isUniTerrState($region);
        $amount = $address->getSgstCharge();
        if (($amount!=0)) {
			if($Urstate)
			{
				$address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('sales')->__('UTGST Charge'),
                'full_info' => array(),
                'value' => $amount
            ));
			}
			else
			{
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => Mage::helper('sales')->__('SGST Charge'),
                'full_info' => array(),
                'value' => $amount
            ));
			}
        }
        return $this;
    }
}