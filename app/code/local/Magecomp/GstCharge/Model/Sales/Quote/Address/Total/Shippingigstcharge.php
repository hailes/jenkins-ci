<?php
class Magecomp_GstCharge_Model_Sales_Quote_Address_Total_Shippingigstcharge extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	const GSTCHARGE_SHIPPINGSETTINGS_ENABLED   =  'gstcharge/shippingsettings/enabled';
	const GSTCHARGE_SHIPPINGSETTINGS_TAXTYPE   =  'gstcharge/shippingsettings/taxtype';
	const GSTCHARGE_OPTIONS_STATE              =  'gstcharge/options/state';
		
	public function __construct()
    {
        $this->setCode('shipping_igst_charge');
    }
	public function collect(Mage_Sales_Model_Quote_Address $address) 
	{
		$address->setPercentShippingIgstCharge(0);
		$address->setShippingIgstCharge(0);
		if(Mage::getStoreConfig(self::GSTCHARGE_SHIPPINGSETTINGS_ENABLED)):
			$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
			$cart = Mage::getModel('sales/quote')->load($quoteId);
		
			$countryId = $address->getCountryId();
			$customerRegionId = $address->getRegionId();
			$systemRegionId = Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);
			
			$maxGstPercent = $gstPercent = $shippingGst = 0;
			foreach ($cart->getAllVisibleItems() as $item) 
			{
				if($countryId == 'IN' && $customerRegionId!=$systemRegionId)
				{
					$gstPercent = $item->getIgstPercent();
				}
				if ($gstPercent > $maxGstPercent)
					$maxGstPercent = $gstPercent;
			}
			
			if(Mage::getStoreConfig(self::GSTCHARGE_SHIPPINGSETTINGS_TAXTYPE))
			{
				$shippingGst = $address->getShippingAmount() * ($maxGstPercent/100);
				$address->setPercentShippingIgstCharge($maxGstPercent);
				$address->setShippingIgstCharge($shippingGst);
			}
			else
			{
				$perpercent = $address->getShippingAmount() / ($maxGstPercent + 100);
				$shippingGst = $perpercent * $maxGstPercent;
				$address->setPercentShippingIgstCharge($maxGstPercent);
				$address->setShippingIgstCharge($shippingGst);
			}
			
			if(Mage::getStoreConfig(self::GSTCHARGE_SHIPPINGSETTINGS_TAXTYPE))
			{
				$address->setGrandTotal($address->getGrandTotal() + $address->getShippingIgstCharge());
				$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getShippingIgstCharge());
			}
			else
			{
				$address->setGrandTotal($address->getGrandTotal());
				$address->setBaseGrandTotal($address->getBaseGrandTotal());
			}
			$address->save();
		
		endif;
		return $this;
	}
	public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
	    $cart = Mage::getModel('sales/quote')->load($quoteId);
		
		$amount = $address->getShippingIgstCharge();
		if ($amount != 0 && $address->getAddressType() == 'shipping') 
		{
			$title = Mage::helper('gstcharge')->__('Shipping IGST');
			$address->addTotal(array(
				'code' => $this->getCode(),
				'title' => $title,
				'value' => $amount
			));
		}
		
		return $this;
    }
}
