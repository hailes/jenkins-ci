<?php
class Magecomp_Gstcharge_Model_Order_Pdf_Items_Invoice_Bundle extends Mage_Bundle_Model_Sales_Order_Pdf_Items_Abstract
{
	public function draw()
	{

		$stringHelper = Mage::helper('core/string');
		$item   = $this->getItem();

		$pdf    = $this->getPdf();

		$page   = $this->getPage();
		$this->_setFontRegular();
		$items = $this->getChilds($item);

		$_prevOptionId = '';
		$drawItems = array();
		$fontSize=6;

		foreach ($items as $_item) {
			$line   = array();
			$hsncode = $this->getHsncodeValue($_item);
			$attributes = $this->getSelectionAttributes($_item);
			if (is_array($attributes)) {
				$optionId = $attributes['option_id'];
			}
			else {
				$optionId = 0;
			}

			if (!isset($drawItems[$optionId])) {
				$drawItems[$optionId] = array(
					'lines'  => array(),
					'height' => 15
				);
			}

			if ($_item->getOrderItem()->getParentItem()) {
				if ($_prevOptionId != $attributes['option_id']) {
					$line[0] = array(
						'font' => 'italic',
						'text' => $stringHelper->str_split($attributes['option_label'], 45, true, true),
						'feed' => 35
					);

					$drawItems[$optionId] = array(
						'lines'  => array($line),
						'height' => 15
					);

					$line = array();

					$_prevOptionId = $attributes['option_id'];
				}
			}
			/* calcuation for subtotal */
			$taxableAmount=$_item->getrow_total() - $_item->getdiscount_amount();
			if(Mage::getStoreConfig('gstcharge/options/taxtype'))
			{
				$subTotal = $_item->getrow_total();
				$itemTotal = $subTotal + $_item->getcgst_charge() + $_item->getsgst_charge() + $_item->getigst_charge();
			}
			else
			{
				$subTotal = $_item->getrow_total() - $_item->getcgst_charge() - $_item->getsgst_charge() - $_item->getigst_charge();
				$taxableAmount=$subTotal - $_item->getdiscount_amount();
				$itemTotal = $taxableAmount + $_item->getcgst_charge() + $_item->getsgst_charge() + $_item->getigst_charge();
			}
			/* */

			/* in case Product name is longer than 80 chars - it is written in a few lines */
			if ($_item->getOrderItem()->getParentItem()) {
				$feed = 40;
				$name = $this->getValueHtml($_item);
			} else {
				$feed = 35;
				$name = $_item->getName();
			}
			$line[] = array(
				'text'  => $stringHelper->str_split($name, 35, true, true),
				'feed'  => $feed,
				'font_size' => $fontSize
			);
			if (!$_item->getOrderItem()->getParentItem()){continue;}
			// draw QTY
			$line[] = array(
				'text'  => round($_item->getQty()),
				'feed'  => 180,
				'font_size' => $fontSize,
			);
			// draw Price
			$line[] = array(
				'text'  => round($_item->getPrice(),2),
				'feed'  => 224,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw Subtotal
			$line[] = array(
				'text'  => round($subTotal,2),
				'feed'  => 260,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw Discount
			$line[] = array(
				'text'  => round($_item->getdiscount_amount(),2),
				'feed'  => 283,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw Taxable amount
			$lines[] = array(
				'text'  => round($taxableAmount,2),
				'feed'  => 340,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw CGST RATE
			$line[] = array(
				'text'  => round($_item->getcgst_percent(),2),
				'feed'  => 365,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw CGST Amount
			$line[] = array(
				'text'  => round($_item->getcgst_charge(),2),
				'feed'  => 405,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw SGST RATE
			$line[] = array(
				'text'  => round($_item->getsgst_percent(),2),
				'feed'  => 425,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw SGST AMOUNT
			$line[] = array(
				'text'  => round($_item->getsgst_charge(),2),
				'feed'  => 465,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw IGST RATE
			$line[] = array(
				'text'  => round($_item->getigst_percent(),2),
				'feed'  => 480,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw IGST AMOUNT
			$line[] = array(
				'text'  => round($_item->getigst_charge(),2),
				'feed'  => 518,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);
			// draw TOTAL AMOUNT
			$line[] = array(
				'text'  => round($itemTotal,2),
				'feed'  => 565,
				'font'  => 'bold',
				'align' => 'right',
				'font_size' => $fontSize,
			);

			$drawItems[$optionId]['lines'][] = $line;
		}

		// custom options
		$options = $item->getOrderItem()->getProductOptions();
		if ($options) {
			if (isset($options['options'])) {
				foreach ($options['options'] as $option) {
					$lines = array();
					$lines[][] = array(
						'text'  => $stringHelper->str_split(strip_tags($option['label']), 40, true, true),
						'font'  => 'italic',
						'feed'  => 35
					);

					if ($option['value']) {
						$text = array();
						$_printValue = isset($option['print_value'])
							? $option['print_value']
							: strip_tags($option['value']);
						$values = explode(', ', $_printValue);
						foreach ($values as $value) {
							foreach ($stringHelper->str_split($value, 30, true, true) as $_value) {
								$text[] = $_value;
							}
						}

						$lines[][] = array(
							'text'  => $text,
							'feed'  => 40
						);
					}

					$drawItems[] = array(
						'lines'  => $lines,
						'height' => 15
					);
				}
			}
		}

		$page = $pdf->drawLineBlocks($page, $drawItems, array('table_header' => true));

		$this->setPage($page);
	}
	private function getHsncodeValue($item)
	{
		$prod = Mage::getModel('catalog/product')->load($item->getProductId());

		if(!($return_location = $prod->getHsncode()))
			return 'N/A';
		else
			return $return_location;
	}
}
