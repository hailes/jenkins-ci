<?php
class Magecomp_Gstcharge_Model_Order_Pdf_Items_Invoice_Default extends Mage_Sales_Model_Order_Pdf_Items_Abstract
{
    /**
     * Draw item line
	 **/
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
		$pdf    = $this->getPdf();
        $page   = $this->getPage();
        $lines  = array();
		$fontSize= 6;
			$hsncode = $this->getHsncodeValue($item);
 		$taxableAmount=$item->getrow_total() - $item->getdiscount_amount();
		if(Mage::getStoreConfig('gstcharge/options/taxtype'))
		{
			$subTotal = $item->getrow_total();
			$itemTotal = $subTotal + $item->getcgst_charge() + $item->getsgst_charge() + $item->getigst_charge();
		}
		else
		{
			$subTotal = $item->getrow_total() - $item->getcgst_charge() - $item->getsgst_charge() - $item->getigst_charge();
			$taxableAmount=$subTotal - $item->getdiscount_amount();
			$itemTotal = $taxableAmount + $item->getcgst_charge() + $item->getsgst_charge() + $item->getigst_charge();
		}
        // draw Product name
        $lines[0] = array(array(
            'text' => Mage::helper('core/string')->str_split($item->getName(), 30, true, true),
            'feed' => 35,
			'font_size' => $fontSize,
        ));
        // draw QTY
        $lines[0][] = array(
            'text'  => round($item->getQty()),
            'feed'  => 180,
			'font_size' => $fontSize,
        );
        // draw Price
        $lines[0][] = array(
            'text'  => round($item->getPrice(),2),
            'feed'  => 224,
            'font'  => 'bold',
            'align' => 'right',
			'font_size' => $fontSize,
        );
        // draw Subtotal
        $lines[0][] = array(
            'text'  => round($subTotal,2),
            'feed'  => 260,
            'font'  => 'bold',
            'align' => 'right',
			'font_size' => $fontSize,
        );
		// draw Discount
        $lines[0][] = array(
            'text'  => round($item->getdiscount_amount(),2),
            'feed'  => 283,
            'font'  => 'bold',
            'align' => 'right',
			'font_size' => $fontSize,
        );	
		// draw Taxable amount
        $lines[0][] = array(
            'text'  => round($taxableAmount,2),
            'feed'  => 340,
            'font'  => 'bold',
            'align' => 'right',
			'font_size' => $fontSize,
        );
		// draw CGST RATE
		$lines[0][] = array(
			'text'  => round($item->getcgst_percent(),2),
			'feed'  => 365,
			'font'  => 'bold',
			'align' => 'right',
			'font_size' => $fontSize,
		);	
		// draw CGST Amount
		$lines[0][] = array(
			'text'  => round($item->getcgst_charge(),2),
			'feed'  => 405,
			'font'  => 'bold',
			'align' => 'right',
			'font_size' => $fontSize,
		);
		// draw SGST RATE
		$lines[0][] = array(
			'text'  => round($item->getsgst_percent(),2),
			'feed'  => 425,
			'font'  => 'bold',
			'align' => 'right',
			'font_size' => $fontSize,
		);	
		// draw SGST AMOUNT
		$lines[0][] = array(
			'text'  => round($item->getsgst_charge(),2),
			'feed'  => 465,
			'font'  => 'bold',
			'align' => 'right',
			'font_size' => $fontSize,
		);
		// draw IGST RATE
		$lines[0][] = array(
			'text'  => round($item->getigst_percent(),2),
			'feed'  => 480,
			'font'  => 'bold',
			'align' => 'right',
			'font_size' => $fontSize,
		);
		// draw IGST AMOUNT
		$lines[0][] = array(
			'text'  => round($item->getigst_charge(),2),
			'feed'  => 518,
			'font'  => 'bold',
			'align' => 'right',
			'font_size' => $fontSize,
		);			
		// draw TOTAL AMOUNT
        $lines[0][] = array(
            'text'  => round($itemTotal,2),
            'feed'  => 565,
            'font'  => 'bold',
            'align' => 'right',
			'font_size' => $fontSize,
        );
		
		$lines[][] = array(
                    'text' => Mage::helper('core/string')->str_split(strip_tags('SKU : '.$item->getSku()), 30, true, true),
                    'font' => 'italic',
                    'feed' => 35,
					'font_size' => $fontSize,
                );
 
        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = array(
                    'text' => Mage::helper('core/string')->str_split(strip_tags($option['label']), 30, true, true),
                    'font' => 'italic',
                    'feed' => 35,
					'font_size' => $fontSize,
                );
 
                if ($option['value']) {
                    $_printValue = isset($option['print_value']) ? $option['print_value'] : strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        $lines[][] = array(
                            'text' => Mage::helper('core/string')->str_split($value, 50, true, true),
                            'feed' => 40,
							'font_size' => $fontSize,
                        );
                    }
                }
            }
        }
		
		$lines[][] = array(
                    'text' => Mage::helper('core/string')->str_split(strip_tags('HSN CODE : '.$hsncode), 30, true, true),
                    'font' => 'italic',
                    'feed' => 35,
					'font_size' => $fontSize,
                );
				
        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );
 
        $page = $pdf->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
	    $this->setPage($page);
    }
	/*
	 * Return Value of custom attribute
	 * */
	private function getHsncodeValue($item)
	{
		$prod = Mage::getModel('catalog/product')->load($item->getProductId());
 
		if(!($return_location = $prod->getHsncode()))
			return 'N/A';
		else
			return $return_location;
	}
}
