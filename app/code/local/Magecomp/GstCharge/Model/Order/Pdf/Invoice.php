<?php
class Magecomp_Gstcharge_Model_Order_Pdf_Invoice extends Mage_Sales_Model_Order_Pdf_Abstract
{
    public function getPdf($invoices = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');
 
        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);
 
        foreach ($invoices as $invoice) 
		{
            if ($invoice->getStoreId())
			{
                Mage::app()->getLocale()->emulate($invoice->getStoreId());
            }
            $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
            $pdf->pages[] = $page;
 
            $order = $invoice->getOrder();
            /* Add image */
            $this->insertLogo($page, $invoice->getStore());
 
            /* Add address */
            $this->insertAddress($page, $invoice->getStore());
 
            /* Add head */
            $this->insertOrder($page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $order->getStoreId()));
 
 			/* Add document text and number */
            $this->insertDocumentNumber($page,Mage::helper('sales')->__('Invoice # ') . $invoice->getIncrementId());
			
			$this->insertTaxHeading($page,Mage::helper('sales')->__('Tax Invoice/Bill of Supply/Cash Memo '));
			$this->insertGstNumber($page,Mage::helper('sales')->__('GST Registration No: ') . Mage::getStoreConfig('gstcharge/options/gstno'));
			$this->insertPanNumber($page,Mage::helper('sales')->__('PAN No: ') . Mage::getStoreConfig('gstcharge/options/panno'));
			$this->insertCinNumber($page,Mage::helper('sales')->__('CIN No: ') . Mage::getStoreConfig('gstcharge/options/cin'));
		
			
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
            $this->_setFontRegular($page);
			$this->_setFontRegular($page, 10);
            
			/* Add table */
            $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);
 
            $page->drawRectangle(25, $this->y, 570, $this->y -25);
            $this->y -=10;
            
			$this->_setFontRegular($page,7);
            /* Add table head */
            $page->setFillColor(new Zend_Pdf_Color_RGB(0.4, 0.4, 0.4));
            
			$page->drawText(Mage::helper('sales')->__('Product'), 35, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Qty'), 180, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Price'), 200, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Subtotal'), 230, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Discount'), 270, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Taxable'), 310, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'), 310, $this->y-10, 'UTF-8');
			
			
				
			$page->drawText(Mage::helper('sales')->__('CGST'), 365, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Rate'), 350, $this->y-10, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'),380, $this->y-10, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('SGST'), 425, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Rate'), 410, $this->y-10, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'),440, $this->y-10, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('IGST'), 485, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Rate'), 470, $this->y-10, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'),500, $this->y-10, 'UTF-8');		
			
					
			$page->drawText(Mage::helper('sales')->__('Total'), 540, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'), 537, $this->y-10, 'UTF-8');
 
            $beforeY = $this->y + 10;
			$this->y -= 30;
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            
			/* Add body */
            foreach ($invoice->getAllItems() as $item)
			{
                if ($item->getOrderItem()->getParentItem())
				{
                    continue;
                }
 
                if ($this->y < 50) 
				{
					$afterY = $this->y ;
					$page->drawLine(25,  $beforeY, 25,  $afterY); //left
					$page->drawLine(25,  $afterY,  570, $afterY); //bottom
					$page->drawLine(570, $afterY,  570, $beforeY); //right
					
					$page->drawLine(175,  $beforeY, 175,  $afterY); //left
					$page->drawLine(195,  $beforeY, 195,  $afterY); //left
					$page->drawLine(227,  $beforeY, 227,  $afterY); //left
					$page->drawLine(265,  $beforeY, 265,  $afterY); //left
					$page->drawLine(305,  $beforeY, 305,  $afterY); //left
					$page->drawLine(345,  $beforeY, 345,  $afterY); //left
					
					$page->drawLine(408,  $beforeY, 408,  $afterY); //left
					$page->drawLine(468,  $beforeY, 468,  $afterY); //left 
					$page->drawLine(530,  $beforeY, 530,  $afterY); //left
					
					$page->drawLine(375,  $beforeY - 25, 375,  $afterY); //left
					$page->drawLine(435,  $beforeY - 25, 435,  $afterY); //left 
					$page->drawLine(495,  $beforeY - 25, 495,  $afterY); //left igst
					
                    $page = $this->newPage(array('table_header' => true));
					
					$beforeY = 800;
                }
                /* Draw item */
				$page = $this->_drawItem($item, $page, $order);
			}
             
			$afterY = $this->y ;
			
			$page->drawLine(25,  $beforeY, 25,  $afterY); //left
			$page->drawLine(25,  $afterY,  570, $afterY); //bottom
            $page->drawLine(570, $afterY,  570, $beforeY); //right
			
			$page->drawLine(175,  $beforeY, 175,  $afterY); //left
			$page->drawLine(195,  $beforeY, 195,  $afterY); //left
			$page->drawLine(227,  $beforeY, 227,  $afterY); //left
			$page->drawLine(265,  $beforeY, 265,  $afterY); //left
			$page->drawLine(305,  $beforeY, 305,  $afterY); //left
			$page->drawLine(345,  $beforeY, 345,  $afterY); //left
			
			$page->drawLine(408,  $beforeY, 408,  $afterY); //left
			$page->drawLine(468,  $beforeY, 468,  $afterY); //left 
			$page->drawLine(530,  $beforeY, 530,  $afterY); //left
			
			$page->drawLine(375,  $beforeY - 25, 375,  $afterY); //left
			$page->drawLine(435,  $beforeY - 25, 435,  $afterY); //left 
			$page->drawLine(495,  $beforeY - 25, 495,  $afterY); //left igst
				
            /* Add totals */
            $page = $this->insertTotals($page, $invoice);
			
			if($this->y < 50) 
			{
				 $page = $this->newPage(array('table_header' => true));
				 $beforeY = 800;
            }
			
			$this->insertSignature($page, $invoice->getStore());
 
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->revert();
            }
        }
        $this->_afterGetPdf();
 
        return $pdf;
    }
	public function newPage(array $settings = array())
    {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(Zend_Pdf_Page::SIZE_A4);
        $this->_getPdf()->pages[] = $page;
        $this->y = 800;
 
        if (!empty($settings['table_header'])) {
            $this->_setFontRegular($page);
			
            $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);
 
            $page->drawRectangle(25, $this->y, 570, $this->y -25);
            $this->y -=10;
            
			$this->_setFontRegular($page,7);
            /* Add table head */
            $page->setFillColor(new Zend_Pdf_Color_RGB(0.4, 0.4, 0.4));
 
            $page->drawText(Mage::helper('sales')->__('Product'), 35, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Qty'), 180, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Price'), 200, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Base Price'), 230, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Discount'), 270, $this->y, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('Taxable'), 310, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'), 310, $this->y-10, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('CGST'), 365, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Rate'), 350, $this->y-10, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'),380, $this->y-10, 'UTF-8');
			
			
			$page->drawText(Mage::helper('sales')->__('SGST'), 425, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Rate'), 410, $this->y-10, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'),440, $this->y-10, 'UTF-8');
			
			$page->drawText(Mage::helper('sales')->__('IGST'), 485, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Rate'), 470, $this->y-10, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'),500, $this->y-10, 'UTF-8');
			
			
			$page->drawText(Mage::helper('sales')->__('Total'), 540, $this->y, 'UTF-8');
			$page->drawText(Mage::helper('sales')->__('Amount'), 537, $this->y-10, 'UTF-8');
 
 
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $this->y -=30;
        }
        return $page;
    }
	protected function insertSignature(&$page, $store = null)
    {
		$this->y = $this->y ? $this->y : 815;
        $image = Mage::getStoreConfig('gstcharge/options/signature', $store);
		
        if ($image) {
            $image = Mage::getBaseDir('media') . '/sales/store/logo/' . $image;
            if (is_file($image)) {
                $image       = Zend_Pdf_Image::imageWithPath($image);
                $top         =  $this->y - 50; //top border of the page
                $widthLimit  = 100; //half of the page width
                $heightLimit = 100; //assuming the image is not a "skyscraper"
                $width       = $image->getPixelWidth();
                $height      = $image->getPixelHeight();

                //preserving aspect ratio (proportions)
                $ratio = $width / $height;
                if ($ratio > 1 && $width > $widthLimit) {
                    $width  = $widthLimit;
                    $height = $width / $ratio;
                } elseif ($ratio < 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width  = $height * $ratio;
                } elseif ($ratio == 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width  = $widthLimit;
                }

                $y1 = $top - $height;
                $y2 = $top;
                $x1 = 450;
                $x2 = $x1 + $width;
                
				$imgY = $this->y - $height - 60 ; 
				
				$page->drawImage($image, $x1, $y1, $x2, $y2);
				$page->drawText(Mage::helper('sales')->__('Authorized Signatory') ,$x1, $imgY ,'UTF-8');
				$page->drawText(Mage::helper('sales')->__(Mage::getStoreConfig('gstcharge/options/signaturetext')) ,$x1, ($top + 3) ,'UTF-8');

                $this->y = $y1 - 10;
            }
        }
    }
	protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        if ($obj instanceof Mage_Sales_Model_Order) {
            $shipment = null;
            $order = $obj;
        } elseif ($obj instanceof Mage_Sales_Model_Order_Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
        }

        $this->y = $this->y ? $this->y : 815;
        $top = $this->y;

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0.45));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.45));
        $page->drawRectangle(25, $top, 570, $top - 55);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $this->setDocHeaderCoordinates(array(25, $top, 570, $top - 55));
        $this->_setFontRegular($page, 10);

        if ($putOrderId) {
            $page->drawText(
                Mage::helper('sales')->__('Order # ') . $order->getRealOrderId(), 35, ($top -= 30), 'UTF-8'
            );
        }
        $page->drawText(
            Mage::helper('sales')->__('Order Date: ') . Mage::helper('core')->formatDate(
                $order->getCreatedAtStoreDate(), 'medium', false
            ),
            35,
            ($top -= 15),
            'UTF-8'
        );
		
		$top -= 10;
        $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $top, 275, ($top - 25));
        $page->drawRectangle(275, $top, 570, ($top - 25));

        /* Calculate blocks info */

        /* Billing Address */
        $billingAddress = $this->_formatAddress($order->getBillingAddress()->format('pdf'));

        /* Payment */
        $paymentInfo = Mage::helper('payment')->getInfoBlock($order->getPayment())
            ->setIsSecureMode(true)
            ->toPdf();
        $paymentInfo = htmlspecialchars_decode($paymentInfo, ENT_QUOTES);
        $payment = explode('{{pdf_row_separator}}', $paymentInfo);
        foreach ($payment as $key=>$value){
            if (strip_tags(trim($value)) == '') {
                unset($payment[$key]);
            }
        }
        reset($payment);

        /* Shipping Address and Method */
        if (!$order->getIsVirtual()) 
		{
            /* Shipping Address */
            $shippingAddress = $this->_formatAddress($order->getShippingAddress()->format('pdf'));
            $shippingMethod  = $order->getShippingDescription();
        }

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontBold($page, 12);
        $page->drawText(Mage::helper('sales')->__('Billing Address: '), 35, ($top - 15), 'UTF-8');

        if (!$order->getIsVirtual()) 
		{
            $page->drawText(Mage::helper('sales')->__('Shipping Address:'), 285, ($top - 15), 'UTF-8');
        } 
		else 
		{
            $page->drawText(Mage::helper('sales')->__('Payment Method:'), 285, ($top - 15), 'UTF-8');
        }

        $addressesHeight = $this->_calcAddressHeight($billingAddress);
        if (isset($shippingAddress)) 
		{
            $addressesHeight = max($addressesHeight, $this->_calcAddressHeight($shippingAddress));
        }

        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->drawRectangle(25, ($top - 25), 570, $top - 33 - $addressesHeight);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 10);
        $this->y = $top - 40;
        $addressesStartY = $this->y;

        foreach ($billingAddress as $value)
		{
            if ($value !== '') 
			{
                $text = array();
                foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) 
				{
                    $text[] = $_value;
                }
                foreach ($text as $part) 
				{
                    $page->drawText(strip_tags(ltrim($part)), 35, $this->y, 'UTF-8');
                    $this->y -= 15;
                }
            }
        }

        $addressesEndY = $this->y;

        if (!$order->getIsVirtual()) 
		{
            $this->y = $addressesStartY;
            foreach ($shippingAddress as $value)
			{
                if ($value!=='') 
				{
                    $text = array();
                    foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) 
					{
                        $text[] = $_value;
                    }
                    foreach ($text as $part) 
					{
                        $page->drawText(strip_tags(ltrim($part)), 285, $this->y, 'UTF-8');
                        $this->y -= 15;
                    }
                }
            }

            $addressesEndY = min($addressesEndY, $this->y);
            $this->y = $addressesEndY;

            $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
            $page->setLineWidth(0.5);
            $page->drawRectangle(25, $this->y, 275, $this->y-25);
            $page->drawRectangle(275, $this->y, 570, $this->y-25);

            $this->y -= 15;
            $this->_setFontBold($page, 12);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $page->drawText(Mage::helper('sales')->__('Payment Method'), 35, $this->y, 'UTF-8');
            $page->drawText(Mage::helper('sales')->__('Shipping Method:'), 285, $this->y , 'UTF-8');

            $this->y -=10;
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));

            $this->_setFontRegular($page, 10);
            $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            $paymentLeft = 35;
            $yPayments   = $this->y - 15;
        }
        else 
		{
            $yPayments   = $addressesStartY;
            $paymentLeft = 285;
        }

        foreach ($payment as $value)
		{
            if (trim($value) != '') 
			{
                //Printing "Payment Method" lines
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach (Mage::helper('core/string')->str_split($value, 45, true, true) as $_value) 
				{
                    $page->drawText(strip_tags(trim($_value)), $paymentLeft, $yPayments, 'UTF-8');
                    $yPayments -= 15;
                }
            }
        }

        if ($order->getIsVirtual()) 
		{
            // replacement of Shipments-Payments rectangle block
            $yPayments = min($addressesEndY, $yPayments);
            $page->drawLine(25,  ($top - 25), 25,  $yPayments);
            $page->drawLine(570, ($top - 25), 570, $yPayments);
            $page->drawLine(25,  $yPayments,  570, $yPayments);

            $this->y = $yPayments - 15;
        } 
		else 
		{
            $topMargin    = 15;
            $methodStartY = $this->y;
            $this->y     -= 15;

            foreach (Mage::helper('core/string')->str_split($shippingMethod, 45, true, true) as $_value) 
			{
                $page->drawText(strip_tags(trim($_value)), 285, $this->y, 'UTF-8');
                $this->y -= 15;
            }

            $yShipments = $this->y;
            $totalShippingChargesText = "(" . Mage::helper('sales')->__('Total Shipping Charges') . " "
                . $order->formatPriceTxt($order->getShippingAmount()) . ")";

            $page->drawText($totalShippingChargesText, 285, $yShipments - $topMargin, 'UTF-8');
            $yShipments -= $topMargin + 10;

            $tracks = array();
            if ($shipment) 
			{
                $tracks = $shipment->getAllTracks();
            }
            if (count($tracks)) 
			{
                $page->setFillColor(new Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
                $page->setLineWidth(0.5);
                $page->drawRectangle(285, $yShipments, 510, $yShipments - 10);
                $page->drawLine(400, $yShipments, 400, $yShipments - 10);
                //$page->drawLine(510, $yShipments, 510, $yShipments - 10);

                $this->_setFontRegular($page, 9);
                $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                //$page->drawText(Mage::helper('sales')->__('Carrier'), 290, $yShipments - 7 , 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Title'), 290, $yShipments - 7, 'UTF-8');
                $page->drawText(Mage::helper('sales')->__('Number'), 410, $yShipments - 7, 'UTF-8');

                $yShipments -= 20;
                $this->_setFontRegular($page, 8);
                foreach ($tracks as $track) 
				{
                    $CarrierCode = $track->getCarrierCode();
                    if ($CarrierCode != 'custom') 
					{
                        $carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($CarrierCode);
                        $carrierTitle = $carrier->getConfigData('title');
                    } else 
					{
                        $carrierTitle = Mage::helper('sales')->__('Custom Value');
                    }

                    //$truncatedCarrierTitle = substr($carrierTitle, 0, 35) . (strlen($carrierTitle) > 35 ? '...' : '');
                    $maxTitleLen = 45;
                    $endOfTitle = strlen($track->getTitle()) > $maxTitleLen ? '...' : '';
                    $truncatedTitle = substr($track->getTitle(), 0, $maxTitleLen) . $endOfTitle;
                    //$page->drawText($truncatedCarrierTitle, 285, $yShipments , 'UTF-8');
                    $page->drawText($truncatedTitle, 292, $yShipments , 'UTF-8');
                    $page->drawText($track->getNumber(), 410, $yShipments , 'UTF-8');
                    $yShipments -= $topMargin - 5;
                }
            } 
			else 
			{
                $yShipments -= $topMargin - 5;
            }
            $currentY = min($yPayments, $yShipments);

            // replacement of Shipments-Payments rectangle block
            $page->drawLine(25,  $methodStartY, 25,  $currentY); //left
            $page->drawLine(25,  $currentY,     570, $currentY); //bottom
            $page->drawLine(570, $currentY,     570, $methodStartY); //right

            $this->y = $currentY;
            $this->y -= 15;
        }
    }
	public function insertTaxHeading(Zend_Pdf_Page $page, $text)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $this->_setFontRegular($page, 10);
        $docHeader = $this->getDocHeaderCoordinates();
        $page->drawText($text, 350, $docHeader[1] - 15, 'UTF-8');
    }
	public function insertGstNumber(Zend_Pdf_Page $page, $text)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $this->_setFontRegular($page, 10);
        $docHeader = $this->getDocHeaderCoordinates();
        $page->drawText($text, 350, $docHeader[1] - 30, 'UTF-8');
    }
	public function insertPanNumber(Zend_Pdf_Page $page, $text)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $this->_setFontRegular($page, 9);
        $docHeader = $this->getDocHeaderCoordinates();
        $page->drawText($text, 350, $docHeader[1] - 42, 'UTF-8');
    }
	public function insertCinNumber(Zend_Pdf_Page $page, $text)
    {
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $this->_setFontRegular($page, 9);
        $docHeader = $this->getDocHeaderCoordinates();
        $page->drawText($text, 350, $docHeader[1] - 52, 'UTF-8');
    }
	protected function _setFontRegular($object, $size = 6)
    {
        $font = Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/DejavuSans/ttf/DejaVuSans.ttf');
        $object->setFont($font, $size);
        return $font;
    }
	protected function _setFontBold($object, $size = 6)
    {
        $font = Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/DejavuSans/ttf/DejaVuSans.ttf');
        $object->setFont($font, $size);
        return $font;
    }
	protected function _setFontItalic($object, $size = 6)
    {
        $font = Zend_Pdf_Font::fontWithPath(Mage::getBaseDir() . '/lib/DejavuSans/ttf/DejaVuSans.ttf');
        $object->setFont($font, $size);
        return $font;
    }
	protected function _formatAddress($address)
    {
        $return = array();
        foreach (explode('|', $address) as $str) {
			foreach (Mage::helper('core/string')->str_split($str, 45, true, true) as $part) {
                if (empty($part)) {
                    continue;
                }
				$return[] = $part;
            }
        }
        return $return;
    }
}
