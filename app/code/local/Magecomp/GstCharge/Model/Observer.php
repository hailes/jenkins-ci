<?php
class Magecomp_Gstcharge_Model_Observer
{
	const GSTCHARGE_OPTIONS_STATE                  =  'gstcharge/options/state';
	const GSTCHARGE_OPTIONS_TAXTYPE                =  'gstcharge/options/taxtype';
	const GSTCHARGE_OPTIONS_ENABLE                 =  'gstcharge/options/enabled';
	const GSTCHARGE_OPTIONS_TAX_MINPRICE           =  'gstcharge/options/tax_minprice';
	const GSTCHARGE_OPTIONS_TAX_PERCENT            =  'gstcharge/options/tax_percent';
	const GSTCHARGE_OPTIONS_TAX_PERCENT_MINPRICE   =  'gstcharge/options/tax_percent_minprice';
	const GSTCHARGE_SHIPPINGSETTINGS_ENABLED       =  'gstcharge/shippingsettings/enabled';
	
	public function salesQuoteCollectTotalsBefore($observer)
    {
		try
		{
			$quote = $observer->getQuote();
			$shippingAddress = $quote->getShippingAddress();
			$countryId = $shippingAddress->getCountryId();
			$customerRegionId = $shippingAddress->getRegionId();
			
			$maxGstPercent = 0;
			$cart = Mage::getSingleton('checkout/session')->getQuote();
			foreach ($cart->getAllVisibleItems() as $item) 
			{
				if($countryId == 'IN' && $customerRegionId == $customerRegionId)
				{
					$gstPercent = $item->getCgstCharge() + $item->getSgstCharge();
				}
				else if($countryId == 'IN' && $customerRegionId != $customerRegionId)
				{
					$gstPercent = $item->getIgstCharge();
				}
				if(Mage::getStoreConfig(self::GSTCHARGE_SHIPPINGSETTINGS_ENABLED)):
					if ($gstPercent > $maxGstPercent)
						$maxGstPercent = $gstPercent;
				endif;
			}
			
			foreach ($quote->getAllAddresses() as $address) 
			{
				$address->setShippingAmount('0');
				$address->save();
            }
		}
		catch(Exception $e)
		{
			Mage::log(" Error ".$e->getMessage(),null,'Gstcharge.log');
			$e->getMessage();	
		}
	}
	public function gstorder($observer)
    {
		try
		{	
			$order = $observer->getOrder();


			if(!(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_ENABLE)))
			{
				return 0;
			}
			
			$quoteId=$order->getQuoteId();
			$quote = Mage::getModel('sales/quote')->load($quoteId);
			$exckudingTax=$quote->getExclPrice();
			
			$shippingAddress = $order->getShippingAddress();
			$billingAddress = $order->getBillingAddress();
			
			if($shippingAddress)
			{
				$CountryId=$shippingAddress->getCountryId();
				$CustomerRegionId=$shippingAddress->getRegionId();
				$SystemRegionId=Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);
				
				if($CountryId!='IN')
				{
					  return 0;
				}
				
				$TotalGstPrice=0;
				$totalCgstPrice = $totalSgstPrice = $totalIgstPrice = 0;
				$maxGstPercent = 0;

				//foreach ($order->getAllVisibleItems() as $item)
				foreach ($order->getAllItems() as $item)
				{
					   $gstPercent=0;
					   if ($item->getData('product_type') == "bundle")
						   	continue;
					   $product=Mage::getModel('catalog/product')->load($item->getProductId());
					   
					   $itemPriceAfterDiscount= ($item->getPrice() * $item->getDiscountPercent())/100 ;
					   $prdPrice=$item->getPrice()-$itemPriceAfterDiscount;
					   
					   $gstPercent=$product->getGstSource();
					   $gstPercentMinPrice=$product->getGstSourceMinprice();
					   $gstPercentAfterMinprice=$product->getGstSourceAfterMinprice();
					   
					   if($gstPercent<=0)
					   {
						   $cats = $product->getCategoryIds();
						   foreach ($cats as $category_id) 
						   {
								$_cat = Mage::getModel('catalog/category')->load($category_id) ;
								$gstPercent=$_cat->getGstCatSource();
								$gstPercentMinPrice=$_cat->getGstCatSourceMinprice();
					   			$gstPercentAfterMinprice=$_cat->getGstCatSourceAfterMinprice();
								
								if($gstPercent!='')
								{
									if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
									{
										$gstPercent=$gstPercentAfterMinprice;
									}
									break;
								}
						   }   
					   }
					   else
					   {
							if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
							{
								$gstPercent=$gstPercentAfterMinprice;
							}
					   }
					   
					  if($gstPercent<=0)
					  {
						  $gstPercent				=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT);
						  $gstPercentMinPrice		=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_MINPRICE);
					   	  $gstPercentAfterMinprice	=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT_MINPRICE);
						  
						  if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
						  {
								$gstPercent=$gstPercentAfterMinprice;
						  }
					  }
						
					   $rowTotal     = $item->getRowTotal();
					   $DiscountAmount=$item->getDiscountAmount();

					   if($exckudingTax)
					   	{
							$GstPrice= ((($rowTotal-$DiscountAmount)*$gstPercent)/100);   
						}
					   	else
					   	{
						    $totalPercent = 100 + $gstPercent;
							$perPrice     = ($rowTotal-$DiscountAmount) / $totalPercent;
							$GstPrice     = $perPrice * $gstPercent;
						}
						
						$TotalGstPrice+=$GstPrice;
					   
					  
					   if($CountryId=='IN' && $CustomerRegionId==$SystemRegionId)
					   {
						   	$item->setCgstCharge($GstPrice/2);
						   	$item->setCgstPercent($gstPercent/2);
						   	$item->setSgstCharge($GstPrice/2);
						   	$item->setSgstPercent($gstPercent/2);
						   	$totalCgstPrice += $GstPrice/2;
							$totalSgstPrice += $GstPrice/2;
					   }
					   else if ($CountryId=='IN' && $CustomerRegionId!=$SystemRegionId)
					   {
						     $item->setIgstCharge($GstPrice);
							 $item->setIgstPercent($gstPercent);
							 $totalIgstPrice += $GstPrice; 
					   }
					   $item->setExclPrice($exckudingTax);
					   
					   $item->save();
					   	if ($gstPercent > $maxGstPercent)
						{
					   		$maxGstPercent = $gstPercent;
						}
				}
				$order->setSgstCharge($totalSgstPrice);
				$order->setCgstCharge($totalCgstPrice);
				$order->setIgstCharge($totalIgstPrice);

			}
			$region = Mage::getModel('directory/region')->load($CustomerRegionId);
			$billingAddress->setStateCode($region->getStateCode());
			$shippingAddress->setStateCode($region->getStateCode());
			
			$billingAddress->save();
			$shippingAddress->save();
			
			$shippingAddress = $quote->getShippingAddress();
			
			$order->setShippingSgstCharge($shippingAddress->getShippingSgstCharge());
			$order->setShippingCgstCharge($shippingAddress->getShippingCgstCharge());
			$order->setShippingIgstCharge($shippingAddress->getShippingIgstCharge());
	
			$order->setPercentShippingSgstCharge($shippingAddress->getPercentShippingSgstCharge());
			$order->setPercentShippingCgstCharge($shippingAddress->getPercentShippingCgstCharge());
			$order->setPercentShippingIgstCharge($shippingAddress->getPercentShippingIgstCharge());
			
			$order->save();
		}
		catch(Exception $e)
		{
			$e->getMessage();	
		}
	}
	public function gstinvoice($observer)
    {
		try
		{	
			if(!(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_ENABLE)))
			{
			  return 0;
			}
			$TotalGstPrice=0;
			$totalCgstPrice = $totalSgstPrice = $totalIgstPrice = 0;
			$invoiceId = $observer->getEvent()->getInvoice();

			$order=$observer->getEvent()->getInvoice()->getOrder();
			
			$quoteId=$order->getQuoteId();
			$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
					 
			$exckudingTax=$quote->getExclPrice();
			
			$shippingAddress = $order->getShippingAddress();
			  
			if($shippingAddress)
			{
				$CountryId=$shippingAddress->getCountryId();
				$CustomerRegionId=$shippingAddress->getRegionId();
				$SystemRegionId=Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);
				  
				if($CountryId!='IN')
				{
					return 0;
				}
				  
				$TotalGstPrice=0;
				foreach($invoiceId->getAllItems() as $item)
				{
					$gstPercent=0;
					$product=Mage::getModel('catalog/product')->load($item->getProductId());
					$itemPriceAfterDiscount= ($item->getPrice() * $item->getDiscountPercent())/100 ;
					$prdPrice=$item->getPrice()-$itemPriceAfterDiscount;
					$gstPercent=$product->getGstSource();
					$gstPercentMinPrice=$product->getGstSourceMinprice();
					$gstPercentAfterMinprice=$product->getGstSourceAfterMinprice();
					if($gstPercent<=0)
					{
						$cats = $product->getCategoryIds();
						foreach ($cats as $category_id) 
						{
							$_cat = Mage::getModel('catalog/category')->load($category_id) ;
							$gstPercent=$_cat->getGstCatSource();
							$gstPercentMinPrice=$_cat->getGstCatSourceMinprice();
							$gstPercentAfterMinprice=$_cat->getGstCatSourceAfterMinprice();
							if($gstPercent!='')
							{
								if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
								{
									$gstPercent=$gstPercentAfterMinprice;
								}
								break;
							}
						}
					}
					else
					{
						if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
						{
							$gstPercent=$gstPercentAfterMinprice;
						}
					}				   
					if($gstPercent<=0)
					{
						$gstPercent = Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT);
						$gstPercentMinPrice =	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_MINPRICE);
						$gstPercentAfterMinprice = Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT_MINPRICE);
							  
						if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
						{
							$gstPercent=$gstPercentAfterMinprice;
						}
					}
						  
					$rowTotal = $item->getRowTotal();
					$DiscountAmount = $item->getDiscountAmount();

					if($exckudingTax)
					{
						$GstPrice= ((($rowTotal-$DiscountAmount)*$gstPercent)/100);
					}
					else
					{
						$totalPercent = 100 + $gstPercent;
						$perPrice = ($rowTotal-$DiscountAmount) / $totalPercent;
						$GstPrice = $perPrice * $gstPercent;
					}
					$TotalGstPrice+=$GstPrice;
					if($CountryId=='IN' && $CustomerRegionId==$SystemRegionId)
					{
						$item->setCgstCharge($GstPrice/2);
						$item->setCgstPercent($gstPercent/2);
						$item->setSgstCharge($GstPrice/2);
						$item->setSgstPercent($gstPercent/2);
						$totalCgstPrice += $GstPrice/2;
						$totalSgstPrice += $GstPrice/2;
					}
					else if ($CountryId=='IN' && $CustomerRegionId!=$SystemRegionId)
					{
						$item->setIgstCharge($GstPrice);
						$item->setIgstPercent($gstPercent);
						$totalIgstPrice += $GstPrice;
					}
						 
					$item->setExclPrice($exckudingTax);
				}
				$invoiceId->setSgstCharge($totalSgstPrice);
				$invoiceId->setCgstCharge($totalCgstPrice);
				$invoiceId->setIgstCharge($totalIgstPrice);
				if($exckudingTax)
				{
					$invoiceId->setBaseGrandTotal($invoiceId->getBaseGrandTotal() + $GstPrice);
						
				}
			}
			$invoiceId->setBaseGrandTotal($order->getBaseGrandTotal());
			$invoiceId->setGrandTotal($order->getGrandTotal());
			$invoiceId->setShippingSgstCharge($order->getShippingSgstCharge());
			$invoiceId->setShippingCgstCharge($order->getShippingCgstCharge());
			$invoiceId->setShippingIgstCharge($order->getShippingIgstCharge());
	
			$invoiceId->setPercentShippingSgstCharge($order->getPercentShippingSgstCharge());
			$invoiceId->setPercentShippingCgstCharge($order->getPercentShippingCgstCharge());
			$invoiceId->setPercentShippingIgstCharge($order->getPercentShippingIgstCharge());
			return $this;
		}
		catch(Exception $e)
		{
			$e->getMessage();
		}
	}
	public function gstcreditmemo($observer)
    {
		try
		{	
			if(!(Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_ENABLE)))
			{
			  return 0;
			}
			
			$creditmemoId = $observer->getEvent()->getCreditmemo();

			$order=$observer->getEvent()->getCreditmemo()->getOrder();
			
			$quoteId=$order->getQuoteId();
			$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
					 
			$exckudingTax=$quote->getExclPrice();
		
			$shippingAddress = $order->getShippingAddress();
			  
			  if($shippingAddress)
			  {
				  $CountryId=$shippingAddress->getCountryId();
				  $CustomerRegionId=$shippingAddress->getRegionId();
				  $SystemRegionId=Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);
				  
				  if($CountryId!='IN')
				  {
						return 0;
				  }
				  
				  $TotalGstPrice=0;
				  
				  foreach($creditmemoId->getAllItems() as $item)
				   {
						 $gstPercent=0;
						 
						 $product=Mage::getModel('catalog/product')->load($item->getProductId());
						 
						 $itemPriceAfterDiscount= ($item->getDiscountAmount() / $item->getPrice()) * 100 ;
						 
						 $prdPrice=$item->getPrice()-$itemPriceAfterDiscount;
						 
						 $gstPercent=$product->getGstSource();
						 $gstPercentMinPrice=$product->getGstSourceMinprice();
						 $gstPercentAfterMinprice=$product->getGstSourceAfterMinprice();
						
						 if($gstPercent<=0)
						 {
							 $cats = $product->getCategoryIds();
							   foreach ($cats as $category_id) 
							   {
									$_cat = Mage::getModel('catalog/category')->load($category_id) ;
									
									$gstPercent=$_cat->getGstCatSource();
									$gstPercentMinPrice=$_cat->getGstCatSourceMinprice();
									$gstPercentAfterMinprice=$_cat->getGstCatSourceAfterMinprice();
									
									if($gstPercent!='')
									{
										if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
										{
											$gstPercent=$gstPercentAfterMinprice;
										}
										break;
									}
							   }  
						 }
						 else
					     {
							if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
							{
								$gstPercent=$gstPercentAfterMinprice;
							}
					   	 }
					   
						 
						if($gstPercent<=0)
						{
							  $gstPercent				=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT);
							  $gstPercentMinPrice		=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_MINPRICE);
							  $gstPercentAfterMinprice	=	Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_TAX_PERCENT_MINPRICE);
							  
							  if($gstPercentMinPrice > 0 && $gstPercentMinPrice > $prdPrice )   
							  {
									$gstPercent=$gstPercentAfterMinprice;
							  }
						}

					     $rowTotal     = $item->getRowTotal();
					     $DiscountAmount=$item->getDiscountAmount();
					   
					     if($exckudingTax)
					     {
								$GstPrice= ((($rowTotal-$DiscountAmount)*$gstPercent)/100);   
						 }
						 else
						 {
								$totalPercent = 100 + $gstPercent;
								$perPrice     = ($rowTotal-$DiscountAmount) / $totalPercent;
								$GstPrice     = $perPrice * $gstPercent;
						 }
						 
						$TotalGstPrice+=$GstPrice;
						 if($CountryId=='IN' && $CustomerRegionId==$SystemRegionId)
						 {
							 $item->setCgstCharge($GstPrice/2);
							 $item->setCgstPercent($gstPercent/2);
							 $item->setSgstCharge($GstPrice/2);
							 $item->setSgstPercent($gstPercent/2);
						 }
						 else if ($CountryId=='IN' && $CustomerRegionId!=$SystemRegionId)
						 {
							   $item->setIgstCharge($GstPrice);
							   $item->setIgstPercent($gstPercent);
						 }
						 
						 $item->setExclPrice(0);
						 
						 $item->save();
				  }
				
				
				$creditmemoId->setShippingSgstCharge($order->getShippingSgstCharge());
				$creditmemoId->setShippingCgstCharge($order->getShippingSgstCharge());
				$creditmemoId->setShippingIgstCharge($order->getShippingSgstCharge());
		
				$creditmemoId->setPercentShippingSgstCharge($order->getPercentShippingSgstCharge());
				$creditmemoId->setPercentShippingCgstCharge($order->getPercentShippingCgstCharge());
				$creditmemoId->setPercentShippingIgstCharge($order->getPercentShippingIgstCharge());
				$creditmemoId->setBaseGrandTotal($order->getBaseGrandTotal());
				$creditmemoId->setGrandTotal($order->getGrandTotal());
				
			}
		}
		catch(Exception $e)
		{
			$e->getMessage();
		}

	}
}
?>
