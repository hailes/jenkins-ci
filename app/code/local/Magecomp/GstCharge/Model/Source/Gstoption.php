<?php
class Magecomp_GstCharge_Model_Source_Gstoption extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) 
		{
		    $helper = Mage::helper('gstcharge');
			$this->_options[] = array('label' => $helper->__('None') , 'value' => 0);
			$this->_options[] = array('label' => $helper->__('3%') , 'value' => 3);
			$this->_options[] = array('label' => $helper->__('5%') , 'value' => 5);
			$this->_options[] = array('label' => $helper->__('12%') , 'value' => 12);
			$this->_options[] = array('label' => $helper->__('18%') , 'value' => 18);
			$this->_options[] = array('label' => $helper->__('24%') , 'value' => 24);
			$this->_options[] = array('label' => $helper->__('28%') , 'value' => 28);
        }
		return $this->_options;
    }
}
