<?php
class Magecomp_Gstcharge_Model_Resource_Reportitem_Collection extends  Mage_Core_Model_Mysql4_Collection_Abstract
{
    public  function __construct() 
	{
        //parent::__construct();
        $this->setModel('adminhtml/report_item');
        $this->setResourceModel('sales/order_item');
        $this->setConnection($this->getResource()->getReadConnection());
    
  	}
	public function setDateRange($from, $to) 
    {
		$resource = Mage::getSingleton('core/resource');
		$this->_reset();
        $this->getSelect()
			  ->join(
    				array('item' => $resource->getTableName('sales/order')),
				    'main_table.order_id=item.entity_id'
			 )
			 ->joinLeft(
			    array('shipping' => $resource->getTableName('sales/order_address')),
    			'item.shipping_address_id = shipping.entity_id'
    		 ) 
			->where("main_table.parent_item_id IS NULL && main_table.created_at BETWEEN '".$from."' AND '".$to."'");
			return $this;			
    }
	public function setStoreIds($storeIds)
    {
        return $this;
    }
}
?>