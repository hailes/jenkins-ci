<?php
class Magecomp_Gstcharge_Model_Resource_Report_Collection extends Mage_Reports_Model_Mysql4_Order_Collection
{
    public  function __construct() 
	{
       parent::__construct();
  	}
	public function setDateRange($from, $to) 
    {
		$resource = Mage::getSingleton('core/resource');
		$this->_reset();
        $this->getSelect()
             ->join(
    				array('item' => $resource->getTableName('sales_flat_order_item')),
				    'main_table.entity_id = item.order_id'
			 )
			 ->joinLeft(
			    array('shipping' => $resource->getTableName('sales/order_address')),
    			'main_table.shipping_address_id = shipping.entity_id'
    		 ) 
			->where("main_table.created_at BETWEEN '".$from."' AND '".$to."'")
			->group("main_table.entity_id");
	        return $this;
    }
}
?>