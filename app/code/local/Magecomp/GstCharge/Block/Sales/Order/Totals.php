<?php
/**
 * @category    Magecomp
 * @package     Magecomp_GstCharge
 */
class Magecomp_GstCharge_Block_Sales_Order_Totals extends Mage_Sales_Block_Order_Totals
{
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
    	parent::_initTotals();
    	
        $source = $this->getSource();
		
		$_order = $this->getOrder();
		$orderId =  $_order->getIncrementId();
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		$billingAddress = $order->getBillingAddress();
		$shippingAddress = $order->getShippingAddress();
		$region = $shippingAddress->getRegion();
		$Urstate = Mage::helper('gstcharge')->isUniTerrState($region);

        /**
         * Add store rewards
         */
        $totals = $this->_totals;
        $newTotals = array();
        if (count($totals)>0) {
        	foreach ($totals as $index=>$arr) {
        		if ($index == "grand_total") {
					
					if (((float)$this->getSource()->getData('shipping_cgst_charge')) != 0) {
	        			$label = $this->__('Shipping CGST');
			            $newTotals['shipping_cgst_charge'] = new Varien_Object(array(
			                'code'  => 'shipping_cgst',
			                'field' => 'shipping_cgst_charge',
			                'value' => $source->getData('shipping_cgst_charge'),
			                'label' => $label
			            ));
        			}
					
					if (((float)$this->getSource()->getData('shipping_sgst_charge')) != 0) {
	        			if($Urstate)
						{$label = $this->__('Shipping UTGST');}
						else
						{$label = $this->__('Shipping SGST');}
			            $newTotals['shipping_sgst_charge'] = new Varien_Object(array(
			                'code'  => 'shipping_sgst',
			                'field' => 'shipping_sgst_charge',
			                'value' => $source->getData('shipping_sgst_charge'),
			                'label' => $label
			            ));
        			}
					
					if (((float)$this->getSource()->getData('shipping_igst_charge')) != 0) {
	        			$label = $this->__('Shipping IGST');
			            $newTotals['shipping_igst_charge'] = new Varien_Object(array(
			                'code'  => 'shipping_igst',
			                'field' => 'shipping_igst_charge',
			                'value' => $source->getData('shipping_igst_charge'),
			                'label' => $label
			            ));
        			}
					if (((float)$this->getSource()->getCgstCharge()) != 0) {
	        			$label = $this->__('CGST Charge');
			            $newTotals['cgst_charge'] = new Varien_Object(array(
			                'code'  => 'cgst_charge',
			                'field' => 'shipping_cgst_charge',
			                'value' => $source->getCgstCharge(),
			                'label' => $label
			            ));
        			}
					if (((float)$this->getSource()->getSgstCharge()) != 0) {
	        			if($Urstate)
						{$label = $this->__('UTGST Charge');}
						else
						{$label = $this->__('SGST Charge');}	
			            $newTotals['sgst_charge'] = new Varien_Object(array(
			                'code'  => 'sgst_charge',
			                'field' => 'shipping_sgst_charge',
			                'value' => $source->getSgstCharge(),
			                'label' => $label
			            ));
        			}
					
					if (((float)$this->getSource()->getIgstCharge()) != 0) {
						$label = $this->__('IGST Charge');
			            $newTotals['igst_charge'] = new Varien_Object(array(
			                'code'  => 'igst_charge',
			                'field' => 'shipping_igst_charge',
			                'value' => $source->getIgstCharge(),
			                'label' => $label
			            ));
        			}
					
        		}
        		$newTotals[$index] = $arr;
        	}
        	$this->_totals = $newTotals;
        }
        
        return $this;
    }
}
