<?php
class Magecomp_Gstcharge_Block_Adminhtml_Report extends Mage_Adminhtml_Block_Widget_Grid_Container
{
   public function __construct() 
   {
		$this->_controller = 'adminhtml_report';
		$this->_blockGroup = 'gstcharge';
		$this->_headerText = Mage::helper('gstcharge')->__('Order Wise Gst Report');
		parent::__construct();
		$this->_removeButton('add');
	}
}