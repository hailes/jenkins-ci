<?php
class Magecomp_Gstcharge_Block_Adminhtml_Reportitem_Grid_Column_Renderer_Hsn extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{  
    public function render(Varien_Object $row)
    {
		$_product = Mage::getModel('catalog/product')->load($this->_getValue($row));
		return $_product->getData('hsncode');

    }
}