<?php
class Magecomp_Gstcharge_Block_Adminhtml_Report_Grid extends Mage_Adminhtml_Block_report_Grid {

	public function __construct() 
	{
		parent::__construct();
		$this->setId('gstreportorderGrid');
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setSubReportSize(false);
	}
	protected function _prepareCollection() 
	{
		parent::_prepareCollection();
		$this->getCollection()->initReport('gstcharge/report_collection');
		return $this;
	}
	protected function _prepareColumns() 
	{
		$this->addColumn('increment_id', array(
			'header'    =>Mage::helper('reports')->__('Bill Number'),
			'align'     =>'right',
			'index'     =>'increment_id',
			'total'     =>'sum',
			'type'      =>'number'
		));
		$this->addColumn('firstname', array(
		   'header'    =>Mage::helper('reports')->__('First Name'),
			'align'     =>'right',
			'index'     =>'customer_firstname',
			'type'      =>'text'
		));
		$this->addColumn('lastname', array(
			'header'    =>Mage::helper('reports')->__('Last Name'),
			'align'     =>'right',
			'index'     =>'customer_lastname',
			'type'      =>'text'
		));
		$this->addColumn('state', array(
			'header'    =>Mage::helper('reports')->__('Satus'),
			'align'     =>'right',
			'index'     =>'state',
			'type'      =>'text'
		));
		$this->addColumn('hsn', array(
			'header'    =>Mage::helper('reports')->__('HSN'),
			'align'     =>'right',
			'index'     =>'product_id',
			'type'      =>'text',
		   'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_hsn'
		));
		$this->addColumn('gstno', array(
			'header'    =>Mage::helper('reports')->__('GST No.'),
			'align'     =>'right',
			'index'     =>'buyer_gst_number',
			'type'      =>'text'
		));
		$this->addColumn('total_qty', array(
			'header'    =>Mage::helper('reports')->__('Qty'),
			'align'     =>'right',
			'index'     =>'total_qty_ordered',
			'type'      =>'text',
			'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_qty'
		));
		$this->addColumn('grand_total', array(
			'header'    =>Mage::helper('reports')->__('Grass Value'),
			'align'     =>'right',
			'index'     =>'grand_total',
			'type'      =>'text',
			'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_grandtotal'
		));
		$this->addColumn('netamount', array(
			'header'    =>Mage::helper('reports')->__('Net Amt W/O Tax'),
			'align'     =>'right',
			'index'     =>'increment_id',
			'type'      =>'text',
			'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_netamount'
		));
		$this->addColumn('igst_charge', array(
			'header'    =>Mage::helper('reports')->__('IGST'),
			'align'     =>'right',
			'index'     =>'increment_id',
			'type'      =>'text',
			'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_igst'
			
		));
		$this->addColumn('cgst_charge', array(
			'header'    =>Mage::helper('reports')->__('CGST'),
			'align'     =>'right',
			'index'     =>'increment_id',
			'type'      =>'text',
			'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_cgst'
		));
		$this->addColumn('sgst_charge', array(
			'header'    =>Mage::helper('reports')->__('SGST/UTGST'),
			'align'     =>'right',
			'index'     =>'increment_id',
			'type'      =>'text',
			'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_sgst'
		));
		$this->addColumn('region', array(
			'header'    =>Mage::helper('reports')->__('Place of Supply'),
			'align'     =>'right',
			'index'     =>'region',
			'type'      =>'text'
		));
		$this->addColumn('rate', array(
			'header'    =>Mage::helper('reports')->__('GST Rate'),
			'align'     =>'right',
			'type'      =>'text',
			 'index'     =>'product_id',
			'renderer'  => 'gstcharge/adminhtml_report_grid_column_renderer_rate'
	
		));
		$this->addExportType('*/*/exportTotalsCsv', Mage::helper('gstcharge')->__('CSV'));
		$this->addExportType('*/*/exportTotalsExcel', Mage::helper('gstcharge')->__('Excel XML'));
		return parent::_prepareColumns();
	}
	public function getRowUrl($row)
	{
		return false;
	}
	public function getReport($from, $to) 
	{
		if ($from == '') 
		{
			$from = $this->getFilter('report_from');
		}
		if ($to == '') 
		{
			$to = $this->getFilter('report_to');
		}
		return $this->getCollection()->getReport($from, $to);
	}
}