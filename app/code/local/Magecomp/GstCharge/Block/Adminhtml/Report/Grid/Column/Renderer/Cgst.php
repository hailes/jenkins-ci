<?php
class Magecomp_Gstcharge_Block_Adminhtml_Report_Grid_Column_Renderer_Cgst extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{ 
    const DECIMALS                  = 2;
    const PERCENT_SIGN              = '';
    public function render(Varien_Object $row)
    {
	    $decimals       = $this->_getDecimals();
        $inid= $this->_getValue($row);
		$order=Mage::getModel('sales/order')->loadByIncrementId($inid);
    	return  number_format($order->getcgst_charge(),$decimals);
    }
	protected function _getDecimals()
    {
        $decimals       = $this->getDecimals();
        return !is_null($decimals) ? $decimals : self::DECIMALS;
    }
}