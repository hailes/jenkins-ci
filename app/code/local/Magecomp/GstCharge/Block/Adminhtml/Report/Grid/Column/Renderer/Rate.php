<?php
class Magecomp_Gstcharge_Block_Adminhtml_Report_Grid_Column_Renderer_Rate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    const DECIMALS                  = 2;
    const PERCENT_SIGN              = '';
    public function render(Varien_Object $row)
    {
		$_product = Mage::getModel('catalog/product')->load($this->_getValue($row));
		return $_product->getData('gst_source')."%";
		
    }
    protected function _getDecimals()
    {
        $decimals       = $this->getDecimals(); 
        return !is_null($decimals) ? $decimals : self::DECIMALS;
    }
}