<?php
class Magecomp_Gstcharge_Block_Adminhtml_Reportitem extends Mage_Adminhtml_Block_Widget_Grid_Container
{
   public function __construct() 
   {
		$this->_controller = 'adminhtml_reportitem';
		$this->_blockGroup = 'gstcharge';
		$this->_headerText = Mage::helper('gstcharge')->__('Order Item Wise Gst Report');
		parent::__construct();
		$this->_removeButton('add');
	}
}