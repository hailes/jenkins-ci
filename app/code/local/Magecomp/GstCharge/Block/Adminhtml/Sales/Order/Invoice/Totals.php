<?php
/**
 * Adminhtml order invoice totals block
 *
 * @category    Magecomp
 * @package     Magecomp_GstCharge
 */
class Magecomp_GstCharge_Block_Adminhtml_Sales_order_Invoice_Totals extends Magecomp_GstCharge_Block_Adminhtml_Sales_Totals
{
    protected $_invoice = null;

    public function getInvoice()
    {
        if ($this->_invoice === null) {
            if ($this->hasData('invoice')) {
                $this->_invoice = $this->_getData('invoice');
            } elseif (Mage::registry('current_invoice')) {
                $this->_invoice = Mage::registry('current_invoice');
            } elseif ($this->getParentBlock()->getInvoice()) {
                $this->_invoice = $this->getParentBlock()->getInvoice();
            }
        }
        return $this->_invoice;
    } 
    public function getSource()
    {
        return $this->getInvoice();
    }
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
        parent::_initTotals();
		$gstTotal = 0;
		if($this->getSource()->getExclPrice())
		{
			if (((float)$this->getSource()->getIgstCharge()) != 0) {
				$gstTotal = $this->getSource()->getIgstCharge();
				$gstTotal = $gstTotal + $this->getSource()->getShippingIgstCharge();
			}
			else
			{
				$gstTotal = $this->getSource()->getSgstCharge() + $this->getSource()->getCgstCharge();
				$gstTotal = $gstTotal + $this->getSource()->getShippingCgstCharge() + $this->getSource()->getShippingSgstCharge();
			}
		}
		
		$this->_totals['grand_total'] = new Varien_Object(array(
            'code'      => 'grand_total',
            'strong'    => true,
            'value'     => ($this->getSource()->getGrandTotal() + $gstTotal),
            'base_value'=> ($this->getSource()->getBaseGrandTotal() + $gstTotal),
            'label'     => $this->helper('sales')->__('Grand Total'),
            'area'      => 'footer'
        ));
        return $this;
    }
}
