<?php


class Tally_OrderPush_Block_Adminhtml_Orderpush extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_orderpush";
	$this->_blockGroup = "orderpush";
	$this->_headerText = Mage::helper("orderpush")->__("Order Manager");
	$this->_addButtonLabel = Mage::helper("orderpush")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}