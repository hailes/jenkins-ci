<?php

class Tally_OrderPush_Block_Adminhtml_Orderpush_Renderer_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
 	public function render(Varien_Object $row)	
	{		
		$html = parent::render($row);
		if ($row->getStatus() != 'pushed') {
			$url = $this->getUrl("orderpush/adminhtml_orderpush/retry/", array('id'=>$row->getId()));
			$html.='<a href="'.$url.'">Retry</a>';
		}
		
        return $html;
	 
	}
 
}
?>