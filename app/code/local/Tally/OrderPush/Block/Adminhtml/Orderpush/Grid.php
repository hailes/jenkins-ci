<?php

class Tally_OrderPush_Block_Adminhtml_Orderpush_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("orderpushGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("orderpush/orderpush")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("orderpush")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("manifest_id", array(
				"header" => Mage::helper("orderpush")->__("Manifest Id"),
				"index" => "manifest_id",
				));
				$this->addColumn("payment_type", array(
				"header" => Mage::helper("orderpush")->__("Payment Type"),
				"index" => "payment_type",
				));
					$this->addColumn('push_datetime', array(
						'header'    => Mage::helper('orderpush')->__('Push Date'),
						'index'     => 'push_datetime',
						'type'      => 'datetime',
					));
				$this->addColumn("request", array(
				"header" => Mage::helper("orderpush")->__("Request"),
				"index" => "request",
				));
				$this->addColumn("response", array(
				"header" => Mage::helper("orderpush")->__("Reponse"),
				"index" => "response",
				));
				$this->addColumn("error_message", array(
				"header" => Mage::helper("orderpush")->__("Error"),
				"index" => "error_message",
				));
				$this->addColumn("status", array(
				"header" => Mage::helper("orderpush")->__("Status"),
				"index" => "status",
				));
				$this->addColumn('action',
		            array(
		            'header'=> Mage::helper('orderpush')->__('Action'),
		            'index' => 'action',
		            'align'     =>'left',           
		            'filter' => false,  
		            'renderer'  => 'Tally_OrderPush_Block_Adminhtml_Orderpush_Renderer_Actions',
		        ));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return '#';
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_orderpush', array(
					 'label'=> Mage::helper('orderpush')->__('Remove Orderpush'),
					 'url'  => $this->getUrl('*/adminhtml_orderpush/massRemove'),
					 'confirm' => Mage::helper('orderpush')->__('Are you sure?')
				));
			return $this;
		}
			

}