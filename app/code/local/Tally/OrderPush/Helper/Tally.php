<?php
class Tally_Orderpush_Helper_Tally extends Mage_Core_Helper_Abstract
{
	private $requiredFolders = ['Tallydata', 'Tallydata/Request', 'Tallydata/Response', 'Tallydata/Return_Request', 'Tallydata/Return_Response'];

	private $requestPath = 'Request';

	private $returnRequestPath = 'Return_Request';

	private $responsePath = 'Response';

	private $returnResponsePath = 'Return_Response';

	private $basePath = 'media/Tallydata';

	public $manifestId;

	public $rmaId;

	private $returnPushStatus = [2];

	public function __construct()
	{
		$this->isdirectory($this->requiredFolders);
	}

	public function pushOrderToTally($manifestId, $isRetry = false, $model=null)
	{
		$this->manifestId = $manifestId;
		$tallyProcess = Mage::getModel('packageprocess/packageprocess');
		if ($isRetry) {
			$data = $tallyProcess->getRetryData($manifestId, $model);
			$this->retries = $model->getRetries();
		} else {
			$data = $tallyProcess->getManifestData($manifestId);
		}
		$manifest = $tallyProcess->getManifestDetails();
		$this->generateXmlData($data, $manifest, $this->requestPath);
	}

	private function generateXmlData($data, $manifestDetails, $path)
	{
		foreach ($data as $key => $manifest) {
			if (empty($manifest['totals']) === false) {
				$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><sales_order></sales_order>');
				$invoice = $xml->addChild('invoice');
				$invoice->addChild('order_id', $this->manifestId.$key);
				$invoice->addChild('party_name', 'Feetscience Web');
				$invoice->addChild('order_date', $manifestDetails['created_at']);
				$invoice->addChild('shipping_amount', $manifest['totals']['shipping_amount']);
				$invoice->addChild('payment_mode', $key);
				$invoice->addChild('order_level_discount', 0);
				$items = $invoice->addChild('items');
				foreach ($manifest['details'] as $index => $itemDetails) {
					
					$item = $items->addChild('item');
					$item->addChild('web_order_id', $itemDetails['incrementId']);
					$item->addChild('price', $itemDetails['rowTotal']);
					$item->addChild('name', $itemDetails['sku'] . '~' . $this->cleanString($itemDetails['name']));
					$item->addChild('qty', $itemDetails['package_qty']);
					$item->addChild('discount_amount', $itemDetails['discount_amount']);
					$item->addChild('tax_percentage', $this->getTaxPercentage($itemDetails['tax_class_id']));
				}

				$today = time();
				$storeXmlPath = sprintf('%s/%s/%s/%s__%s%s.xml', Mage::getBaseDir(), $this->basePath, $path, $this->manifestId, strtoupper($key), $today);
				$xml->asXML($storeXmlPath);
				$request = $xml->asXML();
				$tally = [];
				$tally['request'] = $request;
				$tally['updated_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
				$tally['push_datetime'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
				$id = empty($manifest['id']) === false ? $manifest['id'] : null;
				if ($id) {
					$tally['retries'] = $this->retries + 1;
					$this->updateTallyData($tally, $manifest['id']);
				} else {
					$tally['status'] = 'pending';
					$tally['manifest_id'] = $this->manifestId;
					$tally['payment_type'] = strtoupper($key);
					$tally['method'] = 'inserts';
					$tally['created_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
					$id = $this->insertTallyData($tally);
				}

				$response = $this->tallyCurl($request);
				$parseReponse = $this->parseReponse($response, $id);
			}
		}
	}

	public function parseReponse($response, $id)
	{
		$data = [];
		if (empty($response['error']) === true) {
			$data['response'] = $response['xml'];
			$response = $response['response'];
			$data['updated_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
			if($response['CREATED'] == '1') {
				$status = "Order created Successfully";
				Mage::log('ManiFest_id: '. $this->manifestId . " Pushed successfully", null, 'tally_order_push_noncod.log');
				Mage::getModel('adminhtml/session')->addSuccess($this->__('Order pushed successfully to tally.'));
				$data['status'] = 'pushed';
			} else if($response['ALTERED'] == '1') {
				$status = "Order updated Successfully";
				Mage::log('ManiFest_id: '. $this->manifestId . " Pushed successfully", null, 'tally_order_push_noncod.log');
				Mage::getModel('adminhtml/session')->addSuccess($this->__('Order updated successfully to tally.'));
				$data['status'] = 'pushed';

			} else {
				Mage::log('ManiFest_id: '. $this->manifestId . $response, null, 'tally_order_push.log');
				Mage::getModel('adminhtml/session')->addError($this->__('Order push to tally failed.'));
				$data['status'] = 'error';
			}
		} else {
			$data['error_message'] = $response['error'];
			$data['status'] = 'error';
			Mage::getModel('adminhtml/session')->addError($this->__($response['error']));
		}

		$this->updateTallyData($data, $id);

		return $data;
	}

	public function parseReturnReponse($response, $id)
	{
		$data = [];
		if (empty($response['error']) === true) {
			$data['response'] = $response['xml'];
			$response = $response['response'];
			$data['updated_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
			if($response['CREATED'] == '1') {
				$status = "Return pushed Successfully";
				Mage::log('Rma Id: '. $this->rmaId . " Pushed successfully", null, 'tally_order_push_noncod.log');
				Mage::getModel('adminhtml/session')->addSuccess($this->__('Return pushed successfully to tally.'));
				$data['status'] = 'pushed';
			} else if($response['ALTERED'] == '1') {
				$status = "Return updated Successfully";
				Mage::log('Rma Id: '. $this->rmaId . " Pushed successfully", null, 'tally_order_push_noncod.log');
				Mage::getModel('adminhtml/session')->addSuccess($this->__('Return updated successfully to tally.'));
				$data['status'] = 'pushed';

			} else {
				Mage::log('Rma Id: '. $this->rmaId . $response, null, 'tally_order_push.log');
				Mage::getModel('adminhtml/session')->addError($this->__('Return push to tally failed.'));
				$data['status'] = 'error';
				$data['error'] = $response['LINEERROR'];
				$status = $data['error'];
			}
			Mage::getModel('awrma/entitycomments')->setEntityId($this->rmaId)
				->setCreatedAt(date("Y-m-d h:i:s"))
				->setText("Tally order push: ".$status)
				->setOwner(2)
				->save();
		} else {
			$data['error_message'] = $response['error'];
			$data['status'] = 'error';
			Mage::getModel('adminhtml/session')->addError($this->__($response['error']));
		}

		$this->updateTallyData($data, $id);

		return $data;
	}

	public function isDirectory(array $directry)
	{
		try {
			foreach ($directry as $key => $dir) {
				$logDirectory = Mage::getBaseDir() . "/media/" ;
				if (!file_exists($logDirectory . $dir)) {
					mkdir($logDirectory . $dir, 0777, true);
				}
			}
		}
		catch (Exception $e)
		{
			Mage::log($e->getMessage());
			Mage::getModel('adminhtml/session')->addError($this->__($e->getMessage()));
		}

		return true;
	}

	public function tallyCurl($data, $type='apiurl')
	{
		$headers = [
			"Content-type: application/xml",
			"Accept: application/xml","Content-length:".strlen($data),
			"Connection: close"
		];

		$apiUrl = Mage::getStoreConfig('tally_orderpush/general/'.$type);
		try {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $apiUrl);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_TIMEOUT, 100);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$xmlResponse = curl_exec($curl);
			
			$xml = simplexml_load_string($xmlResponse);
	    	$json = json_encode($xml);
	    	$response = json_decode($json, true);

	    	if($errno = curl_errno($curl)) {
			    $error_message = curl_error($curl);
				
			    return ['error' => $errno.','.$error_message];
			}

			curl_close($curl);

	    	return ['response' => $response, 'xml' => trim($xmlResponse)];
	    }
	    catch(\Exception $e)
	    {
	    	Mage::log($e->getMessage());
	    	Mage::getModel('adminhtml/session')->addSuccess($this->__($e->getMessage()));
	    }

	    return false;
	}

	public function getTaxPercentage($taxClassId)
    {
        $store = Mage::app()->getStore();
        $taxCalculation = Mage::getModel('tax/calculation');
        $request = $taxCalculation->getRateRequest(null, null, null, $store);
        $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));

        return $percent;
    }

    public function cleanString($string)
	{
   		return  preg_replace('/[^A-Za-z0-9\ ]/', '', $string);
	}

	public function updateTallyData($data, $id)
	{
		$model = Mage::getModel("orderpush/orderpush")
			->addData($data)
			->setId($id)
			->save();
	}

	public function insertTallyData($data)
	{
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$insertstatus = $write->insert('tally_post_data', $data);
		
		return $write->lastInsertId();
	}

	public function pushReturnOrder($rmaId, $isRetry = false, $model=null)
	{
		$rmaRequest = Mage::getModel('awrma/entity')->load($rmaId);
		if (in_array($rmaRequest->getStatus(), $this->returnPushStatus)) {
			$rmaData = $rmaRequest->getData();
			$data = [];
			if (empty($rmaData['order_items']) === false) {
				$order = Mage::getModel('sales/order')->loadByIncrementId($rmaData['order_id']);
				$itemsList = array_keys($rmaData['order_items']);
				$package = Mage::getModel('packageprocess/packageprocess')->getCollection()->addFieldToFilter('main_table.item_id', array('in' => $itemsList));
				$package->getSelect()->reset(Zend_Db_Select::COLUMNS)
		                ->columns(['main_table.tax_class_id', 'main_table.invoice_id','main_table.manifest_id', 'item_id'])
		                ->join(['invoice' => 'sales_flat_invoice'], 'invoice.entity_id=main_table.invoice_id', ['invoice.increment_id as incrementId'])
		                ->join(['invoice_item' => 'sales_flat_invoice_item'], 'invoice.entity_id=invoice_item.parent_id', ['row_total', 'tax_amount', 'hidden_tax_amount', 'invoice_item.sku', 'discount_amount', 'name', 'product_id', '(invoice_item.row_total + invoice_item.tax_amount + invoice_item.hidden_tax_amount) as rowTotal', 'invoice_item.qty as package_qty', 'order_item_id'])
		                ->where('main_table.item_id = invoice_item.order_item_id')
		                ->group('sku');
		        $package = $package->getData();

		        $data['id'] = null;
		        if ($isRetry) {
		        	$this->retries = $model->getRetries();
		        	$data['id'] = $model->getId();
		        }
		        $data['order'] = $order;
		        $data['rmaData'] = $rmaData;
		        $data['type'] = $order->getPayment()->getMethodInstance()->getCode() === 'cashondelivery' ? 'COD' : 'PREPAID';
		        $data['manifest_id'] = empty($package[0]['manifest_id']) === false ? $package[0]['manifest_id'] : null;
		        $this->rmaId = $rmaId;
		        $this->generateReturnXmlData($package, $data);
			}
		}
	}

	public function generateReturnXmlData($_items, $data)
	{
		$newXml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><salesrefunds></salesrefunds>');
		$returnxml = $newXml->addChild('refund');
		$returnxml->addChild('order_id', $data['manifest_id'].$data['type']);
		$returnxml->addChild('invoiceDate', date('Y-m-d'));
		$returnxml->addChild('partyname','Feetscience Web');
		$returnitems = $returnxml->addChild('refund_items');
			
		foreach($_items as $itemid => $item ) {
			$returnitem = $returnitems->addChild('item');
			$returnitem->addChild('web_order_id', $item['incrementId']);
			$returnitem->addChild('price', $item['rowTotal']);
			$returnitem->addChild('stockname', $item['sku']);
			$returnitem->addChild('qty_refunded', $data['rmaData']['order_items'][$item['order_item_id']]);
			$returnitem->addChild('discount', $item['discount_amount']);
			$returnitem->addChild('tax_percentage', $this->getTaxPercentage($item['tax_class_id']));		
		}
		
		$today = time();
		$storeXmlPath = sprintf('%s/%s/%s/%s__%s%s.xml', Mage::getBaseDir(), $this->basePath, $this->returnRequestPath, $data['manifest_id'], $data['type'], $today);
		$newXml->asXML($storeXmlPath);
		$xmlData = $newXml->asXML();
		$tally = [];
		$tally['request'] = $xmlData;
		$tally['updated_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$tally['push_datetime'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
		if (empty($data['id']) === false) {
			$tally['retries'] = $this->retries + 1;
			$id = $data['id'];
			$this->updateTallyData($tally, $data['id']);
		} else {
			$tally['status'] = 'pending';
			$tally['rma_id'] = $this->rmaId;
			$tally['manifest_id'] = $data['manifest_id'];
			$tally['payment_type'] = $data['type'];
			$tally['method'] = 'returns';
			$tally['created_at'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
			$id = $this->insertTallyData($tally);
		}
		$response = $this->tallyCurl($xmlData, 'return_apiurl');
		$this->parseReturnReponse($response, $id);
	}
}
