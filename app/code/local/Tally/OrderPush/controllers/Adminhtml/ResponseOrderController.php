<?php

class Tally_OrderPush_Adminhtml_ResponseOrderController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$this->loadLayout()->renderLaout();
	}

	public function getOrderAction()
	{
		$this->loadLayout()->renderLayout();
		$salesOrderPath =Mage::helper("adminhtml")->getUrl('adminhtml/sales_order/index/');
	 	Mage::app()->getResponse()->setRedirect($salesOrderPath);
	}
}