<?php
class Tally_OrderPush_Adminhtml_RequestOrderController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$this->loadLayout()->renderLayout();
		
	}

	public function sendOrderAction()
	{
	 	$this->loadLayout()->renderLayout();
	 	
		$order = Mage::getModel("sales/order")->getCollection()
			->addAttributeToFilter('status',array('cod_confirmed','prepaid_confirmed'));

		$newxml = new SimpleXMLElement("<Invoices></Invoices>");

		foreach ($order->getData() as $k => $v) 
		{
    		$entity_id=$final[$k]['entity_id'] = $v['entity_id'];
    		$buyer_first_name=$final[$k]['customer_firstname'] = $v['customer_firstname'];
    		$buyer_second_name=$final[$k]['customer_lastname'] = $v['customer_lastname'];
    		$buyer_name=$buyer_first_name."\t".$buyer_second_name;
    		$discount_amount=$final[$k]['discount_amount'] = $v['discount_amount'];
    		$shipping_amount=$final[$k]['shipping_amount'] = $v['shipping_amount'];
    		$shipping_tax_amount=$final[$k]['shipping_tax_amount'] = $v['shipping_tax_amount'];
    		$subtotal=$final[$k]['subtotal'] = $v['subtotal'];
    		$tax_amount=$final[$k]['tax_amount'] = $v['tax_amount'];
    		$total_qty_ordered=$final[$k]['total_qty_ordered'] = $v['total_qty_ordered'];
    		$shipping_discount_amount=$final[$k]['shipping_discount_amount'] = $v['shipping_discount_amount'];
    		$subtotal_incl_tax=$final[$k]['subtotal_incl_tax'] = $v['subtotal_incl_tax'];
    		$weight=$final[$k]['weight'] = $v['weight'];
    		$increment_id=$final[$k]['increment_id'] = $v['increment_id'];
    		$created_at=$final[$k]['created_at'] = $v['created_at'];
    		$shipping_incl_tax=$final[$k]['shipping_incl_tax'] = $v['shipping_incl_tax'];
    		$coupon_rule_name=$final[$k]['coupon_rule_name'] = $v['coupon_rule_name'];
    		$grand_total=$final[$k]['grand_total'] = $v['grand_total'];
    		//$temp = $order->getData();
    		$temp = Mage::getModel("sales/order")->load($v['entity_id']);
    		$invoice =  $newxml->addChild('Invoice');
    		$items=$invoice->addChild('Items');

    		foreach ($temp->getAllItems() as $x => $y) 
    		{ //or $order->getAllVisibleItems()
        		if($y->getProductType()=='configurable') 
        		{
            		$product_price=$final[$k][$x]['price'] = $y->getPrice();
            		$product_name=$final[$k][$x]['name'] = $y->getName();
            		$product_qty=$final[$k][$x]['qty'] = $y->getQtyOrdered();
    				$item=$items->addChild('Item');
            		$item->addChild('product_name', $product_name);
    				$item->addChild('product_price', $product_price);
    				$item->addChild('product_qty_ordered', $product_qty);
        		}
    		}

    		$invoice->addChild('order_id',$increment_id);
    		$invoice->addChild('order_date',$created_at);
    		$invoice->addChild('party_name',$buyer_name);
    		$invoice->addChild('total_qty_ordered',$total_qty_ordered);
    		$invoice->addChild('subtotal',$subtotal);
    		$invoice->addChild('tax_amount',$tax_amount);
    		$invoice->addChild('subtotal_incl_tax',$subtotal_incl_tax);
    		$invoice->addChild('discount_amount',$discount_amount);
    		$invoice->addChild('shipping_amount',$shipping_amount);
    		$invoice->addChild('shipping_tax_amount',$shipping_tax_amount);
    		$invoice->addChild('shipping_incl_tax',$shipping_incl_tax);
    		$invoice->addChild('shipping_discount_amount',$shipping_discount_amount);
    		$invoice->addChild('coupon_rule_name',$coupon_rule_name);
    		$invoice->addChild('grand_total',$grand_total);

			$connection = Mage::getSingleton('core/resource')->getConnection('core_write'); // insert
    		$sql_query = "UPDATE `sales_flat_order_grid` SET `tally_order_status` = '1' WHERE `status` = 'cod_confirmed' OR `status` = 'prepaid_confirmed';";
    		$connection->query($sql_query); 
	
			$temp->setData('state', "processing");
			$temp->setstatus("order_in_progress");
			$history = $temp->addStatusHistoryComment('Order was set to processing by our automation tool.', true);
			$history->setIsCustomerNotified(true);
			$temp->save();
		}

		// echo $newxml->asXML();
		Header('Content-type: text/xml');
		$filePath = Mage::getBaseDir()."/TallyData/Orders/Request";
		$today = date("jFYg:ia");
		$newxml->asXML($filePath."/Tally_Request_".$today.".xml"); 

		$message = $this->__('Successfully send order to the Tally');
		Mage::getSingleton('adminhtml/session')->addSuccess($message); 

	 	$sendOrderPath =Mage::helper("adminhtml")->getUrl('adminhtml/sales_order/index/');
	 	Mage::app()->getResponse()->setRedirect($sendOrderPath);
	}
}