<?php

class Tally_OrderPush_Adminhtml_OrderpushController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("orderpush/orderpush")->_addBreadcrumb(Mage::helper("adminhtml")->__("Order  Manager"), Mage::helper("adminhtml")->__("Order Manager"));
        return $this;
    }
    public function indexAction()
    {
        $this->_title($this->__("Order"));
        $this->_title($this->__("Manager Order"));
        
        $this->_initAction();
        $this->renderLayout();
    }
    public function editAction()
    {
        $this->_title($this->__("Order"));
        $this->_title($this->__("Order"));
        $this->_title($this->__("Edit Item"));
        
        $id    = $this->getRequest()->getParam("id");
        $model = Mage::getModel("orderpush/orderpush")->load($id);
        if ($model->getId()) {
            Mage::register("orderpush_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("orderpush/orderpush");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Orderpush Manager"), Mage::helper("adminhtml")->__("Orderpush Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Orderpush Description"), Mage::helper("adminhtml")->__("Orderpush Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("orderpush/adminhtml_orderpush_edit"))->_addLeft($this->getLayout()->createBlock("orderpush/adminhtml_orderpush_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("orderpush")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }
    
    public function newAction()
    {
        
        $this->_title($this->__("OrderPush"));
        $this->_title($this->__("Orderpush"));
        $this->_title($this->__("New Item"));
        
        $id    = $this->getRequest()->getParam("id");
        $model = Mage::getModel("orderpush/orderpush")->load($id);
        
        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        
        Mage::register("orderpush_data", $model);
        
        $this->loadLayout();
        $this->_setActiveMenu("orderpush/orderpush");
        
        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
        
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Orderpush Manager"), Mage::helper("adminhtml")->__("Orderpush Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Orderpush Description"), Mage::helper("adminhtml")->__("Orderpush Description"));
        
        
        $this->_addContent($this->getLayout()->createBlock("orderpush/adminhtml_orderpush_edit"))->_addLeft($this->getLayout()->createBlock("orderpush/adminhtml_orderpush_edit_tabs"));
        
        $this->renderLayout();
        
    }
    public function saveAction()
    {
        
        $post_data = $this->getRequest()->getPost();
        
        if ($post_data) {
            
            try {
                $model = Mage::getModel("orderpush/orderpush")->addData($post_data)->setId($this->getRequest()->getParam("id"))->save();
                
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Orderpush was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setOrderpushData(false);
                
                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array(
                        "id" => $model->getId()
                    ));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setOrderpushData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array(
                    "id" => $this->getRequest()->getParam("id")
                ));
                return;
            }
            
        }
        $this->_redirect("*/*/");
    }
    
    
    
    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("orderpush/orderpush");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array(
                    "id" => $this->getRequest()->getParam("id")
                ));
            }
        }
        $this->_redirect("*/*/");
    }
    
    
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("orderpush/orderpush");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        }
        catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
    
    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'orderpush.csv';
        $grid     = $this->getLayout()->createBlock('orderpush/adminhtml_orderpush_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'orderpush.xml';
        $grid     = $this->getLayout()->createBlock('orderpush/adminhtml_orderpush_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function retryAction()
    {
	    $id = $this->getRequest()->getParam('id');
	    try{
	    	$model = Mage::getModel("orderpush/orderpush")->load($id);
	        if ($model->getId() && $model->getManifestId()) {
                if (strtoupper($model->getMethod()) == 'RETURNS') {
                    Mage::helper('orderpush/tally')->pushReturnOrder($model->getRmaId(), true, $model);
                } else {
	        	    Mage::helper('orderpush/tally')->pushOrderToTally($model->getManifestId(), true, $model);
                }
	    	}
        }
        catch (\Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    { 
        return Mage::getSingleton('admin/session')->isAllowed('admin/orderpush/orderpush_manage');
    }
}
