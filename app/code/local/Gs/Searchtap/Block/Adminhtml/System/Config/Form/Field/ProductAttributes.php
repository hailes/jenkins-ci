<?php

class Gs_Searchtap_Block_Adminhtml_System_Config_Form_Field_ProductAttributes
    extends Mage_Core_Block_Html_Select
{
    public function _toHtml()
    {
        $options = Mage::getResourceModel('catalog/product_attribute_collection')
            ->getItems();
        foreach ($options as $option) {
           // $this->addOption($option['value'], $option['label']);
            if($option->getAttributeCode()!='name' && $option->getAttributeCode()!='sku'
                && $option->getAttributeCode()!='image' && $option->getAttributeCode()!='category'
                && $option->getAttributeCode()!='special_price' && $option->getAttributeCode()!='price'
                && $option->getAttributeCode()!='visibility' && $option->getAttributeCode()!='status'
                && $option->getAttributeCode()!='created_at')
                $this->addOption($option->getAttributecode(), $option->getAttributecode());
        }
        $this->addOption('rating','rating');
        $this->addOption('order_qty','order_qty');
        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}