<?php

class Gs_Searchtap_Block_Adminhtml_System_Config_Form_Field_ProductAdditionalAttributes extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_itemRenderer;

    public function _prepareToRender()
    {
        $this->addColumn('Attribute', array(
            'label' => Mage::helper('gs_searchtap')->__('Attribute'),
            'style' => 'width:80px',
            'renderer' => $this->_getRenderer(),
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('gs_searchtap')->__('Add Attribute');
    }

    protected function _getRenderer()
    {
        if (!$this->_itemRenderer) {
            $this->_itemRenderer = Mage::app()->getLayout()->createBlock(
                'gs_searchtap/adminhtml_system_config_form_field_productAttributes', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_itemRenderer;
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData('option_extra_attr_' . $this->_getRenderer()
                ->calcOptionHash($row->getData('Attribute')),
            'selected="selected"'
        );
    }
}
