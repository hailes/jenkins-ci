<?php
/**
 * Gs_Searchtap extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Searchtap admin block
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Block_Adminhtml_Searchtap extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_searchtap';
        $this->_blockGroup         = 'gs_searchtap';
        parent::__construct();
        $this->_headerText         = Mage::helper('gs_searchtap')->__('Searchtap');
        $this->_updateButton('add', 'label', Mage::helper('gs_searchtap')->__('Add Searchtap'));
	
	$this->_removeButton('save');
	$this->_removeButton('delete');
	$this->_removeButton('reset');

    }
}
