<?php
/**
 * Gs_Searchtap extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Searchtap admin edit tabs
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Block_Adminhtml_Searchtap_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('searchtap_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('gs_searchtap')->__('Searchtap'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Gs_Searchtap_Block_Adminhtml_Searchtap_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_searchtap',
            array(
                'label'   => Mage::helper('gs_searchtap')->__('Searchtap'),
                'title'   => Mage::helper('gs_searchtap')->__('Searchtap'),
                'content' => $this->getLayout()->createBlock(
                    'gs_searchtap/adminhtml_searchtap_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve searchtap entity
     *
     * @access public
     * @return Gs_Searchtap_Model_Searchtap
     * @author Ultimate Module Creator
     */
    public function getSearchtap()
    {
        return Mage::registry('current_searchtap');
    }
}
