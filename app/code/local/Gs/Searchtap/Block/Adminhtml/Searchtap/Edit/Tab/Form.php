<?php
/**
 * Gs_Searchtap extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Searchtap edit form tab
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Block_Adminhtml_Searchtap_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Gs_Searchtap_Block_Adminhtml_Searchtap_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('searchtap_');
        $form->setFieldNameSuffix('searchtap');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'searchtap_form',
            array('legend' => Mage::helper('gs_searchtap')->__('Searchtap'))
        );
        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $store_id = $store->getId();
                    $store_name = $store->getName();
                    $select_store[] = array('value' => $store_id, 'label' => $store_name);
                }
            }
        }
        $fieldset->addField('custom_store', 'select', array(
            'label' => Mage::helper('gs_searchtap')->__('Select Store'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'custom_store',
            'values' => array(
                //  '-1'=> array( 'label' => 'Please Select..', 'value' => '-1'),
                '1' => array('value' => $select_store),
            ),
        ));
        $fieldset->addField(
            'adminkey',
            'text',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Admin Key'),
                'name' => 'adminkey',
                'required' => true,
                'class' => 'required-entry',

            )
        );

        $fieldset->addField(
            'searchkey',
            'text',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Search Key'),
                'name' => 'searchkey',
                'required' => true,
                'class' => 'required-entry',

            )
        );
        $fieldset->addField(
            'collectionname',
            'text',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Collection Name'),
                'name' => 'collectionname',
                'required' => true,
                'class' => 'required-entry',

            )
        );
        $fieldset->addField(
            'htmlcontent',
            'textarea',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Html'),
                'name' => 'htmlcontent',

            )
        );

        $fieldset->addField(
            'jscontent',
            'textarea',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Js Content'),
                'name' => 'jscontent',

            )
        );


        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
            ->getItems();

        foreach ($attributes as $attribute) {
            $select[] = array('value' => $attribute->getAttributecode(), 'label' => $attribute->getAttributecode());
        }

        /*	$fieldset->addField('custom_attribute', 'multiselect', array(
              'label'     => Mage::helper('gs_searchtap')->__('Select custom attribute'),
              'name'      => 'custom_attribute[]',
              'values' => array(
                                    '-1'=> array( 'label' => 'Please Select..', 'value' => '-1'),
                                    '1' => array('value'=> $select),
                               ),
            )); */
        $fieldset->addField(
            'status',
            'select',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Status'),
                'name' => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('gs_searchtap')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('gs_searchtap')->__('Disabled'),
                    ),
                ),
            )
        );
        $formValues = Mage::registry('current_searchtap')->getDefaultValues();

        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getSearchtapData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getSearchtapData());
            Mage::getSingleton('adminhtml/session')->setSearchtapData(null);
        } elseif (Mage::registry('current_searchtap')) {
            $formValues = array_merge($formValues, Mage::registry('current_searchtap')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
