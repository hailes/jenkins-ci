<?php
/**
 * Gs_Searchtap extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Searchtap admin edit form
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Block_Adminhtml_Searchtap_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'gs_searchtap';
        $this->_controller = 'adminhtml_searchtap';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('gs_searchtap')->__('Save Searchtap')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('gs_searchtap')->__('Delete Searchtap')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('gs_searchtap')->__('Save'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
		$this->_addButton(
            'reindexproduct',
            array(
                'label'   => Mage::helper('gs_searchtap')->__('Reindex Product'),
                'onclick' => 'reindexProduct()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
	   function reindexProduct() {
                editForm.submit($('edit_form').action+'reindex/edit/');
            }
        ";
	
	$this->_removeButton('save');
	//$this->_removeButton('delete');
	//$this->_removeButton('reset');
	//$this->_removeButton('back');
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_searchtap') && Mage::registry('current_searchtap')->getId()) {
            return Mage::helper('gs_searchtap')->__(
                "Update Settings",
                $this->escapeHtml(Mage::registry('current_searchtap')->getAdminkey())
            );
        } else {
            return Mage::helper('gs_searchtap')->__('Add Searchtap');
        }
    }
}
