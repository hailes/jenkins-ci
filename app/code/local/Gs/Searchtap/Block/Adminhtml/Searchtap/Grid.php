<?php
/**
 * Gs_Searchtap extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Searchtap admin grid block
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Block_Adminhtml_Searchtap_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('searchtapGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Gs_Searchtap_Block_Adminhtml_Searchtap_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('gs_searchtap/searchtap')
            ->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Gs_Searchtap_Block_Adminhtml_Searchtap_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('gs_searchtap')->__('Id'),
                'index' => 'entity_id',
                'type' => 'number'
            )
        );
        $select_store = array();
        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $store_id = $store->getId();
                    $store_name = $store->getName();
                    $select_store[$store_id] = $store_name;
                }
            }
        }

        $this->addColumn(
            'custom_store',
            array(
                'header' => Mage::helper('gs_searchtap')->__('Store'),
                'index' => 'custom_store',
                'type' => 'options',
                'options' => $select_store,
            )
        );
        $this->addColumn(
            'collectionname',
            array(
                'header' => Mage::helper('gs_searchtap')->__('Collection Name'),
                'index' => 'collectionname',
                'type' => 'text',

            )
        );
        $this->addColumn(
            'status',
            array(
                'header' => Mage::helper('gs_searchtap')->__('Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => array(
                    '1' => Mage::helper('gs_searchtap')->__('Enabled'),
                    '0' => Mage::helper('gs_searchtap')->__('Disabled'),
                )
            )
        );


        $this->addColumn(
            'action',
            array(
                'header' => Mage::helper('gs_searchtap')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('gs_searchtap')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'is_system' => true,
                'sortable' => false,
            )
        );
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Gs_Searchtap_Block_Adminhtml_Searchtap_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('searchtap');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Delete'),
                'url' => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('gs_searchtap')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label' => Mage::helper('gs_searchtap')->__('Change status'),
                'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
                'additional' => array(
                    'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('gs_searchtap')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('gs_searchtap')->__('Enabled'),
                            '0' => Mage::helper('gs_searchtap')->__('Disabled'),
                        )
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Gs_Searchtap_Model_Searchtap
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Gs_Searchtap_Block_Adminhtml_Searchtap_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
