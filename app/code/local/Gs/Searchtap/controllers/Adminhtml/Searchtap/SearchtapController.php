<?php
/**
 * Gs_Searchtap extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Searchtap admin controller
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Adminhtml_Searchtap_SearchtapController extends Gs_Searchtap_Controller_Adminhtml_Searchtap
{
    /**
     * init the searchtap
     *
     * @access protected
     * @return Gs_Searchtap_Model_Searchtap
     */
    protected function _initSearchtap()
    {
        $searchtapId = (int)$this->getRequest()->getParam('id');
        $searchtap = Mage::getModel('gs_searchtap/searchtap');
        if ($searchtapId) {
            $searchtap->load($searchtapId);
        }
        Mage::register('current_searchtap', $searchtap);
        return $searchtap;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('gs_searchtap')->__('Searchtap'))
            ->_title(Mage::helper('gs_searchtap')->__('Searchtaps'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit searchtap - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $searchtapId = $this->getRequest()->getParam('id');
        $searchtap = $this->_initSearchtap();
        if ($searchtapId && !$searchtap->getId()) {
            $this->_getSession()->addError(
                Mage::helper('gs_searchtap')->__('This searchtap no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getSearchtapData(true);
        if (!empty($data)) {
            $searchtap->setData($data);
        }
        Mage::register('searchtap_data', $searchtap);
        $this->loadLayout();
        $this->_title(Mage::helper('gs_searchtap')->__('Searchtap'))
            ->_title(Mage::helper('gs_searchtap')->__('Searchtaps'));
        if ($searchtap->getId()) {
            // $this->_title($searchtap->getAdminkey());
            $this->_title(Mage::helper('gs_searchtap')->__('Searchtap'));
        } else {
            $this->_title(Mage::helper('gs_searchtap')->__('Add searchtap'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new searchtap action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save searchtap - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('searchtap')) {
            try {
                $searchtap = $this->_initSearchtap();
                if ($this->getRequest()->getParam('reindex')) {
                    set_time_limit(0);
                    //$storeId = $searchtap->getData('custom_store');
                    $searchtap_id = $searchtap->getData('entity_id');

                    Mage::getModel('gs_searchtap/observer')->reindexProduct($searchtap_id);

                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('gs_searchtap')->__('Products has been successfully re-indexed.')
                    );
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }

                if (isset($data['custom_attribute']))
                    $data['custom_attribute'] = implode(',', $data['custom_attribute']);
                $searchtap->addData($data);
                $searchtap->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('gs_searchtap')->__('Searchtap settings has been successfully updated.')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    //$this->_redirect('*/*/edit', array('id' => $searchtap->getId()));
                    $this->_redirect('*/*/index');
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setSearchtapData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('gs_searchtap')->__('There was a problem saving the searchtap.')
                );
                Mage::getSingleton('adminhtml/session')->setSearchtapData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('gs_searchtap')->__('Unable to find searchtap to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete searchtap - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $searchtap = Mage::getModel('gs_searchtap/searchtap');
                $searchtap->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('gs_searchtap')->__('Searchtap was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('gs_searchtap')->__('There was an error deleting searchtap.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('gs_searchtap')->__('Could not find searchtap to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete searchtap - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $searchtapIds = $this->getRequest()->getParam('searchtap');
        if (!is_array($searchtapIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('gs_searchtap')->__('Please select searchtaps to delete.')
            );
        } else {
            try {
                foreach ($searchtapIds as $searchtapId) {
                    $searchtap = Mage::getModel('gs_searchtap/searchtap');
                    $searchtap->setId($searchtapId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('gs_searchtap')->__('Total of %d searchtaps were successfully deleted.', count($searchtapIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('gs_searchtap')->__('There was an error deleting searchtaps.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $searchtapIds = $this->getRequest()->getParam('searchtap');
        if (!is_array($searchtapIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('gs_searchtap')->__('Please select searchtaps.')
            );
        } else {
            try {
                foreach ($searchtapIds as $searchtapId) {
                    $searchtap = Mage::getSingleton('gs_searchtap/searchtap')->load($searchtapId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d searchtaps were successfully updated.', count($searchtapIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('gs_searchtap')->__('There was an error updating searchtaps.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName = 'searchtap.csv';
        $content = $this->getLayout()->createBlock('gs_searchtap/adminhtml_searchtap_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName = 'searchtap.xls';
        $content = $this->getLayout()->createBlock('gs_searchtap/adminhtml_searchtap_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName = 'searchtap.xml';
        $content = $this->getLayout()->createBlock('gs_searchtap/adminhtml_searchtap_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('gs_searchtap/searchtap');
    }
}
