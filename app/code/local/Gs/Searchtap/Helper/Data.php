<?php
/**
 * Gs_Searchtap extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Searchtap default helper
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * convert array to options
     *
     * @access public
     * @param $options
     * @return array
     * @author Ultimate Module Creator
     */
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])
            ) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }
	
	 public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag('gs_searchtap/general/enable', $store);
    }
    public function getLimit($store = null)
    {
        return (int) Mage::getStoreConfig('gs_searchtap/general/limit', $store);
    }
    public function getMinLength($store = null)
    {
        return (int) Mage::getStoreConfig('gs_searchtap/general/min_length', $store);
    }
    public function getCacheLifetime($store = null)
    {
        return (int) Mage::getStoreConfig('gs_searchtap/general/cache_lifetime', $store);
    }
    public function getUseLocalStorage($store = null)
    {
        return Mage::getStoreConfigFlag('gs_searchtap/general/use_local_storage', $store);
    }
    public function getJsPriceFormat()
    {
        return Mage::app()->getLocale()->getJsPriceFormat();
    }
    public function getBaseUrl()
    {
        return Mage::app()->getStore()->getBaseUrl();
    }
    public function getBaseUrlMedia()
    {
        return Mage::getSingleton('catalog/product_media_config')->getBaseMediaUrl();
    }
}
