<?php
/**
 * Gs_Searchtap extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Searchtap model
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
class Gs_Searchtap_Model_Indexer extends Mage_Index_Model_Indexer_Abstract
{
    protected $_matchedEntities = array(
        'gs_searchtap_entity' => array(
            Mage_Index_Model_Event::TYPE_SAVE
        )
    );

    // var to protect multiple runs
    protected $_registered = false;
    protected $_processed = false;
    protected $_categoryId = 0;
    protected $_productIds = array();

    /**
     * not sure why this is required.
     * _registerEvent is only called if this function is included.
     *
     * @param Mage_Index_Model_Event $event
     * @return bool
     */
    public function matchEvent(Mage_Index_Model_Event $event)
    {
        return Mage::getModel('catalog/category_indexer_product')->matchEvent($event);
    }


    public function getName()
    {
        return Mage::helper('gs_searchtap')->__('Searchtap Index Data');
    }

    public function getDescription()
    {
        return Mage::helper('gs_searchtap')->__('Sends data to SearchTap servers.');
    }

    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        // if event was already registered once, then no need to register again.
        if ($this->_registered) return $this;

        $entity = $event->getEntity();

        switch ($entity) {
            case Mage_Catalog_Model_Product::ENTITY:
                $this->_registerProductEvent($event);
                break;

            case Mage_Catalog_Model_Category::ENTITY:
                $this->_registerCategoryEvent($event);
                break;

            case Mage_Catalog_Model_Convert_Adapter_Product::ENTITY:
                $event->addNewData('gs_searchtap_reindex_all', true);
                break;

            case Mage_Core_Model_Store::ENTITY:
            case Mage_Core_Model_Store_Group::ENTITY:
                $process = $event->getProcess();
                $process->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
                break;
        }
        $this->_registered = true;
        return $this;
    }

    /**
     * Register event data during product save process
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerProductEvent(Mage_Index_Model_Event $event)
    {
        $eventType = $event->getType();

        if ($eventType == Mage_Index_Model_Event::TYPE_SAVE || $eventType == Mage_Index_Model_Event::TYPE_MASS_ACTION) {
            $process = $event->getProcess();
            /*$this->_productIds = $event->getDataObject()->getData('product_ids');
			
			echo '<pre>';
			print_r($this->_productIds);
			echo '</pre>';
            $this->flagIndexRequired($this->_productIds, 'products');*/

            $process->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
            $process->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
        }
    }

    /**
     * Register event data during category save process
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerCategoryEvent(Mage_Index_Model_Event $event)
    {
        $category = $event->getDataObject();
        /**
         * Check if product categories data was changed
         * Check if category has another affected category ids (category move result)
         */

        if ($category->getIsChangedProductList() || $category->getAffectedCategoryIds()) {
            $process = $event->getProcess();
            // $this->_categoryId = $event->getDataObject()->getData('entity_id');
            // $this->flagIndexRequired($this->_categoryId, 'categories');

            $process->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
            $process->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
        }
    }

    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        // process index event
        if (!$this->_processed) {
            $this->_processed = true;
        }
    }

    public function flagIndexRequired($ids = array(), $type = 'products')
    {
        $ids = (array)$ids;
        $collection = Mage::getModel('gs_searchtap/products')->getCollection();
        $filter = array();
        foreach ($ids as $id) {
            $filter[] = array('like' => "%,{$id},%");
        }
        $collection->addFieldToFilter($type, $filter);
        $collection->setDataToAll('flag', 1);
        $collection->save();
    }

    public function reindexAll()
    {
        set_time_limit(0);
        Mage::getModel('gs_searchtap/observer')->cronReindex();
    }
}