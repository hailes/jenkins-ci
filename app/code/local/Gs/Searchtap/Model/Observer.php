<?php

/**
 * @author Searchtap Team
 * @copyright Copyright (c) 2016 Searchtap
 * @package Gs_Searchtap
 */
class Gs_Searchtap_Model_Observer extends Varien_Event_Observer
{
    protected $product_visibility_array = array('1' => 'Not Visible Individually', '2' => 'Catalog', '3' => 'Search', '4' => 'Catalog,Search');
    public $small_image_size = 450;
    public $thumb_image_size = 250;
    public $storeId;
    public $adminKey;
    public $collectionName;
    public $cert_path;
    private $log_file_name = "searchtap.log";
    public $actualCount = 0;
    public $parentCount = 0;
    public $product_attribute_array = array();

    public function __construct()
    {
        $this->cert_path = Mage::getBaseDir('code') . "/local/Gs/Searchtap/gs_cert/searchtap.io.crt";
    }

    public function getStock($product)
    {
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        if ($stock) {
            $stock_array["in_stock"] = $stock->getIsInStock();
            $stock_array["stock_qty"] = $stock->getQty();
            $stock_array["stock_is_qty_decimal"] = $stock->getIsQtyDecimal();
        }
        return $stock_array;
    }

    public function productJSON($product)
    {
        $storeId = $this->storeId;
        $_categories = array();
        $productId = $product->getId();

        $product_status = $product->getStatus();
        $product_name = $product->getName();
        $product_sku = $product->getSku();
        $product_price = $product->getPrice();
        $product_special_price = $product->getFinalPrice();
        $productId = $product->getId();
        $product_visible = $product->getVisibility();
        $product_visibility = $this->product_visibility_array[$product_visible];

        $product_meta_title = $product->getMetaTitle();
        $product_meta_desc = $product->getMetaDescription();
        $product_meta_keyword = $product->getMetaKeyword();
        $product_weight = $product->getWeight();
        $product_short_desc = strip_tags($product->getShortDescription());
        $product_desc = strip_tags($product->getDescription());
        $currency_code = Mage::app()->getStore($storeId)->getCurrentCurrencyCode();
        $currency_symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore($storeId)->getCurrentCurrencyCode())->getSymbol();
        $gift_message = $product->getData('gift_message_available');
        $createdAt = strtotime($product->getCreatedAt());

        $is_discount_available = 0;
        if ($product_special_price > 0)
            $is_discount_available = ($product_special_price < $product_price) ? 1 : 0;

        if ($is_discount_available == 0)
            $product_special_price = $product_price;

        $stock_array = $this->getStock($product);

        $catpathArray = array();
        $catlevelArray = array();
        $collection2 = $product->getCategoryCollection()
            ->setStoreId($storeId)
            ->addAttributeToSelect('path');

        foreach ($collection2 as $cat1) {

            $pathIds = explode('/', $cat1->getPath());
            array_shift($pathIds);
            $collection_cat = Mage::getModel('catalog/category')->getCollection()
                ->setStoreId($storeId)
                ->addAttributeToSelect('name', 'path', 'entity_id')
                ->addFieldToFilter('entity_id', array('in' => $pathIds));

            $pahtByName = '';
            $level = 1;
            $path_name = array(array());
            $row = 0;
            foreach ($collection_cat as $cat) {
                if (!$cat->hasChildren()) {
                    $_categories[] = $cat->getName();
                }
                $path_name[$row][0] = $cat->getId();
                $path_name[$row][1] = $cat->getName();

                $row++;
            }

            for ($i = 0; $i < $row; $i++) {
                for ($j = 0; $j < $row; $j++) {
                    if ($pathIds[$i] == $path_name[$j][0]) {
                        if ($pahtByName == '') {
                            $pahtByName .= $path_name[$j][1];
                        } else {
                            $pahtByName .= '|||' . $path_name[$j][1];
                        }
                        if (!in_array($path_name[$j][1], $catlevelArray['_category_level_' . $level])) {
                            $catlevelArray['_category_level_' . $level][] = $path_name[$j][1];
                        }
                    }
                }
                if (!in_array($pahtByName, $catpathArray['categories_level_' . $level])) {
                    $catpathArray['categories_level_' . $level][] = $pahtByName;
                }
                $level++;
            }
        }

        $product_base_image = '';
        $product_small_image = '';
        $product_thumbnail = '';

        $image_processing = Mage::getStoreConfig('gs_searchtap/settings/image_processing');
        if ($product->getImage() == 'no_selection') {
            $product_base_image = null;
        } else {
            $product_base_image = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getImage());
        }

        if ($product->getSmallImage() == 'no_selection') {
            $product_small_image = null;
        } else {
            $product_small_image = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getSmallImage());
            if ($image_processing == 1)
                $product_small_image = $this->resizeImageSmall($product_small_image);
        }

        if ($product->getThumbnail() == 'no_selection') {
            $product_thumbnail = null;
        } else {
            $product_thumbnail = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getThumbnail());
            if ($image_processing == 1)
                $product_thumbnail = $this->resizeImageThumbnail($product_thumbnail);
        }

        $product_images = array();
        $product_images_small = array();

        $attributes = $product->getTypeInstance(true)->getSetAttributes($product);
        $media_gallery = $attributes['media_gallery'];
        $backend = $media_gallery->getBackend();
        $backend->afterLoad($product);

//        $mediaGallery = $product->getMediaGalleryImages();

        foreach ($product->getMediaGalleryImages() as $image) {
            $product_images[] = $image->getUrl();
            if ($image_processing == 1)
                $product_images_small[] = $this->resizeImageSmall($image->getUrl());
        }

//        $attributeSetModel = Mage::getModel('eav/entity_attribute_set');

        $attributeSetId = $product->getAttributeSetId();

        $attributes = array();

        $groups = Mage::getModel('eav/entity_attribute_group')
            ->getResourceCollection()
            ->setAttributeSetFilter($attributeSetId)
            ->setSortOrder()
            ->load();

        foreach ($groups as $node) {
            $nodeChildren = Mage::getResourceModel('catalog/product_attribute_collection')
                ->setAttributeGroupFilter($node->getId())
                ->addFieldToFilter('is_user_defined', true)
                ->addVisibleFilter()
                ->load();

            if ($nodeChildren->getSize() > 0) {
                foreach ($nodeChildren->getItems() as $child) {
                    $text = $child->getAttributeCode();
                    $backend_type = $child->getBackendType();
                    $text_value = $product->getData($text);
                    if ($text_value != '') {
                        if ($backend_type == 'datetime')
                            $attributes[$text] = strtotime($product->getResource()->getAttribute($text)->getFrontend()->getValue($product));
                        else {
                            $attributes[$text] = $product->getResource()->getAttribute($text)->getFrontend()->getValue($product);
                            if (is_numeric($attributes[$text])) {
                                if ((int)$attributes[$text] == $attributes[$text])
                                    $attributes[$text] = (int)$attributes[$text];
                                else
                                    $attributes[$text] = (float)$attributes[$text];
                            } else {
                                if ($product->getResource()->getAttribute($text)->getFrontendInput() == 'multiselect') {
                                    $att_txt_array = explode(",", $attributes[$text]);
                                    $attributes[$text] = $att_txt_array;
                                }
                            }
                        }
                    }
                }
            }
        }

        $parent_product_id = 0;
        $childId = array();
        $attributeOptions = array();
        $configurableAttributes = array();
        $variation = array();
        $bundle_price_min = 0;
        $bundle_price_max = 0;

        $product_type_id = $product->getTypeId();
        switch ($product_type_id) {
            case Mage_Catalog_Model_Product_Type::TYPE_SIMPLE:
                $product_type = "simple";
                break;
            case Mage_Catalog_Model_Product_Type::TYPE_BUNDLE:
                $product_type = "bundle";
                $isRequired = true;
                $bundle_in_stock = 0;
                $childrenIds = Mage::getResourceSingleton('bundle/selection')
                    ->getChildrenIds($productId, $isRequired);

                $bundleFlag = true;
                foreach ($childrenIds as $bundle) {
                    $temp = array();
                    if ($bundleFlag) {
                        foreach ($bundle as $bundle_id) {

                            $bundleCollection = Mage::getResourceModel('catalog/product_collection')
                                ->addStoreFilter($this->storeId)
                                ->addAttributeToSelect("*")
                                ->addAttributeToFilter('entity_id', array('eq' => $bundle_id));

                            foreach ($bundleCollection as $bc) {
                                $status = $bc->getStatus();
                                $stockArray = $this->getStock($bc);
                                if ($status == 1 and $stockArray['in_stock'] == 1 and $stockArray['stock_qty'] > 0) {
                                    $actualPrice = (float)$bc->getPrice();
                                    $specialPrice = (float)$bc->getSpecialPrice();
                                    $temp[] = $actualPrice - $specialPrice;
                                }
                            }
                        }
                        if (count($temp) == 0) {
                            $bundleFlag = false;
                            $bundle_price_min = 0;
                        }
                    }
                    $bundle_price_min += min($temp);
                }

                if ($bundle_price_min == 0) {
                    $bundle_in_stock = 0;
                    $childrenIds = Mage::getResourceSingleton('bundle/selection')
                        ->getChildrenIds($productId, true);

                    foreach ($childrenIds as $bundle) {
                        $temp = array();
                        foreach ($bundle as $bundle_id) {
                            $bundleCollection = Mage::getResourceModel('catalog/product_collection')
                                ->addStoreFilter($this->storeId)
                                ->addAttributeToSelect("*")
                                ->addAttributeToFilter('entity_id', array('eq' => $bundle_id));

                            foreach ($bundleCollection as $bc) {
                                $status = $bc->getStatus();
                                $stockArray = $this->getStock($bc);
                                if ($status == 1 and $stockArray['in_stock'] == 1 and $stockArray['stock_qty'] > 0) {
                                    $actualPrice = (float)$bc->getPrice();
                                    $specialPrice = (float)$bc->getSpecialPrice();
                                    $temp[] = $actualPrice - $specialPrice;
                                }
                            }
                        }
                        $bundle_price_min += min($temp);
                    }
                } else {
                    $bundle_in_stock = 1;
                }

                $isRequired = false;
                $childrenIds_nr = Mage::getResourceSingleton('bundle/selection')
                    ->getChildrenIds($productId, $isRequired);

                foreach ($childrenIds_nr as $bundle) {
                    $temp_nr = array();
                    foreach ($bundle as $bundle_id) {
                        $bundleCollection = Mage::getResourceModel('catalog/product_collection')
                            ->addStoreFilter($this->storeId)
                            ->addAttributeToSelect("*")
                            ->addAttributeToFilter('entity_id', array('eq' => $bundle_id));

                        foreach ($bundleCollection as $bc) {
                            $status = $bc->getStatus();
                            $stockArray = $this->getStock($bc);
                            if ($status == 1 and $stockArray['in_stock'] == 1 and $stockArray['stock_qty'] > 0) {
                                $actualPrice_nr = (float)$bc->getPrice();
                                $specialPrice_nr = (float)$bc->getSpecialPrice();
                                $temp_nr[] = $actualPrice_nr - $specialPrice_nr;
                            }
                        }
                    }
                    $bundle_price_max += max($temp_nr);
                }
                break;
            case Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE:
                $product_type = "configurable";
                $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
                $attributeOptions = array();
                foreach ($productAttributeOptions as $productAttribute) {
                    foreach ($productAttribute['values'] as $attribute) {
                        $attributeOptions[$productAttribute['attribute_code']][] = $attribute['store_label'];
                        $configurableAttributes['_' . $productAttribute['attribute_code']][] = $attribute['store_label'];
                    }
                }
                $child_count = 0;
                foreach ($product->getTypeInstance(true)->getUsedProducts(null, $product) as $simple) {
                    $childId[] = (int)$simple->getId();
                    $variation[$child_count]['id'] = $simple->getId();

                    foreach ($attributeOptions as $key => $attribute_options) {
                        $value = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($simple->getId(), $key, $storeId);
                        if (!empty($value)) {
                            $variation[$child_count][$key] = Mage::getModel('catalog/product')->getResource()->getAttribute($key)->getSource()->getOptionText($value);
                        }
                    }

                    $child_collection = Mage::getResourceModel('catalog/product_collection')
                        ->addStoreFilter($this->storeId)
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('entity_id', array('eq' => $simple->getId()));

                    foreach ($child_collection as $col) {
                        $price_value = (float)$col->getPrice();
                        $simpleFinalPrice = (float)$col->getFinalPrice();
                        $is_discount_available_simple = 0;
                        if ($simpleFinalPrice > 0)
                            $is_discount_available_simple = ($simpleFinalPrice < $price_value) ? 1 : 0;

                        $childStatus = (int)$col->getStatus();
                        if ($childStatus == 2)
                            $childStatus = 0;

                        $child_visible = (int)$col->getVisibility();
                        $child_visibility = $this->product_visibility_array[$child_visible];

                        $stock_array_var = $this->getStock($simple);

                        $variation[$child_count]['stock_qty'] = (int)$stock_array_var["stock_qty"];
                        $variation[$child_count]['in_stock'] = (int)$stock_array_var["in_stock"];
                        $variation[$child_count]['price'] = $price_value;
                        $variation[$child_count]['is_discount_available'] = $is_discount_available_simple;
                        $variation[$child_count]['discounted_price'] = $simpleFinalPrice;
                        $variation[$child_count]['status'] = $childStatus;
                        $variation[$child_count]['product_visibility'] = $child_visibility;

                        $child_count++;
                    }
                }
                break;
            case Mage_Catalog_Model_Product_Type::TYPE_GROUPED:
                $product_type = "grouped";
                $associated_products = $product->getTypeInstance(true)->getAssociatedProducts($product);
                foreach ($associated_products as $associate_product) {
                    $childId[] = $associate_product->getId();
                }
                break;

            case Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL:
                $product_type = "virtual";
                break;
            default:
                $product_type = $product->getTypeId();
        }

        //Fetching product tags
        $product_tags = array();

        $model = Mage::getModel('tag/tag');
        $tagCollection = $model->getResourceCollection()
            ->addPopularity()
            ->addStatusFilter($model->getApprovedStatus())
            ->addProductFilter((int)$productId);

        $tagCollection->getSelect()->where('relation.store_id IN (?)', $this->storeId);

        foreach ($tagCollection as $tag) {
            $product_tags[] = $tag->getName();
        }


        $custom_attributes = array();
        $product_array = array(
            "id" => $productId,
            "sku" => $product_sku,
            "name" => $product_name,
            "price" => (float)$product_price,
            "discounted_price" => (float)$product_special_price,
            "currency_code" => $currency_code,
            "currency_symbol" => $currency_symbol,
            "status" => (int)$product_status,
            "small_img_url" => $product_small_image,
            "base_img_url" => $product_base_image,
            "thumb_img_url" => $product_thumbnail,
            "_images" => $product_images,
            "_images_small" => $product_images_small,
            "in_stock" => (int)$stock_array["in_stock"],
            "stock_qty" => (int)$stock_array["stock_qty"],
            "stock_is_qty_decimal" => (int)$stock_array["stock_is_qty_decimal"],
            "created_at" => $createdAt,
            "product_type" => $product_type,
            "parent_product_id" => (int)$parent_product_id,
            "children_ids" => $childId,
            "variations" => $variation,
            "product_visibility" => $product_visibility,
            "_categories" => $_categories,
            "is_discount_available" => (int)$is_discount_available,
            "stock_is_qty_decimal" => (int)$stock_array["stock_is_qty_decimal"],
            "tags" => $product_tags
        );
        if ($product_type == "bundle") {
            $product_array['price_min'] = $bundle_price_min;
            $product_array['price_max'] = $bundle_price_max;
            $product_array['product_in_stock'] = $bundle_in_stock;
        }

        if (!empty($this->product_attribute_array)) {
            foreach ($this->product_attribute_array as $attribute) {
                if ($attribute == "description") {
                    $product_array['short_desc'] = $product_short_desc;
                    $product_array['desc'] = $product_desc;
                } else if ($attribute == "meta_description")
                    $product_array['meta_desc'] = $product_meta_desc;
                else if ($attribute == "meta_title")
                    $product_array['meta_desc'] = $product_meta_title;
                else if ($attribute == "meta_keyword")
                    $product_array['meta_keywords'] = $product_meta_keyword;
                else if ($attribute == "weight")
                    $product_array['weight'] = (float)$product_weight;
                else if ($attribute == "url_path") {
                    $urlKey = Mage::getResourceSingleton('catalog/product')
                        ->getAttributeRawValue($productId, 'url_path', Mage::app()->getStore($storeId));
                    $product_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $urlKey;
                    $product_array['url'] = $product_url;
                } else if ($attribute == "gift_message_available")
                    $product_array['is_gift_enabled'] = (int)$gift_message;
                else if ($attribute == "rating") {
                    $summaryData = Mage::getModel('review/review_summary')
                        ->setStoreId($storeId)
                        ->load($productId);
                    $avg_rating = $summaryData->getRatingSummary();
                    $product_array['rating'] = (float)$avg_rating;
                } else if ($attribute == "order_qty") {
                    $query = Mage::getResourceModel('sales/order_item_collection');

                    $query->getSelect()->reset(Zend_Db_Select::COLUMNS)
                        ->columns(array('sku', 'SUM(qty_ordered) as purchased'))
                        ->where('product_id = ?', array($productId))
                        ->limit(1);

                    $product_qty = $query->getFirstItem();
                    $totalOrders = $product_qty->getPurchased();
                    $product_array['order_qty'] = (int)$totalOrders;
                } else {
                    foreach ($attributes as $att) {
                        if ($att[$attribute]) {
                            if ($attributes[$attribute] != null)
                                $custom_attributes[$attribute] = $attributes[$attribute];
                        }
                    }
                }
            }
        }

        $new_product_array = array_merge($product_array, $custom_attributes, $catpathArray, $catlevelArray, $configurableAttributes);
        return $new_product_array;
    }

    public function searchtapCurlRequest($product_json)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.searchtap.io/v1/collections/" . $this->collectionName,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_CAINFO => $this->cert_path,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $product_json,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "x-auth-token: " . $this->adminKey
            ),
        ));

        curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Mage::log("Exception occurred", null, $this->log_file_name);
        }
    }

    public function searchtapCurlDeleteRequest($productIds)
    {
        $curl = curl_init();

        $data_json = json_encode($productIds);

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.searchtap.io/v1/collections/" . $this->collectionName . "/delete",
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_CAINFO => $this->cert_path,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_POSTFIELDS => $data_json,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "x-auth-token: " . $this->adminKey
            ),
        ));

        $exec = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Mage::log("Exception occurred", null, $this->log_file_name);
        }
        return;
    }

    public function resizeImageThumbnail($imageUrl)
    {
        $newImageUrl = '';
        if (!file_exists(Mage::getBaseDir() . "/media/searchtapimage/thumbnail"))
            mkdir(Mage::getBaseDir() . "/media/searchtapimage/thumbnail", 0777);

        // get image name
        $imageName = substr(strrchr($imageUrl, "/"), 1);

        // resized image path (media/catalog/category/resized/IMAGE_NAME)
        $imageResized = Mage::getBaseDir('media') . DS . "searchtapimage" . DS . "thumbnail" . DS . $imageName;

        // changing image url into direct path
        $dirImg = Mage::getBaseDir() . str_replace("/", DS, strstr($imageUrl, '/media'));

        // if resized image doesn't exist, save the resized image to the resized directory
        if (!file_exists($imageResized) && file_exists($dirImg)) {
            try {
                $imageObj = new Varien_Image($dirImg);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio(TRUE);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($this->thumb_image_size, $this->thumb_image_size);
                $imageObj->save($imageResized);
            } catch (Exception $e) {
                return '';
            }
        }

        $newImageUrl = Mage::getBaseUrl('media') . "searchtapimage/thumbnail/" . $imageName;
        return $newImageUrl;
    }

    public function resizeImageSmall($imageUrl)
    {
        $newImageUrl = '';

        if (!file_exists(Mage::getBaseDir() . "/media/searchtapimage/small"))
            mkdir(Mage::getBaseDir() . "/media/searchtapimage/small", 0777);

        // get image name
        $imageName = substr(strrchr($imageUrl, "/"), 1);

        // resized image path (media/catalog/category/resized/IMAGE_NAME)
        $imageResized = Mage::getBaseDir('media') . DS . "searchtapimage" . DS . "small" . DS . $imageName;

        // changing image url into direct path
        $dirImg = Mage::getBaseDir() . str_replace("/", DS, strstr($imageUrl, '/media'));

        // if resized image doesn't exist, save the resized image to the resized directory
        if (!file_exists($imageResized) && file_exists($dirImg)) {
            try {
                $imageObj = new Varien_Image($dirImg);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio(TRUE);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($this->small_image_size, $this->small_image_size);
                $imageObj->save($imageResized);
            } catch (Exception $e) {
                return '';
            }
        }

        $newImageUrl = Mage::getBaseUrl('media') . "searchtapimage/small/" . $imageName;
        return $newImageUrl;
    }

    public function loadSearchtap($searchtapId)
    {
        $searchtap = Mage::getModel('gs_searchtap/searchtap');
        $searchtap->load($searchtapId);
        return $searchtap;
    }

    public function sendProduct($collection)
    {
        $new_product_array = array();
        // we iterate through the list of products to get attribute values
        foreach ($collection as $product) {
            try {
                $productFlag = true;
                $product_type_id = $product->getTypeId();
                switch ($product_type_id) {
                    case Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE:
                        $child_status = array();
                        $child_count = 0;
                        foreach ($product->getTypeInstance(true)->getUsedProducts(null, $product) as $simple) {
                            $childId[] = (int)$simple->getId();

                            $childStatus = (int)Mage::getResourceModel('catalog/product')->getAttributeRawValue($simple->getId(), 'status', $this->storeId);
                            if ($childStatus == 2)
                                $childStatus = 0;

                            $child_status[$child_count] = $childStatus;

                            $child_count++;
                        }

                        $flag = 0;
                        for ($i = 0; $i < $child_count; $i++) {
                            if ($child_status[$i] == 1)
                                $flag = 1;
                        }
                        if ($flag == 0) {
                            $this->parentCount++;
                            $productFlag = false;
                        }
                        break;
                }

                if ($productFlag) {
                    $new_product_array[] = $this->productJSON($product);
                    $this->actualCount++;
                }
            } catch (Exception $e) {
                Mage::log("Error Indexing Product Id" . $product->getId(), null, $this->log_file_name);
                Mage::logException($e);
            }
        }
        Mage::log("Sending Request to SearchTap", null, $this->log_file_name);

        $product_json = json_encode($new_product_array);

        unset($new_product_array);

        $new_product_array = array();

        $this->searchtapCurlRequest($product_json);

        unset($product_json);

        //take a break of 1 sec before starting working on next requests
        sleep(1);
        Mage::log("Completed Request to SearchTap", null, $this->log_file_name);

        return $new_product_array;
    }

    public function reindexProduct($searchtap_id)
    {
        $this->getUserSelectedAttributes();

        $searchtap = $this->loadSearchtap($searchtap_id);
        $this->storeId = $searchtap->getData('custom_store');
        $this->adminKey = $searchtap->getData('adminkey');
        $this->collectionName = $searchtap->getData('collectionname');

        //$new_product_array = array();
        $product_steps = 1000;

        //get product count for all products having no parents
        //get all products + enabled products
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addStoreFilter($this->storeId)
            ->addAttributeToFilter('status', array('eq' => 1));
        //->addAttributeToFilter('type_id', array('eq' => 'simple'));

        //left join with parent mapping table and only get parent simple product
        $collection->getSelect()->joinLeft(array(
            'link_table' => 'catalog_product_super_link'),
            'link_table.product_id = e.entity_id',
            array('product_id')
        );

        $collection->getSelect()->where('link_table.product_id IS NULL');

        $product_collection = $collection->getSize();

        Mage::log("Starting Indexing. Total Products : " . $product_collection, null, $this->log_file_name);

        //This while loop will index simple products having no parent
        $counter = $i = 0;

        while ($i <= $product_collection) {
            $counter = $counter + 1;

            Mage::log("Fetching Records from DB products", null, $this->log_file_name);

            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addStoreFilter($this->storeId)
                ->addAttributeToSelect('*')// select all attributes
                ->addAttributeToFilter('status', array('eq' => 1))
                //->addAttributeToFilter('type_id', array('eq' => 'simple'))
                ->setPageSize($product_steps)// limit number of results returned
                ->addAttributeToSort('created_at', 'desc')
                ->setCurPage($counter); // set the offset (useful for pagination)

            //left join with parent mapping table and only get parent simple product
            $collection->getSelect()->joinLeft(array(
                'link_table' => 'catalog_product_super_link'),
                'link_table.product_id = e.entity_id',
                array('product_id')
            );

            $collection->getSelect()->where('link_table.product_id IS NULL');


            Mage::log("Completed Fetching records from DB products", null, $this->log_file_name);

            $this->sendProduct($collection);

            $i = $i + $product_steps;

            Mage::log("Products Indexed products: " . $i, null, $this->log_file_name);
            Mage::log("Memory Usage products: " . memory_get_peak_usage(), null, $this->log_file_name);
        }

        $issues = $product_collection - $this->actualCount;
        $issues -= $this->parentCount;
        Mage::log("Indexing Completed ", null, $this->log_file_name);
        Mage::log("Total products needs to be indexed = " . $product_collection, null, $this->log_file_name);
        Mage::log("Total Parent products having no child enabled = " . $this->parentCount, null, $this->log_file_name);
        Mage::log("Total products that actually indexed = " . $this->actualCount, null, $this->log_file_name);
        Mage::log("Issues = " . $issues, null, $this->log_file_name);

        // Logic to remove disabled products

        Mage::log("Removing Disabled Products ", null, $this->log_file_name);

        $removeCollection = Mage::getResourceModel('catalog/product_collection')
            ->addStoreFilter($this->storeId)
            ->addAttributeToFilter('status', array('eq' => 2));

        $removeSize = $removeCollection->getSize();

        $counter = 1;
        $i = 0;
        $product_steps_delete = 10000;
        $badIds = array();

        Mage::log("Total products to remove : " . $removeSize, null, $this->log_file_name);

        while ($i < $removeSize) {
            if ($removeSize == 0)
                break;

            $removeCollection = Mage::getResourceModel('catalog/product_collection')
                ->addStoreFilter($this->storeId)
                ->addAttributeToFilter('status', array('eq' => 2))
                ->addAttributeToSelect('entity_id')
                ->setPageSize($product_steps_delete)
                ->setCurPage($counter);

            foreach ($removeCollection as $product) {
                try {
                    $product_status = $product->getStatus();
                    if ($product_status == 2) {
                        $productId = $product->getId();

                        $badIds[] = $productId;

                        $i = $i + 1;
                    }

                } catch (Exception $e) {
                    Mage::log("Error Deleting Product Ids" . json_encode($badIds), null, $this->log_file_name);
                    Mage::logException($e);
                }
            }
            try {
                $this->searchtapCurlDeleteRequest($badIds);
                Mage::log("Deleted Products" . json_encode($badIds), null, $this->log_file_name);
                unset($badIds);
            } catch (Exception $e) {
                Mage::log("Error Deleting Product Ids" . json_encode($badIds), null, $this->log_file_name);
                Mage::logException($e);
            }

            $counter = $counter + 1;
        }

        Mage::log("Completed Removing Disabled Products", null, $this->log_file_name);

        return;
    }

    public function cronReindex($collection_id)
    {
        $time = gmmktime();
        $start_at = date('Y-m-d H:i:s', $time);

        $id = array();

        $collections = $this->getSearchtapCollection();
        foreach ($collections as $collection) {
            $id[] = $collection->getData('entity_id');
        }

        if ($collection_id) {
            foreach ($id as $id) {
                if ($id == $collection_id) {
                    $this->reindexProduct($collection_id);
                }
            }
        } else {
            foreach ($id as $id)
                $this->reindexProduct($id);
        }

        $end_at = date('Y-m-d H:i:s', $time);

        $data = array('started_at' => $start_at, 'ended_at' => $end_at);

        //Reindexing status updated in Index Management
        $indexer_code = "searchtap_indexer";
        $process = Mage::getSingleton('index/indexer')
            ->getProcessByCode($indexer_code)
            ->changeStatus(Mage_Index_Model_Process::STATUS_PENDING);

        $process->addData($data)->save();
    }

    public function getUserSelectedAttributes()
    {
        $product_attribute = unserialize(Mage::getStoreConfig('gs_searchtap/settings/product_additional_attributes'));
        foreach ($product_attribute as $attribute)
            $this->product_attribute_array[] = $attribute['Attribute'];
    }

    public function sendProductToSearchtapOnTrigger($product_collection)
    {
        $this->getUserSelectedAttributes();

        $flag1 = false;
        $flag2 = false;
        $new_product_array = array();

        $collections = $this->getSearchtapCollection();
        foreach ($collections as $collection) {
            $this->storeId = $collection->getData('custom_store');
            $this->adminKey = $collection->getData('adminkey');
            $this->collectionName = $collection->getData('collectionname');

            foreach ($product_collection as $product) {
                if ($product->getStatus() != 2) {
                    $new_product_array[] = $this->productJSON($product);
                    $flag1 = true;
                } else {
                    $productId = array();
                    $productId[] = $product->getId();
                    $flag2 = true;
                }
            }
            if ($flag1) {
                $product_json = json_encode($new_product_array);
                $this->searchtapCurlRequest($product_json);
            } else if ($flag2) {
                $this->searchtapCurlDeleteRequest($productId);
            }
        }
    }

    public function orderCancel($observer)
    {
        $item = $observer->getEvent()->getItem();

        $product_collection = array();
        $productId = $item->getProductId();

        $product = Mage::getModel('catalog/product')->load($productId);
        if ($productId) {
            $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId);
            if ($parentIds) {
                $productId = $parentIds[0];
                $product = Mage::getModel('catalog/product')->load($productId);
            } else {
                $bundleParentIds = Mage::getResourceSingleton('bundle/selection')->getParentIdsByChild($productId);
                if ($bundleParentIds) {
                    $product_collection[] = $product;
                    $parentProduct = Mage::getModel('catalog/product')->load($bundleParentIds[0]);
                    $product = $parentProduct;
                }
            }

            $product_collection[] = $product;
            $this->sendProductToSearchtapOnTrigger($product_collection);
        }
    }

    public function orderPlace($observer)
    {
        $product_collection = array();
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllItems();
        $productId = array();
        $i = 0;
        $flag = 1;
        foreach ($items as $item) {
            $productId[$i] = $item->getProductId();
            if ($productId[$i]) {

                $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId[$i]);
                if ($parentIds) {
                    $productId[$i] = $parentIds[0];
                    $product = Mage::getModel('catalog/product')->load($productId[$i]);
                    $flag = 2;
                } else {
                    $bundleParentIds = Mage::getResourceSingleton('bundle/selection')->getParentIdsByChild($productId[$i]);
                    if ($bundleParentIds) {
                        $parentProduct = Mage::getModel('catalog/product')->load($bundleParentIds[0]);
                        $product = $parentProduct;
                        $flag = 3;
                    }
                }
                if ($flag == 1)
                    $product = Mage::getModel('catalog/product')->load($productId[$i]);
                else if ($flag == 3) {
                    $product = Mage::getModel('catalog/product')->load($productId[$i]);
                    $product_collection[] = $product;
                }

                $product_collection[] = $product;
            }
            $i++;
        }
        $this->sendProductToSearchtapOnTrigger($product_collection);
    }

    public function save($observer)
    {
        $product = $observer->getEvent()->getProduct();

        $product_collection = array();

        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());

        if ($parentIds) {
            $parentProduct = Mage::getModel('catalog/product')->load($parentIds[0]);
            $product = $parentProduct;
        } else {
            $bundleParentIds = Mage::getResourceSingleton('bundle/selection')->getParentIdsByChild($product->getId());
            if ($bundleParentIds) {
                $product_collection[] = $product;
                $parentProduct = Mage::getModel('catalog/product')->load($bundleParentIds[0]);
                $product = $parentProduct;
            }
        }

        $product_collection[] = $product;

        $this->sendProductToSearchtapOnTrigger($product_collection);
    }

    public function deleteProduct($observer)
    {
        $product = $observer->getEvent()->getProduct();
        $productId[] = $product->getId();
        $collections = $this->getSearchtapCollection();
        foreach ($collections as $collection) {
            $this->storeId = $collection->getData('custom_store');
            $this->adminKey = $collection->getData('adminkey');
            $this->collectionName = $collection->getData('collectionname');

            $this->searchtapCurlDeleteRequest($productId);
        }
    }

    public function getSearchtapCollection()
    {
        $collections = Mage::getModel('gs_searchtap/searchtap')
            ->getCollection()
            ->addFieldToFilter('status', array('eq' => 1));

        $collections->getSelect()->group('custom_store');
        return $collections;
    }

    public function afterImportProductData($observer)
    {
        $adapter = $observer->getEvent()->getAdapter();
        $productIds = $adapter->getAffectedEntityIds();

        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collections = $this->getSearchtapCollection();

        foreach ($collections as $collection_store) {
            $this->storeId = $collection_store->getData('custom_store');
            $this->adminKey = $collection_store->getData('adminkey');
            $this->collectionName = $collection_store->getData('collectionname');
            $this->afterImportProductDataStore($productIds, $attributes);
        }
    }

    public function afterImportProductDataStore($productIds, $attributes)
    {
        $this->getUserSelectedAttributes();

        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter('entity_id', array('in' => $productIds))
            ->addAttributeToSelect($attributes);
        $product_count = 1;
        $product_break = 500;
        foreach ($collection as $product) {
            $product_status = $product->getStatus();
            if ($product_status == 2) {
                $productId[] = $product->getId();
                $this->searchtapCurlDeleteRequest($productId);
                // continue;
            } else {
                $new_product_array[] = $this->productJSON($product);
                if ($product_count == $product_break) {
                    $product_count = 0;
                    $product_json = json_encode($new_product_array);
                    $new_product_array = array();
                    $this->searchtapCurlRequest($product_json);
                }
                $product_count++;
            }
        }

        if (!empty($new_product_array)) {
            $product_json = json_encode($new_product_array);
            $new_product_array = array();
            $this->searchtapCurlRequest($product_json);
        }
    }
}

?>
