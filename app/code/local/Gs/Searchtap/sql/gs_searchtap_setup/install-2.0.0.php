<?php
/**
 * Gs_Searchtap extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Gs
 * @package        Gs_Searchtap
 * @copyright      Copyright (c) 2017
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Searchtap module install script
 *
 * @category    Gs
 * @package     Gs_Searchtap
 * @author      Ultimate Module Creator
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('gs_searchtap/searchtap'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Searchtap ID'
    )
    ->addColumn(
        'adminkey',
        Varien_Db_Ddl_Table::TYPE_TEXT, 300,
        array(
            'nullable'  => false,
        ),
        'Admin Key'
    )
    ->addColumn(
        'searchkey',
        Varien_Db_Ddl_Table::TYPE_TEXT, 300,
        array(
            'nullable'  => false,
        ),
        'Search Key'
    )
    ->addColumn(
        'collectionname',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Collection Name'
    )
    ->addColumn(
        'htmlcontent',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(),
        'Html'
    )
    ->addColumn(
        'jscontent',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(),
        'Js Content'
    )
	->addColumn(
        'custom_store',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'store id'
    )	
	->addColumn(
		'custom_attribute',
		Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(),
        'Custom attribute to send'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Searchtap Modification Time'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Searchtap Creation Time'
    ) 
    ->setComment('Searchtap Table');
$this->getConnection()->createTable($table);
/*
$this->run("
 
INSERT INTO {$this->getTable('gs_searchtap/searchtap')} (adminkey,searchkey,collectionname,htmlcontent,jscontent,status) VALUES ('','','','','','1');

");
*/
$this->endSetup();