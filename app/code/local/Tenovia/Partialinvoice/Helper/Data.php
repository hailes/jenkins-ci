<?php

class Tenovia_Partialinvoice_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function convert_number_to_words($number) 
    {
        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' and paise ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            25                   => 'twenty five',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
       
        if (!is_numeric($number)) {
            return false;
        }
       
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . $this->convert_number_to_words(abs($number));
        }
       
        $string = $fraction = null;
       
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
       
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }
       
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
           $string .= $this->convert_number_to_words($fraction);
        }
       $smt = ucfirst(strtolower($string));
        return $smt;
    }

    public function fetchInvoiceContent($invoice_id,$type)
    {
        $_invoice = Mage::getModel('sales/order_invoice')->load($invoice_id);
        $_order = $_invoice->getOrder();
        
        $payment_order = Mage::getModel('sales/order')->loadByIncrementId($_order->getRealOrderId());
        $payment_title = $payment_order->getPayment()->getMethodInstance()->getTitle();
        $_items = $_invoice->getItemsCollection();
        
        $ordered_items = $_order->getAllVisibleItems();
        
        $amt = (number_format($_invoice->getGrandTotal(),0));
        $grandamount = str_replace(',','',$amt);

        $priceSymbol = '<span style="font-family:dejavusanscondensed">&#x20b9;</span>';

        foreach ($_items as $_item){

            $itemssku =  $_item->getSku();  
            
            foreach($ordered_items as $itemso){
                $itemsosku =  $itemso->getSku();
                if($itemssku == $itemsosku){
                    $product = Mage::getModel('catalog/product')->load($_item->getProductId());
                    $productWeight = $product->getAttributeText("size");
                    $price = number_format(($product->getPrice() * $_item->getQty()) ,2);
                    $total_crosss_amount[] = ($product->getPrice() * $_item->getQty());
                    $taxpercents = ($itemso->getTaxPercent());
                    $selling_price = abs($price - ($_item->getDiscountAmount()+$_item->getTaxAmount()));
                    if($selling_price <=0.00)
                    {
                        $freebie = ' Free !! '.$_item->getName().' ('.$_item->getSku().')';
                        $pricevalue = 'Free';
                        $is_free =1;
                        $discount_amt ='--';
                        $selling_price_1 ='--';
                        $taxpercents ='--';
                        $tax_amount ='--';
                        $total_amt = 'Free';
                        $_item->setDiscountAmount(0);
                        $price =0.00;
                        
                    }
                    else
                    {
                        $pricevalue = $priceSymbol.$price;
                        $freebie = $_item->getName().' ('.$_item->getSku().')';
                        $discount_amt = $priceSymbol.number_format($_item->getDiscountAmount(),2);
                        $selling_price_1 =$priceSymbol.number_format($selling_price,2);
                        $taxpercents = number_format($taxpercents,2).'%';
                        $tax_amount =$priceSymbol.number_format($_item->getTaxAmount(),2);
                        $total_amt = $priceSymbol.number_format(($_item->getRowTotalInclTax() - $_item->getDiscountAmount()),2);
                    }
                }
                
            }
        
            $total_qty[] = $_item->getQty();
            $total_discount[] = $_item->getDiscountAmount();
            
            $total_tax_amount[] = $_item->getTaxAmount();
            $final_values[] = number_format(($_item->getRowTotalInclTax() - $_item->getDiscountAmount()),2);
            $selling_price_2 = $priceSymbol.number_format(($_item->getRowTotalInclTax() - ($_item->getDiscountAmount()+$_item->getTaxAmount())),2);
            $total_selling_price[] = ($_item->getRowTotalInclTax() - ($_item->getDiscountAmount()+$_item->getTaxAmount()));
            $itemsHtml.='<tr>   
                    <td style="padding:8px; text-align:left; color:#000"> '.$freebie.'</td>                        
                    <td style="padding:8px; text-align:center; color:#000"> '.number_format($_item->getQty(),0).'</td>
                    <td style="padding:8px; text-align:center; color:#000"> '.$productWeight.'</td>
                     <td style="padding:8px; text-align:center; color:#000"> '.$pricevalue.'</td>
                     <td style="padding:8px; text-align:center; color:#000"> '.$discount_amt.'</td>
                     <td style="padding:8px; text-align:center; color:#000"> '.$selling_price_2.'</td>
                     <td style="padding:8px; text-align:center; color:#000"> '.$taxpercents.'</td>
                     <td style="padding:8px; text-align:center; color:#000"> '.$tax_amount.'</td>
                                           
                    <td style="padding:8px; text-align:center; color:#000"> '.$total_amt.'</td>
                    </tr>';
        }

        $gmt = number_format($_invoice->getGrandTotal(),2);

        $html='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Feetscience Invoice</title>
            </head>
            <body>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:20px 5px; font-family:"Times New Roman", Times, serif;" bgcolor="#fff">
                <tr>
                    <td align="center" valign="top">
                        <table width="760" border="0" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tr>
                                <td valign="top" style="border-bottom:1px solid #000; font-size:16px; font-weight:bold; padding:10px; vertical-align: top;">Original <br /> Tax Invoice  <br><br><b>'.$payment_title.'</b></td>
                                <td valign="top" style="border-bottom:1px solid #000; font-size:14px; padding:10px; vertical-align: top;">
                                    <p style="margin:0 0 5px; font-weight:bold;">Order ID:'.$_order->getRealOrderId().'</p>
                                    <p style="margin:0;">Order Date: '.$_order->getCreatedAtStoreDate().'</p>
                                </td>
                                <td valign="top" style="border-bottom:1px solid #000; font-size:14px; padding:10px; vertical-align: top;">
                                    <p style="margin:0 0 5px; font-weight:bold;">Invoice No : '.$_invoice->getIncrementId().'</p>
                                    <p style="margin:0;">Invoice Date : '.$_invoice->getCreatedAtStoreDate().'</p>
                                </td>
                                <td valign="top" style="border-bottom:1px solid #000; font-size:14px; padding:10px; vertical-align: top;">
                                    <p style="margin:0 0 5px;">TIN No : 29560075364</p>
                                    <p style="margin:0;">CST NO : 29560075364</p>
                                    <p style="margin:0;">PAN No : AABCP3052F</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom:1px solid #000; font-size:13px;  padding:10px;  vertical-align: top;">
                                    <img src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'/skin/frontend/default/default/images/logo.png" width="200px" height="40px">
                                </td>
                                <td style="border-bottom:1px solid #000; font-size:13px;  padding:10px;  vertical-align: top;">
                                    <h2 style="font-size:16px; margin: 0;"> Sold By</h2>
                                    <p style="margin:5px 0;">Paragon Footwear,<br>
                                        Paragon Polymer Products Pvt. Ltd.,<br>
                                        No.120/1, 2, 3,<br />
                                        Budhihal, Nelamangala,<br />
                                        Bangalore- 562123
                                    </p>
                                </td>
                                <td style="border-bottom:1px solid #000; font-size:13px; padding:10px;  vertical-align: top;">
                                    <h2 style="font-size:16px; margin: 0;"> Shipping Address</h2>
                                    '.$_order->getShippingAddress()->format('html').'
                                </td>
                                <td style="border-bottom:1px solid #000; font-size:13px; padding:10px; vertical-align: top;">
                                    <h2 style="font-size:16px; margin: 0;"> Billing Address</h2>
                                    '.$_order->getBillingAddress()->format('html').'
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <table width="80%" border="0" cellspacing="0" cellpadding="0" style="margin:2px 0; border:1px solid #999; border-top:0;" >
                                        <tr>
                                            <th style="padding:8px; text-align:left; font-weight: normal; background:#cccccc; color:#000" width="200"> Description with Item ID </th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Billed Qty (Pair)</th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Size </th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Gross Amount</th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Discount </th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000;" width="100"> Selling Price </th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Vat Rate</th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000" width="80"> VAT </th>
                                            <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Net Amount (Tax Inclusive) </th>
                                        </tr>
                                        '.$itemsHtml.'
                                        <tr>
                                            <td style="text-align:left; border-bottom:1px solid #999;  padding:10px;">
                                                <p>Shipping Charges: </p>
                                            </td>
                                            <td colspan="8" style="text-align:right; border-bottom:1px solid #999; padding:10px;">
                                                <p>'.$priceSymbol.'<span style="padding:0 5px 0 20px;">'.number_format($_invoice->getShippingAmount(),2).'</span></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold; padding:8px; text-align:center; color:#000">Total : </td>
                                            <td style="font-weight:bold; padding:8px; text-align:center; color:#000"> '.array_sum($total_qty).'</td>
                                            <td style="font-weight:bold; padding:8px; text-align:left; color:#000"> &nbsp;</td>
                                            <td style="padding:8px; text-align:center; color:#000" width="100">'.$priceSymbol.'<b>'.number_format(array_sum($total_crosss_amount),2).'</b></td>
                                            <td style="padding:8px; text-align:center; color:#000" width="100">'.$priceSymbol.'<b>'.number_format(array_sum($total_discount),2).'</b></td>
                                            <td style="padding:8px; text-align:center; color:#000">'.$priceSymbol.'<b>'.number_format(array_sum($total_selling_price),2).'</b></td>
                                            <td style="font-weight:bold; padding:8px; text-align:center; color:#000"> &nbsp;</td>
                                            <td style="padding:8px; text-align:center; color:#000" width="100">'.$priceSymbol.'<b>'.number_format(array_sum($total_tax_amount),2).'</b></td>
                                            <td style="padding:8px; text-align:center; color:#000">'.$priceSymbol.'<b>'.number_format($_invoice->getGrandTotal(),2).'</b></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left; border-bottom:1px solid #999;  padding:10px;">
                                                Total Quantity :  <span style="padding:0 5px 0 20px;">'.number_format($_invoice->getTotalQty()).'</span>
                                            </td>
                                            <td colspan="8" style="text-align:right; border-bottom:1px solid #999; padding:10px;">
                                                Total Invoice Amount : '.$priceSymbol.'<span style="font-weight:bold; padding:0 5px 0 20px;">'.number_format($_invoice->getGrandTotal(),2).'</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9" style="text-align:left;  padding:0 10px;">
                                                <p> <b>Your saved Discount</b> : <span style=" padding:0 5px 0 10px;">'.number_format(array_sum($total_discount),2).'</span></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9" style="text-align:left;  padding:0 10px;">
                                                <p> <b>Store Credit</b> : <span style=" padding:0 5px 0 10px;">'.number_format($_invoice->getMwStorecredit(),2).'</span></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="9" style="text-align:left;  padding:0 10px;">
                                                <p>Total invoice value in Rupees : <span style=" padding:0 5px 0 10px;">'.$this->convert_number_to_words(substr($_invoice->getGrandTotal(), 0, -2)).' Only</span></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="padding:20px 0 0">
                                    <p style="margin:5px 0; font-weight:bold;">Declaration : </p>
                                    <ol style="margin:0; padding:0">
                                        <li style="list-style:none;">The goods are sold interacted for end user consumption/recall sale and not for resale. </li>
                                        <li style="list-style:none;">This is a computer generated invoice. No signature required.</li>
                                        <li style="list-style:none;">Subject to Bangalore and Kottayam jurisdiction only and E.&.O.E.</li>
                                    </ol>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="padding:10px 0 20px; border-bottom:1px solid #999;">
                                    <p style="margin:5px 0; font-weight:bold;">Terms and Conditions: </p>
                                    <ol style="margin:0; padding:0">
                                        <li style="list-style:none;">Do not accept damaged or open packets before signing the POD (Proof of delivery). </li>
                                        <li style="list-style:none;">Tally the contents to the packing list and if any items are missing, please contact customer support. </li>
                                        <li style="list-style:none;">Returns will be accepted only within 21 days from delivery of the order. To know how to place a return, read our Shipping and Return Policy at http://pg.saascart.com/exchange-and-returns-policy.</li>
                                    </ol>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9" style="padding:10px 0;">
                                    <p style="margin:5px 0;"><b>Registered Office Address</b> :Paragon buildings PB: No 61, Sreenivasa Iyer Road, Challukunnu,Kottayam, Kerala - 686 001.</p>
                                    <p> Contact Number : 0481- 2564788, MON - SAT, 9:30 AM - 5:30 PM </p>
                                    <p> Website : www.paragonfootwear.com / Email : consumercare@paragonfootwear.com</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
                <script>window.print();</script>
            </body>
        </html>';

        require_once Mage::getBaseDir('base').DS.'lib'.DS.'htmltopdf'.DS.'mpdf.php';

        // A4 means we use A4 Paper

        $mpdf = new mPDF('utf-8', 'A4', 0, '', 10, 10, 5, 1, 1, 1, '');
        //$mpdf->SetFont('Arial');
        //$mpdf->SetFontSize(12);
        $mpdf->WriteHTML($html);

        if (!file_exists('var/invoice')) {
            mkdir('var/invoice', 0777, true);
        }
        $mpdf->Output('var/invoice/invoice_'.$_invoice->getIncrementId().".pdf",'F');
        if( $type == 'email' )
        {
            $mpdf->Output('lib/htmltopdf/invoice.pdf');
            return true;
        }
        else
        {
            $mpdf->Output('','','_'.$_invoice->getIncrementId());
        }
    }

    public function checkInvoicedPackage($package_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('sales_flat_package_item_details', array('invoice_id', 'sum(shipping_charge) as shippingamount'))
                        ->where('package_id=?', $package_id);

        $packaged = $connection->fetchRow($sql);

        return $packaged;
    }

    public function getShipmentId($invoice_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('sales_flat_package_item_details', array('shipment_id', 'package_id'))
                        ->where('invoice_id=?', $invoice_id);

        $packaged = $connection->fetchRow($sql);

        return $packaged;
    }
}
