<?php
class Tenovia_Storelocator_IndexController extends Mage_Core_Controller_Front_Action{

	public function IndexAction(){
	    echo "Hello Controler";
	}
	public function SelectcityAction(){

		$state = $_POST['selectState'];
		$storeType = $_POST['storeType'];
		$cityResult = array();
		$collection = Mage::getModel('storelocator/storelocator')->getCollection()->addFieldToFilter('type',$storeType)->addFieldToFilter('state',$state);
		foreach ($collection as $cityCollection) {
			 $cityResult[] = $cityCollection->getCity();
		}

		echo json_encode(array_unique($cityResult));
	}

	public function SelectstoreAction(){

		$city = $_POST['selectCity'];
		$storeType = $_POST['storeType'];
		$storeResult = array();
		$collection = Mage::getModel('storelocator/storelocator')->getCollection()->addFieldToFilter('type',$storeType)->addFieldToFilter('city',$city);
		foreach ($collection as $storeCollection) {
			 $storeResult[] = $storeCollection->getLocality();
		}

		echo json_encode(array_unique($storeResult));
	}

}
?>