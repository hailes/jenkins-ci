<?php
class Tenovia_Storelocator_Adminhtml_StorelocatorbackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('pincodedelivery/pincodedeliverybackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Store Locator"));
	   $this->renderLayout();
    }
}