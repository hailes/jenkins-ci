<?php


class Tenovia_Storelocator_Block_Adminhtml_Storelocator extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_storelocator";
	$this->_blockGroup = "storelocator";
	$this->_headerText = Mage::helper("storelocator")->__("Storelocator Manager");
	$this->_addButtonLabel = Mage::helper("storelocator")->__("Add New Item");
	$this->_addButton("Import", array(
            "label" => Mage::helper("core")->__("Import"),
            "onclick" => "location.href = '" . $this->getUrl('admin_storelocator/adminhtml_storelocatorbackend') . "';",
            "class" => "btn btn-danger",
        ));
	parent::__construct();
	
	}

}