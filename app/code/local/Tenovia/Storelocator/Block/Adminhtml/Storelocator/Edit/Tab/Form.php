<?php
class Tenovia_Storelocator_Block_Adminhtml_Storelocator_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("storelocator_form", array("legend"=>Mage::helper("storelocator")->__("Item information")));

				
						// $fieldset->addField("id", "text", array(
						// "label" => Mage::helper("storelocator")->__("ID"),
						// "name" => "id",
						// ));
					
						$fieldset->addField("store_name", "text", array(
						"label" => Mage::helper("storelocator")->__("Store Name"),
						"name" => "store_name",
						));

						$fieldset->addField('type', 'select', array(
						"label" => Mage::helper("storelocator")->__("Type"),
          				'options' => array('Empty Data'=>'Please Select Store Type','Branch' => 'Branch','Dealer' => 'Dealer','Store'=>'Store','head_office'=>'Head office'),
						"name" => "type",
						));
					
						$fieldset->addField("address", "textarea", array(
						"label" => Mage::helper("storelocator")->__("Address"),
						"name" => "address",
						));

						$fieldset->addField("locality", "text", array(
						"label" => Mage::helper("storelocator")->__("Locality"),
						"name" => "locality",
						));
					
						$fieldset->addField("city", "text", array(
						"label" => Mage::helper("storelocator")->__("City"),
						"name" => "city",
						));
					
						$fieldset->addField("state", "text", array(
						"label" => Mage::helper("storelocator")->__("State"),
						"name" => "state",
						));
					
						$fieldset->addField("pin", "text", array(
						"label" => Mage::helper("storelocator")->__("Pin"),
						"name" => "pin",
						));
					
						$fieldset->addField("phone", "text", array(
						"label" => Mage::helper("storelocator")->__("Phone"),
						"name" => "phone",
						));
					
						$fieldset->addField("time", "text", array(
						"label" => Mage::helper("storelocator")->__("Time"),
						"name" => "time",
						));
					
						$fieldset->addField("lattitute", "text", array(
						"label" => Mage::helper("storelocator")->__("Lattitute"),
						"name" => "lattitute",
						));
					
						$fieldset->addField("longnitute", "text", array(
						"label" => Mage::helper("storelocator")->__("longnitute"),
						"name" => "longnitute",
						));
						
						$fieldset->addField("status", "text", array(
						"label" => Mage::helper("storelocator")->__("Status"),
						"name" => "status",
						));

				if (Mage::getSingleton("adminhtml/session")->getStorelocatorData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getStorelocatorData());
					Mage::getSingleton("adminhtml/session")->setStorelocatorData(null);
				} 
				elseif(Mage::registry("storelocator_data")) {
				    $form->setValues(Mage::registry("storelocator_data")->getData());
				}
				return parent::_prepareForm();
		}
}
