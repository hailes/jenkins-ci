<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `store_locator` (
`id` int(11) NOT NULL,
 `store_name` varchar(200) NOT NULL,
 `address` varchar(200) NOT NULL,
 `locality` varchar(200) NOT NULL,
 `city` varchar(200) NOT NULL,
 `state` varchar(200) NULL,
 `pin` varchar(200) NULL,
 `phone` varchar(50) NOT NULL,
 `time` varchar(200) NOT NULL,
 `lattitute` varchar(200) NOT NULL,
 `longnitute` varchar(200) NOT NULL,
 `type` varchar(50) NOT NULL,
 `status` smallint(6) NOT NULL COMMENT 'Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `store_locator` ADD PRIMARY KEY (`id`);
ALTER TABLE `store_locator` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();