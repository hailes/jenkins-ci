<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `tenovia_carrers` (
 `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `resume` varchar(200) NOT NULL,
  `yr_exp` varchar(200) NOT NULL,
  `qualification` varchar(200) NULL,
  `languages` varchar(200) NULL,
  `city` varchar(200) NULL,    
  `state` varchar(200) NULL,  
  `country` varchar(200) NULL,  
  `covering_letter` varchar(600) NULL,
  `mobile` varchar(100) NULL,
  `dob` datetime DEFAULT NULL,
  `marital_status` varchar(200) NULL,
  `area_of_experience` varchar(200) NULL,
  `current_salary` varchar(200) NOT NULL,
  `expected_salary` smallint(6) NOT NULL COMMENT 'Status',
  `extra` varchar(200) NOT NULL,
  `comment` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `tenovia_carrers` ADD PRIMARY KEY (`id`);
ALTER TABLE `tenovia_carrers` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
SQLTEXT;
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 