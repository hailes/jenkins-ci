<?php
class Tenovia_Carrers_IndexController extends Mage_Core_Controller_Front_Action{
	public function saveCarrierAction(){
		//$this->getRequest->getParam('name');
		$name = $_POST['name'];
		$email = $_POST['email'];
		$covering_letter = $_POST['covering_letter'];
		$mobile = $_POST['contact_number'];
		$dob = $_POST['dob'];
		$subject = $_POST['subject'];
		$yr_exp = $_POST['yr_exp'];
		$marital_status = $_POST['marital_status'];
		$area_of_experience = $_POST['area_of_experience'];
		$qualification = $_POST['qualification'];
		$current_salary = $_POST['current_salary'];
		$languages = $_POST['languages'];
		$expected_salary = $_POST['expected_salary'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$country = $_POST['country'];
		if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '')
		{
			try
			{
				$path = Mage::getBaseDir().DS.'media/careers'.DS;  //desitnation directory
					//echo $path;

				$file = $_FILES['filename']['name'];
                                $ext = pathinfo($file, PATHINFO_EXTENSION);
                                $timestamp = time();
                                $resume = $mobile.'_'.str_replace(' ','_',$name).'_'.$timestamp.'.'.$ext;//echo $resume;
				//$file=str_replace(' ','_',$file); //file name
				//$resume = $mobile.$file;
				$target_path = $path.$resume;
				chmod($target_path, 777);
				$uploader = new Varien_File_Uploader('filename'); //load class
				$uploader->setAllowedExtensions(array('doc','pdf','txt','docx','png')); //Allowed extension for file
				$uploader->setAllowCreateFolders(true); //for creating the directory if not exists
				$uploader->setAllowRenameFiles(false); //if true, uploaded file's name will be changed, if file with the same name already exists directory.
				$uploader->setFilesDispersion(false);
				$uploader->save($path,$resume); //save the file on the specified path
			}
			catch (Exception $e)
			{
				echo 'Error Message: '.$e->getMessage();
			}
		}



		//Save Data in Database
		if($name){
		$collection = Mage::getModel('carrers/carrers');
        $collection->setData('name',$name);
        $collection->setData('email',$email);
        $collection->setData('resume',$resume);
        $collection->setData('yr_exp',$yr_exp);
        $collection->setData('qualification',$qualification);
        $collection->setData('languages',$languages);
        $collection->setData('city', $city);
        $collection->setData('state',$state);
        $collection->setData('country',$country);
        $collection->setData('covering_letter',$covering_letter);
        $collection->setData('mobile',$mobile);
        $collection->setData('dob',$dob);
        $collection->setData('marital_status',$marital_status);
        $collection->setData('area_of_experience',$area_of_experience);
        $collection->setData('current_salary',$current_salary);
        $collection->setData('expected_salary',$expected_salary);
        $collection->save();
		$this->_redirectReferer();
        Mage::getSingleton('core/session')->addSuccess('Your form has been submitted');

		// Send email for notification
		$emailTemplate = Mage::getModel('core/email_template')->loadByCode('careers');

		//Getting the Store E-Mail Sender Name.
		$senderName = Mage::getStoreConfig('trans_email/ident_general/name');

		//Getting the Store General E-Mail.
		$senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

		if ($resume != null || $resume != "") {
			$resumeFile = "http://www.paragonfootwear.com/media/careers/".$resume;
		}else{
			$resumeFile = "No Resume Available";
		}
		$subjectM = 'career Resume';
		$emailTemplateVariables = array(
			'name'=>$name,
	        'email'=>$email,
	        'resume'=>$resumeFile,
	        'yr_exp'=>$yr_exp,
	        'qualification'=>$qualification,
	        'languages'=>$languages,
	        'city'=>$city,
	        'state'=>$state,
	        'country'=>$country,
	        'covering_letter'=>$covering_letter,
	        'mobile'=>$mobile,
	        'dob'=>$dob,
	        'marital_status'=>$marital_status,
	        'area_of_experience'=>$area_of_experience,
	        'current_salary'=>$current_salary,
	        'expected_salary'=>$expected_salary
		);

		$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);

		$emailTemplate->setSenderName($senderName);
		$emailTemplate->setSenderEmail($senderEmail);
		$emailTemplate->setTemplateSubject($subjectM);

		$emailTemplate->send('jobs@paragonfootwear.com', $name, $emailTemplateVariables);

		}
		//$this->_redirect("careers");
	}
	public function saveFormAction(){
		//$this->getRequest->getParam('name');
		//echo "<pre>"; print_r($_POST); exit;
		 include_once('src/autoload.php');
		  // Register API keys at https://www.google.com/recaptcha/admin
        $siteKey = '6LeQPigUAAAAANapCZLQAUdMseHhXyXdvVYjcMIG';
        $secret = '6LeQPigUAAAAAH0yWP0U2KpIk0YVNU39V4JLdnvR';
        //$siteKey   = '6Lc7nzQUAAAAAEibNwD-oU_bexaz8dIWyt6mIUCk';
        //$secret    = '6Lc7nzQUAAAAAJud9an7hUzM6AXG7i5Ot7W-X1Xb';
         $recaptcha = new \ReCaptcha\ReCaptcha($secret);
        $resp      = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        if ($resp->isSuccess()) {
		$name = $_POST['name'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$pincode = $_POST['pincode'];
		$telephone = $_POST['telephone'];
		$email = $_POST['email'];



		//Save Data in Database
		if($name){
		$collection = Mage::getModel('importingdistribution/importingdistribution');
        $collection->setData('applicant_name',$name);
        $collection->setData('applicant_email',$email);
        $collection->setData('applicant_city',$city);
        $collection->setData('applicant_country',$country);
        $collection->setData('applicant_pincode',$pincode);
       	$collection->setData('applicant_contactno',$telephone);
				$collection->setData('qty',$_POST['qty']);
				$collection->setData('deliver',$_POST['deliver']);
				$collection->setData('designation', $_POST['designation']);
				$collection->setData('distributer', $_POST['distributer']);
				$collection->setData('pricing',$_POST['pricing']);
				$collection->setData('other_enquiry_details',$_POST['other_enquiry_details']);
				$collection->setData('address',$_POST['address']);
				$collection->setData('company_name',$_POST['company_name']);
				$collection->setData('port_of_destination',$_POST['port_of_destination']);
        $collection->save();
		//$this->_redirectReferer();
      //  Mage::getSingleton('core/session')->addSuccess('Your form has been submitted');

		// Send email for notification
		$emailTemplate = Mage::getModel('core/email_template')->loadByCode('importing');

		//Getting the Store E-Mail Sender Name.
		$senderName = Mage::getStoreConfig('trans_email/ident_general/name');

		//Getting the Store General E-Mail.
		$senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

		$subjectM = 'Importing & Distribution';
		$emailTemplateVariables = array(
				'name' => $name,
				'applicantemail' => $email,
				'company_name'=>$_POST['company_name'],
				'designation'=> $_POST['designation'],
				'distributer'=>$_POST['distributer'],
				'pricing_quotation'=>$_POST['pricing'],
				'deliver_by'=>$_POST['deliver'],
				'address'=>$_POST['address'],
				'additional_information'=>$_POST['other_enquiry_details'],
				'pincode' => $pincode,
				'telephone' => $telephone,
				'city' => $city,
				'country' => $country,
		);
		$toAddress = '';
		if($country=="India"){
		$toAddress =	array("esupport@paragonfootwear.com","export1@paragonfootwear.com","nthomas@paragonfootwear.com","export3@paragonfootwear.com","export@paragonfootwear.com","export2@paragonfootwear.com","sidharth.vijoo@paragonfootwear.com");
		}else{
		$toAddress =	array("esupport@paragonfootwear.com","export1@paragonfootwear.com","nthomas@paragonfootwear.com","export3@paragonfootwear.com","export@paragonfootwear.com","export2@paragonfootwear.com","sidharth.vijoo@paragonfootwear.com");
		}

		// $toAddress =	array("sonu@tenovia.com","exports@paragonfootwear.com","esupport@paragonfootwear.com","export1@paragonfootwear.com","nthomas@paragonfootwear.com","export3@paragonfootwear.com","export@paragonfootwear.com","export2@paragonfootwear.com");
		// }else{
		// $toAddress =	array("sonu@tenovia.com","exports@paragonfootwear.com","export1@paragonfootwear.com","ravi@tenovia.com","nthomas@paragonfootwear.com","export3@paragonfootwear.com","export@paragonfootwear.com","export2@paragonfootwear.com");
		// }

		$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);

		$emailTemplate->setSenderName($senderName);
		$emailTemplate->setSenderEmail($senderEmail);
		$emailTemplate->setTemplateSubject($subjectM);
		$emailTemplate->send($toAddress, $name, $emailTemplateVariables);


		//$this->_redirect("careers");
	}
	$suburbResultArray = array('status'=> 1,'message'=>'Your form has been submitted');
}
else
{
	$suburbResultArray = array(
                'status' => 2,
                'message' => 'There is a problem with Security Image'
            );
}
return	$this->getResponse()->setBody(json_encode($suburbResultArray));
}
public function saveStoryAction(){
		//$this->getRequest->getParam('name');
		//echo "<pre>"; print_r($_POST); exit;
		$name = $_POST['name'];
		$story = $_POST['story'];
		$email = $_POST['email'];
		$telephone = $_POST['telephone'];



		//Save Data in Database
		if(!empty($name) && !empty($story) && !empty($email) && !empty($telephone)){
		$collection = Mage::getModel('paragonstory/paragonstory');
        $collection->setData('name',$name);
        $collection->setData('story',$story);
        $collection->setData('email',$email);
        $collection->setData('mobile',$telephone);
        $collection->save();
		$this->_redirectReferer();
        Mage::getSingleton('core/session')->addSuccess('Your form has been submitted');
	}else{
		$this->_redirectReferer();
        Mage::getSingleton('core/session')->addError('Please fill all the details.');
	}
}
public function saveCouponAction(){
		$name = $_POST['name'];
		$email = $_POST['email'];
		$telephone = $_POST['telephone'];

		if(!empty($email)){
			$formdata = Mage::getModel("lottery/lottery")->getCollection()->addFieldToFilter('email',$email);

			if($formdata->count()!=0){
				$result = 'Already Registered.';
				return $this->getResponse()->setBody($result);
			}
		}
		//Save Data in Database
		if(!empty($name)){

			Mage::getModel("lottery/lottery")
			->setName($name)
			->setEmail($email)
			->setMobile($telephone)
			->save();

		$customerGroupIds = Mage::getModel('customer/group')->getCollection()->getAllIds();
                $generatorCode = Mage::getModel('salesrule/coupon_codegenerator')->setLength(8)->generateCode();
                $codeuppercase=strtoupper($generatorCode);
                 $DiscountType = Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION;
                 $disval=rand(5,20);
				  $days='111111';
					$action =	 array(
						"type"          => "salesrule/rule_condition_product_combine",
						"aggregator"    => "all",
						"value"         => "0",
						"attribute" =>'',
						"operator"=>'',
						"is_value_processed"=>'',
						"conditions" => array(
								array(
								"type"          => "salesrule/rule_condition_product",
								"attribute"     => "special_price",
								"operator"      => ">=",
								"value"         => "1",
								"is_value_processed" =>'',
							)
						)

					);
                //echo $generatorCode; exit;
                // SalesRule Rule model
                $rule = Mage::getModel('salesrule/rule');
                // Rule data
                $rule->setName('New Registration-'.$codeuppercase)
                ->setDescription('New Registration')
                ->setFromDate('')
                ->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
                ->setCouponCode(strtoupper($generatorCode))
                ->setUsesPerCustomer(1)
                ->setUsesPerCoupon(1)
                ->setCustomerGroupIds($customerGroupIds)
                ->setIsActive(1)
                ->setConditionsSerialized('')
                ->setActionsSerialized(serialize($action))
                ->setStopRulesProcessing(0)
                ->setIsAdvanced(1)
                ->setProductIds('')
                ->setSortOrder(0)
                ->setSimpleAction($DiscountType)
                ->setDiscountAmount($disval)
                ->setDiscountStep(0)
                ->setSimpleFreeShipping('0')
                ->setApplyToShipping('0')
                ->setIsRss(0)
                ->setWebsiteIds(array(1))
                ->setFromDate(date('Y-m-d'))
                ->setToDate(date('Y-m-d', strtotime('+'.$days.' days')))
                ->setStoreLabels(array('New Registration'));
				$rule->save();

		 $data = array('disval' => $disval,'couponcode' => $codeuppercase);
            $result= json_encode($data);
	//	"You have Received -".$disval.'% '.'Coupon Code - '.$codeuppercase;
	}else{

		$result = 'Please fill all the details.';

	}
	return $this->getResponse()->setBody($result);
}
}
//}
