<?php
class Tenovia_Carrers_Block_Adminhtml_Carrers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("carrers_form", array("legend"=>Mage::helper("carrers")->__("Item information")));

				
						/* $fieldset->addField("id", "text", array(
						"label" => Mage::helper("carrers")->__("ID"),
						"name" => "id",
						)); */
					
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("carrers")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("carrers")->__("Email"),
						"name" => "email",
						));
					
						$fieldset->addField("covering_letter", "textarea", array(
						"label" => Mage::helper("carrers")->__("Covering Letter"),
						"name" => "covering_letter",
						));
					
						$fieldset->addField("mobile", "text", array(
						"label" => Mage::helper("carrers")->__("Mobile"),
						"name" => "mobile",
						));
					
						$fieldset->addField('resume', 'link', array(
				          'label'     => Mage::helper('carrers')->__('Download Resume'),
				          'style'   => '',
				          'href' => Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_WEB, true ).'media/careers/'.Mage::registry("carrers_data")->getResume(),
				          'value'  => '',
				          'after_element_html' => ''
				        ));

					
						$fieldset->addField("yr_exp", "text", array(
						"label" => Mage::helper("carrers")->__("Years Of Experience"),
						"name" => "yr_exp",
						));
					
						$fieldset->addField("marital_status", "text", array(
						"label" => Mage::helper("carrers")->__("Marital Status"),
						"name" => "marital_status",
						));
					
						// $fieldset->addField("area_of_experience", "text", array(
						// "label" => Mage::helper("carrers")->__("Area of Experience"),
						// "name" => "area_of_experience",
						// ));
						$fieldset->addField('area_of_experience', 'select', array(
						'label'     => Mage::helper('carrers')->__('Area of Experience'),
						'values'   => Tenovia_Carrers_Block_Adminhtml_Carrers_Grid::getOptionArray3(),
						'name' => 'area_of_experience',
						));	
					
						$fieldset->addField("qualification", "text", array(
						"label" => Mage::helper("carrers")->__("Qualification"),
						"name" => "qualification",
						));
					
						$fieldset->addField("current_salary", "text", array(
						"label" => Mage::helper("carrers")->__("Current Salary"),
						"name" => "current_salary",
						));
					
						$fieldset->addField("languages", "text", array(
						"label" => Mage::helper("carrers")->__("Languages Known"),
						"name" => "languages",
						));
					
						$fieldset->addField("expected_salary", "text", array(
						"label" => Mage::helper("carrers")->__("Expected Salary"),
						"name" => "expected_salary",
						));
					
						$fieldset->addField("city", "text", array(
						"label" => Mage::helper("carrers")->__("City"),
						"name" => "city",
						));
					
						$fieldset->addField("state", "text", array(
						"label" => Mage::helper("carrers")->__("State"),
						"name" => "state",
						));
					
						$fieldset->addField("country", "text", array(
						"label" => Mage::helper("carrers")->__("Country"),
						"name" => "country",
						));
					
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('dob', 'date', array(
						'label'        => Mage::helper('carrers')->__('Date of Birth'),
						'name'         => 'dob',
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));

				if (Mage::getSingleton("adminhtml/session")->getCarrersData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getCarrersData());
					Mage::getSingleton("adminhtml/session")->setCarrersData(null);
				} 
				elseif(Mage::registry("carrers_data")) {
				    $form->setValues(Mage::registry("carrers_data")->getData());
				}
				return parent::_prepareForm();
		}
}
