<?php

class Tenovia_Carrers_Block_Adminhtml_Carrers_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("carrersGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("carrers/carrers")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				// $this->addColumn("id", array(
				// "header" => Mage::helper("carrers")->__("ID"),
				// "align" =>"right",
				// "width" => "50px",
			 //    "type" => "number",
				// "index" => "id",
				// ));
                $this->addColumn("created_at", array(
				"header" => Mage::helper("carrers")->__("Created at"),
				"index" => "created_at",
				));
				$this->addColumn("name", array(
				"header" => Mage::helper("carrers")->__("Name"),
				"index" => "name",
				));				
				$this->addColumn("email", array(
				"header" => Mage::helper("carrers")->__("Email"),
				"index" => "email",
				));
				// $this->addColumn("covering_letter", array(
				// "header" => Mage::helper("carrers")->__("Covering Letter"),
				// "index" => "covering_letter",
				// ));
				$this->addColumn("mobile", array(
				"header" => Mage::helper("carrers")->__("Mobile"),
				"index" => "mobile",
				));
				$this->addColumn("resume", array(
				"header" => Mage::helper("carrers")->__("Resume"),
				"index" => "resume",
				"renderer"  => "Tenovia_Carrers_Block_Adminhtml_Carrers_Renderer_Link",
				 "sortable"  => "false",
				
				));
				$this->addColumn("yr_exp", array(
				"header" => Mage::helper("carrers")->__("Years Of Experience"),
				"index" => "yr_exp",
				));
				$this->addColumn("marital_status", array(
				"header" => Mage::helper("carrers")->__("Marital Status"),
				"index" => "marital_status",
				));
				// $this->addColumn("area_of_experience", array(
				// "header" => Mage::helper("carrers")->__("Area of Experience"),
				// "index" => "area_of_experience",
				// ));

				$this->addColumn('area_of_experience', array(
						'header' => Mage::helper('carrers')->__('Area of Experience'),
						'index' => 'area_of_experience',
						'type' => 'options',
						'options'=>Tenovia_Carrers_Block_Adminhtml_Carrers_Grid::getOptionArray3(),				
						));


				$this->addColumn("qualification", array(
				"header" => Mage::helper("carrers")->__("Qualification"),
				"index" => "qualification",
				));
				$this->addColumn("current_salary", array(
				"header" => Mage::helper("carrers")->__("Current Salary"),
				"index" => "current_salary",
				));
				$this->addColumn("languages", array(
				"header" => Mage::helper("carrers")->__("Languages Known"),
				"index" => "languages",
				));
				$this->addColumn("expected_salary", array(
				"header" => Mage::helper("carrers")->__("Expected Salary"),
				"index" => "expected_salary",
				));
				$this->addColumn("city", array(
				"header" => Mage::helper("carrers")->__("City"),
				"index" => "city",
				));
				$this->addColumn("state", array(
				"header" => Mage::helper("carrers")->__("State"),
				"index" => "state",
				));
				$this->addColumn("country", array(
				"header" => Mage::helper("carrers")->__("Country"),
				"index" => "country",
				));
					$this->addColumn('dob', array(
						'header'    => Mage::helper('carrers')->__('Date of Birth'),
						'index'     => 'dob',
						'type'      => 'datetime',
					));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_carrers', array(
					 'label'=> Mage::helper('carrers')->__('Remove Careers'),
					 'url'  => $this->getUrl('*/adminhtml_carrers/massRemove'),
					 'confirm' => Mage::helper('carrers')->__('Are you sure?')
				));
			return $this;
		}
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[Production]='Production';
			$data_array[Human_Resourse]='Human Resourse';
			$data_array[Marketing]='Marketing';
			$data_array[Sales]='Sales';
			$data_array[Footwear_Sales]='Footwear Sales';
			$data_array[Finance]='Finance';
			$data_array[Accounting]='Accounting';
			$data_array[Footwear_Designing]='Footwear Designing';
			$data_array[Fresher]='Fresher';
			$data_array[Other]='Other';
            return($data_array);
		}


		  
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
			

}