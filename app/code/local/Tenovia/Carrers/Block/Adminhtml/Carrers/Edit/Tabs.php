<?php
class Tenovia_Carrers_Block_Adminhtml_Carrers_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("carrers_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("carrers")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("carrers")->__("Item Information"),
				"title" => Mage::helper("carrers")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("carrers/adminhtml_carrers_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
