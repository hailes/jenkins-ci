<?php


class Tenovia_Carrers_Block_Adminhtml_Carrers extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_carrers";
	$this->_blockGroup = "carrers";
	$this->_headerText = Mage::helper("carrers")->__("Careers Manager");
	$this->_addButtonLabel = Mage::helper("carrers")->__("Add New Item");
	parent::__construct();
	
	}

}