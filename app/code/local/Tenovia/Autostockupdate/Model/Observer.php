<?php
class Tenovia_Autostockupdate_Model_Observer extends Varien_Event_Observer{
public function product_save_after(Varien_Event_Observer $observer) {
    $product = $observer->getProduct();
    $stockData = $product->getStockData();

    if ( $product && $stockData['qty'] ) {
        
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getEntityId()); // Load the stock for this product use for 
        $stock->setData('is_in_stock', 1); // Set the Product to InStock                               
        $stock->save(); // Save use for 
    }
}
 }