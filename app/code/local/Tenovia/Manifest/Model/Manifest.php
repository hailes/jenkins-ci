<?php

class Tenovia_Manifest_Model_Manifest extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('manifest/manifest');
    }
}
