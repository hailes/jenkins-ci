<?php
    class Tenovia_Manifest_Model_Mysql4_Manifest_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
    {
        public function _construct()
        {
            $this->_init('manifest/manifest');
        }
    }
