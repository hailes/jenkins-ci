<?php

class Tenovia_Manifest_Model_Mysql4_Manifest extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('manifest/manifest', 'id');
    }
}
