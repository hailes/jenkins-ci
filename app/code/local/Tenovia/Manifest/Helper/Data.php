<?php

class Tenovia_Manifest_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getServiceproviders()
    {
        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $options = array();

        foreach ($methods as $_code => $_method) {
            if (!$_title = Mage::getStoreConfig("carriers/$_code/title")) {
                $_title = $_code;
            }
            if(Mage::app()->getStore()->isAdmin() &&  Mage::getStoreConfig("carriers/$_code/adminshipping")) {
                $options[$_code] = $_title;
            }
            /*elseif ($_code === 'pickupstore' && empty($pickupstores) === false) {
                $options[$_code] = $_title;
            }*/ 
        }

        return $options;
    }

    public function afterManifestProcess($shipids)
    {
        $shipids = explode('||', $shipids);

        foreach ($shipids as $shippingid) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select = $connection->select()
                        ->from('sales_flat_shipment_track', array('order_id', 'feetscience_package_id'))
                        ->where('parent_id=?', $shippingid);

            $data = $connection->fetchRow($select);
                     /* @var $shipment Mage_Sales_Model_Order_Shipment */
            $shipment = Mage::getModel('sales/order_shipment')->load($shippingid);
            $invoice_id = Mage::Helper('partialinvoice')->checkInvoicedPackage($data['feetscience_package_id']);

            $mailTemplate = Mage::getModel('core/email_template');
            $comment = '';
            try {
                $shipment->sendEmail(true, $comment, $invoice_id['invoice_id'])
                    ->setEmailSent(true)
                    ->save();
                // $order = Mage::getModel('sales/order')->load($data['order_id']);
                // Mage::dispatchEvent('sales_order_save_after', array('order' => $order));

                $historyItem = Mage::getResourceModel('sales/order_status_history_collection')
                    ->getUnnotifiedForInstance($shipment, Mage_Sales_Model_Order_Shipment::HISTORY_ENTITY_NAME);
                if ($historyItem) {
                    $historyItem->setIsCustomerNotified(1);
                    $historyItem->save();
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('data_invalid', $e->getMessage());
            }
        }
    }
}
