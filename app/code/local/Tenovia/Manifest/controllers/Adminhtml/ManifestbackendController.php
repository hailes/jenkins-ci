<?php

class Tenovia_Manifest_Adminhtml_ManifestbackendController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title($this->__('Manifest Creation'));
        $this->renderLayout();
    }

    protected function _isAllowed()
    {
        if (Mage::getSingleton('admin/session')->isAllowed('admin/manifest/manifestbackend')) {
            return true;
        }

        return false;
    }
}
