<?php

class Tenovia_Manifest_IndexController extends Mage_Core_Controller_Front_Action
{

    protected function _isAllowed()
    {
        if (Mage::getSingleton('admin/session')->isAllowed('admin/manifest/manifests')) {
            return true;
        }

        return false;
    }
    public function getpackagesAction()
    {
        $barcode = $this->getRequest()->getPost('airwaybarcode');
        $provider = $this->getRequest()->getPost('provider');

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $connection->select()
                        ->from('sales_flat_shipment_track', array('*'))
                        ->where('carrier_code=?', $provider)
                        ->where('airway_bill_barcode=?', $barcode)
                        ->where('manifest_id IS NULL');

        $data = $connection->fetchRow($select);

        if (isset($data['order_id']) === false) {
            echo json_encode(
                            array(
                                'success' => '0',
                            )
                        );
        } else {
            if (count($data) == 0) {
                echo json_encode(
                            array(
                                'success' => '0',
                            )
                        );
            } else {
                $sales_order_id = $data['order_id'];

                $select1 = $connection->select()
                   ->from(array('t1' => 'sales_flat_shipment_grid'), array('total_qty', 'increment_id', 'order_increment_id', 'entity_id'))
                   ->join(array('t2' => 'sales_flat_shipment'), 't2.order_id = t1.order_id', array('shipping_address_id', 'order_id'))
                   ->where('t1.entity_id=?', $data['parent_id'])
                   ->group('t1.entity_id');

                $shipdata = $connection->fetchRow($select1);

                $address = Mage::getModel('sales/order_address')->load($shipdata['shipping_address_id']);
                $custName = $address->getName();
                $custAddr = $address->getStreetFull();
                $region = $address->getRegion();
                $country = $address->getCountry();

                $ship_addrees = $custName.'<br>'.$custAddr.'<br>'.$region.'<br>'.$country;

                $order = Mage::getModel('sales/order')->load($shipdata['order_id']);
                $orderdata = $order->getData();
                $amount = $orderdata['base_total_paid'];
                $payment_method = $order->getPayment()->getMethodInstance()->getTitle();

                $itemselect = $connection->select()
                            ->from('sales_flat_shipment_item', array('product_id', 'qty', 'sku', 'name'))
                            ->where('parent_id=?', $data['parent_id']);

                $productdatas = $connection->fetchAll($itemselect);

                $prodct_name = '';

                foreach ($productdatas as $data1) {
                    $product = Mage::getModel('catalog/product')->load($data1['product_id'], array('size'));

                    $prodct_name .=    $data1['name'].' X '.round($data1['qty']).' ('.$data1['sku'].')'.'<br/>';
                }

                echo json_encode(
                            array(
                                'success' => '1',
                                'ship_id' => $shipdata['entity_id'],
                                'tracking_number' => $data['track_number'],
                                'order_number' => $shipdata['order_increment_id'],
                                'shipment_number' => $shipdata['increment_id'],
                                'qty' => round($shipdata['total_qty']),
                                'shipping_address' => $ship_addrees,
                                'amount' => $amount,
                                'payment_method' => $payment_method,
                                'product' => $prodct_name,
                            )
                        );
            }
        }
    }

    public function savemanifestAction()
    {
        $shipids = $this->getRequest()->getPost('shipids');
        $manifestname = $this->getRequest()->getPost('name');
        $pickdate = $this->getRequest()->getPost('pickdate');
        $provider = $this->getRequest()->getPost('provider');
        $currentdate = date('Y-m-d h:i:s');
        $username = $this->getRequest()->getPost('admin-uname');
        $createdby = $this->getRequest()->getPost('admin-name');

        $manifestname1 = $provider;
        $manifestname2 = date('dMY');
        $manifestname3 = date('his');
        if ($manifestname == '') {
            $name = $manifestname1.'-'.$manifestname2.'-'.$manifestname3;
        } else {
            $name = $manifestname.'-'.$manifestname1.'-'.$manifestname2.'-'.$manifestname3;
        }

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $insertstatus = $write->query("insert into feetscience_manifest(name,pickup_date,service_provider,created_by,ship_id,created_at) values('$name','$pickdate','$provider','$createdby','$shipids','$currentdate')");
        $manifest_id = $write->lastInsertId();

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $shipid = explode('||', $shipids);
        $sap_id = array();

        for ($i = 0;$i < count($shipid);++$i) {
            $insertstatus2 = $write->query("UPDATE `sales_flat_shipment_track` SET manifest_id='$manifest_id' WHERE parent_id='$shipid[$i]'");

            $insertstatus2 = $write->query("UPDATE `sales_flat_package_item_details` SET manifest_id='$manifest_id' WHERE shipment_id='$shipid[$i]'");

            $select = $connection->select()
                                ->from('sales_flat_shipment', array('order_id'))
                                ->where('entity_id=?', $shipid[$i]);
            $datas = $connection->fetchRow($select);

            $order = Mage::getModel('sales/order')->load($datas['order_id']);

            /* Order Manifested qty check**/
            $manifest_qty = Mage::Helper('packageprocess')->checkShippetdQty($datas['order_id']);
            if ($manifest_qty == $order->getTotalQtyOrdered()) {
                $state = 'order_shipped';
            } else {
                $state = 'partially_shipped';
            }

            $select = $connection->select()
                        ->from('sales_flat_shipment_track', array('order_id', 'feetscience_package_id'))
                        ->where('parent_id=?', $shipid[$i]);

            $track = $connection->fetchRow($select);
            
            Mage::getSingleton('core/session')->setPackageId($track['feetscience_package_id']);

            $order->setStatus($state);
            $order->addStatusToHistory($state, 'Order Shipped', true);
            $order->save();
        }
        try{
            $helper_data = Mage::helper('manifest');
            $helper_data->afterManifestProcess($shipids);
            Mage::helper("orderpush/tally")->pushOrderToTally($manifest_id);
        }
        catch(\Exception $e) {
            Mage::log($e->getMessage().'=>'.$e);
            Mage::logException($e);
            $mail = new Zend_Mail('utf-8');
            
            $recipients = array(
                'jayachandran@tenovia.com',
                'muthu@tenovia.com'
            );
            $mailBody   = $manifest_id .",<br>createdBy" .$username. ',<br>Error:'. $e->getMessage();
            
            $mail->setBodyHtml($mailBody)->setSubject('Manifest creation error')->addTo($recipients)->setFrom('feetscience@feetscience.in', "Admin");
            
            try {
                $mail->send();
            }
            catch (Exception $e) {
                Mage::logException($e);
            }
        }

        echo 'success';
    }
}
