<?php
class Tenovia_Subscribe_IndexController extends Mage_Core_Controller_Front_Action{

    public function IndexAction() {
       $data = $this->getRequest()->getParams('email');
       if($data){
         $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getWebsite()->getId())->loadByEmail($data['email']);
         $result = Array();
         if($customer->getId()){
           $result['status'] = false;
           $result['isnew'] = false;
           $result['message'] = '<h5>Email address entered by you is already a registered user<br/> with us, this offer is valid only for new users</h5>';
         }else{
           $subscripbr = Mage::getModel('subscribe/subscribe')->getCollection()->addFieldToFilter('email',$data['email']);
           if(count($subscripbr)!=0){
             $existUser = $subscripbr->getData();
             $emailTemplate = Mage::getModel('core/email_template')->loadDefault('subscribe_email_template');
             //Getting the Store E-Mail Sender Name.
             $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
             //Getting the Store General E-Mail.
             $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
             //Variables for Confirmation Mail.
             $emailTemplateVariables = array();
             $emailTemplateVariables['couponcode'] = $existUser[0]['coupon'];
             //Appending the Custom Variables to Template.
             $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
             //Sending E-Mail to Customers.
             $mail = Mage::getModel('core/email')
              ->setToName('Hi')
              ->setToEmail($existUser[0]['email'])
              ->setBody($processedTemplate)
              ->setSubject('Register Coupon')
              ->setFromEmail($senderEmail)
              ->setFromName($senderName)
              ->setType('html');
              try{
              $mail->send();
              }
              catch(Exception $error)
              {
                $result['status'] = false;
              }
             $result['status'] = true;
             $result['isnew'] = false;
             $result['message'] = '<h5>We have already sent you the 10% discount coupon code to this email address,<br/> the same code has been forwarded to your again</h5>';
           }else{
              $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($data['email']);
              if (!$subscriber->getId() || $subscriber->getStatus() == Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED || $subscriber->getStatus() == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE)
              {
                 $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
                 $subscriber->setSubscriberEmail($data['email']);
                 $subscriber->setSubscriberConfirmCode($subscriber->RandomSequence());
                 Mage::getModel('newsletter/subscriber')->subscribe($data['email']);
             }

            /**
            * Coupon code
            */
            //$customerGroupIds = Mage::getModel('customer/group')->getCollection()->getAllIds();
            $generatorCode = Mage::getModel('salesrule/coupon_codegenerator')->setLength(8)->generateCode();
            $codeuppercase=strtoupper($generatorCode);
            $DiscountType = Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION;
            $disval=rand(5,20);
            $days=15;
            $data['coupon'] = strtoupper($generatorCode);
            Mage::getModel('subscribe/subscribe')->setData($data)->save();
            //echo $generatorCode; exit;
            // SalesRule Rule model
            $rule = Mage::getModel('salesrule/rule');
            // Rule data
            $rule->setName('New Subscribe & Registration 10%')
            ->setDescription('New Subscribe & Registration 10%')
            ->setFromDate('')
            ->setCouponType(Mage_SalesRule_Model_Rule::COUPON_TYPE_SPECIFIC)
            ->setCouponCode(strtoupper($generatorCode))
            ->setUsesPerCustomer(1)
            ->setUsesPerCoupon(1)
            ->setCustomerGroupIds("0,1,2,3")
            ->setIsActive(1)
            ->setConditionsSerialized(serialize(array (
                        'type' => 'salesrule/rule_condition_combine',
                        'attribute' => NULL,
                        'operator' => NULL,
                        'value' => '1',
                        'is_value_processed' => NULL,
                        'aggregator' => 'all',
                        'conditions' =>
                        array (
                          0 =>
                          array (
                            'type' => 'salesrule/rule_condition_address',
                            'attribute' => 'base_subtotal',
                            'operator' => '>',
                            'value' => '500',
                            'is_value_processed' => false,
                          ),
                          1 =>
                          array (
                            'type' => 'salesrule/rule_condition_combine',
                            'attribute' => NULL,
                            'operator' => NULL,
                            'value' => '0',
                            'is_value_processed' => NULL,
                            'aggregator' => 'all',
                            'conditions' =>
                            array (
                              0 =>
                              array (
                                'type' => 'salesrule/rule_condition_product_found',
                                'attribute' => NULL,
                                'operator' => NULL,
                                'value' => '1',
                                'is_value_processed' => NULL,
                                'aggregator' => 'all',
                                'conditions' =>
                                array (
                                  0 =>
                                  array (
                                    'type' => 'salesrule/rule_condition_product',
                                    'attribute' => 'special_price',
                                    'operator' => '>',
                                    'value' => 1,
                                    'is_value_processed' => false,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )))
            ->setActionsSerialized()
            ->setStopRulesProcessing(0)
            ->setIsAdvanced(1)
            ->setProductIds('')
            ->setSortOrder(0)
            ->setSimpleAction($DiscountType)
            ->setDiscountAmount(10)
            ->setDiscountStep(0)
            ->setSimpleFreeShipping('0')
            ->setApplyToShipping('0')
            ->setIsRss(0)
            ->setWebsiteIds(array(1))
            ->setFromDate()
            ->setToDate()
            ->setStoreLabels(array('New Subscribe & Registration 10%'));
            $rule->save();
             $emailTemplate = Mage::getModel('core/email_template')->loadDefault('subscribe_email_template');
             //Getting the Store E-Mail Sender Name.
             $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
             //Getting the Store General E-Mail.
             $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
             //Variables for Confirmation Mail.
             $emailTemplateVariables = array();
             $emailTemplateVariables['couponcode'] = strtoupper($generatorCode);
             //Appending the Custom Variables to Template.
             $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
             //Sending E-Mail to Customers.
             $mail = Mage::getModel('core/email')
              ->setToName('Hi')
              ->setToEmail($data['email'])
              ->setBody($processedTemplate)
              ->setSubject('Register Coupon')
              ->setFromEmail($senderEmail)
              ->setFromName($senderName)
              ->setType('html');
              try{
              $mail->send();
              }
              catch(Exception $error)
              {
                $result['status'] = true;
              }
             $result['status'] = true;
             $result['isnew'] = true;
             $result['message']="Congratulations! Your email address has been registered with us and your 10% discount code for your first purchase has been emailed to your address";
           }
         }
       }else{
         $result['status'] = false;
       }
      return $this->getResponse()->setBody(json_encode($result));
    }
}
