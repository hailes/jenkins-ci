<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `subscribe_user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `gender` int(11) NOT NULL,
  `created_at` datetime NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `subscribe_user`
  ADD PRIMARY KEY (`id`);

  ALTER TABLE `subscribe_user`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

SQLTEXT;

$installer->run($sql);
//demo
//Mage::getModel('core/url_rewrite')->setId(null);
//demo
$installer->endSetup();
