<?php


class Tenovia_Subscribe_Block_Adminhtml_Subscribe extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_subscribe";
	$this->_blockGroup = "subscribe";
	$this->_headerText = Mage::helper("subscribe")->__("Subscribe Manager");
	$this->_addButtonLabel = Mage::helper("subscribe")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}