<?php

class Tenovia_Subscribe_Block_Adminhtml_Subscribe_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("subscribeGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("subscribe/subscribe")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
					"header" => Mage::helper("subscribe")->__("ID"),
					"align" =>"right",
					"width" => "50px",
				  "type" => "number",
					"index" => "id",
				));
				$this->addColumn("email", array(
					"header" => Mage::helper("subscribe")->__("Email ID"),
					"align" =>"right",
					"width" => "50px",
				  "type" => "text",
					"index" => "email",
				));
				$this->addColumn("coupon", array(
					"header" => Mage::helper("subscribe")->__("Coupon Code"),
					"align" =>"right",
					"width" => "50px",
				  "type" => "text",
					"index" => "coupon",
				));				

			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return '#';
		}

		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_subscribe', array(
					 'label'=> Mage::helper('subscribe')->__('Remove Subscribe'),
					 'url'  => $this->getUrl('*/adminhtml_subscribe/massRemove'),
					 'confirm' => Mage::helper('subscribe')->__('Are you sure?')
				));
			return $this;
		}


}
