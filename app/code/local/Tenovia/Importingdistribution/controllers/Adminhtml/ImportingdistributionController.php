<?php

class Tenovia_Importingdistribution_Adminhtml_ImportingdistributionController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
		//return Mage::getSingleton('admin/session')->isAllowed('importingdistribution/importingdistribution');
			return true;
		}

		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("importingdistribution/importingdistribution")->_addBreadcrumb(Mage::helper("adminhtml")->__("Importingdistribution  Manager"),Mage::helper("adminhtml")->__("Importingdistribution Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Importingdistribution"));
			    $this->_title($this->__("Manager Importingdistribution"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Importingdistribution"));
				$this->_title($this->__("Importingdistribution"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("importingdistribution/importingdistribution")->load($id);
				if ($model->getId()) {
					Mage::register("importingdistribution_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("importingdistribution/importingdistribution");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Importingdistribution Manager"), Mage::helper("adminhtml")->__("Importingdistribution Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Importingdistribution Description"), Mage::helper("adminhtml")->__("Importingdistribution Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("importingdistribution/adminhtml_importingdistribution_edit"))->_addLeft($this->getLayout()->createBlock("importingdistribution/adminhtml_importingdistribution_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("importingdistribution")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Importingdistribution"));
		$this->_title($this->__("Importingdistribution"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("importingdistribution/importingdistribution")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("importingdistribution_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("importingdistribution/importingdistribution");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Importingdistribution Manager"), Mage::helper("adminhtml")->__("Importingdistribution Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Importingdistribution Description"), Mage::helper("adminhtml")->__("Importingdistribution Description"));


		$this->_addContent($this->getLayout()->createBlock("importingdistribution/adminhtml_importingdistribution_edit"))->_addLeft($this->getLayout()->createBlock("importingdistribution/adminhtml_importingdistribution_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("importingdistribution/importingdistribution")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Importingdistribution was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setImportingdistributionData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setImportingdistributionData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("importingdistribution/importingdistribution");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("importingdistribution/importingdistribution");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'importingdistribution.csv';
			$grid       = $this->getLayout()->createBlock('importingdistribution/adminhtml_importingdistribution_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'importingdistribution.xml';
			$grid       = $this->getLayout()->createBlock('importingdistribution/adminhtml_importingdistribution_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
