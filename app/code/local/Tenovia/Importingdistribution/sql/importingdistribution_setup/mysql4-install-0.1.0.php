<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `importing_distribution` (
  `id` int(11) NOT NULL,
  `applicant_name` varchar(500) NOT NULL,
  `applicant_city` varchar(500) NOT NULL,
  `applicant_country` varchar(500) NOT NULL,
  `applicant_pincode` varchar(500) NOT NULL,
  `applicant_contactno` varchar(500) NOT NULL,
  `applicant_email` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `importing_distribution` ADD PRIMARY KEY (`id`);
ALTER TABLE `importing_distribution` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 