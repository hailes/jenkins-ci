<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
ALTER TABLE `importing_distribution` ADD `qty` BIGINT NOT NULL AFTER `applicant_email`, ADD `deliver` INT NOT NULL AFTER `qty`, ADD `pricing` VARCHAR(255) NOT NULL AFTER `deliver`, ADD `other_enquiry_details` TEXT NOT NULL AFTER `pricing`, ADD `address` TEXT NOT NULL AFTER `other_enquiry_details`;
SQLTEXT;

$installer->run($sql);
//demo
//Mage::getModel('core/url_rewrite')->setId(null);
//demo
$installer->endSetup();
