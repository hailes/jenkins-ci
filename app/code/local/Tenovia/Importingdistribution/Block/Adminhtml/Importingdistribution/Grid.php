<?php

class Tenovia_Importingdistribution_Block_Adminhtml_Importingdistribution_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("importingdistributionGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("importingdistribution/importingdistribution")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				// $this->addColumn("id", array(
				// "header" => Mage::helper("importingdistribution")->__("ID"),
				// "align" =>"right",
				// "width" => "50px",
			 //    "type" => "number",
				// "index" => "id",
				// ));
				$this->addColumn("id", array(
				"header" => Mage::helper("importingdistribution")->__("ID"),
				"index" => "id",
				));
				$this->addColumn("applicant_name", array(
				"header" => Mage::helper("importingdistribution")->__("Name"),
				"index" => "applicant_name",
				));
				$this->addColumn("applicant_city", array(
				"header" => Mage::helper("importingdistribution")->__("City"),
				"index" => "applicant_city",
				));
				$this->addColumn("applicant_country", array(
				"header" => Mage::helper("importingdistribution")->__("Country"),
				"index" => "applicant_country",
				));
				$this->addColumn("applicant_contactno", array(
				"header" => Mage::helper("importingdistribution")->__("Contact No"),
				"index" => "applicant_contactno",
				));
				$this->addColumn("applicant_email", array(
				"header" => Mage::helper("importingdistribution")->__("Email"),
				"index" => "applicant_email",
				));
				$this->addColumn("applicant_pincode", array(
				"header" => Mage::helper("importingdistribution")->__("Pincode"),
				"index" => "applicant_pincode",
				));
				$this->addColumn("applicant_contactno", array(
				"header" => Mage::helper("importingdistribution")->__("Mobile"),
				"index" => "applicant_contactno",
				));
				$this->addColumn("applicant_contactno", array(
				"header" => Mage::helper("importingdistribution")->__("Mobile"),
				"index" => "applicant_email",
				));
				$this->addColumn("designation", array(
				"header" => Mage::helper("importingdistribution")->__("Designation"),
				"index" => "designation",
				));
				$this->addColumn("company_name", array(
				"header" => Mage::helper("importingdistribution")->__("Company Name"),
				"index" => "company_name",
				));
				$this->addColumn("qty", array(
				"header" => Mage::helper("importingdistribution")->__("Qty"),
				"index" => "qty",
				));
				$this->addColumn("distributer", array(
				"header" => Mage::helper("importingdistribution")->__("Distributer"),
				"index" => "distributer",
				));
				$this->addColumn("deliver", array(
				"header" => Mage::helper("importingdistribution")->__("Deliver By"),
				'type' => 'options',
				'options'=>Tenovia_Importingdistribution_Block_Adminhtml_Importingdistribution_Grid::getOptionArray7(),
				"index" => "deliver",
				));
				$this->addColumn("pricing", array(
				"header" => Mage::helper("importingdistribution")->__("Pricing Quotation"),
				'type' => 'options',
				'options'=>Tenovia_Importingdistribution_Block_Adminhtml_Importingdistribution_Grid::getOptionArray8(),
				"index" => "pricing",
			));
				$this->addColumn("address", array(
				"header" => Mage::helper("importingdistribution")->__("Address"),
				"index" => "address",
				));
				$this->addColumn("other_enquiry_details", array(
				"header" => Mage::helper("importingdistribution")->__("Other Enquiry Details"),

				"index" => "other_enquiry_details",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}



		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_importingdistribution', array(
					 'label'=> Mage::helper('importingdistribution')->__('Remove Importingdistribution'),
					 'url'  => $this->getUrl('*/adminhtml_importingdistribution/massRemove'),
					 'confirm' => Mage::helper('importingdistribution')->__('Are you sure?')
				));
			return $this;
		}

		static public function getOptionArray7()
		{
						$data_array=array();
						$data_array[1]='Sea';
						$data_array[2]='Air';
						return($data_array);
		}
		static public function getOptionArray8()
		{
						$data_array=array();
						$data_array[1]='FOB';
						$data_array[2]='CIF';
						return($data_array);
		}
}
