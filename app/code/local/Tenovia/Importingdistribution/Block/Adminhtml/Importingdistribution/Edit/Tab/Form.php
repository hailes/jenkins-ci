<?php
class Tenovia_Importingdistribution_Block_Adminhtml_Importingdistribution_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("importingdistribution_form", array("legend"=>Mage::helper("importingdistribution")->__("Item information")));

				
						// $fieldset->addField("id", "text", array(
						// "label" => Mage::helper("importingdistribution")->__("ID"),					
						// "class" => "required-entry",
						// "required" => true,
						// "name" => "id",
						// ));
					
						$fieldset->addField("applicant_name", "text", array(
						"label" => Mage::helper("importingdistribution")->__("Name"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_name",
						));
					
						$fieldset->addField("applicant_city", "text", array(
						"label" => Mage::helper("importingdistribution")->__("City"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_city",
						));
					
						$fieldset->addField("applicant_country", "text", array(
						"label" => Mage::helper("importingdistribution")->__("Country"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_country",
						));
					
						$fieldset->addField("applicant_contactno", "text", array(
						"label" => Mage::helper("importingdistribution")->__("Contact No"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_contactno",
						));
					
						$fieldset->addField("applicant_email", "text", array(
						"label" => Mage::helper("importingdistribution")->__("Email"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_email",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getImportingdistributionData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getImportingdistributionData());
					Mage::getSingleton("adminhtml/session")->setImportingdistributionData(null);
				} 
				elseif(Mage::registry("importingdistribution_data")) {
				    $form->setValues(Mage::registry("importingdistribution_data")->getData());
				}
				return parent::_prepareForm();
		}
}
