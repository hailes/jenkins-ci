<?php


class Tenovia_Importingdistribution_Block_Adminhtml_Importingdistribution extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_importingdistribution";
	$this->_blockGroup = "importingdistribution";
	$this->_headerText = Mage::helper("importingdistribution")->__("Importingdistribution Manager");
	$this->_addButtonLabel = Mage::helper("importingdistribution")->__("Add New Item");
	parent::__construct();
	
	}

}