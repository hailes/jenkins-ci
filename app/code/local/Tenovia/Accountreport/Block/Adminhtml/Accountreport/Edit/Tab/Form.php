<?php
class Tenovia_Accountreport_Block_Adminhtml_Accountreport_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("accountreport_form", array("legend"=>Mage::helper("accountreport")->__("Item information")));

				
						$fieldset->addField("order_number", "text", array(
						"label" => Mage::helper("accountreport")->__("Order Number"),
						"name" => "order_number",
						));
						$fieldset->addField("return_id", "text", array(
						"label" => Mage::helper("accountreport")->__("Return ID Number"),
						"name" => "return_id",
						));
					
						$fieldset->addField("name_account", "text", array(
						"label" => Mage::helper("accountreport")->__("Account Name"),
						"name" => "name_account",
						));
					
						$fieldset->addField("bank_name", "text", array(
						"label" => Mage::helper("accountreport")->__("Bank Name"),
						"name" => "bank_name",
						));
					
						$fieldset->addField("account_no", "text", array(
						"label" => Mage::helper("accountreport")->__("Account Number"),
						"name" => "account_no",
						));
					
						$fieldset->addField("branch_name", "text", array(
						"label" => Mage::helper("accountreport")->__("Branch Name"),
						"name" => "branch_name",
						));
					
						$fieldset->addField("ifsc_code", "text", array(
						"label" => Mage::helper("accountreport")->__("IFSC Code"),
						"name" => "ifsc_code",
						));
									
						 $fieldset->addField('refund_status', 'select', array(
						'label'     => Mage::helper('accountreport')->__('Status'),
						'values'   => Tenovia_Accountreport_Block_Adminhtml_Accountreport_Grid::getValueArray7(),
						'name' => 'refund_status',
						));
						$fieldset->addField("details", "textarea", array(
						"label" => Mage::helper("accountreport")->__("Details"),
						"name" => "details",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getAccountreportData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getAccountreportData());
					Mage::getSingleton("adminhtml/session")->setAccountreportData(null);
				} 
				elseif(Mage::registry("accountreport_data")) {
				    $form->setValues(Mage::registry("accountreport_data")->getData());
				}
				return parent::_prepareForm();
		}
}
