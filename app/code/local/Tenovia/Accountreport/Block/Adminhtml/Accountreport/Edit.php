<?php
	
class Tenovia_Accountreport_Block_Adminhtml_Accountreport_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "accountreport";
				$this->_controller = "adminhtml_accountreport";
				$this->_updateButton("save", "label", Mage::helper("accountreport")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("accountreport")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("accountreport")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("accountreport_data") && Mage::registry("accountreport_data")->getId() ){

				    return Mage::helper("accountreport")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("accountreport_data")->getId()));

				} 
				else{

				     return Mage::helper("accountreport")->__("Add Item");

				}
		}
}