<?php

class Tenovia_Accountreport_Block_Adminhtml_Accountreport_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("accountreportGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("accountreport/accountreport")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("accountreport")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("order_number", array(
				"header" => Mage::helper("accountreport")->__("Order Number"),
				"index" => "order_number",
				));
				$this->addColumn("return_id", array(
				"header" => Mage::helper("accountreport")->__("Return ID"),
				"index" => "return_id",
				));
				$this->addColumn("name_account", array(
				"header" => Mage::helper("accountreport")->__("Account Name"),
				"index" => "name_account",
				));
				$this->addColumn("bank_name", array(
				"header" => Mage::helper("accountreport")->__("Bank Name"),
				"index" => "bank_name",
				));
				$this->addColumn("account_no", array(
				"header" => Mage::helper("accountreport")->__("Account Number"),
				"index" => "account_no",
				));
				$this->addColumn("branch_name", array(
				"header" => Mage::helper("accountreport")->__("Branch Name"),
				"index" => "branch_name",
				));
				$this->addColumn("ifsc_code", array(
				"header" => Mage::helper("accountreport")->__("IFSC Code"),
				"index" => "ifsc_code",
				));
						$this->addColumn('refund_status', array(
						'header' => Mage::helper('accountreport')->__('Status'),
						'index' => 'refund_status',
						'type' => 'options',
						'options'=>Tenovia_Accountreport_Block_Adminhtml_Accountreport_Grid::getOptionArray7(),				
						));
				$this->addColumn("created_date", array(
				"header" => Mage::helper("accountreport")->__("Created Date"),
				"index" => "created_date",
				));
				$this->addColumn("updated_date", array(
				"header" => Mage::helper("accountreport")->__("Updated Date"),
				"index" => "updated_date",
				));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_accountreport', array(
					 'label'=> Mage::helper('accountreport')->__('Remove Accountreport'),
					 'url'  => $this->getUrl('*/adminhtml_accountreport/massRemove'),
					 'confirm' => Mage::helper('accountreport')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray7()
		{
            $data_array=array(); 
			$data_array[0]='Refunded';
			$data_array[1]='Non-Refunded';
            return($data_array);
		}
		static public function getValueArray7()
		{
            $data_array=array();
			foreach(Tenovia_Accountreport_Block_Adminhtml_Accountreport_Grid::getOptionArray7() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}