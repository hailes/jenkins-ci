<?php
class Tenovia_Accountreport_Block_Adminhtml_Accountreport_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("accountreport_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("accountreport")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("accountreport")->__("Item Information"),
				"title" => Mage::helper("accountreport")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("accountreport/adminhtml_accountreport_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
