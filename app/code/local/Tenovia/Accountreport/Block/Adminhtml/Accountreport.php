<?php


class Tenovia_Accountreport_Block_Adminhtml_Accountreport extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_accountreport";
	$this->_blockGroup = "accountreport";
	$this->_headerText = Mage::helper("accountreport")->__("COD Account Report");
	$this->_addButtonLabel = Mage::helper("accountreport")->__("Add New Item");
	parent::__construct();
	
	}

}