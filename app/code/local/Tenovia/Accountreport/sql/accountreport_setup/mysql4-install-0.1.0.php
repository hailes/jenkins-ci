<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `cod_return` (
 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
 `return_id` varchar(255) DEFAULT '',
 `order_number` varchar(355) DEFAULT '' ,
 `name_account` varchar(555) DEFAULT '',
 `bank_name` varchar(255) NOT NULL DEFAULT '',
 `account_no` varchar(255) DEFAULT '',
 `branch_name` varchar(100) DEFAULT '',
 `ifsc_code` varchar(100) NOT NULL DEFAULT '',
 `refund_status` int(11) unsigned NOT NULL,
 `details` varchar(100) DEFAULT '',
 `created_date` datetime default NULL ,
 `update_date` datetime default NULL ,
 PRIMARY KEY (`id`)
)
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 