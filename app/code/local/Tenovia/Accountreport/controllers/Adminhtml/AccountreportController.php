<?php

class Tenovia_Accountreport_Adminhtml_AccountreportController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
		//return Mage::getSingleton('admin/session')->isAllowed('accountreport/accountreport');
			return true;
		}

		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("accountreport/accountreport")->_addBreadcrumb(Mage::helper("adminhtml")->__("Accountreport  Manager"),Mage::helper("adminhtml")->__("Accountreport Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Accountreport"));
			    $this->_title($this->__("Manager Accountreport"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Accountreport"));
				$this->_title($this->__("Accountreport"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("accountreport/accountreport")->load($id);
				if ($model->getId()) {
					Mage::register("accountreport_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("accountreport/accountreport");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Accountreport Manager"), Mage::helper("adminhtml")->__("Accountreport Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Accountreport Description"), Mage::helper("adminhtml")->__("Accountreport Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("accountreport/adminhtml_accountreport_edit"))->_addLeft($this->getLayout()->createBlock("accountreport/adminhtml_accountreport_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("accountreport")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Accountreport"));
		$this->_title($this->__("Accountreport"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("accountreport/accountreport")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("accountreport_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("accountreport/accountreport");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Accountreport Manager"), Mage::helper("adminhtml")->__("Accountreport Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Accountreport Description"), Mage::helper("adminhtml")->__("Accountreport Description"));


		$this->_addContent($this->getLayout()->createBlock("accountreport/adminhtml_accountreport_edit"))->_addLeft($this->getLayout()->createBlock("accountreport/adminhtml_accountreport_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("accountreport/accountreport")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Accountreport was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setAccountreportData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setAccountreportData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("accountreport/accountreport");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("accountreport/accountreport");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'accountreport.csv';
			$grid       = $this->getLayout()->createBlock('accountreport/adminhtml_accountreport_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'accountreport.xml';
			$grid       = $this->getLayout()->createBlock('accountreport/adminhtml_accountreport_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
