<?php
class Tenovia_Skuupdate_Adminhtml_SkuupdatebackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('skuupdate/skuupdatebackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Backend Page Title"));
	   $this->renderLayout();
    }
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
          
            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                $uploaderFile = new Varien_File_Uploader('filename');
                $uploaderFile->setAllowedExtensions(array());
                $uploaderFile->setAllowRenameFiles(false);
                $uploaderFile->setFilesDispersion(false);
                $uploaderFilepath = Mage::getBaseDir('media').DS.'importcsv'.DS;
                $uploaderFile->save($uploaderFilepath, $_FILES['filename']['name']);
                $file = $_FILES['filename']['name'];
                $filepath = $uploaderFilepath.$file;                
                $i = 0; $RowCount=0;
                if (($handle = fopen("$filepath", 'r')) !== false) {
                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                   // echo "<pre>"; print_r($data);exit; 
                    if($i>0){   
                    $this->updateData($data);   
                    }                    
                        ++$i; $RowCount++;
                    }

                    Mage::getSingleton('core/session')->addSuccess('Total row inserted-'.$RowCount); 
                    $this->_redirectReferer();
                } else {
                    Mage::getSingleton('adminhtml/session')->addError('There is some Error');
                    $this->_redirect('*/*/index');
                }
            }
        }
    }

     public function updateData($data)
    {
       //echo "<pre>"; print_r($data);exit;    
        $oldsku = $data[0];
        $newsku = $data[1];
       $get_item = Mage::getModel('catalog/product')->loadByAttribute('sku', $oldsku);
       if ($get_item) 
       	{
       		//echo "asfd"; exit;
		$get_item->setSku($newsku)->save();
		echo "successful";
		}
		else
		{
			echo $oldsku.'-Not Available';
		}
    }
}