<?php

class Tenovia_Deliverypincodes_Model_Deliverypincodes extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('deliverypincodes/deliverypincodes');
    }

    public function getAvilableShippingMethods($pincode)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $connection->select()

                                ->from('delivery_pincode_flat_shipping_charge', array('shipping_provider_code', 'deliverable_date', 'iscod', 'isprepaid', 'delivery_type'))

                                ->where('pincode=?', $pincode)

                                ->where('status = 1')

                                ->where('iscod = 1 OR isprepaid = 1')

                                ->group('delivery_type');

        $datas = $connection->fetchAll($select);

        return $datas;
    }

    public function isShipmentAvailable($pincode)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $connection->select()
                                ->from('delivery_pincode_flat_shipping_charge', array('shipping_provider_code'))
                                ->where('pincode=?', $pincode)
                                ->where('status = 1')
                                ->where('iscod = 1 OR isprepaid = 1');

        $data = $connection->fetchRow($select);

        return empty($data['shipping_provider_code']) === false ? true : false;
    }

    public function getAdminAvilableShippingMethods($pincode)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $connection->select()

                                ->from('delivery_pincode_flat_shipping_charge', array('shipping_provider_code', 'deliverable_date', 'iscod', 'isprepaid', 'delivery_type'))

                                ->where('pincode=?', $pincode)

                                ->where('status = 1')

                                ->where('iscod = 1 OR isprepaid = 1');

        $datas = $connection->fetchAll($select);

        return $datas;
    }

    public function getPickupStores($pincode)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $connection->select()

                                ->from('pickupstore', array('*'))

                                ->join('store_locator_master', 'store_locator_master.id = pickupstore.pickupstore_storelocator_id', array('name', 'id as storelocatorid'))

                                ->where('pickupstore.pincode=?', $pincode)

                                ->where('pickupstore.status = 1');

        $datas = $connection->fetchAll($select);

        return $datas;
    }

    public function getStoresDetails($storeId)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select = $connection->select()
                                ->from('store_locator_master', array('*'))
                                ->where('id=?', $storeId)
                                ->where('status = 1');
        $datas = $connection->fetchRow($select);

        return $datas;
    }

    public function isPaymentChargeAvailable()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $pincode = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getPostcode();
        $select = $connection->select()
            ->from('delivery_pincode_flat_shipping_charge', array('iscod', 'is_payment_handling_fee_available', 'isprepaid'))
            ->where('pincode=?', $pincode)
            ->where('status = 1')
            ->where('is_payment_handling_fee_available = 1');

        $datas = $connection->fetchRow($select);

        return $datas;
    }

    public function getPaymentCharge($code)
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $subtotal = 0;
        $items = $quote->getAllItems();
        foreach ($items as $item) {
            $subtotal += $item->getRowTotalInclTax();
        }
        $payment = $this->isPaymentChargeAvailable();
        $paymentChargeApplicableAmount = Mage::getStoreConfig('payment/'.strval($code).'/apply_charge_total');
        if (empty($payment['is_payment_handling_fee_available']) === false && $subtotal < $paymentChargeApplicableAmount) {
            $amount = Mage::helper('paymentcharge')->getPaymentCharge($code);

            return Mage::helper('core')->formatPrice($amount);
        }

        return false;
    }
}
