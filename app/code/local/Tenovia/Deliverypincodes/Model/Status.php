<?php

class Tenovia_Deliverypincodes_Model_Status extends Varien_Object
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    public static function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED => Mage::helper('deliverypincodes')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('deliverypincodes')->__('Disabled'),
        );
    }
}
