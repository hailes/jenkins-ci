<?php

class Tenovia_Deliverypincodes_Model_Mysql4_Deliverypincodes extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the deliverypincodes_id refers to the key field in your database table.
        $this->_init('deliverypincodes/deliverypincodes', 'deliverypincodes_id');
    }
}
