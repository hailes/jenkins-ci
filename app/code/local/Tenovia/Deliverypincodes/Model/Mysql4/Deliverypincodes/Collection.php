<?php

class Tenovia_Deliverypincodes_Model_Mysql4_Deliverypincodes_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('deliverypincodes/deliverypincodes');
    }
}
