<?php

class Tenovia_Deliverypincodes_Block_Deliverypincodes extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getDeliverypincodes()
    {
        if (!$this->hasData('deliverypincodes')) {
            $this->setData('deliverypincodes', Mage::registry('deliverypincodes'));
        }

        return $this->getData('deliverypincodes');
    }
}
