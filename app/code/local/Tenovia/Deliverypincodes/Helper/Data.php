<?php

class Tenovia_Deliverypincodes_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getDeliverableDate($pincode, $carrier)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $itemselect = $connection->select()
                            ->from('delivery_pincode_flat_shipping_charge', array('deliverable_date'))
                            ->where('pincode=?', $pincode)
                            ->where('shipping_provider_code=?', $carrier);

        $discount_details = $connection->fetchRow($itemselect);

        if (empty($discount_details['deliverable_date']) === false) {
            return $discount_details['deliverable_date'];
        } elseif ($carrier === 'flatrate_flatrate') {
            return '(2 - 6 working days)';
        } else {
            return '(1 – 6 working days)';
        }
    }

    public function getPincodeServiceAvailability($pincode, $fetch = false)
    {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $deliverypincodesTable = $resource->getTableName('delivery_pincode_flat_shipping_charge');
        $pincode = empty($pincode) === false ? $pincode : Mage::getSingleton('checkout/cart')->getQuote()->getShippingAddress()->getData('postcode');
        $select = $read->select()
           ->from($deliverypincodesTable, array('*'))
           ->joinleft('regions', 'delivery_pincode_flat_shipping_charge.pincode = regions.pincode', ['country', 'state', 'city', 'pincode'])
           ->where('delivery_pincode_flat_shipping_charge.isprepaid = 1 OR delivery_pincode_flat_shipping_charge.iscod = 1')
           ->where('delivery_pincode_flat_shipping_charge.pincode=?', $pincode);
        if ($fetch) {
            $deliverypincodes = $read->fetchRow($select);
        } else {
            $deliverypincodes = $read->fetchAll($select);
        }

        return $deliverypincodes;
    }

    public function checkServiceCheckout($pincode)
    {
        $deliveryOption = $this->getPincodeServiceAvailability($pincode);
        $data = [];
        $data['iscod'] = false;
        $data['isprepaid'] = false;
        foreach ($deliveryOption as $key => $value) {
            if ($value['iscod'] == 1) {
                $data['iscod'] = true;
            } 
            if ($value['isprepaid'] == 1) {
                $data['isprepaid'] = true;
            }   
        }

        return $data;
    }

    public function isEnabledService($pincode)
    {
        $deliveryPincode = Mage::getModel('deliverypincodes/deliverypincodes');
        $canUseShipment = $deliveryPincode->isShipmentAvailable($pincode);

        return $canUseShipment;
    }

    public function isPaymentChargeApplicable()
    {
        $deliveryPincode = Mage::getModel('deliverypincodes/deliverypincodes');
        $canUseShipment = $deliveryPincode->isPaymentChargeAvailable();
        if (empty($canUseShipment['is_payment_handling_fee_available']) === false) {

            return true;
        }

        return false;
    }

    public function deliveryDate($days)
    {
        $date = new Zend_Date(Mage::getModel('core/date')->timestamp());
        $date->addDay($days);

        return $date->toString("EE,ddSS MMMM");
    }
}


