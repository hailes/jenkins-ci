<?php

class Tenovia_Deliverypincodes_IndexController extends Mage_Core_Controller_Front_Action
{
    public function checkpincodeAction()
    {
        $pincode = $this->getRequest()->getPost('pincode');
        $data = [];
        if (empty($pincode)) {
            $data['status'] = 'failed';
            $data['msg'] = 'invalid input';
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
            exit;
        }
        $storecode = Mage::app()->getStore()->getId();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $deliverypincodesTable = $resource->getTableName('delivery_pincode_flat_shipping_charge');
        Mage::getModel('core/cookie')->set('pincode', $pincode);
        $select = $read->select()
           ->from($deliverypincodesTable, array('*'))
           ->where('store_id=?', $storecode)
           ->where('isprepaid = 1 or iscod = 1')
           ->where('pincode=?', $pincode)
           ->where('status', '1')
           ->group('shipping_provider_code');

        $deliverypincodes = $read->fetchAll($select);
        $deliveryHelper = Mage::helper('deliverypincodes');
        $date = Mage::getModel('core/date')->date('H');
        if ($date > 12) {
          $setDate = 1;
        } else {
          $setDate = 0;
        }

        foreach ($deliverypincodes as $key => $available) {

            $deliveryDate = empty($available['deliverable_date']) === false ? $available['deliverable_date'] : 1;
            $data['data'][$key]['deliveryDate'] = $deliveryHelper->deliveryDate($deliveryDate + $setDate);
            $data['data'][$key]['cod'] = $available['iscod'];
            $data['data'][$key]['prepaid'] = $available['isprepaid'];
            $data['data'][$key]['deliveryType'] = $available['delivery_type'];
        }
        $data['status'] = empty($data['data']) === false ? 'success' : 'failed';
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }
     public function fetchpindetailsAction()
    {

              $pincodefield = $this->getRequest()->getPost('pincodevalue');
                $data = [];
             if (empty($pincodefield))
             {
                $data['status'] = 'failed';
                 $data['msg'] = 'invalid input';
                 $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
                    exit;
                }
            $storecode = Mage::app()->getStore()->getId();
             $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
             $deliverypincodesTablearray = $resource->getTableName('delivery_pincode_flat_shipping_charge');
             Mage::getModel('core/cookie')->set('pincode', $pincodefield);
                  $select = $read->select()
                    ->from($deliverypincodesTablearray, array('*'))
                     ->where('store_id=?', $storecode)
                     ->where('pincode=?', $pincodefield)
                     ->where('status', '1')
                      ->group('shipping_provider_code');
                    $deliverypincodesw = $read->fetchAll($select);


              foreach ($deliverypincodesw as $key => $available) {

                  $data['data'][$key]['city'] = $available['city'];
                  $data['data'][$key]['state'] = $available['state'];
                 }

                empty($data['data']) === false ? 'success' : 'failed';
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));

    }
}
