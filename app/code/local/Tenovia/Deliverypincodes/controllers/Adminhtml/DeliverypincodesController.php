<?php

class Tenovia_Deliverypincodes_Adminhtml_DeliverypincodesController extends Mage_Adminhtml_Controller_action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('deliverypincodes/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Pincode Manager'), Mage::helper('adminhtml')->__('Pincode Manager'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('deliverypincodes/deliverypincodes')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('deliverypincodes_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('deliverypincodes/items');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('deliverypincodes/adminhtml_deliverypincodes_edit'))
                ->_addLeft($this->getLayout()->createBlock('deliverypincodes/adminhtml_deliverypincodes_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('deliverypincodes')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function updateData($data)
    {
       // echo "<pre>"; print_r($data);exit;    
        $store = $data[0];
        $pincode = $data[1];
        $iscod = $data[2];
        $ispayment = $data[3];
        $isprepaid = $data[4];
        $serviceprovider = $data[5];
        $deliverytype = $data[6];
        $deliverydate = $data[7];
        $status = $data[8];
        $shippingcharge = $data[9];
    $city = $data[10];
    $state = $data[11];

        $currentdate = date('Y-m-d h:i:s');
        //$tableName = Mage::getSingleton('core/resource')->getTableName('deliverypincodes');
        $tableName = Mage::getSingleton('core/resource')->getTableName('delivery_pincode_flat_shipping_charge');
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        if ($store != '' && $pincode != '' && $serviceprovider != '') {
            $write->query("insert into $tableName(id,store_id,pincode,iscod,is_payment_handling_fee_available,isprepaid,shipping_provider_code,delivery_type,deliverable_date,status,created_time,shipping_charge,city,state) values('','{$store}','{$pincode}','{$iscod}','{$ispayment}','{$isprepaid}','{$serviceprovider}','{$deliverytype}','{$deliverydate}','{$status}','{$currentdate}','{$shippingcharge}','{$city}','{$state}')");
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
           // echo "sdf"; exit;
            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                $uploaderFile = new Varien_File_Uploader('filename');
                $uploaderFile->setAllowedExtensions(array());
                $uploaderFile->setAllowRenameFiles(false);
                $uploaderFile->setFilesDispersion(false);
                $uploaderFilepath = Mage::getBaseDir('media').DS.'importcsv'.DS;
                $uploaderFile->save($uploaderFilepath, $_FILES['filename']['name']);
                $file = $_FILES['filename']['name'];
                $filepath = $uploaderFilepath.$file;                
                $i = 0; $RowCount=0;
                if (($handle = fopen("$filepath", 'r')) !== false) {
                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                   // echo "<pre>"; print_r($data);exit; 
                    if($i>0){   
                    $this->updateData($data);   
                    }                    
                        ++$i; $RowCount++;
                    }

                    Mage::getSingleton('core/session')->addSuccess('Total row inserted-'.$RowCount); 
                    $this->_redirectReferer();
                } else {
                    Mage::getSingleton('adminhtml/session')->addError('There is some Error');
                    $this->_redirect('*/*/index');
                }
            }
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('deliverypincodes/deliverypincodes');

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $deliverypincodesIds = $this->getRequest()->getParam('deliverypincodes');
        if (!is_array($deliverypincodesIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($deliverypincodesIds as $deliverypincodesId) {
                    $deliverypincodes = Mage::getModel('deliverypincodes/deliverypincodes')->load($deliverypincodesId);
                    $deliverypincodes->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($deliverypincodesIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $deliverypincodesIds = $this->getRequest()->getParam('deliverypincodes');
        if (!is_array($deliverypincodesIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($deliverypincodesIds as $deliverypincodesId) {
                    $deliverypincodes = Mage::getSingleton('deliverypincodes/deliverypincodes')
                        ->load($deliverypincodesId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($deliverypincodesIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'deliverypincodes.csv';
        $content = $this->getLayout()->createBlock('deliverypincodes/adminhtml_deliverypincodes_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'deliverypincodes.xml';
        $content = $this->getLayout()->createBlock('deliverypincodes/adminhtml_deliverypincodes_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}