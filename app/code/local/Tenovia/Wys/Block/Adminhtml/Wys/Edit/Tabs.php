<?php
class Tenovia_Wys_Block_Adminhtml_Wys_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("wys_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("wys")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("wys")->__("Item Information"),
				"title" => Mage::helper("wys")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("wys/adminhtml_wys_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
