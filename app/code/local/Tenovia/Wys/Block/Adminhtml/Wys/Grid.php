<?php

class Tenovia_Wys_Block_Adminhtml_Wys_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("wysGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("wys/wys")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("wys")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("applicant_name", array(
				"header" => Mage::helper("wys")->__("Name"),
				"index" => "applicant_name",
				));
				$this->addColumn("applicant_email", array(
				"header" => Mage::helper("wys")->__("Email"),
				"index" => "applicant_email",
				));
				$this->addColumn("applicant_phone", array(
				"header" => Mage::helper("wys")->__("PhoneNo."),
				"index" => "applicant_phone",
				));
				$this->addColumn("applicant_city", array(
				"header" => Mage::helper("wys")->__("City"),
				"index" => "applicant_city",
				));
				$this->addColumn("facebook_post_url", array(
				"header" => Mage::helper("wys")->__("Facebook URL"),
				"index" => "facebook_post_url",
				));
				$this->addColumn("tiktok_post_url", array(
				"header" => Mage::helper("wys")->__("Tiktok URL"),
				"index" => "tiktok_post_url",
				));
				$this->addColumn("instagram_post_url", array(
				"header" => Mage::helper("wys")->__("Instagram URL"),
				"index" => "instagram_post_url",
				));
				$this->addColumn("youtube_post_url", array(
				"header" => Mage::helper("wys")->__("Youtube URL"),
				"index" => "youtube_post_url",
				));
				$this->addColumn("created_date", array(
				"header" => Mage::helper("wys")->__("Created On"),
				"index" => "created_date",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_wys', array(
					 'label'=> Mage::helper('wys')->__('Remove Wys'),
					 'url'  => $this->getUrl('*/adminhtml_wys/massRemove'),
					 'confirm' => Mage::helper('wys')->__('Are you sure?')
				));
			return $this;
		}
			

}