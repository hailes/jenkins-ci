<?php
class Tenovia_Wys_Block_Adminhtml_Wys_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("wys_form", array("legend"=>Mage::helper("wys")->__("Item information")));

				
						/*$fieldset->addField("id", "text", array(
						"label" => Mage::helper("wys")->__("Id"),
						"name" => "id",
						));*/
					
						$fieldset->addField("applicant_name", "text", array(
						"label" => Mage::helper("wys")->__("Name"),
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_name",
						));
					
						$fieldset->addField("applicant_email", "text", array(
						"label" => Mage::helper("wys")->__("Email"),
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_email",
						));
					
						$fieldset->addField("applicant_phone", "text", array(
						"label" => Mage::helper("wys")->__("PhoneNo."),
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_phone",
						));
					
						$fieldset->addField("applicant_city", "text", array(
						"label" => Mage::helper("wys")->__("City"),
						"class" => "required-entry",
						"required" => true,
						"name" => "applicant_city",
						));
					
						$fieldset->addField("facebook_post_url", "text", array(
						"label" => Mage::helper("wys")->__("Facebook URL"),
						"class" => "required-entry",
						"required" => true,
						"name" => "facebook_post_url",
						));
					
						$fieldset->addField("tiktok_post_url", "text", array(
						"label" => Mage::helper("wys")->__("Tiktok URL"),
						"class" => "required-entry",
						"required" => true,
						"name" => "tiktok_post_url",
						));
					
						$fieldset->addField("instagram_post_url", "text", array(
						"label" => Mage::helper("wys")->__("Instagram URL"),
						"name" => "instagram_post_url",
						));
					
						$fieldset->addField("youtube_post_url", "text", array(
						"label" => Mage::helper("wys")->__("Youtube URL"),
						"name" => "youtube_post_url",
						));
					
						$fieldset->addField("created_date", "text", array(
						"label" => Mage::helper("wys")->__("Created On"),
						"class" => "required-entry",
						"required" => true,
						"name" => "created_date",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getWysData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getWysData());
					Mage::getSingleton("adminhtml/session")->setWysData(null);
				}
				elseif(Mage::registry("wys_data")) {
				    $form->setValues(Mage::registry("wys_data")->getData());
				}
				return parent::_prepareForm();
		}
}
