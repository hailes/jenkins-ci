<?php


class Tenovia_Wys_Block_Adminhtml_Wys extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_wys";
	$this->_blockGroup = "wys";
	$this->_headerText = Mage::helper("wys")->__("Wys Manager");
	$this->_addButtonLabel = Mage::helper("wys")->__("Add New Item");
	parent::__construct();
	
	}

}