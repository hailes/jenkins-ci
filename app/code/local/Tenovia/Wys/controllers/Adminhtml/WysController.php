<?php

class Tenovia_Wys_Adminhtml_WysController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
		//return Mage::getSingleton('admin/session')->isAllowed('wys/wys');
			return true;
		}

		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("wys/wys")->_addBreadcrumb(Mage::helper("adminhtml")->__("Wys  Manager"),Mage::helper("adminhtml")->__("Wys Manager"));
				return $this;
		}
		public function indexAction()
		{
			    $this->_title($this->__("Wys"));
			    $this->_title($this->__("Manager Wys"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{
			    $this->_title($this->__("Wys"));
				$this->_title($this->__("Wys"));
			    $this->_title($this->__("Edit Item"));

				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("wys/wys")->load($id);
				if ($model->getId()) {
					Mage::register("wys_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("wys/wys");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Wys Manager"), Mage::helper("adminhtml")->__("Wys Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Wys Description"), Mage::helper("adminhtml")->__("Wys Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("wys/adminhtml_wys_edit"))->_addLeft($this->getLayout()->createBlock("wys/adminhtml_wys_edit_tabs"));
					$this->renderLayout();
				}
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("wys")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Wys"));
		$this->_title($this->__("Wys"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("wys/wys")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("wys_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("wys/wys");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Wys Manager"), Mage::helper("adminhtml")->__("Wys Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Wys Description"), Mage::helper("adminhtml")->__("Wys Description"));


		$this->_addContent($this->getLayout()->createBlock("wys/adminhtml_wys_edit"))->_addLeft($this->getLayout()->createBlock("wys/adminhtml_wys_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("wys/wys")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Wys was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setWysData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					}
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setWysData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("wys/wys");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					}
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("wys/wys");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'wys.csv';
			$grid       = $this->getLayout()->createBlock('wys/adminhtml_wys_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		}
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'wys.xml';
			$grid       = $this->getLayout()->createBlock('wys/adminhtml_wys_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
