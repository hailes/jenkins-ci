<?php

class Tenovia_Wys_IndexController extends Mage_Core_Controller_Front_Action {
   
    public function saveFormAction() {
        //$this->getRequest->getParam('name');
        //echo "<pre>"; print_r($_POST); exit;
        include_once('src/autoload.php');
        
            $name = $_POST['name'];
            $emailid = $_POST['emailid'];
            $phone = $_POST['phone'];
            $city = $_POST['city'];
            $instagramURL = $_POST['instagramURL'];
            $facebookURL = $_POST['facebookURL'];
            $tiktokURL = $_POST['tiktokURL'];
            $youtubeURL = $_POST['youtubeURL'];
            $countURLs = 0;

            $suburbResultArray = array();
            $statusFlag = 1;
            $outMsg = '';
            
            if($instagramURL != ''){
                $countURLs++;
            }
            if($facebookURL != ''){
                $countURLs++;
            }
            if($tiktokURL != ''){
                $countURLs++;
            }
            if($youtubeURL != ''){
                $countURLs++;
            }

            //Save Data in Database
            if ($name !='' && $emailid !='' && $phone !='' && $city !='' && $countURLs>=2) {
                
                $collection = Mage::getModel('wys/wys');
                $collection->setData('applicant_name', $name);
                $collection->setData('applicant_email', $emailid);
                $collection->setData('applicant_phone', $phone);
                $collection->setData('applicant_city', $city);
                $collection->setData('facebook_post_url', $facebookURL);
                $collection->setData('instagram_post_url', $instagramURL);
                $collection->setData('youtube_post_url', $youtubeURL);
                $collection->setData('tiktok_post_url', $tiktokURL);
                $collection->save();
                //$this->_redirectReferer();
                //  Mage::getSingleton('core/session')->addSuccess('Your form has been submitted');
                // Send email for notification
                $emailTemplate = Mage::getModel('core/email_template')->loadByCode('stimulus');

                //Getting the Store E-Mail Sender Name.
                $senderName = Mage::getStoreConfig('trans_email/ident_general/name');

                //Getting the Store General E-Mail.
                $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

                $subjectM = 'Paragon Footwear - Whats your Stimulus';
                $emailTemplateVariables = array(
                    'name' => $name,
                    'customer_email' => $emailid,
                    'telephone' => $phone,
                    'city' => $city,
                    'facebook_post_url' => $facebookURL,
                    'instagram_post_url' => $instagramURL,
                    'youtube_post_url' => $youtubeURL,
                    'tiktok_post_url' => $tiktokURL,
                );
                $toAddress = '';
                //$toAddress = array("sonu@tenovia.com", "exports@paragonfootwear.com", "esupport@paragonfootwear.com", "export1@paragonfootwear.com");
                $toAddress = array($emailid);

                $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);

                $emailTemplate->setSenderName($senderName);
                $emailTemplate->setSenderEmail($senderEmail);
                $emailTemplate->setTemplateSubject($subjectM);
                $emailTemplate->send($toAddress, $name, $emailTemplateVariables);


                //$this->_redirect("careers");
                $outMsg = 'Your contest entry has been submitted';
                $suburbResultArray = array('status' => $statusFlag, 'message' => $outMsg);
            }else{
                $statusFlag = 0;
                $outMsg = 'Please fill all mandatory fields';
                $suburbResultArray = array('status' => $statusFlag, 'message' => $outMsg);
            }
            
        
        return $this->getResponse()->setBody(json_encode($suburbResultArray));
    }
    
    //Function to check whether already registered for contest
    public function getContestDetailsAction(){
        //include_once('src/autoload.php');
        //echo "<pre>"; print_r($_REQUEST); exit;
            $phone = $_POST['phone'];
            
            $suburbResultArray = array();
            $statusFlag = 1;
            $outMsg = '';
            
            $collection = Mage::getModel("wys/wys")->getCollection();
            //$collection->addFieldToFilter('applicant_phone',$phone);
            //$collection->save();
            $phoneArr =array();
            foreach($collection as $data){
                array_push($phoneArr,$data->getData('applicant_phone'));
            }
            //get Data from Database
            if ($phone !='') {
                    
                    if(in_array($phone,$phoneArr)){
                        $statusFlag = 0;
                        $outMsg = 'You have already registered for the contest';
                        $suburbResultArray = array('status' => $statusFlag, 'message' => $outMsg); 
                    }else{
                        $suburbResultArray = array('status' => $statusFlag, 'message' => $outMsg); 
                    }
            }
            else{
                $statusFlag = 0;
                $outMsg = 'Please fill all mandatory fields12';
                $suburbResultArray = array('status' => $statusFlag, 'message' => $outMsg);
            }
            echo json_encode($suburbResultArray);
            //return $this->getResponse()->setBody(json_encode($suburbResultArray));
    }

}

//}
