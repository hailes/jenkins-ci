<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `what_is_your_stimulusdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_name` varchar(500) NOT NULL,
  `applicant_email` varchar(500) NOT NULL,
  `applicant_phone` varchar(500) NOT NULL,
  `applicant_city` varchar(500) NOT NULL,
  `facebook_post_url` varchar(1000) NOT NULL,
  `tiktok_post_url` varchar(1000) NOT NULL,
  `instagram_post_url` varchar(1000) DEFAULT NULL,
  `youtube_post_url` varchar(1000) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

SQLTEXT;

$installer->run($sql);
//demo
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 