<?php
class Tenovia_Checkoutstep_Block_Onepage extends Mage_Checkout_Block_Onepage
{
    public function getSteps()
    {
        $steps = array();
 
        if (!$this->isCustomerLoggedIn()) {
            $steps['login'] = $this->getCheckout()->getStepData('login');
        }
 
        $stepCodes = array('billing', 'payment');
 
        foreach ($stepCodes as $step) {
            $steps[$step] = $this->getCheckout()->getStepData($step);
        }
        return $steps;
    }

    // protected function _getStepCodes(){
    //     if ($this->getQuote()->isVirtual()) {
    //         return array('login', 'billing', 'payment');
    //     }
    //     return array('login', 'billing', 'payment');
    // }
}