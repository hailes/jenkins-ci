<?php
class Tenovia_Checkoutstep_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getCities()
    {
        $resource  =  Mage::getSingleton('core/resource');
        $readConnection =  $resource->getConnection('core_read');
        $tableName   = $resource->getTableName('regions');
        $query = "SELECT DISTINCT city FROM {$tableName}";
        $results =  $readConnection->fetchAll($query);
        $cities = array();
        if(count($results) > 0){
          foreach($results as $city){
              $cityName = $city['city'];
              $cities[]  = $cityName;
          }
        }
        return $cities;
    }
 
    public function getCitiesAsDropdown($selectedCity = '',$stateId)
    {
        $cities =  $this->getCities();
        $options =  '';
        if(count($cities) > 0){
            foreach($cities as $city){
               $isSelected = $selectedCity == $city ? ' selected="selected"' : null;
               $options .= '<option value="' . $city . '"' . $isSelected . '>' . $city . '</option>';
           }
        }   
        return $options;
    }
}
	 