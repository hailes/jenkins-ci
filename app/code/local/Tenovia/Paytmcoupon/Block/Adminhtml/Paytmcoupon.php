<?php


class Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_paytmcoupon";
	$this->_blockGroup = "paytmcoupon";
	$this->_headerText = Mage::helper("paytmcoupon")->__("Paytmcoupon Manager");
	$this->_addButtonLabel = Mage::helper("paytmcoupon")->__("Add New Item");
	$this->_addButton("Import", array(
            "label" => Mage::helper("core")->__("Import"),
            "onclick" => "location.href = '" . $this->getUrl('admin_paytmcoupon/adminhtml_paytmcouponbackend') . "';",
            "class" => "btn btn-danger",
        ));
	parent::__construct();
	
	}

}