<?php

class Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("paytmcouponGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("paytmcoupon/paytmcoupon")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				// $this->addColumn("id", array(
				// "header" => Mage::helper("paytmcoupon")->__("ID"),
				// "align" =>"right",
				// "width" => "50px",
			 //    "type" => "number",
				// "index" => "id",
				// ));
                
				$this->addColumn("coupon_code", array(
				"header" => Mage::helper("paytmcoupon")->__("Coupon Code"),
				"index" => "coupon_code",
				));
				$this->addColumn("order_no", array(
				"header" => Mage::helper("paytmcoupon")->__("Order Number"),
				"index" => "order_no",
				));
						$this->addColumn('coupon_type', array(
						'header' => Mage::helper('paytmcoupon')->__('Coupon Type'),
						'index' => 'coupon_type',
						'type' => 'options',
						'options'=>Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Grid::getOptionArray3(),				
						));
						
						$this->addColumn('coupon_status', array(
						'header' => Mage::helper('paytmcoupon')->__('Coupon Status'),
						'index' => 'coupon_status',
						'type' => 'options',
						'options'=>Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Grid::getOptionArray4(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_paytmcoupon', array(
					 'label'=> Mage::helper('paytmcoupon')->__('Remove Paytmcoupon'),
					 'url'  => $this->getUrl('*/adminhtml_paytmcoupon/massRemove'),
					 'confirm' => Mage::helper('paytmcoupon')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[Bus]='Bus';
			$data_array[Air]='Air';
            return($data_array);
		}
		static public function getValueArray3()
		{
            $data_array=array();
			foreach(Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='New';
			$data_array[1]='Released';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}