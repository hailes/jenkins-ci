<?php
class Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("paytmcoupon_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("paytmcoupon")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("paytmcoupon")->__("Item Information"),
				"title" => Mage::helper("paytmcoupon")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("paytmcoupon/adminhtml_paytmcoupon_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
