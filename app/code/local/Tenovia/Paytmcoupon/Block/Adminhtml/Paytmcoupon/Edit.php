<?php
	
class Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "paytmcoupon";
				$this->_controller = "adminhtml_paytmcoupon";
				$this->_updateButton("save", "label", Mage::helper("paytmcoupon")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("paytmcoupon")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("paytmcoupon")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("paytmcoupon_data") && Mage::registry("paytmcoupon_data")->getId() ){

				    return Mage::helper("paytmcoupon")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("paytmcoupon_data")->getId()));

				} 
				else{

				     return Mage::helper("paytmcoupon")->__("Add Item");

				}
		}
}