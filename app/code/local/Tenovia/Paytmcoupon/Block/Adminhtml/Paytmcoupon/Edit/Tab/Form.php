<?php
class Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("paytmcoupon_form", array("legend"=>Mage::helper("paytmcoupon")->__("Item information")));

				
						$fieldset->addField("coupon_code", "text", array(
						"label" => Mage::helper("paytmcoupon")->__("Coupon Code"),
						"name" => "coupon_code",
						));
					
						$fieldset->addField("order_no", "text", array(
						"label" => Mage::helper("paytmcoupon")->__("Order Number"),
						"name" => "order_no",
						));
									
						 $fieldset->addField('coupon_type', 'select', array(
						'label'     => Mage::helper('paytmcoupon')->__('Coupon Type'),
						'values'   => Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Grid::getValueArray3(),
						'name' => 'coupon_type',
						));				
						 $fieldset->addField('coupon_status', 'select', array(
						'label'     => Mage::helper('paytmcoupon')->__('Coupon Status'),
						'values'   => Tenovia_Paytmcoupon_Block_Adminhtml_Paytmcoupon_Grid::getValueArray4(),
						'name' => 'coupon_status',
						));

				if (Mage::getSingleton("adminhtml/session")->getPaytmcouponData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPaytmcouponData());
					Mage::getSingleton("adminhtml/session")->setPaytmcouponData(null);
				} 
				elseif(Mage::registry("paytmcoupon_data")) {
				    $form->setValues(Mage::registry("paytmcoupon_data")->getData());
				}
				return parent::_prepareForm();
		}
}
