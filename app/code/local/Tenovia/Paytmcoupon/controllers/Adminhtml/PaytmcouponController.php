<?php

class Tenovia_Paytmcoupon_Adminhtml_PaytmcouponController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
		//return Mage::getSingleton('admin/session')->isAllowed('paytmcoupon/paytmcoupon');
			return true;
		}

		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("paytmcoupon/paytmcoupon")->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmcoupon  Manager"),Mage::helper("adminhtml")->__("Paytmcoupon Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Paytmcoupon"));
			    $this->_title($this->__("Manager Paytmcoupon"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Paytmcoupon"));
				$this->_title($this->__("Paytmcoupon"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("paytmcoupon/paytmcoupon")->load($id);
				if ($model->getId()) {
					Mage::register("paytmcoupon_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("paytmcoupon/paytmcoupon");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmcoupon Manager"), Mage::helper("adminhtml")->__("Paytmcoupon Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmcoupon Description"), Mage::helper("adminhtml")->__("Paytmcoupon Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("paytmcoupon/adminhtml_paytmcoupon_edit"))->_addLeft($this->getLayout()->createBlock("paytmcoupon/adminhtml_paytmcoupon_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("paytmcoupon")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Paytmcoupon"));
		$this->_title($this->__("Paytmcoupon"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("paytmcoupon/paytmcoupon")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("paytmcoupon_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("paytmcoupon/paytmcoupon");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmcoupon Manager"), Mage::helper("adminhtml")->__("Paytmcoupon Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmcoupon Description"), Mage::helper("adminhtml")->__("Paytmcoupon Description"));


		$this->_addContent($this->getLayout()->createBlock("paytmcoupon/adminhtml_paytmcoupon_edit"))->_addLeft($this->getLayout()->createBlock("paytmcoupon/adminhtml_paytmcoupon_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("paytmcoupon/paytmcoupon")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Paytmcoupon was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setPaytmcouponData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setPaytmcouponData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("paytmcoupon/paytmcoupon");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("paytmcoupon/paytmcoupon");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'paytmcoupon.csv';
			$grid       = $this->getLayout()->createBlock('paytmcoupon/adminhtml_paytmcoupon_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'paytmcoupon.xml';
			$grid       = $this->getLayout()->createBlock('paytmcoupon/adminhtml_paytmcoupon_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}

		 public function updateData($data)
    {
         
        $couponcode = $data[0];
        $ordernumber = $data[1];
        $coupontype = $data[2];
        $couponstatus = $data[3];
       
        //$tableName = Mage::getSingleton('core/resource')->getTableName('deliverypincodes');
        $tableName = Mage::getSingleton('core/resource')->getTableName('paytm_coupon');
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        if ($couponcode != '' && $coupontype != '') {
        	// echo "<pre>"; print_r($data[2]);exit;
     $write->query("insert into $tableName(id,coupon_code,order_no,coupon_type,coupon_status) values('','{$couponcode}','{$ordernumber}','{$coupontype}','{$couponstatus}')");
        }
    }

    public function save1Action()
    {
        if ($data = $this->getRequest()->getPost()) {
          // echo "sdf"; exit;
            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                $uploaderFile = new Varien_File_Uploader('filename');
                $uploaderFile->setAllowedExtensions(array());
                $uploaderFile->setAllowRenameFiles(false);
                $uploaderFile->setFilesDispersion(false);
                $uploaderFilepath = Mage::getBaseDir('media').DS.'importcsv'.DS;
                $uploaderFile->save($uploaderFilepath, $_FILES['filename']['name']);
                $file = $_FILES['filename']['name'];
                $filepath = $uploaderFilepath.$file;                
                $i = 0; $RowCount=0;
                if (($handle = fopen("$filepath", 'r')) !== false) {
                    while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                   // echo "<pre>"; print_r($data);exit; 
                    if($i>0){   
                    $this->updateData($data);   
                    }                    
                        ++$i; $RowCount++;
                    }

                    Mage::getSingleton('core/session')->addSuccess('Total row inserted-'.$RowCount); 
                    $this->_redirectReferer();
                } else {
                    Mage::getSingleton('adminhtml/session')->addError('There is some Error');
                    $this->_redirect('*/*/index');
                }
            }
        }
    }
}
