<?php
class Tenovia_Paytmcoupon_Adminhtml_PaytmcouponbackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('paytmcoupon/paytmcouponbackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Coupon List"));
	   $this->renderLayout();
    }
}