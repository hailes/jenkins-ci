<?php

class Tenovia_Paytmscheduler_Adminhtml_PaytmschedulerController extends Mage_Adminhtml_Controller_Action
{
		protected function _isAllowed()
		{
		//return Mage::getSingleton('admin/session')->isAllowed('paytmscheduler/paytmscheduler');
			return true;
		}

		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("paytmscheduler/paytmscheduler")->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmscheduler  Manager"),Mage::helper("adminhtml")->__("Paytmscheduler Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Paytmscheduler"));
			    $this->_title($this->__("Manager Paytmscheduler"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Paytmscheduler"));
				$this->_title($this->__("Paytmscheduler"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("paytmscheduler/paytmscheduler")->load($id);
				if ($model->getId()) {
					Mage::register("paytmscheduler_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("paytmscheduler/paytmscheduler");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmscheduler Manager"), Mage::helper("adminhtml")->__("Paytmscheduler Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmscheduler Description"), Mage::helper("adminhtml")->__("Paytmscheduler Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("paytmscheduler/adminhtml_paytmscheduler_edit"))->_addLeft($this->getLayout()->createBlock("paytmscheduler/adminhtml_paytmscheduler_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("paytmscheduler")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Paytmscheduler"));
		$this->_title($this->__("Paytmscheduler"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("paytmscheduler/paytmscheduler")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("paytmscheduler_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("paytmscheduler/paytmscheduler");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmscheduler Manager"), Mage::helper("adminhtml")->__("Paytmscheduler Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Paytmscheduler Description"), Mage::helper("adminhtml")->__("Paytmscheduler Description"));


		$this->_addContent($this->getLayout()->createBlock("paytmscheduler/adminhtml_paytmscheduler_edit"))->_addLeft($this->getLayout()->createBlock("paytmscheduler/adminhtml_paytmscheduler_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("paytmscheduler/paytmscheduler")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Paytmscheduler was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setPaytmschedulerData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setPaytmschedulerData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("paytmscheduler/paytmscheduler");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("paytmscheduler/paytmscheduler");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
		public function sendmailAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
					/* Mail Send */

		  // $templateId = '9' ;
			$templateId = '11' ;
            $sendername = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderemail = Mage::getStoreConfig('trans_email/ident_general/email');
            $sender = Array('name' => $sendername,
            'email' => $senderemail);

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$mail_status=1;
$query="SELECT * FROM paytm_scheduler WHERE id ='".$id."' AND mail_status = '".$mail_status."' " ;
$result = $readConnection->fetchAll($query);
if($result){
$email=$result[0]['email_address'];
$emailName=$sendername;
            $vars = Array();
            $vars = Array('order_number'=>$result[0]['order_number'],'email'=>$result[0]['email_address'],'buscoupon'=>$result[0]['coupon1'],'aircoupon'=>$result[0]['coupon2']);
            $storeId = Mage::app()->getStore()->getId();
            $translate = Mage::getSingleton('core/translate');
            Mage::getModel('core/email_template')
            ->sendTransactional($templateId, $sender, $email, $emailName, $vars, $storeId);
            $translate->setTranslateInline(true);
      /*Backup */
        $table        = $resource->getTableName('paytmcoupon_mail_log');  
        $now=now();
        $query = "INSERT INTO {$table} (id,order_id,send_date) VALUES ('','{$result[0]['order_number']}','{$now}')";
           $writeAdapter   = $resource->getConnection('core_write');
    $result_coupon=$writeAdapter->query($query);
/* Start Message Sent */

      $url = Mage::getStoreConfig('sales/otpsms_setting/url');
      $userName = Mage::getStoreConfig('sales/otpsms_setting/username');
      $password = Mage::getStoreConfig('sales/otpsms_setting/password');
      $senderId = Mage::getStoreConfig('sales/otpsms_setting/sender');

      $mobileNumber = $result[0]['mobile_number'];

      $message = "PayTM cash back offer BUS Coupon-".$result[0]['coupon1']." and AIR Coupon-" .$result[0]['coupon2']." from Paragonfootwear.com order number" .$result[0]['order_number']."";
      
        $curl_url = sprintf('%s?uname=%s&pass=%s&send=%s&dest=%s&msg=%s&concat=1', $url,$userName,$password, $senderId, $mobileNumber, urlencode($message));
     //echo $curl_url; exit;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $curl_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $store = curl_exec($ch);        
      curl_close($ch);


      Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("PayTm Coupons mail Send successfully"));
				} 
				else
				{

					  Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Sorry No Mail/SMS sent"));
				}

			}
				
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}	
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'paytmscheduler.csv';
			$grid       = $this->getLayout()->createBlock('paytmscheduler/adminhtml_paytmscheduler_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'paytmscheduler.xml';
			$grid       = $this->getLayout()->createBlock('paytmscheduler/adminhtml_paytmscheduler_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
