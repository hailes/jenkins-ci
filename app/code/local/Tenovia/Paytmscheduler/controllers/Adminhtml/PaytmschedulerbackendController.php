<?php
class Tenovia_Paytmscheduler_Adminhtml_PaytmschedulerbackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('paytmscheduler/paytmschedulerbackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Paytmscheduler"));
	   $this->renderLayout();
    }
}