<?php
class Tenovia_Paytmscheduler_Block_Adminhtml_Paytmscheduler_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("paytmscheduler_form", array("legend"=>Mage::helper("paytmscheduler")->__("Item information")));

				
						$fieldset->addField("order_number", "text", array(
						"label" => Mage::helper("paytmscheduler")->__("Order Number"),
						"name" => "order_number",
						));
					
						$fieldset->addField("email_address", "text", array(
						"label" => Mage::helper("paytmscheduler")->__("Email Address"),
						"name" => "email_address",
						));
					
						$fieldset->addField("mobile_number", "text", array(
						"label" => Mage::helper("paytmscheduler")->__("Mobile Number"),
						"name" => "mobile_number",
						));
					
						$fieldset->addField("order_value", "text", array(
						"label" => Mage::helper("paytmscheduler")->__("Order Value"),
						"name" => "order_value",
						));
					
						$fieldset->addField("coupon1", "text", array(
						"label" => Mage::helper("paytmscheduler")->__("Coupon 1"),
						"name" => "coupon1",
						));
					
						$fieldset->addField("coupon2", "text", array(
						"label" => Mage::helper("paytmscheduler")->__("Coupon 2"),
						"name" => "coupon2",
						));
					
						$fieldset->addField("mail_delivery_date", "text", array(
						"label" => Mage::helper("paytmscheduler")->__("Mail Delivery Date"),
						"name" => "mail_delivery_date",
						));
									
						 $fieldset->addField('mail_status', 'select', array(
						'label'     => Mage::helper('paytmscheduler')->__('Mail Status'),
						'values'   => Tenovia_Paytmscheduler_Block_Adminhtml_Paytmscheduler_Grid::getValueArray8(),
						'name' => 'mail_status',
						));

				if (Mage::getSingleton("adminhtml/session")->getPaytmschedulerData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPaytmschedulerData());
					Mage::getSingleton("adminhtml/session")->setPaytmschedulerData(null);
				} 
				elseif(Mage::registry("paytmscheduler_data")) {
				    $form->setValues(Mage::registry("paytmscheduler_data")->getData());
				}
				return parent::_prepareForm();
		}
}
