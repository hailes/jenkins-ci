<?php

class Tenovia_Paytmscheduler_Block_Adminhtml_Paytmscheduler_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("paytmschedulerGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("paytmscheduler/paytmscheduler")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("paytmscheduler")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("order_number", array(
				"header" => Mage::helper("paytmscheduler")->__("Order Number"),
				"index" => "order_number",
				));
				$this->addColumn("email_address", array(
				"header" => Mage::helper("paytmscheduler")->__("Email Address"),
				"index" => "email_address",
				));
				$this->addColumn("mobile_number", array(
				"header" => Mage::helper("paytmscheduler")->__("Mobile Number"),
				"index" => "mobile_number",
				));
				$this->addColumn("order_value", array(
				"header" => Mage::helper("paytmscheduler")->__("Order Value"),
				"index" => "order_value",
				));
				$this->addColumn("coupon1", array(
				"header" => Mage::helper("paytmscheduler")->__("Coupon 1"),
				"index" => "coupon1",
				));
				$this->addColumn("coupon2", array(
				"header" => Mage::helper("paytmscheduler")->__("Coupon 2"),
				"index" => "coupon2",
				));
				$this->addColumn("mail_delivery_date", array(
				"header" => Mage::helper("paytmscheduler")->__("Mail Delivery Date"),
				"index" => "mail_delivery_date",
				));
						$this->addColumn('mail_status', array(
						'header' => Mage::helper('paytmscheduler')->__('Mail Status'),
						'index' => 'mail_status',
						'type' => 'options',
						'options'=>Tenovia_Paytmscheduler_Block_Adminhtml_Paytmscheduler_Grid::getOptionArray8(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_paytmscheduler', array(
					 'label'=> Mage::helper('paytmscheduler')->__('Remove Paytmscheduler'),
					 'url'  => $this->getUrl('*/adminhtml_paytmscheduler/massRemove'),
					 'confirm' => Mage::helper('paytmscheduler')->__('Are you sure?')
				));
			$this->getMassactionBlock()->addItem('send_mail', array(
					 'label'=> Mage::helper('paytmscheduler')->__('Send Coupon Mail'),
					 'url'  => $this->getUrl('*/adminhtml_paytmscheduler/sendmail'),
					 'confirm' => Mage::helper('paytmscheduler')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray8()
		{
            $data_array=array(); 
			$data_array[1]='Yes';
			$data_array[0]='No';
            return($data_array);
		}
		static public function getValueArray8()
		{
            $data_array=array();
			foreach(Tenovia_Paytmscheduler_Block_Adminhtml_Paytmscheduler_Grid::getOptionArray8() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}