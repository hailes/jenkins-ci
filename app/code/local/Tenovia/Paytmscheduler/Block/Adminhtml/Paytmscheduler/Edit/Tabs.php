<?php
class Tenovia_Paytmscheduler_Block_Adminhtml_Paytmscheduler_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("paytmscheduler_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("paytmscheduler")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("paytmscheduler")->__("Item Information"),
				"title" => Mage::helper("paytmscheduler")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("paytmscheduler/adminhtml_paytmscheduler_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
