<?php


class Tenovia_Paytmscheduler_Block_Adminhtml_Paytmscheduler extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_paytmscheduler";
	$this->_blockGroup = "paytmscheduler";
	$this->_headerText = Mage::helper("paytmscheduler")->__("Paytmscheduler Manager");
	$this->_addButtonLabel = Mage::helper("paytmscheduler")->__("Add New Item");
	parent::__construct();
	
	}

}