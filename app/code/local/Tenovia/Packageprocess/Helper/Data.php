<?php

class Tenovia_Packageprocess_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function checkScanedQty($sku, $_orderid, $itemQty)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('feetscience_package_details', array('sum(qty) as qtypacked'))
                        ->where('productid=?', $sku)
                        ->where('order_number=?', $_orderid);

        $packaged = $connection->fetchRow($sql);
        $orderd_qty = $itemQty * 1;

        $balance_qty_to_pack = $orderd_qty - $packaged['qtypacked'];

        return $balance_qty_to_pack;
    }

    public function checkPackedQty($_orderid)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('feetscience_package_details', array('sum(qty) as qtypacked'))
                        ->where('order_number=?', $_orderid);

        $packaged = $connection->fetchRow($sql);

        return $packaged['qtypacked'];
    }

    public function checkPackedQtySales($_orderid)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('sales_flat_package_item_details', array('sum(qty) as qtypacked'))
                        ->where('order_id=?', $_orderid)
                        ->where('package_id IS NOT NULL');

        $packaged = $connection->fetchRow($sql);

        return $packaged['qtypacked'];
    }

    public function getSplittedDetails($_orderid, $product_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('sales_flat_package_item_details', array('*'))
                        ->where('order_id=?', $_orderid)
                        ->where('product_id=?', $product_id);

        $packaged = $connection->fetchRow($sql);

        return $packaged;
    }

    public function checkShippetdQty($_orderid)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('sales_flat_package_item_details', array('sum(qty) as qtyshipped'))
                        ->where('manifest_id IS NOT NULL')
                        ->where('order_id=?', $_orderid);

        $packaged = $connection->fetchRow($sql);

        return $packaged['qtyshipped'];
    }

    public function getDiscountAmount($shipid, $sku, $order_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $itemselect = $connection->select()
                            ->from('sales_flat_package_item_details', array('sum(discount_amount_item)+sum(item_special_discount)+sum(item_store_credit_discount) as discountTotal'))
                            ->where('shipment_id=?', $shipid)
                            ->where('sku=?', $sku)
                            ->where('order_id=?', $order_id)
                            ->group('shipment_id');

        $discount_details = $connection->fetchRow($itemselect);

        return $discount_details;
    }

    public function getItemDetail($order_id, $sku)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $itemselect = $connection->select()
                            ->from('sales_flat_order_item', array('*'))
                            ->where('sku=?', $sku)
                            ->where('order_id=?', $order_id);

        $itemDetails = $connection->fetchRow($itemselect);

        return $itemDetails;
    }

    public function getShipmentQty($package_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('sales_flat_package_item_details', array('product_id,sku,sum(qty) as qtytoship,sum(weight) as packageWeight'))
                        ->where('package_id=?', $package_id)
                        ->group('sku');

        $packaged = $connection->fetchAll($sql);

        return $packaged;
    }

    public function packageCalcualtion($addedDetails, $_order, $currentItem)
    {
        $subtotal = [];

        foreach ($addedDetails as $qty => $sku) {
            $qtyProd = intval(stristr($qty, '_', true));

            $totalQty[] = $qtyProd;

            $item = $this->getItemDetail($_order->getId(), $sku);

            $subtotalCurrent[] = $item['price_incl_tax'] * $qtyProd;

            $subtotalBase[] = $item['base_price_incl_tax'] * $qtyProd;
        }

        $subtotalCurrent[] = $currentItem->getPriceInclTax() * 1;

        $subtotalBase[] = $currentItem->getBasePriceInclTax() * 1;

        $subtotalBaseCurr = array_sum($subtotalBase);

        $subtotalCurrentCurr = array_sum($subtotalCurrent);

        $totalQty[] = 1;

        $qtyToSplit = array_sum($totalQty);

        $baseDiscountValue = $_order->getBaseDiscountAmount() / $_order->getTotalQtyOrdered();

        $discountValue = $_order->getDiscountAmount() / $_order->getTotalQtyOrdered();

        $baseDiscountValueScaned = $baseDiscountValue * $qtyToSplit;

        $discountValueScaned = $discountValue * $qtyToSplit;

        $baseShippingValue = $_order->getBaseShippingAmount() / $_order->getTotalQtyOrdered();

        $shippingValue = $_order->getShippingAmount() / $_order->getTotalQtyOrdered();

        $baseShippingValueScaned = $baseShippingValue * $qtyToSplit;

        $shippingValueScaned = $shippingValue * $qtyToSplit;

        $grandTotalBaseCal = $baseDiscountValueScaned + $baseShippingValueScaned + $subtotalBaseCurr;

        $grandTotalCurreCal = $discountValueScaned + $shippingValueScaned + $subtotalCurrentCurr;

        $subTotalText = Mage::helper('adminhtml/sales')->displayPriceAttributeCustom($_order, $subtotalBaseCurr, $subtotalCurrentCurr);

        $grandTotalText = Mage::helper('adminhtml/sales')->displayPriceAttributeCustom($_order, $grandTotalBaseCal, $grandTotalCurreCal);

        $discountTotalText = Mage::helper('adminhtml/sales')->displayPriceAttributeCustom($_order, $baseDiscountValueScaned, $discountValueScaned);

        $shippingTotalText = Mage::helper('adminhtml/sales')->displayPriceAttributeCustom($_order, $baseShippingValueScaned, $shippingValueScaned);

        return ['subTotal' => $subTotalText, 'grandTotal' => $grandTotalText, 'discountTotal' => $discountTotalText, 'shippingTotal' => $shippingTotalText];
    }

    public function getPendigPackagedOrders()
    {
        $statusList = ['prepaid_authorized', 'cod_authorized', 'partial_package', 'partially_shipped', 'processing'];
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $itemselect = $connection->select()
                            ->from('sales_flat_package_item_details', array('distinct(order_id) as order_id'))
                            ->join('sales_flat_order', 'sales_flat_package_item_details.order_id = sales_flat_order.entity_id',['entity_id'])
                            ->where('sales_flat_order.status in(?)', $statusList)
                            ->where('sales_flat_package_item_details.package_id IS NULL');
        $orders = $connection->fetchAll($itemselect);
        $orderIds = array_column($orders, 'order_id');

        return $orderIds;
    }

    public function getPendigPackagedOrdersDetails($orderId)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $itemselect = $connection->select()
                            ->from('sales_flat_package_item_details', array('order_id','product_id','sku','unit_price','original_price', 'item_name','sum(qty) as qty'))
                            ->where('package_id IS NULL')
                            ->where('order_id=?',$orderId)
                            ->group('sku');

        $orders = $connection->fetchAll($itemselect);

        return $orders;
    }
}
