<?php

class Tenovia_Packageprocess_IndexController extends Mage_Core_Controller_Front_Action
{
    public function packageddataAction()
    {
        $orderid = $this->getRequest()->getPost('orderid');

        $barcode = $this->getRequest()->getPost('barcode');

        $addedData = $this->getRequest()->getPost('addedData');

        $helper = Mage::helper('packageprocess');

        $addedDetails = [];

        if (!empty($addedData)) {
            parse_str($addedData, $addedDetails);
        }

        $order = Mage::getModel('sales/order')->load($orderid);

        $data = $order->getData();

        $orderStore = $order->getStoreId();

        $items = $order->getAllItems();

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $prodbarcode = array();

        $details = array();

        $i = 0;

        foreach ($items as $itemId => $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());

            $prodbarcode[$item->getProductId()] = $product->getBarcode();

            ++$i;
        }

        if (in_array($barcode, $prodbarcode)) {
            $product_id = array_search($barcode, $prodbarcode);

            $products = Mage::getModel('catalog/product')->load($product_id);

            $select2 = $connection->select()

                                ->from('sales_flat_order_item', array('*'))

                                ->where('product_id=?', $products->getId())

                                ->where('order_id=?', $orderid);

            $datas = $connection->fetchRow($select2);

                 //Qty check 

                 if (isset($addedDetails)) {
                     $addedDetails = array_flip($addedDetails);

                     $addedQty = array_search($datas['sku'], $addedDetails);

                     $addedQty = intval(stristr($addedQty, '_', true));

                     $balanceQty = $helper->checkScanedQty($datas['sku'], $order->getIncrementId(), $datas['qty_ordered']);

                     $qtyscan = $balanceQty - $addedQty;

                     if ($qtyscan >= 1) {
                         $addQty = 1;
                     } else {
                         $addQty = 0;
                     }
                 } else {
                     $addQty = 1;
                 }

            if ($addQty == 1) {
                $product_details = Mage::getModel('catalog/product')->load($datas['product_id']);

                $itemDetails = $helper->getSplittedDetails($orderid, $datas['product_id']);
                $_item = Mage::getModel('sales/order_item')->load($datas['item_id']);

                $totalCalculation = $helper->packageCalcualtion($addedDetails, $order, $_item);

                $currentdate = date('Y-m-d h:i:s');

                echo json_encode(

                        array(

                            'success' => '1',

                            'id' => 'prod_'.trim($datas['sku']),

                            'name' => $datas['name'],

                            'price' => Mage::helper('adminhtml/sales')->displayPriceAttribute($_item, 'price_incl_tax'),

                            'pprice' => $datas['price_incl_tax'],

                            'sku' => trim($datas['sku']),

                            'qty' => intval($addQty),

                            'calculations' => $totalCalculation,

                            'image' => $product_details->getImageUrl(),

                            'current_time' => $currentdate,

                            'shipping_charge' => $itemDetails['shipping_charge'],

                        )

                    );
            } else {
                echo json_encode(

                        array(

                            'success' => '0',

                        )

                    );
            }
        } else {
            echo json_encode(

                        array(

                            'success' => '0',

                        )

                    );
        }
    }

    public function packagesaveAction()
    {
        $orderid = $this->getRequest()->getPost('orderid');

        $currentdate = date('Y-m-d h:i:s');

        $username = $this->getRequest()->getPost('admin-uname');

        $addedData = $this->getRequest()->getPost('addedData');

        $helper = Mage::helper('packageprocess');

        parse_str($addedData, $addedDetails);

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $order = Mage::getModel('sales/order')->loadByIncrementId($orderid);

        $package_name = $username.'_'.date('m-d-Y');

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        try{

            $sql = 'SELECT MAX(package_id) as id FROM feetscience_package_creation';
            $resultsnew = $connection->fetchAll($sql);
            $idvalue = $resultsnew[0]['id'];

            $package_name = $orderid.'-'.date('m-d-Y').'-00'.$idvalue;

            $product_details = '';

            foreach ($addedDetails as $sku => $qty) {
                $qty = intval(stristr($qty, '_', true));

                $product_details .= sprintf('%s*%d, ', $sku, $qty);
            }

            $product_details = rtrim($product_details, ', ');

            $deliveryDate = $order->getDeliverableDate();

            $product_details .= '-->'.$order->getShippingDescription().'('.$deliveryDate.')';

            $write->beginTransaction();


            $insertstatus = $write->query("insert into feetscience_package_creation(package_name,created_by,product_details) values('$package_name','$username',".'"'.$product_details.'"'.')');

            $package_id = $write->lastInsertId();

            $products = Mage::getModel('catalog/product');

            /* store the data item by item with single qty */

            if ($addedDetails && $package_id) {
                $package_details_query = 'INSERT INTO `feetscience_package_details`(package_id,order_number,productid,qty,price,created_date,product_entity_id,sales_flat_pkg_itm_dtl_entityid) VALUES ';

                foreach ($addedDetails as $sku => $qty) {
                    $qty = intval(stristr($qty, '_', true));

                    $select = $connection->select()

                                    ->from('sales_flat_order_item', array('*'))

                                    ->where('sku=?', $sku)

                                    ->where('order_id=?', $order->getId());

                    $result = $connection->fetchRow($select);

                    $pPrice = $result['price'];
                    $productId = $result['product_id'];

                    $select_entity = $connection->select()

                                    ->from('sales_flat_package_item_details', array('entity_id'))

                                    ->where('product_id=?', $productId)

                                    ->where('order_id=?', $order->getId())

                                    ->where('package_id IS NULL');

                    $entity_result = $connection->fetchAll($select_entity);

                    for ($i = 1; $i <= $qty; ++$i) {
                        $entity_id_to = $entity_result[$i - 1]['entity_id'];
                        $package_details_query .= "('$package_id','$orderid','$sku',1,'$pPrice','$currentdate','$productId','$entity_id_to'),";

                        /*
                            Update the package_id to custom sales_flat_package_item_details table
                            
                        **/

                        $update_package = $write->query("Update sales_flat_package_item_details set package_id='$package_id' where entity_id='$entity_id_to'");
                    }
                }

                $package_details_query = rtrim($package_details_query, ',');

                $package_details_query .= ';';
            }

            $insertstatus = $write->query($package_details_query);

            $write->commit();
        } 
        catch(Exception $e) {

            $write->rollback();
            Mage::logException($e);
            $mail = new Zend_Mail('utf-8');
            
            $recipients = array(
                'jayachandran@tenovia.com',
                'muthu@tenovia.com'
            );
            $mailBody   = $orderid .'=>'. $package_id . ",<br>createdBy" .$username. ',<br>Error:'. $e->getMessage();
            
            $mail->setBodyHtml($mailBody)->setSubject('Package Scanning error')->addTo($recipients)->setFrom('feetscience@feetscience.in', "Admin");
            
            try {
                $mail->send();
            }
            catch (Exception $e) {
                Mage::logException($e);
            }
            echo json_encode(['success' => false, 'message' => $e->getMessage()]);

            exit();
        }

        $packedQty = $helper->checkPackedQty($orderid);

        $order_totQty = round($order->getData('total_qty_ordered'));

        if ($packedQty == $order_totQty) {
            $status = 'package_created';
        } else {
            $status = 'partial_package';
        }

        $append = ' by '.$username;

        $order->setStatus($status);

        $order->addStatusToHistory($status, 'Package Created'.$append, true);

        $order->save();

        echo json_encode(['success' => true]);
    }
}
