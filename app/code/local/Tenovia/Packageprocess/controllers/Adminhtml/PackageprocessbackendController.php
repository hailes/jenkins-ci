<?php

class Tenovia_Packageprocess_Adminhtml_PackageprocessbackendController extends Mage_Adminhtml_Controller_action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title($this->__('Sales Order Process'));
        $this->renderLayout();
    }

    public function packageAction()
    {
        $this->loadLayout();
        $this->_title($this->__('Package Creation'));
        $this->renderLayout();
    }

    protected function _isAllowed()
    {
        if (Mage::getSingleton('admin/session')->isAllowed('admin/packageprocess/packageprocessbackend')) {
            return true;
        }

        return false;
    }

}
