<?php

class Tenovia_Packageprocess_Model_Packageprocess extends Mage_Core_Model_Abstract
{
    protected $manifest;

    protected $manifestId;

    protected $invoice;

    private $codCode = 'cashondelivery';

    protected function _construct()
    {
        $this->_init('packageprocess/packageprocess');
    }

    public function getManifestData($manifestId)
    {
        $this->manifestId = $manifestId;
        $data = [];
        $data['COD'] = $this->loadByManifestId(true);
        $data['PREPAID'] = $this->loadByManifestId(false);

        return $data;
    }

    public function getRetryData($manifestId, $model)
    {
        $data = [];
        $this->manifestId = $manifestId;
        $isCod = $model->getPaymentType() == 'COD' ? true : false;
        $data[$model->getPaymentType()] = $this->loadByManifestId($isCod);
        $data[$model->getPaymentType()]['id'] = $model->getId();

        return $data;
    }

    public function loadByManifestId($isCod = false)
    {
        $collection = $this->getCollection()->addFieldToFilter('main_table.manifest_id', $this->manifestId);
        $collection->getSelect()->join(['payment'=> 'sales_flat_order_payment'], 'payment.parent_id=main_table.order_id', array('method'));
        $condition = $isCod === true ? 'eq' : 'neq';
        $collection->addFieldToFilter('payment.method', [$condition => $this->codCode]);

        return ['totals' => $this->getTotalAmount($collection), 'details' => $this->getCollectionQuery($collection), 'shipment' => $this->getShippingMethod()];
    }

    private function getCollectionQuery($_package)
    {
        $_package->getSelect()->reset(Zend_Db_Select::COLUMNS)
                ->columns(['entity_id', 'tax_class_id', 'invoice_id'])
                ->join(['invoice' => 'sales_flat_invoice'], 'invoice.entity_id=main_table.invoice_id', ['invoice.increment_id as incrementId'])
                ->join(['invoice_item' => 'sales_flat_invoice_item'], 'invoice.entity_id=invoice_item.parent_id', ['row_total', 'tax_amount', 'hidden_tax_amount', 'invoice_item.sku', 'discount_amount', 'name', 'product_id', '(invoice_item.row_total + invoice_item.tax_amount + invoice_item.hidden_tax_amount) as rowTotal', 'invoice_item.qty as package_qty', 'order_item_id'])
                ->where('main_table.item_id = invoice_item.order_item_id')
                ->group(['package_id', 'sku']);
        $_package = $_package->getData();
        
        return $_package;
    }

    public function getTotalAmount($collection)
    {
        $invoiceIds = $this->getInvoiceIDs($collection);

        $_invoice = Mage::getModel('sales/order_invoice')->getCollection()
                        ->addFieldToFilter('entity_id', array('in' => $invoiceIds));

        $_invoice->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(['SUM(grand_total) as grand_total', 'sum(total_qty) AS total_qty', 'sum(shipping_amount) as shipping_amount', 'sum(tax_amount) as tax_amount', 'sum(discount_amount) as discount_amount'])
                ->group('store_id');

        $this->invoice = $_invoice->getFirstItem()->getData();

        return $this->invoice;
    }

    public function getInvoiceIDs($invoice)
    {
        $invoiceIds = [];
        foreach ($invoice as $invoice_id) {
            $invoiceIds[] = $invoice_id->getInvoiceID();
        }

        return array_unique($invoiceIds);
    }

    public function getShippingMethod()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('sales_flat_shipment_track', array('title', 'sum(weight) as weight'))
                        ->where('manifest_id=?', $this->manifestId)
                        ->group('manifest_id');

        $shipment = $connection->fetchRow($sql);

        return $shipment;
    }

    public function getManifestDetails()
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('feetscience_manifest', ['id', 'created_at'])
                        ->where('id=?', $this->manifestId);

        $manifest = $connection->fetchRow($sql);

        return $manifest;
    }
}
