<?php

class Tenovia_Packageprocess_Model_Observer
{
    private $message = [
        'success' => 'Thank you for shopping at Paragon. Your order #%s is currently being processed. Please reach us at 1800 425 55552 for any queries.',
        'cancell' => 'Your order #%s has been cancelled. Hope to see you soon at paragon!',
        'shipped' => 'Your order #%s has been dispatched via %s, AWB %s and will reach you in 5-7 days.',
        'partiallyShipped' => 'Some items of your order #%s have been dispatched via %s, AWB %s and will reach you in 3-4 days.',
        'return' => 'We have received your return request for the order #%s placed at Paragon. Our team will get in touch with you shortly.',
        'partiallyCancelled' => 'Some items of your order #%s have been cancelled. Hope to see you soon at Paragon!',
        'codCallVerified' => 'Thank you for shopping at Paragon. We have confirmed your Cash on Delivery order #%s of %s. Please reach us at 1800 425 55552 for any queries. Thank you for shopping at Paragon!'
    ];

    public function addPackage($arvgs)
    {
        $order = $arvgs->getOrder();

        $order_id = $order->getId();

        $order_incrementId = $order->getIncrementId();

        $customer_id = $order->getCustomerId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($order_incrementId);
        $items = $order->getAllItems();
        $itemcount = count($items);

        $i = 0;
        #loop for all order items$tax_info = $order->getFullTaxInfo();print_r($tax_info);
        $arr = array();
        $connectionWrite = Mage::getSingleton('core/resource')->getConnection('core_write');
        foreach ($items as $itemId => $item) {
            $qty_array[$item->getProductId()] = $item->getData('qty_ordered');
            $id_array[$item->getId()] = $item->getId();

            for ($j = 0;$j < $qty_array[$item->getProductId()];++$j) {
                $sj[] = $id_array[$item->getId()];
            }
            ++$i;
        }
        if (is_array($sj)) {
            foreach ($sj as $key => $value) {
                $data = array();
                $result = '';
                $result = Mage::getModel('sales/order_item')->load($value);

                $data['entity_id'] = '';
                $data['item_id'] = $result->getData('item_id');
                $data['product_id'] = $result->getData('product_id');
                $data['item_name'] = $result->getData('name');
                $data['sku'] = $result->getData('sku');
                $data['discount_amount_item'] = ($result->getData('discount_amount') + ($order->getData('mw_storecredit_discount'))) / $result->getData('qty_ordered');
                $data['unit_price'] = $result->getData('price');
                $data['original_price'] = $result->getData('original_price');
                $data['qty'] = 1;
                $data['discount_percentage'] = $result->getData('discount_percent');
                $data['item_row_total'] = $data['qty'] * $data['unit_price'];
                $data['tax_amount_item'] = $result->getData('tax_amount') / $result->getData('qty_ordered');

                //load product_id
                $_product = Mage::getModel('catalog/product')->load($result->getData('product_id'));
                $taxClassId = $_product->getTaxClassId();
                $taxClass = Mage::getModel('tax/class')->load($taxClassId);
                $taxClassName = $taxClass->getClassName();
                $weight = $_product->getData('weight');
                $data['tax_class_name'] = $taxClassName;
                $data['tax_class_id'] = $taxClassId;
                $data['item_order_discount'] = ($data['unit_price']) - ($data['discount_amount_item']);
                $data['shipping_charge'] = ($order->getShippingAmount()) / ($order->getData('total_qty_ordered'));
                $data['base_total'] = ($data['item_order_discount']) + ($data['shipping_charge']);
                $data['invoice_id'] = $result->getData('invoice_id');
                $data['package_id'] = $result->getData('package_id');
                $data['shipment_id'] = $result->getData('shipment_id');
                $data['created_time'] = date('Y-m-d:H:i:s');
                $data['updated_time'] = date('Y-m-d:H:i:s');
                $data['is_packaged'] = '0';
                $data['order_id'] = $result->getData('order_id');
                $data['weight'] = $weight;
                //add insertion process
                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                $write->insert('sales_flat_package_item_details', $data);
            }
        }
        /*$shippingAdd = $order->getShippingAddress();
        $pincode = $shippingAdd->getPostcode();
        $delivery_helper = Mage::helper('deliverypincodes');
        $deliveryAvi = $delivery_helper->getDeliverableDate($pincode, $order->getShippingMethod());
        if (Mage::getSingleton('core/session')->getPickupStore()) {
            $updateDetails['pickupstore_id'] = Mage::getSingleton('core/session')->getPickupStore();
        }
       
        $updateDetails['deliverable_date'] = $deliveryAvi;*/
        // $write->update('sales_flat_order', $updateDetails, "entity_id = '$order_id'");
    }

    public function sendordersms($observer)
    {
        $order = $observer->getOrder();
        if ($order->isSmsSent()) {
            $status = $order->getStatus();
            $url = 'http://fastsms.way2mint.com/SendSMS/sendmsg.php';
            $userName = 'Bachishoes';
            $password = 'fEEt@cIe';
            $senderId = 'ALERTS';

            $ordernumber = $order->getIncrementId();
            $orderid = $order->getId();
            $billing_address = $order->getBillingAddress();
            $mobileNumber = $billing_address->getTelephone();

            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $packageId = Mage::getSingleton('core/session')->getPackageId();

            if ($status == 'prepaid_authorized') {
                $message = sprintf($this->message['success'], $ordernumber);
            } elseif ($status == 'order_shipped') {
                $shipselect = $connection->select()
                            ->from('sales_flat_shipment_track', array('title', 'track_number'))
                            ->where('feetscience_package_id=?', $packageId)
                            ->where('order_id=?', $orderid);
                $shipdata = $connection->fetchRow($shipselect);
                if (empty($shipdata) === false) {
                    $message = sprintf($this->message['shipped'], $ordernumber, $shipdata['title'], $shipdata['track_number']);
                }
            } elseif ($status == 'partially_shipped') {
                $shipselect = $connection->select()
                            ->from('sales_flat_shipment_track', array('title', 'track_number'))
                            ->where('feetscience_package_id=?', $packageId)
                            ->where('order_id=?', $orderid);
                $shipdata = $connection->fetchRow($shipselect);
                if (empty($shipdata) === false) {
                    $message = sprintf($this->message['partiallyShipped'], $ordernumber, $shipdata['title'], $shipdata['track_number']);
                }
            } elseif ($status == 'partially_cancelled') {
                $message = sprintf($this->message['partiallyCancelled'], $ordernumber);
            } elseif ($status == 'canceled') {
                $message = sprintf($this->message['canceled'], $ordernumber);
            } elseif ($status == 'cod_authorized') {
                // $orderTotal = Mage::helper('core')->currency($order->getGrandTotal(), true, false);
                $orderTotal = 'Rs.'. number_format($order->getGrandTotal(), 2);
                $message = sprintf($this->message['codCallVerified'], $ordernumber, $orderTotal);
            } elseif ($status == 'return') {
                $message = sprintf($this->message['return'], $ordernumber);
            }

            if (empty($message) === false && empty($mobileNumber) === false) {
                // $curl_url = sprintf('%s?uname=%s&pass=%s&send=%s&dest=%s&msg=%s', $url, $userName, $password, $senderId, $mobileNumber, urlencode($message));
                // $ch = curl_init();
                // curl_setopt($ch, CURLOPT_URL, $curl_url);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                // $store = curl_exec($ch);
                // Mage::log('SMS Reposne : '.$store, null, 'sms.log');

                // Mage::getSingleton('core/session')->unsPackageId();

                // if ($order->getCurrentStatusHistory()->getId() && empty($store) === false) {
                //     $statusHistory = Mage::getModel('sales/order_status_history')
                //         ->load($order->getCurrentStatusHistory()->getId())
                //         ->setIsSmsSent(1)
                //         ->setIsCustomerNotified(1)
                //         ->setIsVisibleOnFront(1)
                //         ->save();
                // }
                // curl_close($ch);
            }
        }
    }

}
