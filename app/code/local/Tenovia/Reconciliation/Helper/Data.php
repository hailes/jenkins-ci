<?php
class Tenovia_Reconciliation_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getShipmentDetails($awbNo)
	{
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                    ->from('sales_flat_shipment_track', array('feetscience_package_id', 'airway_bill_barcode'))
                    ->where('track_number=?', $awbNo);

        $packaged = $connection->fetchRow($sql);

        return $packaged;
	}
}
	 