<?php
class Tenovia_Reconciliation_Model_Store extends  Mage_Dataflow_Model_Convert_Adapter_Abstract
{
	protected $_storeModel;

	protected $helper;

	public function load()
	{

	}

	public function save()
	{
		
	}

	public function  getStoreModel()
	{
		if (is_null($this->_storeModel)) {
			$storeModel = Mage::getModel('reconciliation/reconciliation');
			$this->_storeModel = Mage::objects()->save($storeModel);
		}
		return Mage::objects()->load($this->_storeModel);
	}

	public function saveRow(array $importData)
	{
		$store = $this->getStoreModel();

		$shipmentDetails = $this->getHelper()->getShipmentDetails($importData['awb_no']);
		if (empty($importData['awb_no'])) {
			$message = Mage::helper('reconciliation')->__('Skip  import row, required field awb_no not defined');
			Mage::throwException($message);
		} else if(empty($shipmentDetails['feetscience_package_id'])) {
			$message = Mage::helper('reconciliation')->__('Skip  import row, package details not available for given awb_no');
			Mage::throwException($message);
		} else {
			$store->load($importData['awb_no'], 'awb_no');
		}
		$date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$store->setAwbNo($importData['awb_no']);
		$store->setPackageId($shipmentDetails['feetscience_package_id']);
		$store->setLogisticReferenceNo($importData['logistic_reference_no']);
		$store->setCustomerId($importData['customer_id']);
		$store->setAmount($importData['amount']);
		$store->setUtrNo($importData['utr_no']);
		$store->setDeliveredDate($importData['delivered_date']);
		$store->setStatus($importData['status']);
		$store->setReceivedBy($importData['received_by']);
		$store->setPaymentReceivedOn($importData['payment_received_on']);
		$store->setStatus($importData['status']);
		$store->setCreatedAt($date);
		$store->setUpdatedAt($date);

		$user = Mage::getSingleton('admin/session');
		$adminuserId = $user->getUser()->getUserId();

		$store->setUserId($adminuserId);

		$store->save();

		return true;

	}

	public function getHelper()
	{
		if (is_null($this->helper)) {
			$this->helper = Mage::helper('reconciliation');
		}

		return $this->helper;
	}
}	