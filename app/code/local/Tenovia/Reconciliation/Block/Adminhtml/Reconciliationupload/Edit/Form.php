<?php
class Tenovia_Reconciliation_Block_Adminhtml_Reconciliationupload_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			"id" => "edit_form",
			"action" => $this->getUrl("*/*/save"),
			"method" => "post",
			"enctype" =>"multipart/form-data",
			)
		);
		$fieldset = $form->addFieldset("reconciliation_form", array("legend"=>Mage::helper("reconciliation")->__("Import File")));

		
		$fieldset->addField('reconciliationfile', 'file', array(
		          'label'     => Mage::helper('reconciliation')->__('Reconciliation File'),
		          'required'  => false,
		          'name'      => 'reconciliationfile',
		          'required' => true
		)); 

		$form->setUseContainer(true);
		$this->setForm($form);

		return parent::_prepareForm();
	}
}
