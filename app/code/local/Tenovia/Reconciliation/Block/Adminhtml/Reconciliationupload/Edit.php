<?php
	
class Tenovia_Reconciliation_Block_Adminhtml_Reconciliationupload_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{
			parent::__construct();
			$this->removeButton('back')
        	->removeButton('reset')
        	->_updateButton('save', 'label', $this->__('Upload'))
        	->_updateButton('save', 'reconciliationupload_id', 'upload_button');
        	$url = $this->getSkinUrl('reconciliation_sample.csv');
        	$this->_addButton('downloadSample', array(
		        'label'   => Mage::helper('reconciliation')->__('Download Sample'),
		        'onclick' => "setLocation('{$url}')",
		        'class'   => 'update'
		    ));
		}

		protected function _construct()
	    {
	        parent::_construct();

	        $this->_objectId   = 'reconciliationupload_id';
	        $this->_blockGroup = 'reconciliation';
	        $this->_controller = 'adminhtml_reconciliationupload';
	    }

	     public function getHeaderText()
	    {
	        return Mage::helper('reconciliation')->__('Import Reconciliation File');
	    }

}