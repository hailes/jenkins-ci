<?php


class Tenovia_Reconciliation_Block_Adminhtml_Reconciliation extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_reconciliation";
	$this->_blockGroup = "reconciliation";
	$this->_headerText = Mage::helper("reconciliation")->__("Reconciliation Manager");
	$this->_addButtonLabel = Mage::helper("reconciliation")->__("Add New Item");
	parent::__construct();
	
	}

}