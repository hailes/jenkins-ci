<?php

class Tenovia_Reconciliation_Block_Adminhtml_Reconciliation_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("reconciliationGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("reconciliation/reconciliation")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("reconciliation")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("awb_no", array(
				"header" => Mage::helper("reconciliation")->__("Aws Bill Number"),
				"index" => "awb_no",
				));
				$this->addColumn("logistic_reference_no", array(
				"header" => Mage::helper("reconciliation")->__("Logistic Reference Number"),
				"index" => "logistic_reference_no",
				));
				$this->addColumn("customer_id", array(
				"header" => Mage::helper("reconciliation")->__("Customer Id"),
				"index" => "customer_id",
				));
				$this->addColumn("amount", array(
				"header" => Mage::helper("reconciliation")->__("Amount"),
				"index" => "amount",
				));
				$this->addColumn("utr_no", array(
				"header" => Mage::helper("reconciliation")->__("UTR No"),
				"index" => "utr_no",
				));
					$this->addColumn('delivered_date', array(
						'header'    => Mage::helper('reconciliation')->__('Delivery Date'),
						'index'     => 'delivered_date',
						'type'      => 'datetime',
					));
				$this->addColumn("status", array(
				"header" => Mage::helper("reconciliation")->__("Status"),
				"index" => "status",
				));
				$this->addColumn("received_by", array(
				"header" => Mage::helper("reconciliation")->__("Received By"),
				"index" => "received_by",
				));
					$this->addColumn('payment_received_on', array(
						'header'    => Mage::helper('reconciliation')->__('Payment Received On'),
						'index'     => 'payment_received_on',
						'type'      => 'datetime',
					));
				$this->addColumn("package_id", array(
				"header" => Mage::helper("reconciliation")->__("Package Id"),
				"index" => "package_id",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_reconciliation', array(
					 'label'=> Mage::helper('reconciliation')->__('Remove Reconciliation'),
					 'url'  => $this->getUrl('*/adminhtml_reconciliation/massRemove'),
					 'confirm' => Mage::helper('reconciliation')->__('Are you sure?')
				));
			return $this;
		}
			

}