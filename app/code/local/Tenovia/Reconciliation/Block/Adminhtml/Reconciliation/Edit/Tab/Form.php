<?php
class Tenovia_Reconciliation_Block_Adminhtml_Reconciliation_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("reconciliation_form", array("legend"=>Mage::helper("reconciliation")->__("Item information")));

				
						$fieldset->addField("awb_no", "text", array(
						"label" => Mage::helper("reconciliation")->__("Aws Bill Number"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "awb_no",
						));
					
						$fieldset->addField("logistic_reference_no", "text", array(
						"label" => Mage::helper("reconciliation")->__("Logistic Reference Number"),
						"name" => "logistic_reference_no",
						));
					
						$fieldset->addField("customer_id", "text", array(
						"label" => Mage::helper("reconciliation")->__("Customer Id"),
						"name" => "customer_id",
						));
					
						$fieldset->addField("amount", "text", array(
						"label" => Mage::helper("reconciliation")->__("Amount"),
						"name" => "amount",
						));
					
						$fieldset->addField("utr_no", "text", array(
						"label" => Mage::helper("reconciliation")->__("UTR No"),
						"name" => "utr_no",
						));
					
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('delivered_date', 'date', array(
						'label'        => Mage::helper('reconciliation')->__('Delivery Date'),
						'name'         => 'delivered_date',
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));
						$fieldset->addField("status", "text", array(
						"label" => Mage::helper("reconciliation")->__("Status"),
						"name" => "status",
						));
					
						$fieldset->addField("received_by", "text", array(
						"label" => Mage::helper("reconciliation")->__("Received By"),
						"name" => "received_by",
						));
					
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('payment_received_on', 'date', array(
						'label'        => Mage::helper('reconciliation')->__('Payment Received On'),
						'name'         => 'payment_received_on',
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));

				if (Mage::getSingleton("adminhtml/session")->getReconciliationData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getReconciliationData());
					Mage::getSingleton("adminhtml/session")->setReconciliationData(null);
				} 
				elseif(Mage::registry("reconciliation_data")) {
				    $form->setValues(Mage::registry("reconciliation_data")->getData());
				}
				return parent::_prepareForm();
		}
}
