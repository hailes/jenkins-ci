<?php
	
class Tenovia_Reconciliation_Block_Adminhtml_Reconciliation_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "reconciliation";
				$this->_controller = "adminhtml_reconciliation";
				$this->_updateButton("save", "label", Mage::helper("reconciliation")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("reconciliation")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("reconciliation")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("reconciliation_data") && Mage::registry("reconciliation_data")->getId() ){

				    return Mage::helper("reconciliation")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("reconciliation_data")->getId()));

				} 
				else{

				     return Mage::helper("reconciliation")->__("Add Item");

				}
		}
}