<?php
class Tenovia_Reconciliation_Block_Adminhtml_Reconciliation_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("reconciliation_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("reconciliation")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("reconciliation")->__("Item Information"),
				"title" => Mage::helper("reconciliation")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("reconciliation/adminhtml_reconciliation_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
