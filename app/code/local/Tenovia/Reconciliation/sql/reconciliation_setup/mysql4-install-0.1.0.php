<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `sales_flat_shipment_reconciliation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `awb_no` varchar(255) NOT NULL,
  `package_id` int(11) not null,
  `logistic_reference_no` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) DEFAULT NULL,
  `amount` double(16,2) DEFAULT NUll,
  `utr_no` varchar(255) DEFAULT NULL,
  `delivered_date` datetime DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `received_by` varchar(60) DEFAULT NULL,
  `payment_received_on` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `awb_no` (`awb_no`)
);
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 