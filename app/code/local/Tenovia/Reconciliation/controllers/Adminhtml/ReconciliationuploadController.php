<?php

class Tenovia_Reconciliation_Adminhtml_ReconciliationuploadController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu("reconciliation/reconciliation")->_addBreadcrumb(Mage::helper("adminhtml")->__("Reconciliation  Upload"),Mage::helper("adminhtml")->__("Reconciliation Upload"));
		return $this;
	}
	public function indexAction() 
	{
	    $this->_title($this->__("Reconciliation Upload"));
	    $this->_title($this->__("Reconciliation Upload"));

		$this->_initAction();
		$this->renderLayout();
	}
	
	public function saveAction()
	{
		if(isset($_FILES['reconciliationfile']['name'])) {

		    try {

		    	$uploader = new Varien_File_Uploader('reconciliationfile');

		        $uploader->setAllowedExtensions(array('csv')); // or pdf or anything

		     	$uploader->setAllowRenameFiles(false);

		     	$uploader->setFilesDispersion(false);

		      	$path = Mage::getBaseDir('var') . '/import/' ;

		     	$uploader->save($path, 'stores.csv');

		     	$data['reconciliationfile'] = $path. $_FILES['reconciliationfile']['name'];

		     	$this->_redirect("adminhtml/system_convert_profile/run", array("id" => 10));

		     	return true;

		      } catch(Exception $e) {		     
		      	Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__($e->getMessage()));
		      }

		}

		$this->_redirect("*/*/");
	}

	protected function _isAllowed()
    { 
        return Mage::getSingleton('admin/session')->isAllowed('admin/manifest/reconciliation');
    }
}
