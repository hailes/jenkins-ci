<?php

class Tenovia_Sms_Model_Observer
{

    public function sendordersms($observer)
    {

        $order = $observer->getOrder();
        $createdAt = $order->getCreatedAt();
        $order_total=$order->getSubtotal();        
        $total_round=round($order_total,2);  
        //echo"<pre>";print_r($total_round); exit;
        //$total_round=600;
 if($order->getStatus()=='complete' && $total_round >=500)
{
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$table        = $resource->getTableName('paytm_coupon'); 
$sql_bus="SELECT id,coupon_code FROM paytm_coupon  WHERE coupon_status='0' AND  coupon_type='Bus'";
$results_bus = $readConnection->fetchAll($sql_bus);
$sql_air="SELECT id,coupon_code FROM paytm_coupon  WHERE coupon_status='0' AND  coupon_type='Air'";
$results_air = $readConnection->fetchAll($sql_air);
$paytm_coupon_bus=$results_bus['0']['coupon_code']; 
$paytm_coupon_air=$results_air['0']['coupon_code'];
$ordernumber = $order->getIncrementId();
$orderid = $order->getId();
// $order_details = Mage::getModel('sales/order')->loadByIncrementId($ordernumber);
$customer_email = $order->getCustomerEmail();
$mobile_number=$order->getShippingAddress()->getTelephone();
$orderGrandTotal    = $order->getBaseGrandTotal();
$date=now();
$mail_delivery_date=date('Y-m-d', strtotime($date. ' + 15 days'));
$mail_status='0';
$writeAdapter   = $resource->getConnection('core_write');
$table        = $resource->getTableName('paytm_scheduler');  
 //echo $paytm_coupon_bus; exit;
if($paytm_coupon_bus || $paytm_coupon_air ) {
    $query = "INSERT INTO {$table} (id,order_number,email_address,mobile_number,order_value,coupon1,coupon2,mail_delivery_date,mail_status) VALUES ('','{$ordernumber}','{$customer_email}','{$mobile_number}','{$orderGrandTotal}','{$paytm_coupon_bus}','{$paytm_coupon_air}','{$mail_delivery_date}','{$mail_status}')";
    //echo $query; exit;
    $result_coupon=$writeAdapter->query($query);
    if($result_coupon)
    {
        $bus_id=$results_bus['0']['id'];
        $air_id=$results_air['0']['id'];

   
        $bus_sql = "UPDATE paytm_coupon SET coupon_status='1' WHERE id=".$bus_id;
        $air_sql = "UPDATE paytm_coupon SET coupon_status='1' WHERE id=".$air_id;
        $update_bus=$writeAdapter->query($bus_sql);
        $update_air=$writeAdapter->query($air_sql);
        $order_bus_sql = "UPDATE paytm_coupon SET order_no ='".$ordernumber."' WHERE id = '".$bus_id."' ";
        $order_air_sql = "UPDATE paytm_coupon SET order_no ='".$ordernumber."' WHERE id = '".$air_id."' ";

        $update_bus=$writeAdapter->query($order_bus_sql);
        $update_air=$writeAdapter->query($order_air_sql);
    } 
}
//echo $mobile_number; exit;
}
        if ($order->isSmsSent()) {
            $status = $order->getStatus();
            $url = Mage::getStoreConfig('sales/sms_setting/url');
            $userName = Mage::getStoreConfig('sales/sms_setting/username');
            $password = Mage::getStoreConfig('sales/sms_setting/password');
            $senderId = Mage::getStoreConfig('sales/sms_setting/sender');
            $ordernumber = $order->getIncrementId();
            $orderid = $order->getId();
            $billing_address = $order->getBillingAddress();
            $mobileNumber = $billing_address->getTelephone();
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $parameters = [];
            $parameters['{orderId}'] = $ordernumber;           
            $parameters['{grandTotal}'] = 'Rs.'. number_format($order->getGrandTotal(), 2);
            $firstName = $order->getBillingAddress()->getFirstname();
            $lasttName = $order->getBillingAddress()->getLastname();
            // $shipselect = $connection->select()
            //             ->from('sales_flat_shipment_track', array('title', 'track_number'))
            //             ->where('order_id=?', $orderid);
            // $shipdata = $connection->fetchRow($shipselect);
            // if (empty($shipdata) === false) {
            //  $parameters['{trackNumber}'] = $shipdata['track_number'];
            //  $parameters['{title}'] = $shipdata['title'];
            // }

            $messageObj = Mage::getModel('sms/sms')->load($status, 'order_status');

            if ($messageObj->getId() && empty($mobileNumber) === false) {
                $message = strtr($messageObj->getMessage(), $parameters);
                
                $searchReplaceArray = array(
                '{{first_name}}' => $firstName,
                '{{last_name}}' => $lasttName,
                '{{order_id}}' => $ordernumber,
                '{{order_created_date}}' => $createdAt,
                ' ' =>'%20'
                );
                
                $txtMessage = str_replace(
                array_keys($searchReplaceArray), 
                array_values($searchReplaceArray), 
                $message
                );

               
                $curl_url = sprintf('%s?uname=%s&pass=%s&send=%s&dest=%s&msg=%s&concat=1', $url,$userName,$password, $senderId, $mobileNumber, urlencode($message));
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $curl_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $store = curl_exec($ch);

                if ($order->getCurrentStatusHistory()->getId() && empty($store) === false) {
                    $statusHistory = Mage::getModel('sales/order_status_history')
                        ->load($order->getCurrentStatusHistory()->getId())
                        ->setIsSmsSent(1)
                        ->setIsCustomerNotified(1)
                        ->setIsVisibleOnFront(1)
                        ->save();
                }
                curl_close($ch);
            }
        }
    }

}