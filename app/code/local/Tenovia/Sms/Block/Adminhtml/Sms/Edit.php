<?php
	
class Tenovia_Sms_Block_Adminhtml_Sms_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "sms";
				$this->_controller = "adminhtml_sms";
				$this->_updateButton("save", "label", Mage::helper("sms")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("sms")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("sms")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("sms_data") && Mage::registry("sms_data")->getId() ){

				    return Mage::helper("sms")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("sms_data")->getId()));

				} 
				else{

				     return Mage::helper("sms")->__("Add Item");

				}
		}
}