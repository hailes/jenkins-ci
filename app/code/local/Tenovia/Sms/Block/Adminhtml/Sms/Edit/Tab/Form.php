<?php
class Tenovia_Sms_Block_Adminhtml_Sms_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("sms_form", array("legend"=>Mage::helper("sms")->__("Item information")));

				
						$fieldset->addField("message", "textarea", array(
						"label" => Mage::helper("sms")->__("Message"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "message",
						));
									
						 $fieldset->addField('order_status', 'select', array(
						'label'     => Mage::helper('sms')->__('Order status'),
						'values'   => Tenovia_Sms_Block_Adminhtml_Sms_Grid::getOrderStatus(),
						'name' => 'order_status',					
						"class" => "required-entry",
						"required" => true,
						));				
							
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('sms')->__('Status'),
						'values'   => Tenovia_Sms_Block_Adminhtml_Sms_Grid::getValueArray3(),
						'name' => 'status',					
						"class" => "required-entry",
						"required" => true,
						));

				if (Mage::getSingleton("adminhtml/session")->getSmsData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getSmsData());
					Mage::getSingleton("adminhtml/session")->setSmsData(null);
				} 
				elseif(Mage::registry("sms_data")) {
				    $form->setValues(Mage::registry("sms_data")->getData());
				}
				return parent::_prepareForm();
		}
}
