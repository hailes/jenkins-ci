<?php

class Tenovia_Sms_Block_Adminhtml_Sms_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("smsGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("sms/sms")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
			$this->addColumn("id", array(
				"header" => Mage::helper("sms")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
			));

			$this->addColumn("message", array(
				"header" => Mage::helper("sms")->__("Message"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "text",
				"index" => "message",
			));
                
			$this->addColumn('order_status', array(
				'header' => Mage::helper('sms')->__('Order status'),
				'index' => 'order_status',
				'type' => 'options',
				'options'=>Tenovia_Sms_Block_Adminhtml_Sms_Grid::getOrderStatus(),				
			));
			
			$this->addColumn('status', array(
				'header' => Mage::helper('sms')->__('Status'),
				'index' => 'status',
				'type' => 'options',
				'options'=>Tenovia_Sms_Block_Adminhtml_Sms_Grid::getOptionArray3(),				
			));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_sms', array(
					 'label'=> Mage::helper('sms')->__('Remove Sms'),
					 'url'  => $this->getUrl('*/adminhtml_sms/massRemove'),
					 'confirm' => Mage::helper('sms')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOrderStatus()
		{
            $orderStatus = Mage::getModel('sales/order_status')->getResourceCollection()->getData();
            $status = [];
			foreach ($orderStatus as $key => $value) {
				$status[$value['status']] = $value['label'];
			}
            return($status);
		}
		static public function getValueArray1()
		{
            $data_array=array();
			foreach(Tenovia_Sms_Block_Adminhtml_Sms_Grid::getOptionArray1() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray2()
		{
            $data_array=array(); 
			$data_array[0]='order_id';
            return($data_array);
		}
		static public function getValueArray2()
		{
            $data_array=array();
			foreach(Tenovia_Sms_Block_Adminhtml_Sms_Grid::getOptionArray2() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[0]='Enabled';
			$data_array[1]='Disabled';
            return($data_array);
		}
		static public function getValueArray3()
		{
            $data_array=array();
			foreach(Tenovia_Sms_Block_Adminhtml_Sms_Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}