<?php


class Tenovia_Sms_Block_Adminhtml_Sms extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_sms";
	$this->_blockGroup = "sms";
	$this->_headerText = Mage::helper("sms")->__("Sms Manager");
	$this->_addButtonLabel = Mage::helper("sms")->__("Add New Item");
	parent::__construct();
	
	}

}