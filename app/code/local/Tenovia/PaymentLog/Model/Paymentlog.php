<?php

class Tenovia_PaymentLog_Model_Paymentlog extends Mage_Core_Model_Abstract
{
    protected function _construct(){

       $this->_init("paymentlog/paymentlog");

    }

    public function loadByOrderId($orderId) 
    {
	    $matches = $this->getResourceCollection()
	        ->addFieldToFilter('order_id', $orderId);

	    foreach ($matches as $match) {
	        return $this->load($match->getId());
	    }

	    return $this->setData('order_id', $orderId);
	}

}
	 