<?php

class Tenovia_Viewmanifest_Model_Mysql4_Viewmanifest extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the viewmanifest_id refers to the key field in your database table.
        $this->_init('viewmanifest/viewmanifest', 'id');
    }
}
