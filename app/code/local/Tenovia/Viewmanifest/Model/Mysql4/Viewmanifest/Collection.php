<?php

class Tenovia_Viewmanifest_Model_Mysql4_Viewmanifest_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('viewmanifest/viewmanifest');
    }
}
