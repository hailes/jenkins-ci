<?php

class Tenovia_Viewmanifest_Model_Viewmanifest extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('viewmanifest/viewmanifest');
    }
}
