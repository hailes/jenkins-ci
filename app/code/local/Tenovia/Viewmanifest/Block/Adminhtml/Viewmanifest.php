<?php

class Tenovia_Viewmanifest_Block_Adminhtml_Viewmanifest extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_viewmanifest';
        $this->_blockGroup = 'viewmanifest';
        $this->_headerText = Mage::helper('viewmanifest')->__('View Manifest');
    //$this->_addButtonLabel = Mage::helper('viewmanifest')->__('Add Item');
    parent::__construct();
        $this->_removeButton('add');
    }
}
