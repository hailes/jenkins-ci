<?php

class Tenovia_Viewmanifest_Block_Adminhtml_Viewmanifest_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'viewmanifest';
        $this->_controller = 'adminhtml_viewmanifest';
    }

    public function getHeaderText()
    {
        if (Mage::registry('viewmanifest_data') && Mage::registry('viewmanifest_data')->getId()) {
            return Mage::helper('viewmanifest')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('viewmanifest_data')->getTitle()));
        } else {
            return Mage::helper('viewmanifest')->__('Add Item');
        }
    }
}
