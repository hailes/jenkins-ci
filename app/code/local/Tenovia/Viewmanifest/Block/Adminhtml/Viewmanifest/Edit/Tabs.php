<?php

class Tenovia_Viewmanifest_Block_Adminhtml_Viewmanifest_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('viewmanifest_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('viewmanifest')->__('Item Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
          'label' => Mage::helper('viewmanifest')->__('Item Information'),
          'title' => Mage::helper('viewmanifest')->__('Item Information'),
          'content' => $this->getLayout()->createBlock('viewmanifest/adminhtml_viewmanifest_edit_tab_form')->toHtml(),
      ));

        return parent::_beforeToHtml();
    }
}
