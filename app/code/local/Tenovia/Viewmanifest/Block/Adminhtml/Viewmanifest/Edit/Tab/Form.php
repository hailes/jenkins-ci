<?php

class Tenovia_Viewmanifest_Block_Adminhtml_Viewmanifest_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('viewmanifest_form', array('legend' => Mage::helper('viewmanifest')->__('Item information')));

        $fieldset->addField('title', 'text', array(
          'label' => Mage::helper('viewmanifest')->__('Title'),
          'class' => 'required-entry',
          'required' => true,
          'name' => 'title',
      ));

        $fieldset->addField('filename', 'file', array(
          'label' => Mage::helper('viewmanifest')->__('File'),
          'required' => false,
          'name' => 'filename',
      ));

        $fieldset->addField('status', 'select', array(
          'label' => Mage::helper('viewmanifest')->__('Status'),
          'name' => 'status',
          'values' => array(
              array(
                  'value' => 1,
                  'label' => Mage::helper('viewmanifest')->__('Enabled'),
              ),

              array(
                  'value' => 2,
                  'label' => Mage::helper('viewmanifest')->__('Disabled'),
              ),
          ),
      ));

        $fieldset->addField('content', 'editor', array(
          'name' => 'content',
          'label' => Mage::helper('viewmanifest')->__('Content'),
          'title' => Mage::helper('viewmanifest')->__('Content'),
          'style' => 'width:700px; height:500px;',
          'wysiwyg' => false,
          'required' => true,
      ));

        if (Mage::getSingleton('adminhtml/session')->getViewmanifestData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getViewmanifestData());
            Mage::getSingleton('adminhtml/session')->setViewmanifestData(null);
        } elseif (Mage::registry('viewmanifest_data')) {
            $form->setValues(Mage::registry('viewmanifest_data')->getData());
        }

        return parent::_prepareForm();
    }
}
