<?php

class Tenovia_Viewmanifest_Block_Adminhtml_Viewmanifest_Renderer_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = parent::render($row);
        $rowdata = $row->getData();

        $printurl = $this->getUrl('viewmanifest/adminhtml_viewmanifest/print/id/'.$row->getId());
        $viewurl = $this->getUrl('viewmanifest/adminhtml_viewmanifest/view/id/'.$row->getId());

        $html .= '<button  onclick="window.open(\''.$printurl.'\')">Print</button>&nbsp;&nbsp;<button  onclick="window.location='."'".$viewurl."'".'">View</button>';

        return $html;
    }
}
