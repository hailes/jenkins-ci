<?php

class Tenovia_Viewmanifest_Block_Viewmanifest extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getViewmanifest()
    {
        if (!$this->hasData('viewmanifest')) {
            $this->setData('viewmanifest', Mage::registry('viewmanifest'));
        }

        return $this->getData('viewmanifest');
    }
}
