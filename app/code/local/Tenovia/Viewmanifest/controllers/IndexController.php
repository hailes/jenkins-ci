<?php

class Tenovia_Viewmanifest_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {

        /*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/viewmanifest?id=15 
    	 *  or
    	 * http://site.com/viewmanifest/id/15 	
    	 */
        /* 
        $viewmanifest_id = $this->getRequest()->getParam('id');

        if($viewmanifest_id != null && $viewmanifest_id != '')	{
            $viewmanifest = Mage::getModel('viewmanifest/viewmanifest')->load($viewmanifest_id)->getData();
        } else {
            $viewmanifest = null;
        }	
        */

         /*
    	 * If no param we load a the last created item
    	 */
        /*
        if($viewmanifest == null) {
            $resource = Mage::getSingleton('core/resource');
            $read= $resource->getConnection('core_read');
            $viewmanifestTable = $resource->getTableName('viewmanifest');
            
            $select = $read->select()
               ->from($viewmanifestTable,array('viewmanifest_id','title','content','status'))
               ->where('status',1)
               ->order('created_time DESC') ;
               
            $viewmanifest = $read->fetchRow($select);
        }
        Mage::register('viewmanifest', $viewmanifest);
        */

        $this->loadLayout();
        $this->renderLayout();
    }
}
