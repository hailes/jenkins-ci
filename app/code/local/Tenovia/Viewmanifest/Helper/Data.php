<?php

class Tenovia_Viewmanifest_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getInvoicedTotal($package_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                    ->from('sales_flat_package_item_details', array('invoice_id'))
                    ->where('invoice_id IS NOT NULL')
                    ->where('package_id=?', $package_id);

        $packaged = $connection->fetchRow($sql);

        $_invoice = Mage::getModel('sales/order_invoice')->load($packaged['invoice_id']);

        return $_invoice;
    }
}
