<?php
class Tenovia_Otp_Block_Index extends Mage_Core_Block_Template{
    public function getOrderlist(){

        $order_items =  Mage::getModel('sales/order_item')->getCollection()->addFieldToSelect('*');

        $order_items->getSelect()->group('main_table.product_id');

        $item_ids = Array();
        foreach ($order_items as  $value) {
            $item_ids[$value->getProductId()]=$value->getProductId();
        }


        $product = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->setStoreId(Mage::app()->getStore()->getId())
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addAttributeToFilter('entity_id',array('in'=>$item_ids))
            ->addAttributeToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        return $product;
    }

    public function getProductdata($id){
        return Mage::getModel('catalog/product')->load($id);
    }
    public function getCouponlist(){
      $collection  = Mage::getModel('salesrule/rule')->getCollection()->addFieldToSelect('*')->addFieldToFilter('is_active',1);
      $collection->getSelect()->where(' main_table.from_date is NULL');
      $customerSession = Mage::getSingleton('customer/session');
      if ($customerSession->isLoggedIn()){
        $group = 1;
      }
      else{
        $group = 0;
      }
      $couponCode = Array();
      foreach ($collection as $value) {
        $unserializeCondition = unserialize($value->getConditionsSerialized());
        //if($unserializeCondition['conditions']){
      if(in_array('conditions', $unserializeCondition) && $unserializeCondition['conditions']){

          foreach ($unserializeCondition['conditions'] as  $valueCondition) {
            if( $valueCondition['type'] == 'salerule/rule_condition_customer'){

              switch ($valueCondition['attribute']) {
                case 'email':
                    $result = $this->operatorCheckWithMail($valueCondition['operator'],$valueCondition['value']);
                    if($result==1){
                      if($value->getCode()){
                        if(in_array($group,$value->getCustomerGroupIds())){
                          $couponCode[]= array(
                            'code'=>$value->getCode(),
                            'name'=> $value->getName(),
                            'description'=>$value->getDescription()
                          );
                        }
                      }
                    }
                  break;

                default:
                $resultIds =   $this->operatorCheckWithId($valueCondition['operator'],$valueCondition['value']);
                  if($resultIds == 1){
                    if($value->getCode()){
                      if(in_array($group,$value->getCustomerGroupIds())){
                        $couponCode[]= array(
                          'code'=>$value->getCode(),
                          'name'=> $value->getName(),
                          'description'=>$value->getDescription()
                        );
                      }
                    }
                  }
                  break;
              }
            }
          }
        }

      }
      $collectionDate = Mage::getModel('salesrule/rule')->getCollection()->addFieldToSelect('*')->addFieldToFilter('is_active',1)->addFieldToFilter('from_date',array('gteq'=>date('Y-m-d')));
      foreach ($collectionDate as $dateCoupon) {
        $unserializeConditionUndated = unserialize($dateCoupon->getConditionsSerialized());
        if($unserializeConditionUndated['conditions']){
          foreach ($unserializeConditionUndated['conditions'] as  $valueConditionArray) {
            if( $valueConditionArray['type'] == 'salerule/rule_condition_customer'){
              switch ($valueConditionArray['attribute']) {
                case 'email':
                    $resultDate = $this->operatorCheckWithMail($valueConditionArray['operator'],$valueConditionArray['value']);
                    if($resultDate==1){
                      if($dateCoupon->getCode()){
                        if(in_array($group,$dateCoupon->getCustomerGroupIds())){
                          $couponCode[] = array(
                            'code'=>$dateCoupon->getCode(),
                            'name'=> $dateCoupon->getName(),
                            'description'=>$dateCoupon->getDescription()
                          );
                        }
                      }
                    }
                  break;

                default:
                $resultIdsDate =   $this->operatorCheckWithId($valueConditionArray['operator'],$valueConditionArray['value']);
                  if($resultIdsDate == 1){
                    if($dateCoupon->getCode()){
                      if(in_array($group,$dateCoupon->getCustomerGroupIds())){
                        $couponCode[] = array(
                          'code'=>$dateCoupon->getCode(),
                          'name'=> $dateCoupon->getName(),
                          'description'=>$dateCoupon->getDescription()
                        );
                      }
                    }
                  }
                  break;
              }
            }
          }
        }
      }
    return $couponCode;
    }
    public function operatorCheckWithMail($operator,$conditionValue)
    {

      $customerMailIds = explode(',',$conditionValue);
      $customerEmail = Mage::getSingleton('customer/session')->getCustomer()->getEmail();

      $result = '';
      switch ($operator) {
        case '!=':
            if(!in_array($customerEmail, $customerMailIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
          case "==":
              if(in_array($customerEmail, $customerMailIds, TRUE)){
                $result =  1;
              }else{
                  $result =  0;
              }

            break;

        case '!()':
            if(!in_array($customerEmail, $customerMailIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        case '()':
            if(in_array($customerEmail, $customerMailIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        case '!{}':
          if(!in_array($customerEmail, $customerMailIds, TRUE)){
            $result =  1;
          }else{
              $result =  0;
          }
          break;
        case '{}':
          if(in_array($customerEmail, $customerMailIds, TRUE)){
            $result =  1;
          }else{
              $result =  0;
          }
          break;
        case '![]':
            if(!in_array($customerEmail, $customerMailIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        case '[]':
            if(in_array($customerEmail, $customerMailIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        default:
            $result =  0;
          break;
      }
      return $result;

    }
    public function operatorCheckWithId($operator,$conditionValue)
    {
      $customerIds = explode(',',$conditionValue);
      $customerEntityId = Mage::getSingleton('customer/session')->getCustomer()->getEntityId();

      $result = '';
      switch ($operator) {
        case '!=':
            if(!in_array($customerEntityId, $customerIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        case '==':
            if(in_array($customerEntityId, $customerIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;

        case '!()':
            if(!in_array($customerEntityId, $customerIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        case '()':
            if(in_array($customerEntityId, $customerIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        case '!{}':
          if(!in_array($customerEntityId, $customerIds, TRUE)){
            $result =  1;
          }else{
              $result =  0;
          }
          break;
        case '{}':
          if(in_array($customerEntityId, $customerIds, TRUE)){
            $result =  1;
          }else{
              $result =  0;
          }
          break;
        case '![]':
            if(!in_array($customerEntityId, $customerIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        case '[]':
            if(in_array($customerEntityId, $customerIds, TRUE)){
              $result =  1;
            }else{
                $result =  0;
            }
          break;
        default:
          $result =  0;
          break;
      }
      return $result;
    }
}
