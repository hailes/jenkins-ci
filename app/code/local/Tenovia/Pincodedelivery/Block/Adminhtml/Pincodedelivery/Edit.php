<?php
	
class Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "pincodedelivery";
				$this->_controller = "adminhtml_pincodedelivery";
				$this->_updateButton("save", "label", Mage::helper("pincodedelivery")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("pincodedelivery")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("pincodedelivery")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("pincodedelivery_data") && Mage::registry("pincodedelivery_data")->getId() ){

				    return Mage::helper("pincodedelivery")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("pincodedelivery_data")->getId()));

				} 
				else{

				     return Mage::helper("pincodedelivery")->__("Add Item");

				}
		}
}