<?php
class Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("pincodedelivery_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("pincodedelivery")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("pincodedelivery")->__("Item Information"),
				"title" => Mage::helper("pincodedelivery")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("pincodedelivery/adminhtml_pincodedelivery_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
