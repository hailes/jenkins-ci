<?php

class Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("pincodedeliveryGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("pincodedelivery/pincodedelivery")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				// $this->addColumn("id", array(
				// "header" => Mage::helper("pincodedelivery")->__("ID"),
				// "align" =>"right",
				// "width" => "50px",
			 //    "type" => "number",
				// "index" => "id",
				// ));
                
				// $this->addColumn("store_id", array(
				// "header" => Mage::helper("pincodedelivery")->__("Store ID"),
				// "index" => "store_id",
				// ));
				$this->addColumn("pincode", array(
				"header" => Mage::helper("pincodedelivery")->__("PinCode"),
				"index" => "pincode",
				));
						$this->addColumn('iscod', array(
						'header' => Mage::helper('pincodedelivery')->__('Is COD'),
						'index' => 'iscod',
						'type' => 'options',
						'options'=>Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray2(),				
						));
						
						$this->addColumn('is_payment_handling_fee_available', array(
						'header' => Mage::helper('pincodedelivery')->__('Payment Fee'),
						'index' => 'is_payment_handling_fee_available',
						'type' => 'options',
						'options'=>Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray3(),				
						));
						
						$this->addColumn('isprepaid', array(
						'header' => Mage::helper('pincodedelivery')->__('Is Prepaid'),
						'index' => 'isprepaid',
						'type' => 'options',
						'options'=>Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray4(),				
						));
						$this->addColumn('state', array(
						'header' => Mage::helper('pincodedelivery')->__('State'),
						'index' => 'state',
						'type' => 'options',
						'options'=>Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray11(),				
						));
						
				$this->addColumn("shipping_provider_code", array(
				"header" => Mage::helper("pincodedelivery")->__("Shipping Provider"),
				"index" => "shipping_provider_code",
				));
				$this->addColumn("delivery_type", array(
				"header" => Mage::helper("pincodedelivery")->__("Delivery Type"),
				"index" => "delivery_type",
				));
				$this->addColumn("deliverable_date", array(
				"header" => Mage::helper("pincodedelivery")->__("Deliverable Day"),
				"index" => "deliverable_date",
				));
				$this->addColumn("shipping_charge", array(
				"header" => Mage::helper("pincodedelivery")->__("Shipping Charge"),
				"index" => "shipping_charge",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('pincodedelivery')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray9(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_pincodedelivery', array(
					 'label'=> Mage::helper('pincodedelivery')->__('Remove Pincodedelivery'),
					 'url'  => $this->getUrl('*/adminhtml_pincodedelivery/massRemove'),
					 'confirm' => Mage::helper('pincodedelivery')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray2()
		{
            $data_array=array(); 
			$data_array[1]='Yes';
			$data_array[0]='No';
            return($data_array);
		}
		static public function getValueArray2()
		{
            $data_array=array();
			foreach(Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray2() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[1]='Yes';
			$data_array[0]='No';
            return($data_array);
		}
		static public function getValueArray3()
		{
            $data_array=array();
			foreach(Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[1]='Yes';
			$data_array[0]='No';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}

		static public function getOptionArray11()
		{
             $countryName = Mage::getModel('directory/country')->load('IN')->getName(); //get country name
            $states = Mage::getModel('directory/country')->load('IN')->getRegions();//state names
            $data_array=array();
            foreach ($states as $state)
            {
              $data_array[$state->getId()]=$state->getName();
             }
            return($data_array);
        }


		static public function getValueArray11()
		{
            $data_array=array();
			foreach(Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray11() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray9()
		{
            $data_array=array(); 
			$data_array[1]='Enable';
			$data_array[0]='Disable';
            return($data_array);
		}
		static public function getValueArray9()
		{
            $data_array=array();
			foreach(Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getOptionArray9() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}
