<?php
class Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("pincodedelivery_form", array("legend"=>Mage::helper("pincodedelivery")->__("Item information")));

				
						$fieldset->addField("store_id", "text", array(
						"label" => Mage::helper("pincodedelivery")->__("Store ID"),
						"name" => "store_id",
						));
					
						$fieldset->addField("pincode", "text", array(
						"label" => Mage::helper("pincodedelivery")->__("PinCode"),
						"name" => "pincode",
						));
									
						 $fieldset->addField('iscod', 'select', array(
						'label'     => Mage::helper('pincodedelivery')->__('Is COD'),
						'values'   => Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getValueArray2(),
						'name' => 'iscod',
						));				
						 $fieldset->addField('is_payment_handling_fee_available', 'select', array(
						'label'     => Mage::helper('pincodedelivery')->__('Payment Fee'),
						'values'   => Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getValueArray3(),
						'name' => 'is_payment_handling_fee_available',
						));				
						 $fieldset->addField('isprepaid', 'select', array(
						'label'     => Mage::helper('pincodedelivery')->__('Is Prepaid'),
						'values'   => Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getValueArray4(),
						'name' => 'isprepaid',
						));
						$fieldset->addField("shipping_provider_code", "text", array(
						"label" => Mage::helper("pincodedelivery")->__("Shipping Provider"),
						"name" => "shipping_provider_code",
						));
					
						$fieldset->addField("delivery_type", "text", array(
						"label" => Mage::helper("pincodedelivery")->__("Delivery Type"),
						"name" => "delivery_type",
						));
					
						$fieldset->addField("deliverable_date", "text", array(
						"label" => Mage::helper("pincodedelivery")->__("Deliverable Day"),
						"name" => "deliverable_date",
						));
					
						$fieldset->addField("shipping_charge", "text", array(
						"label" => Mage::helper("pincodedelivery")->__("Shipping Charge"),
						"name" => "shipping_charge",
						));
						$fieldset->addField("city", "text", array(
						"label" => Mage::helper("pincodedelivery")->__("City"),
						"name" => "city",
						));
						$fieldset->addField("state", "select", array(
						"label" => Mage::helper("pincodedelivery")->__("State"),
						'values'   => Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getValueArray11(),
						"name" => "state",
						));
			
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('pincodedelivery')->__('Status'),
						'values'   => Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery_Grid::getValueArray9(),
						'name' => 'status',
						));
						// $fieldset->addField("id", "text", array(
						// "label" => Mage::helper("pincodedelivery")->__("ID"),
						// "name" => "id",
						// ));
					

				if (Mage::getSingleton("adminhtml/session")->getPincodedeliveryData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPincodedeliveryData());
					Mage::getSingleton("adminhtml/session")->setPincodedeliveryData(null);
				} 
				elseif(Mage::registry("pincodedelivery_data")) {
				    $form->setValues(Mage::registry("pincodedelivery_data")->getData());
				}
				return parent::_prepareForm();
		}
}