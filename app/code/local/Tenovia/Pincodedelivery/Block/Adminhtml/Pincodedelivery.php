<?php


class Tenovia_Pincodedelivery_Block_Adminhtml_Pincodedelivery extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_pincodedelivery";
	$this->_blockGroup = "pincodedelivery";
	$this->_headerText = Mage::helper("pincodedelivery")->__("Pincodedelivery Manager");
	$this->_addButtonLabel = Mage::helper("pincodedelivery")->__("Add New Item");
	$this->_addButton("Import", array(
            "label" => Mage::helper("core")->__("Import"),
            "onclick" => "location.href = '" . $this->getUrl('admin_pincodedelivery/adminhtml_pincodedeliverybackend') . "';",
            "class" => "btn btn-danger",
        ));
	
	parent::__construct();
	}

}