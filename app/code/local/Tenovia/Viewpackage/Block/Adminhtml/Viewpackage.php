<?php

class Tenovia_Viewpackage_Block_Adminhtml_Viewpackage extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_viewpackage';
        $this->_blockGroup = 'viewpackage';
        $this->_headerText = Mage::helper('viewpackage')->__('View Package');
    //$this->_addButtonLabel = Mage::helper('viewpackage')->__('Add Item');
    parent::__construct();
        $this->_removeButton('add');
    }
}
