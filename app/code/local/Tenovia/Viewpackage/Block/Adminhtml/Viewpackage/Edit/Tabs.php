<?php

class Tenovia_Viewpackage_Block_Adminhtml_Viewpackage_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('viewpackage_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('viewpackage')->__('Item Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
          'label' => Mage::helper('viewpackage')->__('Item Information'),
          'title' => Mage::helper('viewpackage')->__('Item Information'),
          'content' => $this->getLayout()->createBlock('viewpackage/adminhtml_viewpackage_edit_tab_form')->toHtml(),
      ));

        return parent::_beforeToHtml();
    }
}
