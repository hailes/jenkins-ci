<?php

class Tenovia_Viewpackage_Block_Adminhtml_Viewpackage_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('viewpackageGrid');
        $this->setDefaultSort('package_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('viewpackage/viewpackage')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('package_id', array(
          'header' => Mage::helper('viewpackage')->__('Id'),
          'align' => 'right',
          'width' => '50px',
          'index' => 'package_id',
      ));

        $this->addColumn('package_name', array(
          'header' => Mage::helper('viewpackage')->__('Package Name'),
          'align' => 'left',
          'index' => 'package_name',
      ));

        $this->addColumn('product_details', array(
          'header' => Mage::helper('viewpackage')->__('Product Details'),
          'align' => 'left',
          'width' => '42%',
          'index' => 'product_details',
      ));

        $this->addColumn('created_date', array(
          'header' => Mage::helper('viewpackage')->__('Created Date'),
          'align' => 'left',
          'index' => 'created_date',
      ));

        $this->addColumn('created_by', array(
          'header' => Mage::helper('viewpackage')->__('Created By'),
          'align' => 'left',
          'index' => 'created_by',
      ));

        $this->addColumn('action',
            array(
            'header' => Mage::helper('viewpackage')->__('Action'),
            'index' => 'action',
            'align' => 'left',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Tenovia_Viewpackage_Block_Adminhtml_Viewpackage_Renderer_Actions',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
