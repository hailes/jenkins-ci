<?php

class Tenovia_Viewpackage_Block_Adminhtml_Viewpackage_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('viewpackage_form', array('legend' => Mage::helper('viewpackage')->__('Item information')));

        $fieldset->addField('title', 'text', array(
          'label' => Mage::helper('viewpackage')->__('Title'),
          'class' => 'required-entry',
          'required' => true,
          'name' => 'title',
      ));

        $fieldset->addField('filename', 'file', array(
          'label' => Mage::helper('viewpackage')->__('File'),
          'required' => false,
          'name' => 'filename',
      ));

        $fieldset->addField('status', 'select', array(
          'label' => Mage::helper('viewpackage')->__('Status'),
          'name' => 'status',
          'values' => array(
              array(
                  'value' => 1,
                  'label' => Mage::helper('viewpackage')->__('Enabled'),
              ),

              array(
                  'value' => 2,
                  'label' => Mage::helper('viewpackage')->__('Disabled'),
              ),
          ),
      ));

        $fieldset->addField('content', 'editor', array(
          'name' => 'content',
          'label' => Mage::helper('viewpackage')->__('Content'),
          'title' => Mage::helper('viewpackage')->__('Content'),
          'style' => 'width:700px; height:500px;',
          'wysiwyg' => false,
          'required' => true,
      ));

        if (Mage::getSingleton('adminhtml/session')->getViewpackageData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getViewpackageData());
            Mage::getSingleton('adminhtml/session')->setViewpackageData(null);
        } elseif (Mage::registry('viewpackage_data')) {
            $form->setValues(Mage::registry('viewpackage_data')->getData());
        }

        return parent::_prepareForm();
    }
}
