<?php

class Tenovia_Viewpackage_Block_Adminhtml_Viewpackage_Renderer_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = parent::render($row);
        $rowdata = $row->getData();
        $package_id = $row->getPackageId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $rowsql = "select p2.entity_id,p1.package_id from feetscience_package_details as p1 left join sales_flat_order as p2 on p2.increment_id = p1.order_number  where p1.package_id='$package_id' group by p1.order_number";
        $results = $readConnection->fetchRow($rowsql);
        $order_id = $results['entity_id'];

        $order = Mage::getModel('sales/order')->load($order_id);
        $shipment_mehods = $order->getShippingMethod();
        $shippingAddress = $order->getShippingAddress();
        $orderDeliveryPincode = $shippingAddress ? $shippingAddress->getPostcode() : null;
        
        //show view label
        $rowsql2 = "SELECT COUNT(*) AS countval,parent_id FROM sales_flat_shipment_track WHERE feetscience_package_id='$package_id'";
        $resultsnew = $readConnection->fetchAll($rowsql2);
        $url = $this->getUrl('shipsync/index/index/order_id/', array('order_id' => $order_id)).'?type=package&id='.$package_id;
        $selectMethods = '';

        if (is_array($resultsnew)) {
            foreach ($resultsnew as $res1) {
                $countval = $res1['countval'];
                $ship_id = $res1['parent_id'];
            }
            $show_button = '';
            $viewpackage = Mage::Helper('viewpackage');
            $avilableShippingMethods = $viewpackage->getAvilableShippingMethods($orderDeliveryPincode);
            $shipment_url = $this->getUrl('adminhtml/sales_order_shipment/new/order_id/'.$order_id.'/package_id/'.$package_id);

            if ($viewpackage->checkPackageInvoiced($package_id) > 0) {
                $shippment_details = $viewpackage->checkPackageShipped($package_id);
                if ($shippment_details['shipped'] > 0) {
                    $labelsql = "SELECT package_id,cod,return_label_image FROM shipping_shipment_package WHERE order_shipment_id='$ship_id'";
                    $labeldata = $readConnection->fetchRow($labelsql);
                    if (!empty($labeldata)) {
                        $track_id = $labeldata['package_id'];
                        $viewurl = $this->getUrl('shipsync/index/label/', array('id' => $track_id));
                        $returnlabelurl = $this->getUrl('shipsync/index/returnlabel/', array('id' => $track_id));
                        $show_button = '<a href='."$viewurl".'>Fedex Label</a>&nbsp;&nbsp;';

                        if ($labeldata['cod'] == 'COD' &&  !empty($labeldata['return_label_image'])) {
                            $show_button .= '<a href='."$returnlabelurl".'>Return label</a>';
                        }
                    }
                } else {
                    $selectMethods = '<select class="select-shipping-method-dropdown">';
                    foreach ($avilableShippingMethods as $code => $title) {
                        $selected = '';
                        if ($shipment_mehods == $code.'_'.$code) {
                            $selected = 'selected';
                        }

                        if ($code == 'fedex') {
                            $selectMethods .= '<option '.$selected.' value='.$url.'&carrier='.$code.'>'.$title.'</option>';
                        } else {
                            $selectMethods .= '<option '.$selected.' value='.$shipment_url.'?carrier='.$code.'>'.$title.'</option>';
                        }
                    }
                    $selectMethods .= '</select><button class="processed-shipment">Go</button>';
                }
            }
        }

        $viewurl = $this->getUrl('viewpackage/adminhtml_viewpackage/view/id/'.$row->getPackageId());
        $invoiceurl = $this->getUrl('adminhtml/sales_order_invoice/new/order_id/'.$order_id.'/package_id/'.$package_id.'/type/package');

        $helper = Mage::Helper('partialinvoice');
        $invoice_details = $helper->checkInvoicedPackage($package_id);
        $print_url = '';
        $invoice_url = '';

        if (!empty($invoice_details['invoice_id'])) {
            $printurl = $this->getUrl('adminhtml/sales_order_invoice/print/invoice_id/'.$invoice_details['invoice_id']);
            $print_url = '<a href='."$printurl".'>Print</a>&nbsp;&nbsp';
        } else {
            $invoice_url = '<button  onclick="window.open(\''.$invoiceurl.'\')">Invoice</button>&nbsp;&nbsp;';
        }
        $html .= $print_url.'<a href='."$viewurl".'>View</a>&nbsp;&nbsp;'.$invoice_url.$selectMethods.$show_button;

        return $html;
    }
}
