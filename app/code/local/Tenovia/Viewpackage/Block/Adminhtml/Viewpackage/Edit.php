<?php

class Tenovia_Viewpackage_Block_Adminhtml_Viewpackage_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'viewpackage';
        $this->_controller = 'adminhtml_viewpackage';
    }

    public function getHeaderText()
    {
        if (Mage::registry('viewpackage_data') && Mage::registry('viewpackage_data')->getId()) {
            return Mage::helper('viewpackage')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('viewpackage_data')->getTitle()));
        } else {
            return Mage::helper('viewpackage')->__('Add Item');
        }
    }
}
