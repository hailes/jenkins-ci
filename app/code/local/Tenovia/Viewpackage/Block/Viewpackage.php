<?php

class Tenovia_Viewpackage_Block_Viewpackage extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getViewpackage()
    {
        if (!$this->hasData('viewpackage')) {
            $this->setData('viewpackage', Mage::registry('viewpackage'));
        }

        return $this->getData('viewpackage');
    }
}
