<?php

class Tenovia_Viewpackage_Model_Status extends Varien_Object
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    public static function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED => Mage::helper('viewpackage')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('viewpackage')->__('Disabled'),
        );
    }
}
