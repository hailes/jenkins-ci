<?php

class Tenovia_Viewpackage_Model_Mysql4_Viewpackage extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the viewpackage_id refers to the key field in your database table.
        $this->_init('viewpackage/viewpackage', 'id');
    }
}
