<?php

class Tenovia_Viewpackage_Model_Mysql4_Viewpackage_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('viewpackage/viewpackage');
    }
}
