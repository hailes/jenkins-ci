<?php

class Tenovia_Viewpackage_Model_Viewpackage extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('viewpackage/viewpackage');
    }
}
