<?php

class Tenovia_Viewpackage_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {

        /*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/viewpackage?id=15 
    	 *  or
    	 * http://site.com/viewpackage/id/15 	
    	 */
        /* 
        $viewpackage_id = $this->getRequest()->getParam('id');

        if($viewpackage_id != null && $viewpackage_id != '')	{
            $viewpackage = Mage::getModel('viewpackage/viewpackage')->load($viewpackage_id)->getData();
        } else {
            $viewpackage = null;
        }	
        */

         /*
    	 * If no param we load a the last created item
    	 */
        /*
        if($viewpackage == null) {
            $resource = Mage::getSingleton('core/resource');
            $read= $resource->getConnection('core_read');
            $viewpackageTable = $resource->getTableName('viewpackage');
            
            $select = $read->select()
               ->from($viewpackageTable,array('viewpackage_id','title','content','status'))
               ->where('status',1)
               ->order('created_time DESC') ;
               
            $viewpackage = $read->fetchRow($select);
        }
        Mage::register('viewpackage', $viewpackage);
        */

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _isAllowed()
    { 
        if (Mage::getSingleton('admin/session')->isAllowed('admin/Tenovia_Viewpackage')){

            return true;
        }

        return false;
    }
}
