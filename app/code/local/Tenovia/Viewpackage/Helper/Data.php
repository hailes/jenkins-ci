	<?php

class Tenovia_Viewpackage_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function checkPackageInvoiced($package_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                    ->from('sales_flat_package_item_details', array('count(invoice_id) as invoiced'))
                    ->where('invoice_id IS NOT NULL')
                    ->where('package_id=?', $package_id);

        $packaged = $connection->fetchRow($sql);

        return $packaged['invoiced'];
    }

    public function checkPackageShipped($package_id)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                    ->from('sales_flat_package_item_details', array('count(shipment_id) as shipped', 'shipment_id'))
                    ->where('shipment_id IS NOT NULL')
                    ->where('package_id=?', $package_id);

        $packaged = $connection->fetchRow($sql);

        return $packaged;
    }

    public function getAvilableShippingMethods($pincode)
    {
        $deliveryPincode = Mage::getModel('deliverypincodes/deliverypincodes');
        $pincode = trim($pincode);
        $delivery = $deliveryPincode->getAdminAvilableShippingMethods($pincode);
        // $pickupstores = $deliveryPincode->getPickupStores($pincode);
        $serviceProviders = array_column($delivery, 'shipping_provider_code');
        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $options = array();

        foreach ($methods as $_code => $_method) {
            if (!$_title = Mage::getStoreConfig("carriers/$_code/admintitle")) {
                $_title = $_code;
            }
            if (in_array(sprintf('%s_%s', $_code, $_code), $serviceProviders)) {
                $options[$_code] = $_title;
            } else if(Mage::app()->getStore()->isAdmin() &&  Mage::getStoreConfig("carriers/$_code/adminshipping")) {
                $options[$_code] = $_title;
            }
            /*elseif ($_code === 'pickupstore' && empty($pickupstores) === false) {
                $options[$_code] = $_title;
            }*/ 
        }

        return $options;
    }

    public function getShipmentBarcode($serviceprovider, $pincode)
    {
        $serviceproviders = $this->getAutoCompleteBarcodeList($pincode);
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = $connection->select()
                        ->from('barcode_inventrys', array('*'))
                        ->where('serviceprovider in (?)', $serviceproviders)
                        ->where('available=1')
                        ->group('serviceprovider')
                        ->order('id');

        $barcode = $connection->fetchAll($sql);

        return array_column($barcode, 'barcode', 'serviceprovider');
    }

    public function getAutoCompleteBarcodeList($pincode)
    {
        $carriers = [];
        $shippingmethods = $this->getAvilableShippingMethods($pincode);
        foreach ($shippingmethods as $_code => $title) {
            if (Mage::getStoreConfig("carriers/$_code/barcode")) {
                $carriers[] = $_code;
            }
        }

        return $carriers;
    }
}
