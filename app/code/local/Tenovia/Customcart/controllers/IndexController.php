<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class Tenovia_Customcart_IndexController extends Mage_Checkout_CartController {

    public function IndexAction() {

        $this->loadLayout();

        echo "Hello Controler";
        $this->renderLayout();
    }

    public function addcartcustomAction() {
//echo 'Hello Controler00000';
        //Mage::getModel('customcart/observer')->addcustomcart();
        $cart = Mage::getSingleton('checkout/cart');
        $cart->init();
        $isAjax                     = Mage::app()->getFrontController()->getRequest()->getParam('isAjax');
        $bulk_simple_product_idArr  = Mage::app()->getFrontController()->getRequest()->getParam('bulk_simple_product_id');
        $bulk_sizeArr               = Mage::app()->getFrontController()->getRequest()->getParam('super_attribute');
        $bulkUnitPriceArr           = Mage::app()->getFrontController()->getRequest()->getParam('bulkUnitPrice');
        $bulkQuantityArr            = Mage::app()->getFrontController()->getRequest()->getParam('bulkQuantity');
        $bulk_parent_id             = Mage::app()->getFrontController()->getRequest()->getParam('bulk_parent_id');
//print_r($bulkQuantity);
        $parentProduct = Mage::getModel('catalog/product')->load($bulk_parent_id);
        
        if($isAjax == 1){
            $response = array();
            try {
                
                /**
                * Check product availability
                */
               //echo Mage::getSingleton('core/session')->getIsBulkOrder() .'--'.Mage::helper('checkout/cart')->getSummaryCount();exit;
               if (Mage::getSingleton('core/session')->getIsBulkOrder() == 2 && Mage::helper('checkout/cart')->getSummaryCount() >=1) {
                       $response['status'] = 'ERROR';
                       $response['message'] = 'Regular products already in cart. Regular products and industrial products can not be bought together';
               }
                foreach ($bulk_simple_product_idArr as $key => $bulk_simple_product) {
                    $bulk_size              = $bulk_sizeArr[$key];
                    $bulkUnitPrice          = $bulkUnitPriceArr[$key];
                    $bulkQuantity           = $bulkQuantityArr[$key];
                    $product                = Mage::getModel('catalog/product')->load($bulk_simple_product);
                    
                    $childProduct = Mage::getModel('catalog/product')->load($bulk_simple_product);//print_r($childProduct);
                    
                    $product->setPrice($bulkUnitPrice);
                    $product->setBasePrice($bulkUnitPrice);
                    
                    if($bulkQuantity > 0){
                        $paramater = array('product' => $bulk_simple_product,
                            'qty' => $bulkQuantity,
                            'is_bulk_order' => '1',
                            'form_key' => Mage::getSingleton('core/session')->getFormKey()
                        );
                        
                        /**
                        * Check product availability
                        */
                       if (!$product) {
                               $response['status'] = 'ERROR';
                               $response['message'] = $this->__('Unable to find Product ID');
                       }
                       
                        if($response['status'] != 'ERROR'){
                            $request = new Varien_Object();
                            $request->setData($paramater);
                            $cart->addProduct($product, $request);
                        }
                        
                    }
                }
                $cart->save();
                
                
                
                $quote = Mage::getModel("checkout/cart")->getQuote();
                        $quoteId = $quote->getId();
                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $write->query("UPDATE `sales_flat_quote` 
                        SET `is_bulk_order`=1 WHERE entity_id='$quoteId'");
                
                
                $this->_getSession()->setCartWasUpdated(true);

                if (!$cart->getQuote()->getHasError() && $response['status'] != 'ERROR'){
                    Mage::getSingleton('core/session')->setIsBulkOrder('1');

                         //$product_image_src=Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100);
    $product_image_src=Mage::helper('catalog/image')->init($parentProduct, 'small_image')->resize(100,100);
    $product_image = '<img src="'.$product_image_src.'" class="product-image" alt=""/>';

                        $message = $this->__("You've just added this product to the cart:").' '.Mage::helper('core')->htmlEscape($parentProduct->getName());
                        $response['status'] = 'SUCCESS';
                        $response['message'] = $message;
                        $this->loadLayout();
                        
                }
                
            } catch (Mage_Core_Exception $e) {
                    $msg = "";
                    if ($this->_getSession()->getUseNotice(true)) {
                            $msg = $e->getMessage();
                    } else {
                            $messages = array_unique(explode("\n", $e->getMessage()));
                            foreach ($messages as $message) {
                                    $msg .= $message.'<br/>';
                            }
                    }

                    $response['status'] = 'ERROR';
                    $response['message'] = $msg;
            } catch (Exception $e) {
                    $response['status'] = 'ERROR';
                    $response['message'] = $this->__('Cannot add the item to shopping cart.');
                    Mage::logException($e);
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        }else{
            return parent::addAction();
        }
    }

}
