<?php
class Tenovia_Customcart_Model_Observer
{
    
    public function addcustomcart(Varien_Event_Observer $observer)
    {
         try {
            $Event = $observer->getEvent();
                $product=$Event->getProduct();//echo '<pre>';print_r($product);exit;
                $quote_item=$Event->getQuoteItem();
                $item = ($quote_item->getParentItem()?$quote_item->getParentItem():$quote_item );
                
                if(Mage::app()->getRequest()->getActionName()=='addcartcustom'){
                    $item->setCustomPrice($product['bulk_price']);
                    $item->setOriginalCustomPrice($product['bulk_price']);
                    // Enable super mode on the product.
                    $item->getProduct()->setIsSuperMode(true);
                }
        } catch (Exception $e) {
            // log any issues, but allow system to continue.

            mage::logException($e);
            if (mage::getIsDeveloperMode()) {
                die($e->getMessage());
            }
        }
        return $this;
    }
}
?>