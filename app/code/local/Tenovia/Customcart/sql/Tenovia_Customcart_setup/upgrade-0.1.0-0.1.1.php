<?php
        ini_set('display_errors', '1');

        $installer = $this;
        $installer->startSetup();
        $installer->getConnection()
                 ->addColumn(
                  $installer->getTable('sales_flat_quote'), //Get the sales_flat_quote Table
                  'is_bulk_order', //New Field Name
             array(
               'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER, //Field Type like TYPE_INTEGER ...
               'nullable'  => false,
               'length'    => 1,
               'default'   => '0',
               'comment'   => '1:is bulk,0:not bulk'
            )
        );             
        $installer->endSetup();
        ?>