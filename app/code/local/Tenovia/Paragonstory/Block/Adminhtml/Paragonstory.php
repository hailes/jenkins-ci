<?php


class Tenovia_Paragonstory_Block_Adminhtml_Paragonstory extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_paragonstory";
	$this->_blockGroup = "paragonstory";
	$this->_headerText = Mage::helper("paragonstory")->__("Paragonstory Manager");
	$this->_addButtonLabel = Mage::helper("paragonstory")->__("Add New Item");
	parent::__construct();
	
	}

}