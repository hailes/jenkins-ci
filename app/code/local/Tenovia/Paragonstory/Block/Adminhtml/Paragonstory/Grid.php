<?php

class Tenovia_Paragonstory_Block_Adminhtml_Paragonstory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("paragonstoryGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("paragonstory/paragonstory")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("paragonstory")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("name", array(
				"header" => Mage::helper("paragonstory")->__("Name"),
				"index" => "name",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("paragonstory")->__("Email"),
				"index" => "email",
				));
				$this->addColumn("story", array(
				"header" => Mage::helper("paragonstory")->__("Story"),
				"index" => "story",
				));
				$this->addColumn("mobile", array(
				"header" => Mage::helper("paragonstory")->__("Mobile No"),
				"index" => "mobile",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_paragonstory', array(
					 'label'=> Mage::helper('paragonstory')->__('Remove Paragonstory'),
					 'url'  => $this->getUrl('*/adminhtml_paragonstory/massRemove'),
					 'confirm' => Mage::helper('paragonstory')->__('Are you sure?')
				));
			return $this;
		}
			

}