<?php
class Tenovia_Paragonstory_Block_Adminhtml_Paragonstory_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("paragonstory_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("paragonstory")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("paragonstory")->__("Item Information"),
				"title" => Mage::helper("paragonstory")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("paragonstory/adminhtml_paragonstory_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
