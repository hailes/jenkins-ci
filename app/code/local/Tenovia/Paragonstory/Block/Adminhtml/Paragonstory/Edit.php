<?php
	
class Tenovia_Paragonstory_Block_Adminhtml_Paragonstory_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "paragonstory";
				$this->_controller = "adminhtml_paragonstory";
				$this->_updateButton("save", "label", Mage::helper("paragonstory")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("paragonstory")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("paragonstory")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("paragonstory_data") && Mage::registry("paragonstory_data")->getId() ){

				    return Mage::helper("paragonstory")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("paragonstory_data")->getId()));

				} 
				else{

				     return Mage::helper("paragonstory")->__("Add Item");

				}
		}
}