<?php
class Tenovia_Paragonstory_Block_Adminhtml_Paragonstory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("paragonstory_form", array("legend"=>Mage::helper("paragonstory")->__("Item information")));

				
						// $fieldset->addField("id", "text", array(
						// "label" => Mage::helper("paragonstory")->__("ID"),
						// "name" => "id",
						// ));
					
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("paragonstory")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("story", "textarea", array(
						"label" => Mage::helper("paragonstory")->__("Story"),
						"name" => "story",
						));
					
						$fieldset->addField("mobile", "text", array(
						"label" => Mage::helper("paragonstory")->__("Mobile No"),
						"name" => "mobile",
						));
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("paragonstory")->__("Email"),
						"name" => "email",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getParagonstoryData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getParagonstoryData());
					Mage::getSingleton("adminhtml/session")->setParagonstoryData(null);
				} 
				elseif(Mage::registry("paragonstory_data")) {
				    $form->setValues(Mage::registry("paragonstory_data")->getData());
				}
				return parent::_prepareForm();
		}
}
