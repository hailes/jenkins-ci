<?php
class Tenovia_Coupon_IndexController extends Mage_Core_Controller_Front_Action{

    public function IndexAction() {
       $customerEmail = $this->getRequest()->getParam('email');
       $response = array();
     
            $customer =$websiteId = Mage::app()->getWebsite()->getId();

            // Instance of customer loaded by the given email
            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId($websiteId)->loadByEmail($customerEmail);
            if($customer->getId()){
                $response = array(
                    'responcecode'=>1,
                    'message'=>'User already exist'
                );
            }else{
                $response = array(
                    'responcecode'=>2,
                    'message'=>'User not exist'
                );
            }
        
      	 return $this->getResponse()->setBody( Mage::helper('core')->jsonEncode($response)); 
    }
    
}