<?php
class Customer_Salerule_Model_Observer
{

			public function updateSalesRulesConditionsForCustomerAttributes(Varien_Event_Observer $observer)
			{
				$customerCondition = Mage::getModel('salerule/rule_condition_customer');
				$customerAttributes = $customerCondition->loadAttributeOptions()->getAttributeOption();
				$attributes = array();
				foreach ($customerAttributes as $code=>$label) {
						$attributes[] = array('value'=>'salerule/rule_condition_customer|'.$code, 'label'=>$label);
				}
				$additional = $observer->getAdditional();
				$conditions = array(
						array('label'=>'Customer Attribute', 'value'=>$attributes)
				);
				$additional->setConditions($conditions);
				return $additional;
			}

}
