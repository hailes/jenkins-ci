<?php

class Customer_Salerule_Model_Rule_Condition_Customer extends Mage_Rule_Model_Condition_Abstract
{
    /**
     * @return $this
     * These are customer attributes we want to use as condition rules for
     */
    public function loadAttributeOptions()
    {
        $attributes = array(
            'entity_id' => 'Customer ID',
            'email' => 'Email Address'

        );

        $this->setAttributeOption($attributes);

        return $this;
    }

    /**
     * Validate Customer Rule Condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $customer = ($object instanceof Mage_Customer_Model_Customer) ? $object : $object->getQuote()->getCustomer();

        $customer
            ->setEntityId($customer->getEntityId())
            ->setEmail($customer->getEmail());


        return parent::validate($customer);
    }
}
