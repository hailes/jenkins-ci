<?php
class Atwix_Picklist_Helper_Data extends Mage_Core_Helper_Abstract
{
  
    /**
     * Returns indexes of the fetched array as headers for CSV
     * @param array $products
     * @return array
     */
    protected function _getCsvHeaders($products)
    {
        $product = current($products);
		
        $headers = array_keys($product[0]);
		
        return $headers;
    }
 
    /**
     * Generates CSV file with product's list according to the collection in the $this->_list
     * @return array
     */
    public function generateMlnList()
    {
        $packageProcess = Mage::helper('packageprocess');
        $ordersList = $packageProcess->getPendigPackagedOrders();
        require_once Mage::getBaseDir('base').DS.'lib'.DS.'htmltopdf'.DS.'mpdf.php';
        
        $orderIds = [];
        foreach ($ordersList as $key => $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            $mpdf = new mPDF('utf-8', 'A4', 0, '', 10, 10, 5, 1, 1, 1, '');
            $mpdf->tableMinSizePriority=true;
            $items  = $packageProcess->getPendigPackagedOrdersDetails($orderId);
            $itemsHtml = '';
            $qty = $packageProcess->checkPackedQtySales($orderId);
            $qtyPackaged =  empty($qty) === false ? $qty : 0;
			foreach( $items as $index => $item )
			{
                $product_details = Mage::getModel('catalog/product')->load($item['product_id']);
                $product_small_image_path = Mage::helper('catalog/image')->init($product_details,'small_image')->resize(75);
                $itemsHtml.='<tr>
                    <td style="padding:8px; text-align:left; color:#000;border:1px solid #999;"> '.++$index.'</td>                        
                    <td style="padding:8px; text-align:left; color:#000;border:1px solid #999;"> '.$order->getCreatedAtStoreDate()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT).'</td>                        
                    <td style="padding:8px; text-align:left; color:#000;border:1px solid #999;"> '.$item['item_name'].' ('.$item['sku'].')'.'</td>    
                    <td style="padding:8px; text-align:left; color:#000;border:1px solid #999;"> <img width="75px" height="75px" src="'.$product_small_image_path.'"></td>    
                    <td style="padding:8px; text-align:left; color:#000;border:1px solid #999;"> '.$item['sku'].'</td>     
                    <td style="padding:8px; text-align:center; color:#000;border:1px solid #999;"> '.$item['original_price'].'</td>
                    <td style="padding:8px; text-align:center; color:#000;border:1px solid #999;"> '.number_format($item['qty'], 0).'</td>
                    <td style="padding:8px; text-align:center; color:#000;border:1px solid #999;"> '.$product_details->getRackPosition().'</td>';
			}

            $html ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Itsybitsy</title>
                        </head>
                        <body>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:20px 5px; font-family:"Times New Roman", Times, serif;" bgcolor="#fff">
                            <tr>
                                <td colspan="3"><b>Order Id : '.$order->getIncrementId().'</b></td>
                                <td colspan="3"><b>Customer Name :</b> '.$order->getCustomerFirstname().' '.$order->getCustomerLastname().'</td>
                            </tr>
                            <tr>
                                 <td colspan="3"><div class="panel panel-default no-radius">
                                    <div class="panel-heading"><b>Shipping Address</b></div>
                                        <div class="panel-body">'.
                                            $order->getShippingAddress()->format('html').'
                                        </div>
                                    </div>
                                </td>
                                <td colspan="4"><b>Shipping Method :</b> '.$order->getShippingDescription().'
                                    <br>
                                    <b>Order Total :</b> '.$order->getGrandTotal().'
                                    <br>
                                    <b>Total Qty Ordered:</b> '.$order->getTotalQtyOrdered().'
                                     <br>
                                    <b>Total Qty Packaged:</b> '.$qtyPackaged.'
                                </td>
                            </tr>
                            <tr>
                                <th style="padding:8px; text-align:left; font-weight: normal; background:#cccccc; color:#000">No.</th>                        
                                <th style="padding:8px; text-align:left; font-weight: normal; background:#cccccc; color:#000"> Order Date </th>                        
                                <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Name  </th>
                                <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Image  </th>
                                <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Sku  </th>
                                <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Price </th>
                                <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Qty </th>
                                <th style="padding:8px; text-align:center; font-weight: normal; background:#cccccc; color:#000"> Rack Position</th>
                            </tr> 
                            '.$itemsHtml.'
                        </table>
                    </body>
                    </html>';

            $mpdf->WriteHTML($html);
            $mpdf->Output('var/pdf/'.$orderId.'.pdf');
            $orderIds[] = $orderId;
		}

        return $orderIds;
    }

}

?>