<?php

class Atwix_Picklist_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Returns generated CSV file
     */
    public function indexAction()
    {
		$current_date = Date('d-m-Y H:m:s');
        $filename = $current_date.'.pdf';
        $orderIds = Mage::helper('picklist')->generateMlnList();
        $datadir = "var/pdf/";
        $outputName = $datadir."merged.pdf";

        $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName ";
        //Add each pdf file to the end of the command
        foreach($orderIds as $file) {
            $cmd .= $datadir.$file.'.pdf'." ";
        }
        $result = shell_exec($cmd);
        $content  = file_get_contents($outputName);
        shell_exec('rm '.$datadir.'*.pdf');
        $this->_prepareDownloadResponse($filename, $content, 'text/pdf');
		
    }

    protected function _isAllowed()
    { 
        if (Mage::getSingleton('admin/session')->isAllowed('admin/atwix/atwix_exgrid/actions/picklist')){

            return true;
        }

        return false;
    }

}

?>