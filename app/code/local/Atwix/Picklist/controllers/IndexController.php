<?php

class Atwix_Picklist_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Returns generated CSV file
     */
    public function picklistAction()
    { 
        $filename = 'data.csv';
        $content = Mage::helper('picklist')->generateMlnList();
 
        $ob  = $this->_prepareDownloadResponse($filename, $content, 'text/csv');
    }

    public function productdetailsAction()
    {
        $productIdsList = json_decode(file_get_contents('php://input'), true);
        $data = [];
        if(empty($productIdsList) === false) {
            foreach ($productIdsList as $key => $product) {
                $productDetails = Mage::getModel('catalog/product')->load($product);
                if ($productDetails) {
                    $image = Mage::helper('catalog/image')->init($productDetails,'small_image')->resize(75);
                    $data[$product]['image'] = (string)$image;
                    $data[$product]['rackPosition'] = $productDetails->getRackPosition();
                }
            }
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }
}

?>