<?php

class Atwix_ExtendedGrid_Block_Adminhtml_Sales_Order_Renderer_Statusbuttons extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
 
	public function render(Varien_Object $row)	{	
	
	 	$user_role_have_per = Mage::getSingleton('admin/session')->isAllowed('admin/atwix/atwix_exgrid/actions/package');
	
		$value =  $row->getData();
		
		$order_id = $value['entity_id'];

		$button = "";
		
		$invoiceurl  = $this->getUrl('adminhtml/sales_order_invoice/new/order_id/'.$order_id);
		$packageurl  = $this->getUrl('admin_packageprocess/adminhtml_packageprocessbackend/package/id/'.$order_id);
		
		$order = Mage::getModel('sales/order')->load($order_id);
		$helper = Mage::Helper("packageprocess");
		
        $packedQty  = $helper->checkPackedQty($order->getIncrementId());
		
		$ordered_qty = $order->getTotalQtyOrdered();
		
		$orderid = $value['increment_id'];
		if( $user_role_have_per )
		{
			if($value['status']=='cod_authorized' || $value['status']=='prepaid_authorized' || $value['status']=='partial_package') {
				$button = '<input type="button" value="Package" class ="form-button" onclick="window.location='."'".$packageurl."'".'"/>';
			}
			elseif( $value['status']=='partially_shipped' && $packedQty != $ordered_qty || $value['status']=='partial_invoice' && $packedQty != $ordered_qty ) {
				$button = '<input type="button" value="Package" class ="form-button" onclick="window.location='."'".$packageurl."'".'"/>';
			}
        }      
                
                 
		return $button;
	 
	}
 
}
?>