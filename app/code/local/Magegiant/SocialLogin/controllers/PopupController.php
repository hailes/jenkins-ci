<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_SocialLogin
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * SocialLogin Popup Controller
 *
 * @category    Magegiant
 * @package     Magegiant_SocialLogin
 * @author      Magegiant Developer
 */
class Magegiant_Sociallogin_PopupController extends Mage_Core_Controller_Front_Action
{

    /**
     * get customr session
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * process quicklogin action
     */
    public function loginAction()
    {
        //$sessionId = session_id();
        $username = $this->getRequest()->getPost('socialogin_email', false);
        $password = $this->getRequest()->getPost('socialogin_password', false);
        $session  = Mage::getSingleton('customer/session');

        $result = array('success' => false);

        if ($username && $password) {
            try {
                $session->login($username, $password);
            } catch (Exception $e) {
                $result['error'] = $e->getMessage();
            }
            if (!isset($result['error'])) {
                $result['success'] = true;
            }
        } else {
            $result['error'] = $this->__(
                'Please enter a username and password.');
        }

        //session_id($sessionId);
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * send pass to customer
     */
    public function sendPassAction()
    {
        //$sessionId = session_id();
        $email    = $this->getRequest()->getPost('socialogin_email_forgot', false);
        $customer = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email);

        if ($customer->getId()) {
            try {
                $newPassword = $customer->generatePassword();
                $customer->changePassword($newPassword, false);
                $customer->sendPasswordReminderEmail();
                $result = array(
                    'success' => true,
                    'message' => Mage::helper('sociallogin')->__('Email has been sent'),
            );
            } catch (Exception $e) {
                $result = array('success' => false, 'error' => $e->getMessage());
            }
        } else {
            $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__('Not found customer has email %s',$email));
        }

        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * create customer acount if not exist
     */
    public function createAccAction()
    {
        $session = Mage::getSingleton('customer/session');
        if ($session->isLoggedIn()) {
            $result = array('success' => false, 'Can Not Login!');
        } else {
            $firstName   = $this->getRequest()->getPost('firstname', false);
            $lastName    = $this->getRequest()->getPost('lastname', false);
            $pass        = $this->getRequest()->getPost('pass', false);
            $passConfirm = $this->getRequest()->getPost('passConfirm', false);
            $email       = $this->getRequest()->getPost('email', false);
            $mobile       = $this->getRequest()->getPost('mobile', false);

            $isExist = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('mobile',$mobile);
            if(!empty($isExist->getData())) {
                $result = array('success' => false, 'error' => 'Mobile number already exist.');
            }else{
                $customer    = Mage::getModel('customer/customer')
                ->setFirstname($firstName)
                ->setLastname($lastName)
                ->setEmail($email)
                ->setPassword($pass)
                ->setConfirmation($passConfirm);

            if ($pass == $passConfirm) {
                try {
                    $customer->save();
                    Mage::dispatchEvent('sociallogin_customer_register_success',
                        array('customer' => $customer)
                    );
                    $result = array('success' => true);
                    $session->setCustomerAsLoggedIn($customer);
                    $url = $this->_welcomeCustomer($customer);
                    //$this->_redirectSuccess($url);
                } catch (Exception $e) {
                    $result = array('success' => false, 'error' => $e->getMessage());
                }
            }else{
                $result = array('success' => false, 'error' => 'Please make sure your passwords match.');
            }
            }
        }
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    // copy to AccountController
    protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
    {
        $this->_getSession()->addSuccess(
            $this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName())
        );
        if ($this->_isVatValidationEnabled()) {
            // Show corresponding VAT message to customer
            $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType();
            $userPrompt        = '';
            switch ($configAddressType) {
                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
                    break;
                default:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
            }
            $this->_getSession()->addSuccess($userPrompt);
        }

        $customer->sendNewAccountEmail(
            $isJustConfirmed ? 'confirmed' : 'registered',
            '',
            Mage::app()->getStore()->getId()
        );

        $successUrl = Mage::getUrl('customer/account/login', array('_secure' => true));
        if ($this->_getSession()->getBeforeAuthUrl()) {
            $successUrl = $this->_getSession()->getBeforeAuthUrl(true);
        }

        return $successUrl;
    }

    protected function _isVatValidationEnabled($store = null)
    {
        return Mage::helper('customer/address')->isVatValidationEnabled($store);
    }
    /**
     * create customer acount if not exist
     */
    public function validatesAction()
    {
        //$sessionId = session_id();
        $email    = $this->getRequest()->getPost('socialogin_email_forgot', false);
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $customer = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email);
            if($customer->getId()){
                try {
                    $newPassword = $customer->generatePassword();
                    $customer->changePassword($newPassword, false);
                    $customer->sendPasswordReminderEmail();
                    $result = array(
                        'success' => true,
                        'message' => Mage::helper('sociallogin')->__('Email has been sent'),
                        'otp'=>false
                );
                } catch (Exception $e) {
                   
                    $result = array('success' => false, 'error' => $e->getMessage(),'otp'=>false);
                }
            }else{
                $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__('Not found customer has email %s',$email),'otp'=>false);
            }
           
          }else if(preg_match('/^[0-9]{10}+$/', $email)){
            $customer = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('mobile')->addAttributeToFilter('mobile',$email)->load()->getData();       
            if($customer[0]['entity_id']){
                $this->sendotp($email);
                $result = array('success' => true, 'message' => Mage::helper('sociallogin')->__(''),'otp'=>true);
            }else{
                $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__("Mobile Number doesn't exists. Please try with email"),'otp'=>false);
            }           
          }else{
            $result = array('success' => false, 'error' => 'Enter Valid Mobile Number/Email','otp'=>false);
          }
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * OTP Verfication function
     *
     * @return void
     */
    public function confirmAction()
    {
        $email = $this->getRequest()->getPost('socialogin_email_forgot');
        $otp = $this->getRequest()->getPost('otp');
        $verfieOtp = Mage::getSingleton('core/session')->getOtp();

        if($verfieOtp['otp']==$otp && $verfieOtp['mobile']==$email)
        {
            if ($email) {
                /**
             * @var $flowPassword Mage_Customer_Model_Flowpassword
             */
            $customers = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('mobile')->addAttributeToFilter('mobile',$email)->getData();  
            $flowPassword = Mage::getModel('customer/flowpassword');
            $flowPassword->setEmail($customers[0]['email'])->save();

            if (!$flowPassword->checkCustomerForgotPasswordFlowEmail($customers[0]['email'])) {
                $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__('You have exceeded requests to times per 24 hours from 1 e-mail.'));                
            }

            if (!$flowPassword->checkCustomerForgotPasswordFlowIp()) {
                $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__('You have exceeded requests to times per hour from 1 IP.'));                                                
            }        

            /** @var $customer Mage_Customer_Model_Customer */
            
            $customer = Mage::getModel('customer/customer')->load($customers[0]['entity_id']);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                    $url = Mage::getUrl('customer/account/resetpassword').'?id='.$customer->getId().'&token='.$newResetPasswordLinkToken;
                    $result = array('success' => true, 'message' => $url);
                } catch (Exception $exception) {                   
                    $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__($exception->getMessage()));
                }
            }
        }
            Mage::getSingleton('core/session')->unsOtp();
        }else{
            $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__('Invalid OTP.'));                
        }        
               
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
    /**
     * Resend OTP function
     *
     * @return void
     */
    public function resendAction()
    {
        $mobile = $this->getRequest()->getPost('socialogin_email_forgot');
        $this->sendotp($mobile);
        
        $result = array('success' => true, 'message' => Mage::helper('sociallogin')->__('resend'));
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
    /**
     * Generate OTP function
     *
     * @param string $mobile
     * @return void
     */
    public function sendotp($mobile)
    {
        
      $digits_needed=6  ;
      $random_number='';
      $count=0;

      while ( $count < $digits_needed ) {
          $random_digit = mt_rand(0, 9);
          
          $otp .= $random_digit;
          $count++;
      }

      $date =  Mage::getModel('core/date')->date('Y-m-d H:i:s');

      $url = Mage::getStoreConfig('sales/otpsms_setting/url');
      $userName = Mage::getStoreConfig('sales/otpsms_setting/username');
      $password = Mage::getStoreConfig('sales/otpsms_setting/password');
      $senderId = Mage::getStoreConfig('sales/otpsms_setting/sender');

      $mobileNumber = $mobile;

      $message = "OTP: " .$otp." - Use this verification code to reset the password at paragonfootwear.com. Enjoy Shopping!";

      
      $curl_url = sprintf('%s?uname=%s&pass=%s&send=%s&dest=%s&msg=%s&concat=1', $url,$userName,$password, $senderId, $mobileNumber, urlencode($message));
      //echo $curl_url;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $curl_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $store = curl_exec($ch);        
      curl_close($ch);
        Mage::getSingleton('core/session')->setOtp(array('otp'=>$otp,'mobile'=>$mobile));
    }
    public function mobilenumberAction(){
        $email    = $this->getRequest()->getPost('mobile', false);
        $customer = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('mobile')->addAttributeToFilter('mobile',$email)->getData();       
        $session = Mage::getSingleton('customer/session');
        $customerModel =  Mage::getModel('customer/customer')->load( $session->getCustomerId());
        if($customerModel->getMobile() == $email){            
            $result = array('success' => true, 'error' => Mage::helper('sociallogin')->__(''),'otp'=>false);
        }elseif(count($customer)!=0){
            $result = array('success' => false, 'error' => Mage::helper('sociallogin')->__('Alreday regigert mobile '),'otp'=>false);
        }else{
            $result = array('success' => true, 'error' => Mage::helper('sociallogin')->__(''),'otp'=>false);
        }
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }
}