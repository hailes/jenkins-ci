<?php

    class Biztech_Trackorder_Helper_Data extends Mage_Core_Helper_Abstract
    {
				
        public function getTrackorderUrl()
        {
            return $this->_getUrl('trackorder/index');
        }
		
		public function getPackagedId($order_id)
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$select 	= $connection->select()
							->from('sales_flat_package_item_details', array('DISTINCT(package_id)','invoice_id','shipment_id','manifest_id','created_time','is_packaged','sum(base_total) as subtotal'))
							->where('order_id = ?',$order_id)
							->where('is_packaged = 1')
							->group('package_id'); 
							
			$data 	= $connection->fetchAll($select); 
			return $data;
		}
		
		public function getManifestDetails($manifest_id)
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$select 	= $connection->select()
							->from('feetscience_manifest', array('*'))
							->where('id = ?',$manifest_id); 
							
			$data 	= $connection->fetchRow($select); 
			
			return $data;
		}
		
		public function getPackageDetails($package_id)
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$select 	= $connection->select()
							->from('feetscience_package_creation', array('*'))
							->where('package_id = ?',$package_id); 
							
			$data 	= $connection->fetchRow($select); 
			
			return $data;
		}
		
		public function getPackageData($package_id)
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$select 	= $connection->select()
							->from('feetscience_package_details', array('sum(qty) as qty','product_entity_id'))
							->where('package_id = ?',$package_id)
							->group('productid'); 
							
			$data 	= $connection->fetchAll($select); 
			
			return $data;
		}
		
		public function getShippingTrack($shipment_id)
		{
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			$select 	= $connection->select()
							->from('shipping_shipment_package', array('*'))
							->where('order_shipment_id = ?',$shipment_id);
							
			$data 	= $connection->fetchRow($select); 
			
			return $data;
		}
	}